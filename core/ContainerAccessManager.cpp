#include "ContainerAccessManager"
#include "Container"

#include <e5Logger>

#include <QTimer>
#include <QCoreApplication>
#include <QDebug>

ContainerAccessManager *ContainerAccessManager::_instance = 0;
QHash<QObject*, int> ContainerAccessManager::_owners = QHash<QObject*, int>();
QList<Container*> ContainerAccessManager::_containers = QList<Container*>();

ContainerAccessManager::ContainerAccessManager():QNetworkAccessManager(0) {
}

ContainerAccessManager *ContainerAccessManager::instance(QObject *parent) {
	if(!_instance)
		_instance=new ContainerAccessManager;
	if(parent)
		connect(parent, SIGNAL(destroyed(QObject*)), _instance, SLOT(ownerDeleted(QObject*)));
	if(!_owners.contains(parent))
		_owners.insert(parent, 1);
	else
		_owners[parent]++;
	return _instance;
}

void ContainerAccessManager::addContainer(Container *c) {
	if(!_instance)
	{
		_instance=new ContainerAccessManager;
	}
	_containers.append(c);
	connect(c, SIGNAL(destroyed(QObject*)), _instance, SLOT(containerDeleted(QObject*)));
}

QNetworkReply *ContainerAccessManager::createRequest(Operation op, QNetworkRequest const &req, QIODevice *outgoingData) {
	if(op==GetOperation && req.url().scheme() == "e5internal") {
		QNetworkReply *r=new ContainerReply(req, op, this);
		return r;
	}
	return QNetworkAccessManager::createRequest(op, req, outgoingData);
}

void ContainerAccessManager::ownerDeleted(QObject *owner) {
	if(!_owners.contains(owner))
		return;

	_owners.remove(owner);
	if(_owners.isEmpty() && _containers.isEmpty()) {
		_instance->deleteLater();
		_instance=0;
	}
}

void ContainerAccessManager::deref(QObject *parent) {
	if(!_owners.contains(parent))
		return;
	--_owners[parent];
	if(_owners[parent] <= 0)
		ownerDeleted(parent);
}

void ContainerAccessManager::containerDeleted(QObject *container) {
	_containers.removeAll(static_cast<Container*>(container));
	if(_owners.isEmpty() && _containers.isEmpty()) {
		_instance->deleteLater();
		_instance=0;
	}
}


ContainerReply::ContainerReply(QNetworkRequest const &req, QNetworkAccessManager::Operation const op, ContainerAccessManager *parent) :
	QNetworkReply(parent),
	_container(0),
	_file(0),
	_state(0)
{
	setRequest(req);
	setUrl(req.url());
	setOperation(op);
	QNetworkReply::open(QNetworkReply::ReadOnly|QNetworkReply::Unbuffered);
	qRegisterMetaType<QNetworkReply::NetworkError>("QNetworkReply::NetworkError");

	QUrl const &url=req.url();
	QString const ID=url.host();
	QString filename=url.path();

	while(filename.startsWith('/'))
		filename=filename.mid(1);

	setHeader(QNetworkRequest::LocationHeader, url);
	//qDebug() <<ContainerAccessManager::_containers.size();
	foreach(Container *cur, ContainerAccessManager::_containers) 
	{
		//qDebug() <<cur->id();
		if(cur->id() == ID) {
			_container=cur;
			break;
		}
	}
	
	if(!_container) {
		qDebug() << "No such container " << ID;
		setError(ContentNotFoundError, tr("No such container: %1").arg(ID));
		QMetaObject::invokeMethod(this, "error", Qt::QueuedConnection, Q_ARG(QNetworkReply::NetworkError, QNetworkReply::ContentNotFoundError));
#if QT_VERSION >= 0x040800
		setFinished(true);
#endif
		QMetaObject::invokeMethod(this, "finished", Qt::QueuedConnection);
		return;
	}

	_file=_container->open(filename);
	if(!_file) {
		qDebug() << "No such file " << filename << " in container " << ID;
		setError(ContentNotFoundError, tr("No such file: %1 in %2").arg(filename).arg(ID));
		QMetaObject::invokeMethod(this, "error", Qt::QueuedConnection, Q_ARG(QNetworkReply::NetworkError, QNetworkReply::ContentNotFoundError));
#if QT_VERSION >= 0x040800
		setFinished(true);
#endif
		QMetaObject::invokeMethod(this, "finished", Qt::QueuedConnection);
		return;
	}

	setHeader(QNetworkRequest::ContentTypeHeader, _container->mimeType(filename));
	setHeader(QNetworkRequest::ContentLengthHeader, size());

	// This is a workaround for a crash in e5Publish Preview,
	// seemingly caused by WebKit crashing if those signals
	// are emitted all at once.
	// Sending any 2 of the signals in the same event loop
	// invokation seems to trigger the crash.
	// Crash last observed with Qt 4.8/QtWebKit 2.2 snapshots
	// from 2011/08/30
	QTimer::singleShot(0, this, SLOT(nextState()));

/*	QMetaObject::invokeMethod(this, "metaDataChanged", Qt::QueuedConnection);
	QMetaObject::invokeMethod(this, "downloadProgress", Qt::QueuedConnection, Q_ARG(qint64, size()), Q_ARG(qint64, size()));
	QMetaObject::invokeMethod(this, "readyRead", Qt::QueuedConnection);
#if QT_VERSION >= 0x040800
		setFinished(true);
#endif
	QMetaObject::invokeMethod(this, "finished", Qt::QueuedConnection); */
}

ContainerReply::~ContainerReply() {
	if(_file)
		delete _file;
}

qint64 ContainerReply::size() const {
	if(!_file)
		return 0;
	return _file->size();
}

qint64 ContainerReply::bytesAvailable() const {
	if(!_file)
		return 0;
	if(!_file->bytesAvailable() && !_file->atEnd()) {
		// Workaround for quazip bug
		return QNetworkReply::bytesAvailable() + _file->size() - _file->pos();
	}
	return QNetworkReply::bytesAvailable() + _file->bytesAvailable();
}

bool ContainerReply::isSequential() const {
	return true;
}

bool ContainerReply::atEnd() const {
	if(!_file)
		return true;
	return _file->atEnd();
}

qint64 ContainerReply::readData(char *data, qint64 maxSize) {
	if(!_file)
		return -1;

	return _file->read(data, maxSize);
}

qint64 ContainerReply::writeData(char *data, qint64 maxSize) {
	return -1;
}

void ContainerReply::abort() {
	QNetworkReply::close();
}


void ContainerReply::nextState()
{
	switch(_state) {
	case 0:
		emit metaDataChanged();
		break;

	case 1:
		emit downloadProgress(size(), size());
		break;

	case 2:
		emit readyRead();
		break;

	case 3:
#if QT_VERSION >= 0x040800
		setFinished(true);
#endif
		emit finished();
		return;

	default:
		return;
	}

	_state++;
	QTimer::singleShot(1, this, SLOT(nextState()));
}
