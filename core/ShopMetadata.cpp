#include "ShopMetadata"

#include "e5Logger"
#include "Downloader"
#include "ContainerAccessManager"

ShopMetadata::ShopMetadata(QString const &name, QString const &description, QUrl const &iconUrl) 
{
	insert("name", name);
	insert("description", description);
	insert("iconUrl", iconUrl.toString());
}

QMultiHash<QString,QString> const &ShopMetadata::metadata() const 
{
	return _metadata;
}

QString ShopMetadata::value(QString const &keyword) const 
{
	QString r=_metadata.value(keyword, QString::null);
	return r;
}

QString ShopMetadata::name() const {
	return value("name");
}
QString ShopMetadata::description() const {
	return value("description");
}

QUrl ShopMetadata::iconUrl() const {
	return QUrl(value("iconUrl"));
}

QByteArray ShopMetadata::icon() const 
{
	QByteArray icon;
	QUrl u = iconUrl();
	icon=Downloader::get( u );
	return icon;
}
