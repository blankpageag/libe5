#include "Voucher"


Voucher::Voucher(QString const &name, QString const &description, QString const &author, QString const &publisher, QString const &releaseDate, QUrl const &iconUrl, QUrl const &tocUrl, ItemCategory *parent):
ShopContent(name, description, author, publisher, releaseDate, iconUrl, tocUrl, parent)
{
}
