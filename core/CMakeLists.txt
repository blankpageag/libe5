set(PUBLIC_HEADERS e5LoggingService e5Logger Container ContainerPlugin TOC TOCPlugin TOCItem TOCEntry Content ContentCache ContentPlugin Metadata ContainerAccessManager Condition ConditionManager ShopItem ItemCategory ShopContent Consumable Subscription Voucher Shop ShopMetadata ShopPlugin PluginLoader e5Globals JsonRpc)
set(SRCS Content.cpp ContentCache.cpp e5Logger.cpp e5LoggingService.cpp Container.cpp TOC.cpp TOCItem.cpp TOCEntry.cpp Metadata.cpp ContainerAccessManager.cpp Condition.cpp ConditionManager.cpp ShopItem.cpp ItemCategory.cpp Shop.cpp ShopContent.cpp ShopMetadata.cpp Consumable.cpp Subscription.cpp Voucher.cpp e5Globals.cpp JsonRpc.cpp)
set(EXTRA_LIBS)
find_package(QJSON)
include_directories(${qjson_INCLUDE_DIR} ${QJSON_INCLUDE_DIR} ${qjson_INCLUDE_DIR}/qjson ${QJSON_INCLUDE_DIR}/qjson /usr/include/qjson /usr/local/include/qjson)

# Include the RPC generator target:
add_executable(RpcApiGenerator RpcApiGenerator.cpp)
target_link_libraries(RpcApiGenerator ${QT_QTMAIN_LIBRARY} ${QT_QTCORE_LIBRARY})

if(CMAKE_CROSSCOMPILING)
	message(STATUS "Cross Compiling - looking for native RpcApiGenerator...")
	find_program(RPC_API_GENERATOR NAMES RpcApiGenerator PATHS /bin /usr/bin /usr/local/bin DOC "Location of native RpcApiGenerator")
	if(EXISTS "${RPC_API_GENERATOR}")
		set(RPCAPIGENERATOR "${RPC_API_GENERATOR}" CACHE INTERNAL "RpcApiGenerator")
	else()
		message(FATAL_ERROR "You need a native version of RpcApiGenerator when crosscompiling.")
	endif()
	message(STATUS "Found, using ${RPCAPIGENERATOR}")
else()
	set(RPCAPIGENERATOR ${CMAKE_CURRENT_BINARY_DIR}/RpcApiGenerator CACHE INTERNAL "RpcApiGenerator")
	install(TARGETS RpcApiGenerator DESTINATION bin)
endif()


if(WITHOUT_PLUGINS)
	set(PRIVATE_HEADERS ${PRIVATE_HEADERS}
		../plugins/container/filesystem/FilesystemContainer
		../plugins/container/filesystem/FilesystemContainerPlugin
		../plugins/container/zipfile/ZipfileContainer
		../plugins/container/zipfile/ZipfileContainerPlugin
		../plugins/toc/filesystem/FilesystemTOC
		../plugins/toc/filesystem/FilesystemTOCPlugin
		../plugins/toc/epub/EpubTOC
		../plugins/toc/epub/EpubTOCPlugin
		../plugins/content/html/HTMLContent
		../plugins/content/html/HTMLContentPlugin
		../plugins/shop/feedbooks/FeedbooksShop
		../plugins/shop/feedbooks/FeedbooksPlugin
		../plugins/shop/e5Commerce-0.x/e5Commerce0Shop
		../plugins/shop/e5Commerce-0.x/e5Commerce0Plugin
		${CMAKE_CURRENT_BINARY_DIR}/e5Commerce
		../plugins/shop/e5SaaS/e5SaaSShop
		../plugins/shop/e5SaaS/e5SaaSPlugin
		../plugins/shop/e5SaaS/e5SaaSShopContent
		../plugins/shop/e5SaaS/e5SaaSCategory
	)
	set(SRCS ${SRCS}
		../plugins/container/filesystem/FilesystemContainer.cpp
		../plugins/container/filesystem/FilesystemContainerPlugin.cpp
		../plugins/container/zipfile/ZipfileContainer.cpp
		../plugins/container/zipfile/ZipfileContainerPlugin.cpp
		../plugins/toc/filesystem/FilesystemTOC.cpp
		../plugins/toc/filesystem/FilesystemTOCPlugin.cpp
		../plugins/toc/epub/EpubTOC.cpp
		../plugins/toc/epub/EpubTOCPlugin.cpp
		../plugins/content/html/HTMLContent.cpp
		../plugins/content/html/HTMLContentPlugin.cpp
		../plugins/shop/feedbooks/FeedbooksShop.cpp
		../plugins/shop/feedbooks/FeedbooksPlugin.cpp
		../plugins/shop/e5Commerce-0.x/e5Commerce0Shop.cpp
		../plugins/shop/e5Commerce-0.x/e5Commerce0Plugin.cpp
		${CMAKE_CURRENT_BINARY_DIR}/e5Commerce
		../plugins/shop/e5SaaS/e5SaaSShop.cpp
		../plugins/shop/e5SaaS/e5SaaSShopContent.cpp
		../plugins/shop/e5SaaS/e5SaaSCategory.cpp
		../plugins/shop/e5SaaS/e5SaaSPlugin.cpp
	)

	# generation of RPC JSON calls for e5Commerce v2.0 integration:
	set_source_files_properties(${CMAKE_CURRENT_BINARY_DIR}/e5Commerce PROPERTIES GENERATED true)
	add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/e5Commerce COMMAND ${RPCAPIGENERATOR} ${CMAKE_CURRENT_SOURCE_DIR}/../plugins/shop/e5SaaS/e5Commerce.rpc ${CMAKE_CURRENT_BINARY_DIR}/e5Commerce DEPENDS ${RPCAPIGENERATOR} MAIN_DEPENDENCY ${CMAKE_CURRENT_SOURCE_DIR}/../plugins/shop/e5SaaS/e5Commerce.rpc COMMENT "Creating e5Commerce API wrapper")
	include_directories(${CMAKE_CURRENT_BINARY_DIR})

	find_package(QuaZip REQUIRED)
	if(WIN32)
		if(NOT ${ZLIB_DIR} MATCHES "^$")
			message("Including ${ZLIB_DIR}/include/zlib")
			include_directories(${ZLIB_DIR}/include/zlib ${ZLIB_DIR}/include)		
		else()
			message("Including ${CMAKE_INSTALL_PREFIX}/include/zlib")
			include_directories(${CMAKE_INSTALL_PREFIX}/include/zlib ${CMAKE_INSTALL_PREFIX}/include)
		endif()
	endif()
	include_directories(${QuaZip_INCLUDE_DIR})
	include_directories(${QJSON_INCLUDE_DIR})
	include_directories(${qjson_INCLUDE_DIR})
	include_directories(${CMAKE_CURRENT_BINARY_DIR})
	set(EXTRA_LIBS ${EXTRA_LIBS} ${QuaZip_LIBRARY} ${QJSON_LIBRARIES})
endif()

#if(iPhone)
#	set(PUBLIC_HEADERS ${PUBLIC_HEADERS} e5URLProtocol.h)
#	set(SRCS ${SRCS} e5URLProtocol.mm)
#	set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -fobjc-abi-version=2)
#	set(EXTRA_LIBS ${EXTRA_LIBS} -lobjc)
#endif()

qt4_wrap_cpp(MOC_SRCS ${PUBLIC_HEADERS} ${PRIVATE_HEADERS})
set(SRCS ${SRCS} ${MOC_SRCS})
add_definitions(-DBUILDING_LIBE5)
add_library(e5 SHARED ${SRCS})
if(NOT android)
	set_target_properties(e5 PROPERTIES VERSION 0.0.0 SOVERSION 0)
endif()
if(NOT iPhone)
	# For QPixmap in ItemCategory
	set(EXTRA_LIBS ${EXTRA_LIBS} ${QT_QTGUI_LIBRARY})
endif()
#set_target_properties(e5 PROPERTIES FRAMEWORK true FRAMEWORK_VERSION 0 PUBLIC_HEADER "${PUBLIC_HEADERS}" INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/Library/Frameworks" DEBUG_POSTFIX -d)
install(FILES ${PUBLIC_HEADERS} ${CMAKE_CURRENT_BINARY_DIR}/e5Visibility DESTINATION include)
install(TARGETS e5 LIBRARY DESTINATION ${LIB_DESTINATION} FRAMEWORK DESTINATION Library/Frameworks PUBLIC_HEADER DESTINATION include ARCHIVE DESTINATION ${LIB_DESTINATION} RUNTIME DESTINATION ${LIB_DESTINATION})
target_link_libraries(e5 ${QT_QTMAIN_LIBRARY} ${QT_QTCORE_LIBRARY} ${QT_QTNETWORK_LIBRARY} ${QT_QTXML_LIBRARY} ${QtExt_LIBRARY} ${qjson_LIBRARIES} ${QJSON_LIBRARIES} ${EXTRA_LIBS})

if(WITHOUT_PLUGINS)
	if(NOT WITHOUT_TIDY)
		if(iPhone)
			# crApple makes tidy part of their API, but doesn't
			# allow linking to it...
			target_link_libraries(e5 -ltydi)
		else()
			target_link_libraries(e5 -ltidy)
		endif()
	endif()
	target_link_libraries(e5 -lquazip)
endif()

configure_file(e5Visibility.in e5Visibility)
