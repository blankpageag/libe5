#include "ConditionManager"
#include "Condition"
#include <QDebug>

ConditionManager::ConditionManager():QObject(0) {
}

void ConditionManager::addCondition(QDomElement const &e) {
	Condition * const c=new Condition(e, this);
	QString const &name=e.attribute("id", "unnamedcondition");
	if(_conditions.contains(name)) {
		qDebug() << "Duplicate condition " << name << ", probably bogus definition file";
		delete _conditions[name];
	}
	_conditions.insert(name, c);
}

void ConditionManager::childEvent(QChildEvent *e) {
	if(!e->removed())
		return;
	Condition *c=qobject_cast<Condition*>(e->child());
	if(!c)
		return;
	QString const &key=_conditions.key(c);
	if(!key.isEmpty())
		_conditions.remove(key);
}

void ConditionManager::reEvaluate() {
	foreach(Condition *c, _conditions)
		c->evaluate(false);
}
