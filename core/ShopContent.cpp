#include "ShopContent"
#include <QFile>
#include <QDate>
#include <Downloader>

ShopContent::ShopContent():ShopItem(QString(), QString(), QString(), NULL)
{
}

ShopContent::ShopContent(QString const &name, QString const &description, QString const &author, QString const &publisher, QString const &releaseDate, QUrl const &iconUrl, QUrl const &tocUrl, ItemCategory *parent):ShopItem(name, description, iconUrl, parent)
{
	insert("tocUrl", tocUrl.toString());
	insert("publisher", publisher);
	insert("releaseDate", releaseDate);

}


QString ShopContent::publisher() const 
{ 
	return value("publisher"); 
}

QString ShopContent::author() const 
{ 
	return value("author"); 
}

QString ShopContent::date() const
{ 
	QDate dd = QDate::fromString( value("releaseDate"), Qt::ISODate );
	return dd.toString("yyyy-MM-dd");
}

QString ShopContent::time() const
{ 
	QDate dd = QDate::fromString( value("releaseDate"), Qt::ISODate );
	return dd.toString("hh-mm-ss");
}


QString ShopContent::productId() const 
{ 
	return QString::number( id() );
}

QString ShopContent::fileId() const 
{ 
	return value("file_id");
}


QUrl ShopContent::tocUrl() const 
{
	return QUrl(value("tocUrl"));
}


QByteArray ShopContent::tocImage() const
{
	QByteArray toc;
	QUrl u = tocUrl();
	toc=Downloader::get( u );
	return toc;
}

