#include "ContentCache"

void ContentCache::contentDeleted(QObject *o)
{
	iterator it = begin();
	while(it != end()) {
		if(it.value() == o) {
			it = erase(it);
		} else {
			++it;
		}
	}
}
