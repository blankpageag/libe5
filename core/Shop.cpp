#include "Shop"
#include "ShopPlugin"
#include "Consumable"
#include "PluginLoader"
#include <QDebug>

IMPLEMENT_PLUGIN_LOADER(ShopPlugin,Shop,QUrl,shop)


#ifdef NO_PLUGINS
#include "../plugins/shop/feedbooks/FeedbooksPlugin"
#include "../plugins/shop/e5Commerce-0.x/e5Commerce0Plugin"
#include "../plugins/shop/e5SaaS/e5SaaSPlugin"
void Shop::initializeStaticPlugins() {
	_plugins.insert("feedbooks", new FeedbooksPlugin);
	_plugins.insert("e5commerce0", new e5Commerce0Plugin);
	_plugins.insert("e5saas", new e5SaaSPlugin);
}
#else
void Shop::initializeStaticPlugins() {}
#endif


Shop::Shop(QString const &name, QString const &description, QUrl url):ShopMetadata(name, description), _url(url), _root(0)
{ 
	insert("sandboxMode", QString("false"));
}

/**
 * Set the sandbox mode of the shop.
 * @param sandboxMode bool 
 */
void Shop::setSandbox(bool sandboxMode)
{
	insert( "sandboxMode", QString(sandboxMode?"true":"false"));
	qDebug() <<"Sandbox mode of shop "<<name()<< "set to "<<sandbox();
}
