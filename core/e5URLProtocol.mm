#import "e5URLProtocol.h"
#include "ContainerAccessManager"
#include <Downloader>
#include <QUrl>

@implementation e5URLProtocol
+(BOOL)canInitWithRequest:(NSURLRequest*)request {
	NSURL *url=[request URL];
	if(url == nil)
		return FALSE;
	NSString *scheme=[url scheme];
	if(scheme == nil)
		return FALSE;
	return [[scheme lowercaseString] isEqual:@"e5internal"];
}

+(NSURLRequest*)canonicalRequestForRequest:(NSURLRequest*)request {
	// Is this any different? return [NSURLRequest requestWithURL:[request URL]];
	return request;
}

+(BOOL)requestIsCacheEquivalent:(NSURLRequest*)a toRequest:(NSURLRequest*)b {
	return [[[a URL] absoluteString] isEqualToString:[[b URL] absoluteString]];
}

-(void)startLoading {
	NSURL *nsUrl = [[self request] URL];
	if(nsUrl == nil)
		return;

	NSString *type;
	QUrl url([[nsUrl absoluteString] UTF8String]);
	QString const extension=url.path().section('.', -1).toLower();
	if(extension == "xml")
		type=@"application/xml";
	else if(extension == "png")
		type=@"image/png";
	else if(extension == "jpg" || extension == "jpeg")
		type=@"image/jpeg";
	else if(extension == "gif")
		type=@"image/gif";
	else if(extension == "tif" || extension == "tiff")
		type=@"image/tiff";
	else if(extension == "avi")
		type=@"video/avi";
	else if(extension == "mpg" || extension == "mpeg")
		type=@"video/mpeg";
	else if(extension == "mp4" || extension == "mov" || extension == "m4v" || extension == "m4a")
		type=@"video/mp4";
	else if(extension == "webm")
		type=@"video/webm";
	else if(extension == "ogg")
		type=@"application/ogg";
	else if(extension == "aac")
		type=@"audio/x-aac";
	else if(extension == "mp3")
		type=@"audio/mpeg";
	else if(extension == "html" || extension == "htm" || extension == "xhtml" || extension == "xhtm" || extension == "xht")
		type=@"text/html";
	else if(extension == "txt")
		type=@"text/plain";
	else
		type=@"application/octet-stream";
		
	ContainerAccessManager *cam=ContainerAccessManager::instance(0);
	QByteArray const content=Downloader::get(url, cam);
	cam->deref(0);
	
	NSData *data=[NSData dataWithBytes:content.data() length:content.length()];
	NSURLResponse *response = [[NSURLResponse alloc] initWithURL:nsUrl MIMEType:type expectedContentLength:content.length() textEncodingName:@"utf-8"];
	[response autorelease];
	[self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
	[self.client URLProtocol:self didLoadData:data];
	[self.client URLProtocolDidFinishLoading:self];
}

-(void)stopLoading {
}
@end
