// vim:syntax=cpp.doxygen
#ifndef _TOCPLUGIN_
#define _TOCPLUGIN_ 1

#include "Container"
extern "C" {
#include <stdint.h>
}

class TOC;

#ifdef NO_PLUGINS
#define EXPORT_PLUGIN(name, classname)
#else
#define EXPORT_PLUGIN(name, classname) Q_EXPORT_PLUGIN2(name, classname)
#endif

/**
 * @class TOCPlugin
 * @brief Base class for TOC plugins
 * @author Bernhard Rosenkraenzer <br@blankpage.ch>
 * @date 2010
 */
class E5_EXPORT TOCPlugin {
public:
	/**
	 * Check the priority of the TOCPlugin for the content of the given @c container.
	 *
	 * The priority is a number between 0 and 255, where 0 means the plugin
	 * can't handle this container at all, and 255 means this is definitely
	 * the best plugin for handling the given container.
	 * 
	 * See the description of ContainerPlugin::priority for an example of
	 * the correct use of priorities.
	 *
	 * @param[in] container container to check
	 *
	 * @return Priority this plugin should be given when handling the container
	 */
	CONST virtual uint8_t priority(Container const * const container) const = 0;
	/**
	 * Get the TOC object for the @c container.
	 *
	 * @param[in] container Container
	 *
	 * @return TOC object or @c 0 on error
	 */
	virtual TOC *get(Container * const container) const = 0;
};

Q_DECLARE_INTERFACE(TOCPlugin, "ch.blankpage.e5.core.toc/0.1");
#endif
