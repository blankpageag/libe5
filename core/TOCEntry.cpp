#include "TOCEntry"
#include "TOC"

#include "e5Logger"
#include "Downloader"
#include "ContainerAccessManager"
#include "Content"

void TOCEntry::append(TOCItem * const t, QStringList const &conditions) {
	Content * const c=dynamic_cast<Content * const>(t);
	if(!c)
		return;
	if(conditions.count())
		_conditions.insert(c, conditions);
	append(c);
}


Content *TOCEntry::append(QString const &file, QStringList const &conditions) {
	Content *c=Content::get(this, file);
	if(!c)
		return 0;
	append(c, conditions);
	return c;
}


QStringList TOCEntry::conditions(Content const * const c) const {
	// The const_cast is ugly, but it's needed until QHash can tell that
	// hash-wise, a pointer and a const-pointer are the same...
	// Given we can actually guarantee c remains const, the const_cast
	// is less evil than removing the const from the function
	// signature...
	if(!_conditions.contains(const_cast<Content * const>(c)))
		return QStringList();
	return _conditions.value(const_cast<Content * const>(c));
}


void TOCEntry::setFromSerialized(QDataStream *stream)
{
	QMultiHash<QString, QString> metadata;
	QHash<QString, QString> eventDates;

	*stream >> metadata;
	*stream >> eventDates;

	_metadata = metadata;
	_eventDates = eventDates;
}


void TOCEntry::setConditions(Content * const c, QStringList const &conditions)
{
	_conditions.insert(c, conditions);
	QMetaObject::invokeMethod(c, "metadataChanged");
}


QList<Content*> TOCEntry::contentForConditions(QStringList const &conditions) const
{
	QList<Content*> results;
	for(int i=0; i<count(); i++) {
		Content * const c=dynamic_cast<Content * const>(at(i));
		if(!c)
			continue;
		QStringList const co=c->conditions();
		bool ok=true;
		foreach(QString const &condition, conditions) {
			if(condition.startsWith('!')) {
				if(co.contains(condition.mid(1)))
					ok = false;
			} else if(co.contains("!" + condition))
				ok = false;
			if(!ok)
				break;
		}
		if(ok)
			results << c;
	}
	return results;
}


void TOCEntry::setTocImage(QString iImagePath) 
{
	if(_metadata.contains("e5:tocImage"))
	{
		((Metadata*)this)->replace("e5:tocImage", iImagePath);
	}
	else 
	{
		((Metadata*)this)->insert("e5:tocImage", iImagePath);
	}
	emit tocImageChanged();
}


QByteArray TOCEntry::tocImage() const 
{
	QByteArray image;
	if(_metadata.contains("e5:tocImage")) {
		TOC *parentToc = toc();
		Container *container = 0;
		if(parentToc) {
			container = parentToc->container();
		}

		QString urlStr = _metadata.value("e5:tocImage");
		if(!urlStr.contains(baseUrl())) {
			urlStr = baseUrl() + urlStr;
		}

		QUrl url(urlStr);

		// If the TOC image is in the container, we don't need the ContainerAccessManager overhead.
		// Read it directly from the container.
		if(container && (url.scheme().isEmpty() || url.scheme() == "e5internal")) {
			QString path = url.path();
			if(path.startsWith('/')) {
				path.remove(0, 1);
			}
			image = container->content(path);
		} else {
			ContainerAccessManager *m = ContainerAccessManager::instance(0);
			image = Downloader::get(url, m);
			m->deref(0);
		}
	}

	if(!image.isEmpty()) {
		return image;
	}
	return QByteArray();	
}


void TOCEntry::append(TOCItem * const c)
{
	QList<TOCItem*>::append(c);
	emit itemAdded(c, count() - 1);
}


void TOCEntry::append(QList<TOCItem*> const& values)
{
	foreach(TOCItem *c, values) {
		append(c);
	}
}


void TOCEntry::clear()
{
	for(int i = count() - 1; i >= 0; i--) {
		removeAt(i);
	}
}


void TOCEntry::insert(int position, TOCItem * const c)
{
	QList<TOCItem*>::insert(position, c);
	emit itemAdded(c, position);
}


void TOCEntry::move(int from, int to)
{
	insert(to, takeAt(from));
}


void TOCEntry::prepend(TOCItem * const c)
{
	QList<TOCItem*>::prepend(c);
	emit itemAdded(c, 0);
}


int	TOCEntry::removeAll(TOCItem * const c)
{
	for(int i = count() - 1; i >= 0; i--) {
		if(at(i) == c) {
			removeAt(i);
		}
	}
}


void TOCEntry::removeAt(int i)
{
	TOCItem *item = at(i);
	emit itemAboutToBeRemoved(item, i);
	QList<TOCItem*>::removeAt(i);
	emit itemRemoved(item, i);
}


bool TOCEntry::removeOne(TOCItem * const c)
{
	int i = indexOf(c);
	if(i == -1) {
		return false;
	} else {
		removeAt(i);
		return true;
	}
}


void TOCEntry::replace(int i, TOCItem * const c)
{
	QList<TOCItem*>::replace(i, c);
	emit itemChanged(c, i);
}


TOCItem* TOCEntry::takeAt(int i)
{
	TOCItem *item = at(i);
	emit itemAboutToBeRemoved(item, i);
	QList<TOCItem*>::removeAt(i);
	emit itemRemoved(item, i);
	return item;
}


QList<TOCEntry*> TOCEntry::entriesOfType(QString const &t) const {
	QList<TOCEntry*> result;
	for(int i=0; i<count(); i++) {
		TOCEntry * const e = dynamic_cast<TOCEntry*>(at(i));
		if(!e)
			continue;
		if(static_cast<Metadata*>(e)->contentType().contains(t))
			result << e;
	}
	return result;
}


TOCItem* TOCEntry::findItem(QString const& itemId)
{
	if(id() == itemId) {
		return this;
	}

	TOCItem *item = 0;
	for(int i = 0; i < count(); i++) {
		if(TOCEntry *childEntry = qobject_cast<TOCEntry*>(at(i))) {
			item = childEntry->findItem(itemId);
			if(item) {
				return item;
			}
		} else if(at(i)->id() == itemId) {
			return at(i);
		}
	}

	return 0;
}


void TOCEntry::serialize(QDataStream *outputStream) const
{
	*outputStream << metadata(false);
	*outputStream << eventDates(false);
}
