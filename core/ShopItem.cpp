#include "ShopItem"

#include <ItemCategory>

ShopItem::ShopItem(QString const &name, QString const &description, QUrl const &iconUrl, ItemCategory *parent):
ShopMetadata(name, description, iconUrl)
,_parent(parent) 
{
	if(_parent)
		_parent->addChild(this);
}

ShopItem::~ShopItem()
{
	if(_parent)
		_parent->removeChild(this);
}
