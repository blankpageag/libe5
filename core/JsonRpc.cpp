#include "JsonRpc"
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <serializer.h> // QJson
#include <parser.h> // QJson

JsonRpc::JsonRpc(QUrl const &url):_url(url) {
}

QByteArray JsonRpc::generateCall(QString const &method, QVariantList const &args, int id) const {
	QVariantMap call;
	call.insert("jsonrpc", "2.0");
	call.insert("method", method);
	call.insert("params", args);
	call.insert("id", id);
  	QJson::Serializer serializer;
	QByteArray junk=serializer.serialize(call);
	//qDebug() << junk;
	return junk;
}

QVariant JsonRpc::callRPC(QString const &method, QVariantList const &args, int id) const {
	QByteArray const call=generateCall(method, args, id);
	QNetworkAccessManager nam;
	QNetworkRequest req;
	req.setUrl(_url);
	req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
	QEventLoop l;
	QNetworkReply *r=nam.post(req, call);
	QObject::connect(r, SIGNAL(finished()), &l, SLOT(quit()));
	l.exec();
	QJson::Parser parser;
	bool ok;
	QVariant result=parser.parse(r->readAll(), &ok);
	delete r;    
	//qDebug() << result;
	// qDebug() << result.toMap().value("name").toString();
	return result;
}
