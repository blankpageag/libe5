#include "e5LoggingService"

#include <QIOStream>


void e5LoggingService::log(QtMsgType type, const char *msg)
{ 
	switch (type) {
		case QtDebugMsg:
			fprintf(stderr, "e5 Debug: %s\n", msg);
			break;
		case QtWarningMsg:
			fprintf(stderr, "e5 Warning: %s\n", msg);
			break;
		case QtCriticalMsg:
			fprintf(stderr, "e5 Fatal: %s\n", msg);
			break;
		case QtFatalMsg:
			fprintf(stderr, "e5 Fatal: %s\n", msg);
			abort();
		default:
			fprintf(stderr, "e5 Unknown Message Type: %s\n", msg);
	}
}








