#include "ItemCategory"
#include "Consumable"
#include <QDebug>

ItemCategory::ItemCategory(QString const &name, QString const &description, QUrl const &iconUrl, ItemCategory *parent):ShopItem(name, description, iconUrl, parent),_loaded(false) {
}

ItemCategory::~ItemCategory() {
}

void ItemCategory::addChild(ShopItem *child) {
	_children.append(child);
}

void ItemCategory::removeChild(ShopItem *child) {
	_children.removeAll(child);
}

QList<ShopItem*> const &ItemCategory::children() const {
	if(!_loaded)
		const_cast<ItemCategory*>(this)->load();
	return _children;
}

bool ItemCategory::hasSubcategories() const {
	foreach(ShopItem *s, children())
		if(s->type().contains("ItemCategory"))
			return true;
	return false;
}

bool ItemCategory::hasConsumables() const {
	foreach(ShopItem *s, children())
		if(s->type().contains("Consumable"))
			return true;
	return false;
}

static void addSubcategories(QList<ItemCategory const *> &list, int &start, int &end, int &pos, ItemCategory const *parent) {
	/**
	 * Lukas: do not add the root category to the list of subcategories
	if(start<++pos)
		list << parent;
	 */
	if(end>0 && end<=pos)
		return;

	QList<ShopItem*> const &c=parent->children();
	foreach(ShopItem *it, c) {
		if(it->type().contains("ItemCategory")) {
			addSubcategories(list, start, end, pos, static_cast<ItemCategory*>(it));
			if(end>0 && end<=pos)
				break;
		}
	}
	return;
}

QList<ItemCategory const *> ItemCategory::categories(int start, int end) const {
	QList<ItemCategory const *> result;
	int pos = 0;
	addSubcategories(result, start, end, pos, this);
	return result;
}

static void addProducts(QList<Consumable const *> &result, int &start, int &end, int &pos, ItemCategory const *parent) {
	QList<ShopItem*> const &c=parent->children();
	foreach(ShopItem *it, c) {
		if(it->type().contains("ItemCategory")) {
			addProducts(result, start, end, pos, static_cast<ItemCategory*>(it));
			if(end>=0 && end<pos)
				break;
		} else if(it->type().contains("Consumable")) {
			if(start<++pos)
			{
				//qDebug() << "Found product: "<<(static_cast<Consumable*>(it))->name();
				result << static_cast<Consumable*>(it);
			}
			if(end>=0 && end<pos)
				break;
		} else
			qWarning() << "Encountered something that is neither a ItemCategory nor a Consumable in" << parent->name() << "Ignoring.";
	}
}

QList<Consumable const *> ItemCategory::products(int start, int end) const {
	QList<Consumable const *> result;
	int pos = 0;
	addProducts(result, start, end, pos, this);
	return result;
}
