#include "Container"
#include "TOC"
#include "TOCPlugin"
#include "TOCEntry"
#include "Content"
#include "PluginLoader"
#include <QFileInfo>

IMPLEMENT_PLUGIN_LOADER(TOCPlugin,TOC,Container * const,toc)

enum TOCItemType {
	UnknownItemType = -1,
	TOCEntryItem = 0,
	ContentItem = 1
};


TOC::TOC(Container * const container) :
	TOCEntry(0,"__e5_toc_root"),
	_conditionManager(),
	_container(container),
	_id(QString::null)
{
	connect(this, SIGNAL(itemAdded(TOCItem*,int)),
			this, SIGNAL(changed()));
	connect(this, SIGNAL(itemChanged(TOCItem*,int)),
			this, SIGNAL(changed()));
	connect(this, SIGNAL(itemRemoved(TOCItem*,int)),
			this, SIGNAL(changed()));
}


#ifdef NO_PLUGINS
#include "../plugins/toc/filesystem/FilesystemTOCPlugin"
#include "../plugins/toc/epub/EpubTOCPlugin"
void TOC::initializeStaticPlugins() {
	_plugins.insert("filesystem", new FilesystemTOCPlugin);
	_plugins.insert("epub", new EpubTOCPlugin);
}
#else
void TOC::initializeStaticPlugins() {
}
#endif

QString TOC::baseUrl() const {
	return container()->baseUrl();
}

TOCEntry *TOC::addEntry(QString const &id) {
	TOCEntry *e=new TOCEntry(this, id);
	append(e);
	return e;
}

bool TOC::isMetadata(QString const &fn) const {
	return false;
}

QString TOC::mimeType(QString const &fn) const {
	QString const &ext=QFileInfo(fn).suffix().toLower();
	if(ext == "html" || ext == "htm")
		return "text/html";
	else if(ext == "xhtml" || ext == "xhtm" || ext == "xht")
		return "application/xhtml+xml";
	else if(ext == "jpg" || ext == "jpeg")
		return "image/jpeg";
	else if(ext == "png")
		return "image/png";
	else if(ext == "gif")
		return "image/gif";
	else if(ext == "webp")
		return "image/webp";
	else if(ext == "jp2" || ext == "j2k")
		return "image/jp2";
	else if(ext == "tiff" || ext == "tif")
		return "image/tiff";
	else if(ext == "bmp")
		return "image/bmp";
	else if(ext == "ogv")
		return "video/ogg";
	else if(ext == "oga")
		return "audio/ogg";
	else if(ext == "ogg")
		return "application/ogg";
	else if(ext == "mkv")
		return "video/x-matroska";
	else if(ext == "mp4" || ext == "m4v" || ext == "mov")
		return "video/mp4";
	else if(ext == "m4a" || ext == "aac")
		return "audio/mp4";
	else if(ext == "mpeg" || ext == "mpg")
		return "video/mpeg";
	else if(ext == "mp3")
		return "audio/mpeg";
	else if(ext == "3gp")
		return "video/3gpp";
	else if(ext == "webm")
		return "video/webm";
	else if(ext == "avi")
		return "video/avi";
	else if(ext == "wmv")
		return "video/x-ms-wmv";
	else if(ext == "rm")
		return "application/vnd.rn-realmedia";
	else if(ext == "wav")
		return "audio/x-wav";
	else if(ext == "ncx")
		return "application/x-dtbncx+xml";
	else if(ext == "opf")
		return "application/oebps-package+xml";
	else if(ext == "css")
		return "text/css";
	else if(ext == "js")
		return "text/javascript"; // the CORRECT mime type is application/javascript, but some crappy browsers don't understand that one
	else if(ext == "otf")
		return "font/otf";
	else if(ext == "ttf")
		return "font/ttf";
	else if(ext == "eot")
		return "application/vnd.ms-fontobject.eot";
	else if(ext == "woff")
		return "font/x-woff";
	return "application/octet-stream";
}


TOCItem* TOC::restoreFromSerialized(QDataStream *stream)
{
	stream->setVersion(QDataStream::Qt_4_6);

	int type = UnknownItemType;
	*stream >> type;

	if(type == UnknownItemType) {
		qWarning() << "Tried to restore an invalid item type.";
		return 0;
	}

	// Get the parent and its index
	QString parentId;
	int index = -1;
	QString id;
	*stream >> parentId;
	*stream >> index;
	*stream >> id;
	if(index == -1) {
		qWarning() << "Invalid index found while trying to restore an item";
		return 0;
	}
	TOCEntry *parent = findEntry(parentId);
	if(!parent) {
		qWarning() << "Couldn't find the parent while trying to restore an item. Parentid:" << parentId;
		return 0;
	}

	if(index >= 0 && index < parent->count()) {
		if(parent->at(index)->id() == id) {
			return parent->at(index);
		}
	}

	TOCItem *item = 0;
	if(type == TOCEntryItem) {
		TOCEntry *entry = new TOCEntry(parent, id);
		entry->setFromSerialized(stream);
		item = entry;

	} else if(type == ContentItem) {
		item = Content::getFromSerialized(parent, stream);
	}

	if(item) {
		parent->insert(index, item);
		return item;
	} else {
		qCritical() << "No item restored.";
		return 0;
	}
}


TOCItem* TOC::restoreFromSerialized(QByteArray const& serializedData)
{
	QDataStream stream(const_cast<QByteArray*>(&serializedData), QIODevice::ReadOnly);
	return restoreFromSerialized(&stream);
}


void TOC::serialize(TOCItem *item, QDataStream *outputStream) const
{
	outputStream->setVersion(QDataStream::Qt_4_6);
	if(item->inherits("TOCEntry")) {
		*outputStream << TOCEntryItem;
	} else if(item->inherits("Content")) {
		*outputStream << ContentItem;
	} else {
		*outputStream << UnknownItemType;
		return;
	}

	TOCEntry *parent = item->parentEntry();
	if(parent) {
		*outputStream << parent->id();
		*outputStream << parent->indexOf(item);
	} else {
		*outputStream << QString();
		*outputStream << int(-1);
	}

	*outputStream << item->id();
	item->serialize(outputStream);
}


QByteArray TOC::serialized(TOCItem *item) const
{
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);
	serialize(item, &stream);
	return data;
}


QStringList TOC::validate() const 
{
	//qDebug() <<"Validation of TOC: " << this->title();
	QStringList result;
	QStringList const &files=container()->files();
	//qDebug() << files.size() << " to validate...";
	for(int i=0; i<count(); i++) {
		TOCEntry *e=dynamic_cast<TOCEntry*>(at(i));
		if(!e) {
			result << tr("Top-level TOC contains something other than a TOCEntry: %1").arg(e->metaObject()->className());
			continue;
		}
		for(int j=0; j<e->count(); j++) {
			Content *c=dynamic_cast<Content*>(e->at(j));
			if(!c) {
				// FIXME we need to validate the sub-items!
				continue;
			}
			result.append(c->errors());
			QString const file=c->baseUrl().section('/', 3) + c->fileName();
			//qDebug() <<"\t\t - checking if file exists: " << file;
			if(!files.contains(file)) {
				result << tr("Nonexistant file referenced: %1").arg(file);
				qCritical() <<tr("Nonexistant file referenced: %1").arg(file); 
			}
		}
	}
	return result;
}


QString TOC::id() const {
	if(_id.isEmpty()) {
        
        //_id=QString::number(qHash(Metadata::id()));
        
	qint64 timestamp = QDateTime::currentMSecsSinceEpoch();
	qsrand(timestamp);
	_id = QString::number(qHash(QString::number(timestamp+qrand())));

        // the method below is currently broken because it does NOT generate an unique id if a two pdf files with the same amount of pages are uploaded
        // so we will just use an random id until this bug is fixed
        
        /*
		// Looks like we didn't get an actual ID - so let's generate one that
		// - remains the same for the lifetime of the object
		// - is the same for this file on any OS
		QStringList hash=container()->originalFiles();
		// Just the file list by itself isn't good enough because epub
		// generators tend to use the same naming scheme, so chances are
		// epubs with an identical number of chapters and images generated
		// by the same generator will have the same file list.
		// Picking the first file would likely give us the mimetype file
		// for an epub, which is also constant - picking the last file could
		// give us a constant page (e.g. "Downloaded through Feedbooks"), so
		// instead we pick one in the middle...
		// Still not perfect, but quite likely to generate something unique
		// and nevertheless constant.
		if(hash.isEmpty())
			// Out of luck. Let's just return anything.
			_id=Metadata::id();
		else {
			hash << QString::fromUtf8(container()->content(hash.at(hash.count()/2)));
			// We need to sort it because the order in which files are read
			// depends on the OS and locale
			hash.sort();
			_id=QString::number(qHash(hash.join(";")));
		}
		if(_id.contains('/')) {
			qWarning() << "dc:identifier contains invalid character: /";
			_id.replace('/','-');
		}
		if(_id.contains(' ')) {
			qWarning() << "dc:identifier contains invalid character: space";
			_id.replace(' ', '-');
		}
		if(_id.contains(':')) {
			qWarning() << "dc:identifier contains invalid character: :";
			_id.replace(':', '-');
		}
		if(_id.contains('@')) {
			qWarning() << "dc:identifier contains invalid character: :";
			_id.replace('@', '-');
		}
        */
	}
	return _id;
}
