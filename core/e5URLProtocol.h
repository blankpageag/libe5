#import <Foundation/Foundation.h>

/**
 * Objective-C++ connector to e5 URLs
 * Applications should run
 * @c [NSURLProtocol registerClass:[e5URLProtocol class]];
 * to teach WebKit about e5 URLs.
 */
@interface e5URLProtocol : NSURLProtocol {
}
@end
