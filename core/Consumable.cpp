#include <QtNetwork>
#include "Consumable"

Consumable::Consumable(QString const &author, QString const &publisher, QString const &releaseDate, QString const &name, QString const &description, QUrl const &iconUrl, QUrl const &tocUrl,ItemCategory *parent):
ShopContent(name, description, author, publisher, releaseDate, iconUrl, tocUrl, parent){
	insert("author", author);
}


bool Consumable::getInPackages(QString filePath, int packageSize,  QNetworkAccessManager* nam, int start, int end) 
{
	return get( filePath, nam, start, end);
}


bool Consumable::downloadInPackages(QString const &filename, int packageSize, int start, int end)  
{
	if( downloadedBytes() < start )
	{
		qWarning() << "Starting download of consumable "<<name()<<" from higher byte ("<<start<<") as previously downloaded ("<<downloadedBytes()<<")";
	}	
	return getInPackages(filename, packageSize, NULL, start, end );
}


// Please leave filename passed by value instead of by reference. Otherwise we get crashes in e5 Publish, see TRAC #324.
bool Consumable::download(QString filename, int start, int end)  
{
	if( downloadedBytes() < start )
	{
		qWarning() << "Starting download of consumable "<<name()<<" from higher byte ("<<start<<") as previously downloaded ("<<downloadedBytes()<<")";
	}
	
	QWeakPointer<Consumable> pointer(this);
	QByteArray content=get(NULL, start, end);
	if(pointer.isNull()) {		// Shit! We have been destroyed in the meantime! Get the hell out of here to prevent a crash!
		return false;
	}
	return saveContentToFile(filename, content, (start == 0));
}


// Please leave filename passed by value instead of by reference. Otherwise we get crashes in e5 Publish, see TRAC #324.
bool Consumable::download(QString filename, QByteArray const &receipt, int start, int end) 
{
	if( downloadedBytes() < start )
	{
		qWarning() << "Starting download of consumable "<<name()<<" from higher byte ("<<start<<") as previously downloaded ("<<downloadedBytes()<<")";
	}
	
	QWeakPointer<Consumable> pointer(this);
	QByteArray content=get(receipt, NULL, start, end);	
	if(pointer.isNull()) {		// Shit! We have been destroyed in the meantime! Get the hell out of here to prevent a crash!
		return false;
	}
	//_downloadInProgress = false;
	return saveContentToFile(filename, content, (start == 0));
}


int Consumable::downloadFileSize() const
{
	return -1;
}



bool Consumable::downloadInPackages(QString const &filename, int packageSize, QByteArray const &receipt, int start, int end) 
{
	if( downloadedBytes() < start )
	{
		qWarning() << "Starting download of consumable "<<name()<<" from higher byte ("<<start<<") as previously downloaded ("<<downloadedBytes()<<")";
	}
	return getInPackages(filename, packageSize, receipt, NULL, start, end);	
}


bool Consumable::put(QIODevice *device, QNetworkAccessManager *nam)
{
	Q_UNUSED(device);
	Q_UNUSED(nam);
	return false;
}


bool Consumable::put(QByteArray const& data, QNetworkAccessManager *nam)
{
	QBuffer buffer(const_cast<QByteArray*>(&data));
	if(UNLIKELY(!buffer.open(QIODevice::ReadOnly))) {
		return false;
	}
	return put(&buffer, nam);
}


bool Consumable::upload(QUrl const& url, QNetworkAccessManager *nam)
{
	bool ownNam = false;
	if(!nam) {
		nam = new QNetworkAccessManager;
		ownNam = true;
	}

	QNetworkReply *reply = nam->get(QNetworkRequest(url));
	bool success = put(reply);

	if(ownNam) {
		delete nam;
	}

	return success;
}


bool Consumable::saveContentToFile(QString const &filename, QByteArray const &content, bool truncate) 
{
	if(content.isEmpty())
	{
		qWarning() << "Content to store is empty";		
		return false;
	}
	
	QIODevice::OpenMode mode = QFile::Truncate;
	if( !truncate)
	{
		mode = QFile::Append;
	}
	
	QFile f(filename);
	// check whether to start a new file or append resumed downloads to the end of the file:
	if( !f.open(QFile::WriteOnly |  mode ) )
	{
		qWarning() << "Cannot write content to file: "<<filename;
		return false;
	}
	
	f.write(content);
	f.close();	
	return true;
}


bool Consumable::uploadProgress(qint64 bytesSent, qint64 totalBytes)
{
	emit uploadProgressed(bytesSent, totalBytes);
	return true;
}
