#include "Content"
#include "TOC"
#include "ContentPlugin"
#include "ContentCache"
#include "PluginLoader"
#include <QDebug>

#ifdef NO_PLUGINS
#include "../plugins/content/html/HTMLContentPlugin"
#endif

static QHash<QString,ContentPlugin*> plugins;

Content::Content(TOCEntry *tocEntry, QString const &filename):_generated(false), 
TOCItem(tocEntry, QUrl(static_cast<TOC*>(tocEntry->parent())->baseUrl() + filename)),_filename(filename) 
{
}

Content *Content::get(TOCEntry *tocEntry, QString const &filename) {
	QPair<TOCEntry*,QString> current=QPair<TOCEntry*,QString>(tocEntry,filename);
	TOCEntry *toc=tocEntry;
	while(toc && !dynamic_cast<TOC*>(toc))
		toc=static_cast<TOCEntry*>(toc->parent());
	if(!toc) {
		qCritical()  << "TOC Entry (parent of Content::get for " << filename << ") doesn't seem to have a TOC parent";
		return 0;
	}
	ContentCache &cache=static_cast<TOC*>(toc)->container()->_contentCache;
	if(!cache.contains(current)) {
		if(plugins.isEmpty()) {
#ifndef NO_PLUGINS
			QDir pluginDir = pluginDirectory("content");
			foreach(QString const &filename, pluginDir.entryList(QStringList() << "*" PLUGINEXT, QDir::Files)) {
				QString const pluginFile=pluginDir.absoluteFilePath(filename);
				QPluginLoader loader(pluginFile);
				QObject *plugin=loader.instance();
				if(!plugin)
					qDebug() << pluginFile << " is not a correct plugin: " << loader.errorString();
				else {
					ContentPlugin *p=qobject_cast<ContentPlugin*>(plugin);
					if(!p)
						qDebug() << pluginFile << " is not a Content plugin";
					else
						plugins.insert(pluginFile, p);
				}
			}
#else
			plugins.insert("html", new HTMLContentPlugin);
#endif
		}
		ContentPlugin *best=0;
		uint8_t bestP=0;
		foreach(ContentPlugin *p, plugins) {
			uint8_t const priority=p->priority(tocEntry, filename);
			if(priority > bestP) {
				bestP=priority;
				best=p;
			}
		}
		if(!best)
			return 0;
		cache[current]=best->get(tocEntry, filename);
		QObject::connect(cache[current], SIGNAL(destroyed(QObject*)), &cache, SLOT(contentDeleted(QObject*)));
	}
	return cache[current];
}


Content* Content::getFromSerialized(TOCEntry *parent, QDataStream *stream)
{
	QString filename;
	QStringList conditions;
	QMultiHash<QString, QString> metadata;
	QHash<QString, QString> eventDates;

	*stream >> filename;
	*stream >> conditions;
	*stream >> metadata;
	*stream >> eventDates;

	Content *content = Content::get(parent, filename);
	if(content) {
		content->setConditions(conditions);
		content->_metadata = metadata;
		content->_eventDates = eventDates;
	}
	return content;
}


QString Content::fileName() const
{
	return _filename.section('/', -1);
}


QString Content::source() const
{
	return QString();
}

QString Content::html() const {
	if(_html.isNull())
		const_cast<Content*>(this)->createHtml();
	QString html=_html.toString();
	if(!html.contains("<?xml")) {
		// Inserting the doctype in front of <?xml is harmful...
		if(!html.contains("<!DOCTYPE")) {
			html.prepend("<!DOCTYPE html>\n");
		}

		if(!html.contains("<?xml")) {
			html.prepend("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		}
	}
	return html;
}

QDomDocument Content::dom() const {
	if(_html.isNull())
		const_cast<Content*>(this)->createHtml();
	return _html;
}


void Content::optimizeForEditing() {
	if(_html.isNull())
		createHtml();
	QHash<QString, int> idMap;
	addIDs(_html, _html.documentElement().toElement(), idMap);
}

QString Content::baseUrl() const {
	QString bU=Metadata::baseUrl();
	if(_filename.contains('/')) {
		QString path=_filename.section('/', 0, -2);
		// FIXME this is not 100% correct: It is necessary because
		// there's things like EPUBs with a OPS/ root, where files
		// (obviously) start with OPS/ again.
		// There may be a few cases where the check isn't sufficient,
		// e.g. if the TOC's base URL is e5internal://some/thing
		// and the html root is /thing/somethingelse/whatever
		if(!bU.endsWith(path + "/"))
			bU += path + "/";
	}
	return bU;
}

void Content::addIDs(QDomDocument &document, QDomElement const &root, QHash<QString,int> &idMap) {
	QDomNode child=root.firstChild();
	while(!child.isNull()) {
		QDomElement e=child.toElement();
		if(!e.isNull()) {
			if(!e.hasAttribute("id"))
				e.setAttribute("id", "__e5_" + e.tagName() + "_" + QString::number(++idMap[e.tagName()]));
			addIDs(document, e, idMap);
		}
		child=child.nextSibling();
	}
}

static QDomElement elementByAttribute(QDomDocument &doc, QString const &attribute, QString const &content, QDomElement const &root=QDomElement()) {
	QDomElement r = root.isNull() ? doc.documentElement() : root;
	if(r.hasAttribute(attribute) && r.attribute(attribute) == content)
		return r;

	QDomNode n=r.firstChild();
	while(!n.isNull()) {
		QDomElement e=n.toElement();
		if(!e.isNull()) {
			QDomElement childResult=elementByAttribute(doc, attribute, content, e);
			if(!childResult.isNull())
				return childResult;
		}
		n=n.nextSibling();
	}

	return QDomElement();
}

static inline QDomElement elementById(QDomDocument &doc, QString const &id, QDomElement const &root=QDomElement()) {
	return elementByAttribute(doc, "id", id, root);
}

void Content::setHtml(QString const &html) 
{
	_html.setContent(html);
}


void Content::serialize(QDataStream *outputStream) const
{
	*outputStream << _filename;
	*outputStream << conditions();
	*outputStream << metadata(false);
	*outputStream << eventDates(false);
}
