#include <QBuffer>
#include <Downloader>
#include <QTemporaryFile>
#include <ClassTools>

#include "Container"
#include "ContainerPlugin"
#include "ContainerAccessManager"
#include "TOC"
#include "e5Globals"

IMPLEMENT_PLUGIN_LOADER(ContainerPlugin,Container,QUrl,container)

bool isLocalFile(QUrl const& url)
{
	return url.scheme().isEmpty() || url.scheme() == "file";
}


#ifdef NO_PLUGINS
#include "../plugins/container/filesystem/FilesystemContainerPlugin"
#include "../plugins/container/zipfile/ZipfileContainerPlugin"
void Container::initializeStaticPlugins() {
	_plugins.insert("filesystem", new FilesystemContainerPlugin);
	_plugins.insert("zipfile", new ZipfileContainerPlugin);
}
#else
void Container::initializeStaticPlugins() {}
#endif



Container::Container(QUrl const &url, QObject *parent):Metadata(url,parent),_toc(0),_tocLoaded(false) {
	ContainerAccessManager::addContainer(this);
}

Container::~Container() {
	destroy(_toc);
}

QStringList Container::files() const {
	QStringList unfilteredResult=originalFiles();
	QStringList result;
	foreach(QString const &r, unfilteredResult)
		if(!_removedFiles.contains(r))
			result << r;
	// virtualFiles and references are added after removedFiles filtering to
	// support the combination of deleting and re-adding
	for(QHash<QString,QByteArray>::ConstIterator it=_virtualFiles.begin(); it!=_virtualFiles.end(); it++)
		result << it.key();
	for(QHash<QString,QUrl>::ConstIterator it=_references.begin(); it!=_references.end(); it++)
		result << it.key();
	return result;
}


bool Container::contains(QString const &filename) {
	return files().contains(filename);
}


QIODevice* Container::open(QString const &filename) const {
		
	if(_removedFiles.contains(filename))
		return 0;
	static bool downloadingReference = false;
	if(_virtualFiles.contains(filename)) {
		QBuffer *b=new QBuffer;
		b->setData(_virtualFiles[filename]);
		b->open(QBuffer::ReadOnly);
		return b;
	} else if(_references.contains(filename)) {
		QUrl url = _references.value(filename);
		if(isLocalFile(url) || url.scheme() == "qrc") {
			QString path;
			if(url.scheme() == "qrc") {
				path = ":";
			}
			path += url.path();

			QFile *file = new QFile(path);
			if(!file->open(QIODevice::ReadOnly)) {
				delete file;
				return 0;
			}

			return file;
		} else if(!downloadingReference) {
			downloadingReference = true;
			QByteArray content = Downloader::get(url, ContainerAccessManager::instance());
			downloadingReference = false;
			ContainerAccessManager::deref();
			if(content.isNull()) {
				return 0;
			}
			QBuffer *b=new QBuffer;
			b->setData(content);
			b->open(QBuffer::ReadOnly);
			return b;
		}
	}
	return openOriginal(filename);
}

bool Container::add(QString const &fn, QByteArray const &content) 
{
	QString pr = fn;
	if( pr.contains( baseUrl() ) )
	{
		pr = pr.remove( baseUrl() );
	}
	
	if( pr.at(0) == '/' )
	{
		pr = pr.remove(0,1);
	}
	
	_virtualFiles.insert(pr, content);

	// Lukas: delete this file from the removed file list:
	if( _removedFiles.QList<QString>::contains(fn) )
	{
		_removedFiles.removeAt( _removedFiles.indexOf( fn ));
	}
	return true;
}

QString Container::add(QString const &prefix, QString const &suffix, QByteArray const &content) 
{
	QString generated;
	// Avoid name clashes
	do {
		generated = randomizedFileName( prefix, suffix );
	} while(contains(generated));

	if(Container::add( generated, content )) {
		return generated;
	}
	return QString();
}



bool Container::add(QString const &fn, QUrl const &url) 
{
	QString pr = fn;
	if( pr.contains( baseUrl() ) )
	{
		pr = pr.remove( baseUrl() );
	}
	
	if( pr.at(0) == '/' )
	{
		pr = pr.remove(0,1);
	}
	
	_references.insert(pr, url);
	
	// Lukas: delete this file from the removed file list:
	if( _removedFiles.QList<QString>::contains(fn) )
	{
		_removedFiles.removeAt( _removedFiles.indexOf( fn ));
	}
	return true;
}

bool Container::remove(QString const &fn) {
	if(_virtualFiles.contains(fn)) {
		_virtualFiles.erase(_virtualFiles.find(fn));
		return true;
	}
	if(_references.contains(fn)) {
		_references.erase(_references.find(fn));
		return true;
	}
	if(!files().contains(fn))
		return false;
	_removedFiles << fn;
	return true;
}

QByteArray Container::content(QString const &filename) const {
	QIODevice *dev=open(filename);
	if(!dev)
		return QByteArray();
	QByteArray ret=dev->readAll();
	dev->close();
	delete dev;
	return ret;
}

TOC *Container::toc() {
	if(!_toc)
		_toc=TOC::get(this);
	if(_toc && !_tocLoaded) {
		_tocLoaded=true;
		_toc->load();
	}
	return _toc;
}

QString Container::id() const {
	if(!_toc) {
		const_cast<Container*>(this)->_toc=TOC::get(const_cast<Container*>(this));
		for(int i=0; i<10; i++)
			QCoreApplication::instance()->processEvents();
	}
	if(!_toc) // Invalid file, so let's return something
		return Metadata::id();
	return _toc->id();
}

bool Container::isMetadata(QString const &fn) const {
	return const_cast<Container*>(this)->toc()->isMetadata(fn);
}

QString Container::mimeType(QString const &fn) const {
	return const_cast<Container*>(this)->toc()->mimeType(fn);
}

QString Container::baseUrl() const {
	return "e5internal://" + id() + "/";
}

QString Container::extract(QString const &fn) {
	QFile f(QDir::temp().absoluteFilePath(fn.section('/', -1)));
	if(f.open(QFile::WriteOnly)) {
		f.write(content(fn));
		f.close();
		return f.fileName();
	}
	return QString::null;
}


qint64 Container::size(QString const &filename) const
{
	if(_removedFiles.contains(filename)) {
		return -1;
	}

	if(_virtualFiles.contains(filename)) {
		return _virtualFiles[filename].length();
	}

	if(_references.contains(filename)) {
		QUrl url = _references.value(filename);
		if(isLocalFile(url)) {
			return QFileInfo(url.path()).size();
		} else if(url.scheme() == "qrc") {
			return QFileInfo(":" + url.path()).size();
		}
	}

	return sizeOriginal(filename);
}
