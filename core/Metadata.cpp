#include "Metadata"

#include "e5Logger"
#include "Downloader"
#include "TOC"
#include "ContainerAccessManager"

Metadata::Metadata(QUrl const& url, QObject *parent) :
	QObject(parent),
	_url(url)
{
}


QMultiHash<QString,QString> const& Metadata::metadata(bool inherit) const
{
	if(_metadata.isEmpty()) {
		Metadata const *m;
		if(inherit && (m=qobject_cast<Metadata*>(parent())))
			return m->_metadata;
	}
	return _metadata;
}


QString Metadata::value(QString const& keyword, bool inherit) const
{
	QString r=_metadata.value(keyword, QString::null);
	if(r.isEmpty() && inherit && parent() && parent()->inherits("Metadata"))
		return static_cast<Metadata*>(parent())->value(keyword);
	return r;
}


#ifndef __APPLE__
#define fallbackToParent(dc, func) \
QString md=_metadata.value(dc, QString::null); \
if(!md.isEmpty()) \
return md; \
Metadata const *m; \
if(inherit && (m=qobject_cast<Metadata const *>(parent()))) \
return m->func(); \
return QString::null
#else
// Workaround for qobject_cast not working across shared libraries
// in OS X
#define fallbackToParent(dc, func) \
QString md=_metadata.value(dc, QString::null); \
if(!md.isEmpty()) \
return md; \
if(inherit && parent() && parent()->inherits("Metadata")) \
return reinterpret_cast<Metadata const *>(parent())->func(); \
return QString::null
#endif


QString Metadata::title(bool inherit) const
{
	fallbackToParent("dc:title", title);
}


QString Metadata::subtitle(bool inherit) const
{
	fallbackToParent("e5:subtitle", subtitle);
}


QString Metadata::copyright(bool inherit) const
{
	fallbackToParent("dc:rights", copyright);
}


QString Metadata::publisher(bool inherit, bool fallbackToAuthor) const
{
	if(_metadata.contains("dc:publisher"))
		return _metadata.value("dc:publisher");
	Metadata const * const m=qobject_cast<Metadata const * const>(parent());
	if(inherit && m) {
		QString p=m->publisher(true, false);
		if(!p.isEmpty())
			return p;
	}
	if(fallbackToAuthor) {
		if(_metadata.contains("dc:creator"))
			return _metadata.value("dc:creator");
		if(inherit && m)
			return m->author(true, false);
	}
	return QString::null;
}


QString Metadata::author(bool inherit, bool fallbackToPublisher) const
{
	if(_metadata.contains("dc:creator"))
		return _metadata.value("dc:creator");
	Metadata const * const m=qobject_cast<Metadata const * const>(parent());
	if(inherit && m) {
		QString p=m->author(true, false);
		if(!p.isEmpty())
			return p;
	}
	if(fallbackToPublisher) {
		if(_metadata.contains("dc:publisher"))
			return _metadata.value("dc:publisher");
		if(inherit && m)
			return m->publisher(true, false);
	}
	return QString::null;
}


QString Metadata::authorEmail(bool inherit) const
{
	fallbackToParent("e5:authorEmail", authorEmail);
}


QString Metadata::photographerEmail(bool inherit) const
{
	fallbackToParent("e5:photographerEmail", photographerEmail);
}


QString Metadata::photographer(bool inherit) const
{
	fallbackToParent("e5:photographer", photographer);
}


QString Metadata::iconPath(bool inherit) const
{
	fallbackToParent("e5:icon", iconPath);
}


unsigned int Metadata::version(bool inherit) const
{
	// Can't use fallbackToParent because that would return QString::null
	Metadata const *m=this;
	while(m) {
		QString const v=m->_metadata.value("e5:version", QString::null);
		if(!v.isEmpty())
			return v.toUInt();
		m=inherit ? qobject_cast<Metadata const *>(m->parent()) : 0;
	}
	return 0;
}


QString Metadata::id() const
{
	if(_metadata.contains("dc:identifier"))
		return _metadata.value("dc:identifier");
	if(_url.isValid())
		return QString::number(qHash(_url));
	return QString::null;
}


QString Metadata::language(bool inherit) const
{
	fallbackToParent("dc:language", language);
}


QString Metadata::subject(bool inherit) const
{
	fallbackToParent("dc:subject", subject);
}


QString Metadata::description(bool inherit) const
{
	fallbackToParent("dc:description", description);
}


QString Metadata::type(bool inherit) const
{
	fallbackToParent("dc:type", type);
}


int Metadata::issue(bool inherit) const
{
	return value("e5:issue", inherit).toInt();
}


QDate Metadata::date(bool inherit) const
{
	// FIXME I'm pretty sure Dublin Core has date values as well
	// we should check for them and if one is generic enough,
	// deprecate e5:date
	return QDate::fromString(value("e5:date", inherit), Qt::ISODate);
}


QString Metadata::eventDate(QString const &event, bool inherit) const
{
	if(_eventDates.contains(event))
		return _eventDates.value(event);
	if(!inherit)
		return QString::null;
	if(Metadata const *m=qobject_cast<Metadata const *>(parent()))
		return m->eventDate(event, true);
	return QString::null;
}


QHash<QString,QString> const& Metadata::eventDates(bool inherit) const
{
	if(!_eventDates.isEmpty() || !inherit)
		return _eventDates;
	if(Metadata const *m=qobject_cast<Metadata const *>(parent()))
		return m->eventDates(true);
	return _eventDates;
}


QStringList Metadata::contentType(bool inherit) const
{
	QString s=value("e5:type", inherit);
	// Workaround for a typo in an old version of the spec that seems
	// to have propagated to various ncx files...
	// We don't need to search inside a list because the typo in the
	// spec was fixed before it started allowing multiple e5:types
	if(s == "advertisment")
		return QStringList() << "advertisement";
	return s.split(' ', QString::SkipEmptyParts);
}


QStringList Metadata::features(bool inherit) const
{
	return value("e5:features", inherit).split(' ');
}


QByteArray Metadata::icon(bool inherit) const
{
	QByteArray icon;
	if(_metadata.contains("e5:icon")) 
	{
		Container *container = 0;
		if(const TOCItem *item = dynamic_cast<const TOCItem*>(this)) {
			TOC *toc = item->toc();
			if(toc) {
				container = toc->container();
			}
		}

		QString urlStr = _metadata.value("e5:icon");
		if(!urlStr.contains(baseUrl())) {
			urlStr = baseUrl() + urlStr;
		}

		QUrl url(urlStr);

		// If the icon is in the container, we don't need the ContainerAccessManager overhead.
		// Read it directly from the container.
		if(container && (url.scheme().isEmpty() || url.scheme() == "e5internal")) {
			QString path = url.path();
			if(path.startsWith('/')) {
				path.remove(0, 1);
			}
			icon = container->content(path);

		} else {
			ContainerAccessManager *m = ContainerAccessManager::instance(0);
			icon = Downloader::get(url, m);
			m->deref(0);
		}
	}
	if(!icon.isEmpty() || !inherit)
		return icon;	
	if(Metadata const *m=qobject_cast<Metadata const *>(parent()))
	{
		return m->icon();
	}
	return QByteArray();
}


void Metadata::setIcon(QString iIconPath) 
{
	_metadata.remove("e5:icon");
	insert("e5:icon", iIconPath);
	emit metadataChanged();
}


QByteArray Metadata::cover(bool inherit) const
{
	QByteArray icon;
	if(_metadata.contains("e5:cover")) {
		QString url=_metadata.value("e5:cover");
		icon=Downloader::get(QUrl(url));
	}
	if(!icon.isEmpty() || !inherit)
		return icon;
	if(Metadata const *m=qobject_cast<Metadata const *>(parent()))
		return m->icon();
	return QByteArray();
}


QStringList Metadata::keywords(bool inherit, bool inheritIfNotEmpty) const
{
	QStringList keywords=_metadata.values("e5:keyword");
	if(Metadata const *m=qobject_cast<Metadata const *>(parent())) {
		if(inheritIfNotEmpty) {
			keywords << m->keywords(true, true);
		} else if(inherit && keywords.isEmpty())
			keywords << m->keywords(true, false);
	}
	return keywords;
}


QString Metadata::baseUrl() const
{
	QString id;
	QObject const *toc=this;
	while(toc && !qobject_cast<TOC const *>(toc))
		toc=toc->parent();
	if(TOC const *t=qobject_cast<TOC const *>(toc))
		return t->baseUrl();
	return QString::null;
}


void Metadata::insert(QString const& tag, QString const& value)
{
	_metadata.insert(tag, value);
	emit metadataChanged();
}


void Metadata::replace(QString const& tag, QString const& value)
{
	_metadata.replace(tag, value);
	emit metadataChanged();
}


void Metadata::setMetadata(QMultiHash<QString, QString> const& metadata)
{
	_metadata = metadata;
	emit metadataChanged();
}
