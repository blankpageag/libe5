#include "TOCItem"
#include "TOC"

TOC *TOCItem::toc() const {
	TOCItem const *e=this;
	do {
		if(TOC const *t=dynamic_cast<TOC const *>(e))
			return const_cast<TOC*>(t);
		e=e->parentEntry();
	} while(e);
	return 0;
}
