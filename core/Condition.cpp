#include "Condition"
#include "ConditionManager"

#include <Accelerometer>

Condition::Condition(QDomElement const &e, ConditionManager *parent):QObject(parent),_met(true),_element(e) {
	evaluate(true);
}

bool Condition::evaluate(bool initial) {
	bool old=_met;
	_met=true;
	QDomNodeList const &children=_element.childNodes();
	for(int i=0; i<children.count(); i++) {
		QDomElement e=children.at(i).toElement();
		if(!e.isNull()) {
			if(e.tagName() == "user") {
			} else if(e.tagName() == "orientation") {
				QString const &type=e.attribute("type").toLower();
				Accelerometer * const acc=0; //Accelerometer::instance();
				if(acc) {
					Accelerometer::Orientation const o=acc->orientation();
					if((type == "portrait"
#if defined(Q_OS_IPHONE) || defined(Q_OS_ANDROID) // FIXME At some point, there will probably be landscape Android devices...
					    || type == "default"
#else
					    || type == "notdefault"
#endif
					   ) && !(o & Accelerometer::Portrait))
						_met = false;
					if((type == "landscape"
#if defined(Q_OS_IPHONE) || defined(Q_OS_ANDROID) // FIXME At some point, there will probably be landscape Android devices...
					    || type == "notdefault"
#else
					    || type == "default"
#endif
					   ) && !(o & Accelerometer::Landscape))
						_met = false;
					connect(acc, SIGNAL(orientationChanged(Accelerometer::Orientation)), this, SLOT(evaluate()));
					acc->startTracking();
				} else {
					// No accelerometer - let's assume we're on a traditional computer
					if(type == "portrait" || type == "notdefault")
						_met = false;
				}
			}
		}
		if(!_met)
			break;
	}
	if(!initial && (_met != old))
		emit changed();
	return _met;
}
