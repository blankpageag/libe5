#include "e5Globals"


QString randomName(int digits)
{
	QString ran;
	int min = 0;
	int max = 10;
	static bool seeded = false;

	if(!seeded) {
		qsrand ( time(NULL) );
		seeded = true;
	}

	for( int i=0; i< digits; i++ ) {
		const float scale = qrand()/float(RAND_MAX);
		int rN=floor(min + scale*(max-min));
		ran = ran + QString::number(rN);
	}
	return ran;
	
}


QString randomizedFileName(QString prefix, QString suffix, int numberRandomDigits)
{	
	QString out = prefix + randomName( numberRandomDigits );
	if( !suffix.isEmpty() )
	{
		if( out.at( out.length() - 1) != '.' )
		{
			out = out + ".";
		}
		out = out + suffix;
	}	
	return out;
}
