#include "e5Logger"


#include "e5LoggingService"


e5Logger* e5Logger::_pInstance = NULL;;

e5Logger::e5Logger(): _sService( NULL )
{
}

void e5Logger::startLogging()
{ 
	// create a logging servcie class and register it to the logging qdebug service
	getInstance()->_sService = new e5LoggingService();
	qInstallMsgHandler( e5Logger::loggerOutputFunction );
}



e5Logger* e5Logger::getInstance()
{
	
	if( _pInstance == NULL )
	{
		_pInstance = new e5Logger();
	}
	
	return _pInstance;
	
}





void e5Logger::loggerOutputFunction(QtMsgType type, const char *msg)
{ 
	if( getInstance()->_sService != NULL )
	{	
		getInstance()->_sService->log( type, msg);
	}
	else 
	{
		qWarning( "LOGGING WARNING: e5 Logging service not started!\n Please correctly initiated the logging service using DEFINE_LOGGING() macro.\n Following log message was posted:" );
		qWarning( msg );
	}

}




