#include <QtPlugin>
#include <QXmlStreamReader>
#include "e5TemplateContentPlugin"
#include "e5TemplateContent"


#include "e5Logger"
#include <QIOStream>

#define _Extension_ "e5temp"

CONST uint8_t e5TemplateContentPlugin::priority(TOCEntry const * const tocEntry, QString const &filename) const {
	QString const &ext=filename.section('.', -1).toLower();
	if(ext == _Extension_) 
	{
		return 100;
	}
	return 0;
}

Content *e5TemplateContentPlugin::get(TOCEntry * const tocEntry, QString const &filename) const {
	return new e5TemplateContent(tocEntry, filename);
}

EXPORT_PLUGIN(indesignXMLContent, e5TemplateContentPlugin);
