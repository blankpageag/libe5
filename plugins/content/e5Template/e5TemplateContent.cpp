#include "e5TemplateContent"
#include <QXmlStreamReader>
#include <QDebug>
#include <QDir>


#include <e5Logger>
#include <TOC>
#include <TOCEntry>
#include <ContainerAccessManager>
#include <Downloader>

#include <grantlee/engine.h>
#include <grantlee/template.h>
#include <grantlee/context.h>
#include <grantlee/templateloader.h>
#include <grantlee/metatype.h>

#ifndef WITHOUT_TIDY
extern "C" {
#include <tidy/buffio.h>
#include <tidy/tidy.h>
}

#ifdef IPHONE
// Workaround for ****ing crApple disallowing to link against tidy
#define CRAPPLE_IS_REALLY_RETARDED
#define TidyBuffer TydiBuffer
#define TidyDoc TydiDoc
#define TidyXhtmlOut TydiXhtmlOut
#define TidyXmlDecl TydiXmlDecl
#define TidyMakeClean TydiMakeClean
#define TidyMark TydiMark
#define TidyWrapLen TydiWrapLen
#define tidyCreate tydiCreate
#define tidyBufInit tydiBufInit
#define tidyOptSetBool tydiOptSetBool
#define tidyOptSetInt tydiOptSetInt
#define tidySetCharEncoding tydiSetCharEncoding
#define tidySetErrorBuffer tydiSetErrorBuffer
#define tidyParseString tydiParseString
#define tidyCleanAndRepair tydiCleanAndRepair
#define tidyRunDiagnostics tydiRunDiagnostics
#define tidySaveBuffer tydiSaveBuffer
#define tidyBufFree tydiBufFree
#define tidyRelease tydiRelease
#endif
#endif


#define _E5_TEMPLATE_SRC_ATTRIBUTE_NAME_ "e5:src"


e5TemplateContent::e5TemplateContent(TOCEntry *tocEntry, QString const &filename):
Content(tocEntry, filename)
{
	setGenerated(true);
	Grantlee::MetaType::init();
	qRegisterMetaType< QList<QObject*> >("QList<QObject*>");
}

void e5TemplateContent::createHtml() 
{	
	if(!value( _E5_TEMPLATE_SRC_ATTRIBUTE_NAME_ ).isNull())
	{
		QString oldHTMLFile = _filename;
		_filename = value( _E5_TEMPLATE_SRC_ATTRIBUTE_NAME_ ); 
		_filename = _filename.remove( baseUrl() ); 
		// delete it
		_metadata.remove( _E5_TEMPLATE_SRC_ATTRIBUTE_NAME_ );
		TOCEntry* par = static_cast<TOCEntry*> (parent());
		Container* con = par->toc()->container();
		con->remove(oldHTMLFile);
	}

	//qDebug() << "Create content from template file: "<<_filename<<" (container id="<<toc()->container()->id()<<")";
	QIODevice *dev=toc()->container()->open(_filename);
	
	if(!dev)
	{
		qCritical() << "Could not open template file in template plugin: "<<_filename;
		return;
	}	
	
	QByteArray array = dev->readAll();
   Grantlee::Engine* mEngine = new Grantlee::Engine(this);
	Grantlee::Template myTemp = mEngine->newTemplate( array, "my template" );
	Grantlee::Context c;
	QObject* data = (QObject*) parent();
	c.insert("e5Content", QVariant::fromValue(static_cast<QObject*>(this)) );
	c.insert("e5TocEntry", QVariant::fromValue(data));
	c.insert("e5Toc", QVariant::fromValue(static_cast<QObject*>(toc())));
	QString sHtml = myTemp->render(&c);

	
	/*
	QString errorMsg;
	int errorLine, errorColumn;
	qDebug() << "Generated html:" << sHtml;
	if(!_html.setContent( sHtml, &errorMsg, &errorLine, &errorColumn)) {
		qWarning() << "Template produced invalid XML:" << errorMsg << "In line" << errorLine << ", column" << errorColumn;
#ifdef WITHOUT_TIDY
		qDebug() << "Might be auto-fixable if you had built with Tidy support";
		sHtml = "<html><body><p>" + tr("Invalid XHTML in template output: In line %1, column %2: %3").arg(QString::number(errorLine)).arg(QString::number(errorColumn())).arg(errorMsg) + "</p></body></html>";
#else
		TidyBuffer output, errors;
		TidyDoc tdoc = tidyCreate();
		tidyBufInit(&output);
		tidyBufInit(&errors);
		tidyOptSetBool(tdoc, TidyXhtmlOut, yes);
		tidyOptSetBool(tdoc, TidyXmlDecl, yes);
		tidyOptSetBool(tdoc, TidyMakeClean, yes);
		tidyOptSetBool(tdoc, TidyMark, no);
		tidyOptSetInt(tdoc, TidyWrapLen, -1);
		tidySetCharEncoding(tdoc, "utf8");
		tidySetErrorBuffer(tdoc, &errors);
		tidyParseString(tdoc, sHtml.toUtf8().data());
		tidyCleanAndRepair(tdoc);
		tidyRunDiagnostics(tdoc);
		tidySaveBuffer(tdoc, &output);
		sHtml = QString::fromUtf8(reinterpret_cast<char const * const>(output.bp));
#endif
		
	}
	*/
	_html.setContent(sHtml);
	setGenerated(true);	
}

void e5TemplateContent::setHtml(QString const &html) 
{
	if( isGenerated() )
	{
		if( value(_E5_TEMPLATE_SRC_ATTRIBUTE_NAME_).isNull() ) 
		{
			
			QFileInfo info( fileName() );
			QString basePath = baseUrl().section('/', 3);
			
			// store the generated file in the metadata:
			if(value(_E5_TEMPLATE_SRC_ATTRIBUTE_NAME_).isNull())
			{
				((Metadata*)this)->insert(_E5_TEMPLATE_SRC_ATTRIBUTE_NAME_, basePath + fileName());
			}
			else 
			{
				((Metadata*)this)->replace(_E5_TEMPLATE_SRC_ATTRIBUTE_NAME_, basePath + fileName());
			}
			
			QString htmlFileName = fileName();
			htmlFileName = htmlFileName.remove( info.suffix() );
			htmlFileName = htmlFileName + "xhtml";
			_filename = basePath + htmlFileName;
			
			TOCEntry* par = static_cast<TOCEntry*> (parent());
			Container* con = par->toc()->container();
			//qDebug() <<"Switch to html storage instead of generated content, create:"<<_filename;
			con->add(_filename,  html.toUtf8() );
			
			_generated = false;			
			_html.setContent(html);
		}
	}
	else 
	{
		_html.setContent(html);
	}
	
	
}



QString e5TemplateContent::source() const
{
	QString file;
	if( isGenerated() )
	{
		file  = fileName();
		if( !file.contains(baseUrl()))
		{
			file = baseUrl() + file;
		}
	}
	else if( _metadata.contains(_E5_TEMPLATE_SRC_ATTRIBUTE_NAME_) )
	{
		file = value( _E5_TEMPLATE_SRC_ATTRIBUTE_NAME_ );
	}
	
	ASSERT( !file.isEmpty() );
	
	QByteArray templateSource;
	ContainerAccessManager *m=ContainerAccessManager::instance(0);
	templateSource=Downloader::get(QUrl(file), m);
	m->deref(0);
	
	QString out( templateSource );
	return out;
}
