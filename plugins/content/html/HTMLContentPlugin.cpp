#include <QtPlugin>
#include <QXmlStreamReader>
#include <TOC>
#include "HTMLContentPlugin"
#include "HTMLContent"

CONST uint8_t HTMLContentPlugin::priority(TOCEntry const * const tocEntry, QString const &filename) const {
	QString const &ext=filename.section('.', -1).toLower();
	if(ext == "html" || ext == "xhtml" || ext == "htm" || ext == "xhtm")
		return 10;
	else if(ext == "xml") {
		// Can't tell without looking at the file...
		QIODevice *f=tocEntry->toc()->container()->open(filename);
		bool supported=false;
		if(f) {
			QXmlStreamReader r(f);
			r.readNextStartElement();
			if(r.name().toString() == "html")
				supported=true;
			delete f;
		}
		return supported ? 10 : 0;
	}
	return 0;
}

Content *HTMLContentPlugin::get(TOCEntry * const tocEntry, QString const &filename) const {
	return new HTMLContent(tocEntry, filename);
}

EXPORT_PLUGIN(htmlContent, HTMLContentPlugin);
