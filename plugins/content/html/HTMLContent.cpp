#include "HTMLContent"
#include <Container>
#include <TOC>
#include <QDomDocument>
#include <QDomElement>
#include <QDebug>

#ifndef WITHOUT_TIDY
extern "C" {
#include <tidy/buffio.h>
#include <tidy/tidy.h>
}

#ifdef IPHONE
// Workaround for ****ing crApple disallowing to link against tidy
#define CRAPPLE_IS_REALLY_RETARDED
#define TidyBuffer TydiBuffer
#define TidyDoc TydiDoc
#define TidyXhtmlOut TydiXhtmlOut
#define TidyXmlDecl TydiXmlDecl
#define TidyMakeClean TydiMakeClean
#define TidyMark TydiMark
#define TidyWrapLen TydiWrapLen
#define tidyCreate tydiCreate
#define tidyBufInit tydiBufInit
#define tidyOptSetBool tydiOptSetBool
#define tidyOptSetInt tydiOptSetInt
#define tidySetCharEncoding tydiSetCharEncoding
#define tidySetErrorBuffer tydiSetErrorBuffer
#define tidyParseString tydiParseString
#define tidyCleanAndRepair tydiCleanAndRepair
#define tidyRunDiagnostics tydiRunDiagnostics
#define tidySaveBuffer tydiSaveBuffer
#define tidyBufFree tydiBufFree
#define tidyRelease tydiRelease
#endif
#endif

// This is not needed anymore since we use a local HTTP server, so we can provide the videos
// directly as an HTTP stream.
// TODO Figure out on which platforms this is needed
// (it is needed when the URL to a video/audio tag is
// passed to a player that can't necessarily handle
// the URL type (e.g. e5internal://) instead of pointing
// the player at the content stream
//#define VIDEO_TAG_PASSES_URL_INSTEAD_OF_CONTENT

HTMLContent::HTMLContent(TOCEntry *toc, QString const &filename):Content(toc, filename) 
{	
}


void HTMLContent::extractMedia(QDomNodeList const &nodes) {
#ifdef VIDEO_TAG_PASSES_URL_INSTEAD_OF_CONTENT
	QUrl base=baseUrl();
	QUrl containerBase=base;
	containerBase.setPath("/");
	for(int i=0; i<nodes.count(); i++) {
		QString url=base.resolved(nodes.at(i).toElement().attribute("src")).toString();
		qDebug()<<"Import replace of: "<<url;
		if(url.startsWith(containerBase.toString())) {
			QString mediaURL=QUrl::fromLocalFile(toc()->container()->extract(url.mid(containerBase.toString().length()))).toString();
			mediaURL.replace(' ', "%20");
			// store the original url
			nodes.at(i).toElement().setAttribute("originalSrc", url);
			nodes.at(i).toElement().setAttribute("src", mediaURL);
		}
	}
#endif	
}

void HTMLContent::createHtml()
{
	_errors.clear();

	QIODevice *dev=toc()->container()->open(_filename);
	if(!dev) {
		qCritical() << "Could not open html file in html plugin: "<<_filename;
		_errors << QString(tr("Could not open html file in html plugin: %1")).arg(_filename);
		emit loadErrors(_errors);
		return;
	}

	QString error;
	int errorLine, errorColumn;
	if(!_html.setContent(dev, false, &error, &errorLine, &errorColumn)) {
		qDebug() << "Running " << _filename << " through tidy because it's not valid XHTML:" << endl
		     << "In line " << errorLine << ", column " << errorColumn << ": " << error;
#ifdef WITHOUT_TIDY
		qDebug() << "Might be auto-fixable if you had built with Tidy support";
		_errors << QString("Invalid XHTML: In line %1, column %2: %3").arg(QString::number(errorLine)).arg(QString::number(errorColumn)).arg(error);
#else
		// Not valid XHTML -- so we have to preprocess it
		dev->seek(0);
		TidyBuffer output;
		TidyBuffer errors;
		TidyDoc tdoc = tidyCreate();
		tidyBufInit(&output);
		tidyBufInit(&errors);
		tidyOptSetBool(tdoc, TidyXhtmlOut, yes);
		tidyOptSetBool(tdoc, TidyXmlDecl, yes);
		tidyOptSetBool(tdoc, TidyMakeClean, yes);
		tidyOptSetBool(tdoc, TidyMark, no);
		tidyOptSetInt(tdoc, TidyWrapLen, -1);
		tidySetCharEncoding(tdoc, "utf8");
		tidySetErrorBuffer(tdoc, &errors);
		tidyParseString(tdoc, dev->readAll());
		int cleanErrors = tidyCleanAndRepair(tdoc);
		int diagnosticErrors = tidyRunDiagnostics(tdoc);
		tidySaveBuffer(tdoc, &output);
		_html.setContent(QString::fromUtf8(reinterpret_cast<char const * const>(output.bp)));

		QString errorStr = QString::fromUtf8(reinterpret_cast<char const * const>(errors.bp));
		if(cleanErrors != 0 || diagnosticErrors != 0) {
			qDebug() << "TIDY WARNINGS/ERRORS:" << errorStr;
		}
		if(cleanErrors < 0 || cleanErrors == 2 || diagnosticErrors < 0 || diagnosticErrors == 2) {
			_errors << errorStr;
		}

		tidyBufFree(&output);
		tidyBufFree(&errors);
		tidyRelease(tdoc);
#endif
	}

	delete dev;

	if(_html.firstChild().isProcessingInstruction()) {
		QDomProcessingInstruction xml = _html.firstChild().toProcessingInstruction();
		if(LIKELY(xml.target() == "xml")) { // There just MIGHT be other processing instructions at the beginning...
			// Strictly speaking, we should parse the encoding, see if it's valid,
			// and if so, keep it.
			// Practically, everything should be utf-8 or compatible anyway, so
			// throwing away a bogus declaration (e.g. US-ASCII) doesn't hurt.
			// FIXME we may have to change this if we get CJK content
			_html.removeChild(xml);
		}
	}

	QDomNodeList htmlList = _html.elementsByTagName("html");
	QDomElement html;
	bool validHtml = true;
	if(htmlList.isEmpty()) {
		validHtml = false;
	} else {
		html = htmlList.at(0).toElement();
		if(html.elementsByTagName("body").isEmpty()) {
			validHtml = false;
		}
	}

	if(validHtml) {
/*		QDomElement base = _html.elementsByTagName("base").at(0).toElement();
		if(base.isNull()) {
			base = _html.createElement("base");
			QDomElement head = _html.elementsByTagName("head").at(0).toElement();
			if(head.isNull()) {
				head = _html.createElement("head");
				if(html.isNull()) {
					// Yuck... If the document doesn't conform to standards,
					// we can't conform either...
					_html.insertBefore(head, _html.firstChild());
				} else {
					html.insertBefore(head, html.firstChild());
				}
			}
			head.insertBefore(base, head.firstChild());
		}
		base.setAttribute("href", baseUrl());*/
		if(!html.hasAttribute("xmlns"))
			html.setAttribute("xmlns", "http://www.w3.org/1999/xhtml");

		extractMedia(_html.elementsByTagName("video"));
		extractMedia(_html.elementsByTagName("audio"));

	} else {
		_errors << QString(tr("%1 is not a valid (X)HTML document.")).arg(_filename);
	}

	if(!_errors.isEmpty()) {
		emit loadErrors(_errors);
	}
}


QStringList HTMLContent::errors() const
{
	return _errors;
}
