#include "ImageContent"
#include <TOC>
#include <QDomDocument>
#include <QDomElement>
#include <QFileInfo>

ImageContent::ImageContent(TOCEntry *toc, QString const &filename):Content(toc, filename + ".xhtml") {
	toc->toc()->container()->add(filename + ".xhtml", html().toUtf8());
	_metadata.insert("dc:title", QFileInfo(filename).baseName());
}

void ImageContent::createHtml() {
	QString pictureFile = _filename.section('/', baseUrl().count("/")-3);
	pictureFile=pictureFile.left(pictureFile.length()-6); // cut off .xhtml
	_html.setContent(
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		"<!DOCTYPE html>"
		"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			"<head>"
				"<title>Image</title>"
				"<style type=\"text/css\">\n"
					"body {"
						"margin: 0;"
						"padding: 0;"
					"}\n"
					"img {"
						"margin: 0;"
						"padding: 0;"
					"}\n"
				"</style>"
			"</head>"
			"<body>"
				"<div><img style=\"display: block; width: 100%\" src=\"" + pictureFile + "\" alt=\"" + pictureFile + "\"/></div>"
			"</body>"
		"</html>"
	);
}
