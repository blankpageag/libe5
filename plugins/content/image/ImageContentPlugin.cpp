#include <QtPlugin>
#include <QXmlStreamReader>
#include "ImageContentPlugin"
#include "ImageContent"

CONST uint8_t ImageContentPlugin::priority(TOCEntry const * const tocEntry, QString const &filename) const {
	QString const &ext=filename.section('.', -1).toLower();
	if(ext == "png" || ext == "jpeg" || ext == "jpg" || ext == "gif" || ext == "tif" || ext == "tiff" || ext == "jp2" || ext == "webp")
		return 10;
	return 0;
}

Content *ImageContentPlugin::get(TOCEntry * const tocEntry, QString const &filename) const {
	return new ImageContent(tocEntry, filename);
}

EXPORT_PLUGIN(imageContent, ImageContentPlugin);
