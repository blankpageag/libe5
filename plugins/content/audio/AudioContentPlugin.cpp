#include <QtPlugin>
#include <QXmlStreamReader>
#include "AudioContentPlugin"
#include "AudioContent"

CONST uint8_t AudioContentPlugin::priority(TOCEntry const * const tocEntry, QString const &filename) const {
	QString const &ext=filename.section('.', -1).toLower();
	if(ext == "ogg" || ext == "oga" || ext == "aac" || ext == "m4a" || ext == "mp3" || ext == "wav")
		return 10;
	return 0;
}

Content *AudioContentPlugin::get(TOCEntry * const tocEntry, QString const &filename) const {
	return new AudioContent(tocEntry, filename);
}

EXPORT_PLUGIN(audioContent, AudioContentPlugin);
