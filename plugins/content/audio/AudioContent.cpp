#include "AudioContent"
#include <TOC>
#include <QDomDocument>
#include <QDomElement>
#include <QFileInfo>

AudioContent::AudioContent(TOCEntry *toc, QString const &filename):Content(toc, filename + ".xhtml") {
	toc->toc()->container()->add(filename + ".xhtml", html().toUtf8());
	_metadata.insert("dc:title", QFileInfo(filename).baseName());
}

void AudioContent::createHtml() {
	QString audioFile = _filename.section('/', baseUrl().count("/")-3);
	audioFile=audioFile.left(audioFile.length()-6); // cut off .xhtml
	_html.setContent(
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		"<!DOCTYPE html>"
		"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			"<head>"
				"<title>Audio</title>"
			"</head>"
			"<body>"
				"<div style=\"padding: 0; margin: 0;\"><audio src=\"" + audioFile + "\" controls=\"controls\" autoplay=\"autoplay\"/></div>"
			"</body>"
		"</html>"
	);
}
