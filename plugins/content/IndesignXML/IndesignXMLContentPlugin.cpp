#include <TOC>
#include <QtPlugin>
#include <QXmlStreamReader>
#include "IndesignXMLContentPlugin"
#include "IndesignXMLContent"

CONST uint8_t IndesignXMLContentPlugin::priority(TOCEntry const * const tocEntry, QString const &filename) const {
	QString const &ext=filename.section('.', -1).toLower();
	if(ext == "xml") {
		QIODevice *f=tocEntry->toc()->container()->open(filename);
		bool supported=false;
		if(f) {
			QXmlStreamReader r(f);
			r.readNextStartElement();
			if(r.name().toString() == "Root")
				supported=true;
			delete f;
		}
		return supported ? 100 : 0;
	}
	return 0;
}

Content *IndesignXMLContentPlugin::get(TOCEntry * const tocEntry, QString const &filename) const {
	return new IndesignXMLContent(tocEntry, filename);
}

EXPORT_PLUGIN(indesignXMLContent, IndesignXMLContentPlugin);
