#include "IndesignXMLContent"
#include <TOC>
#include <QXmlStreamReader>

void IndesignXMLContent::toHtml(QString &s, bool firstParagraph) const {
	s.replace(QChar::LineSeparator, "<br/>").replace('\n', "<br/>");
	if(s.contains(QChar::ParagraphSeparator)) {
		QStringList l=s.split(QChar::ParagraphSeparator, QString::SkipEmptyParts);
		s=QString::null;
		foreach(QString p, l) {
			if(firstParagraph) {
				s += "<p class=\"first\" id=\"p" + QString::number(++_attributeCount["p"]) + "\">" + p + "</p>";
				firstParagraph=false;
			} else
				s += "<p id=\"p" + QString::number(++_attributeCount["p"]) + "\">" + p + "</p>";
		}
	}
}

static QString attributes(QXmlStreamAttributes const &attr, QStringList const &omit=QStringList()) {
	QString result=QString::null;
	foreach(QXmlStreamAttribute const &a, attr) {
		QString attrib=a.name().toString();
		if(omit.contains(attrib))
			continue;
		QString value=a.value().toString();
		result += QString(" ") + attrib + "=\"" + value.replace('"', "&quot;") + "\"";
	}
	return result;
}

IndesignXMLContent::IndesignXMLContent(TOCEntry *tocEntry, QString const &filename):Content(tocEntry, filename) {
}

void IndesignXMLContent::createHtml() {
	_attributeCount.clear();
	QIODevice *f=toc()->container()->open(_filename);
	QString HTML;
	if(f) {
		QString base=toc()->container()->url().toString();
		if(!base.contains(":/"))
		{
			base = "file:" + base;
		}
		if(!base.endsWith("/"))
		{
			base += "/";
		}
		
		HTML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			"<!DOCTYPE html>"
			"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
				"<head>"
					"<base href=\"" + base + "\"/>"
					"<link rel=\"stylesheet\" type=\"text/css\" href=\"file:" PLUGINDATA "/content/IndesignXML/style.css\"/>";
		
		// Load any CSS stylesheets dumped into the directory structure...
		QStringList otherFiles=toc()->container()->files();
		foreach(QString const &file, otherFiles)
		{
			if(file.toLower().endsWith(".css"))
			{
					HTML += "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + file + "\"/>";
			}
		}
		
		HTML += "</head>";
		
		// Convert the tags to HTML
		QXmlStreamReader r(f);
		bool firstParagraph=false;
		bool firstTextTag=true;
		while(!r.atEnd() && !r.hasError()) {
			switch(r.tokenType()) {
				case QXmlStreamReader::StartElement: {
					QXmlStreamAttributes attr=r.attributes();
					if(r.name() == "Root")
						HTML += "<body" + attributes(attr) + ">";
					else if(r.name() == "Title") {
						QString title=r.readElementText();
						if(!title.isEmpty()) {
							toHtml(title);
							//_metadata.insert("dc:title", title);
							if(!attr.hasAttribute("id"))
								attr.append("id", "title" + QString::number(++_attributeCount["title"]));
							HTML += "<h1 class=\"title\"" + attributes(attr) + ">" + title + "</h1>";
						}
					} else if(r.name() == "Subtitle") {
						QString title=r.readElementText();
						if(!title.isEmpty()) {
							toHtml(title);
								//_metadata.insert("dc:title", title);
							HTML += "<h2 class=\"subtitle\"" + attributes(attr) + ">" + title + "</h2>";
						}
					} else if(r.name() == "Author") {
						QString author=r.readElementText();
						if(!author.isEmpty()) {
							toHtml(author);
							//_metadata.insert("dc:creator", author);
							if(!attr.hasAttribute("id"))
								attr.append("id", "author" + QString::number(++_attributeCount["author"]));
							HTML += "<h2 class=\"author\"" + attributes(attr) + ">" + author + "</h2>";
						}
					} else if(r.name() == "authorEmail") {
						QString authorEmail=r.readElementText();
						if(!authorEmail.isEmpty()) {
							if(!attr.hasAttribute("id"))
								attr.append("id", "authorEmail" + QString::number(++_attributeCount["authorEmail"]));
							HTML += "<div class=\"addresses\"" + attributes(attr) + ">" + authorEmail + "</div>";
							if(firstTextTag) {
								firstParagraph=true;
								firstTextTag=false;
							} else
								firstParagraph=false;
						}
					} else if(r.name() == "Heading") {
						QString heading = r.readElementText();
						//toHtml(heading);
						
						HTML += "<p class=\"heading\"" + attributes(attr) + ">" + heading + "</p>";
					} else if(r.name() == "Bild") {
						QString frameAttributes=attributes(attr, QStringList() << "href" << "width" << "height");
						if(frameAttributes.contains(" class=\""))
							frameAttributes.replace(" class=\"", "class=\"imageframe ");
						else
							frameAttributes+=" class=\"imageframe\"";

						if(!frameAttributes.contains(" id=\""))
							frameAttributes += " id=\"imageframe" + QString::number(++_attributeCount["imageframe"]) + "\"";

						if(!attr.hasAttribute("id"))
							attr.append("id", "image" + QString::number(++_attributeCount["image"]));
						HTML += "<div " + frameAttributes + "><img src=\"";
						QString img=attr.value("href").toString();
						// Indesign likes encoding images as absolute file names
						// when it really means relative ones
						if(img.startsWith("file:"))
							img=img.mid(6);
						while(img.startsWith("/"))
							img=img.mid(1);
						HTML += img;
						HTML += "\"" + attributes(attr, QStringList() << "href") + "/></div>";
						// Anything contained inside Bild is bogus --> skip it
						r.readElementText();
					} else if(r.name() == "Text" || r.name() == "Textabschnitt") {
						if(!attr.hasAttribute("id"))
							attr.append("id", "text" + QString::number(++_attributeCount["text"]));
						HTML += "<div class=\"text\"" + attributes(attr) + ">";
						if(firstTextTag) {
							firstParagraph=true;
							firstTextTag=false;
						} else
							firstParagraph=false;
					} else {
						QString const tag=r.name().toString();
						if(!attr.hasAttribute("id"))
							attr.append("id", tag + QString::number(++_attributeCount[tag]));
						HTML += "<span class=\"" + tag + "\" " + attributes(attr) + ">";
					}
					break;
				}
				case QXmlStreamReader::EndElement: {
					if(r.name() == "Root")
						HTML += "</body>";
					else if(r.name() == "Text" || r.name() == "Textabschnitt")
						HTML += "</div>";
					else
						HTML += "</span>";
					break;
				}
				case QXmlStreamReader::Characters: {
					QString text=r.text().toString();
					toHtml(text, firstParagraph);
					HTML += text;
					break;
				}}	
			r.readNext();
		}
		if(r.hasError()) {
			HTML += "<h3>Error: " + r.errorString() + "</h3>";
		}
		HTML += "</html>";
		delete f;
	}
	_html.setContent(HTML);
}
