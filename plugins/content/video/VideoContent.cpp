#include "VideoContent"
#include <TOC>
#include <QDomDocument>
#include <QDomElement>
#include <QFileInfo>

VideoContent::VideoContent(TOCEntry *toc, QString const &filename):Content(toc, filename + ".xhtml") {
	toc->toc()->container()->add(filename + ".xhtml", html().toUtf8());
	_metadata.insert("dc:title", QFileInfo(filename).baseName());
}

void VideoContent::createHtml() {
	QString videoFile = _filename.section('/', baseUrl().count("/")-3);
	videoFile=videoFile.left(videoFile.length()-6); // cut off .xhtml
	_html.setContent(
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		"<!DOCTYPE html>"
		"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			"<head>"
				"<title>Video</title>"
			"</head>"
			"<body>"
				"<div style=\"padding: 0; margin: 0;\"><video src=\"" + videoFile + "\" controls=\"controls\" autoplay=\"autoplay\"/></div>"
			"</body>"
		"</html>"
	);
}
