#include <QtPlugin>
#include <QXmlStreamReader>
#include "VideoContentPlugin"
#include "VideoContent"

CONST uint8_t VideoContentPlugin::priority(TOCEntry const * const tocEntry, QString const &filename) const {
	QString const &ext=filename.section('.', -1).toLower();
	if(ext == "avi" || ext == "mpg" || ext == "mpeg" || ext == "ogv" || ext == "ogt" || ext == "ogm" || ext == "mp4" || ext == "m4v"
	   || ext == "webm" || ext == "wmv" || ext == "mov")
		return 10;
	return 0;
}

Content *VideoContentPlugin::get(TOCEntry * const tocEntry, QString const &filename) const {
	return new VideoContent(tocEntry, filename);
}

EXPORT_PLUGIN(videoContent, VideoContentPlugin);
