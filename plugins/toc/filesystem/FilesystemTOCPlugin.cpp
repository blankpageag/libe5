#include <QtPlugin>
#include "FilesystemTOCPlugin"
#include "FilesystemTOC"

CONST uint8_t FilesystemTOCPlugin::priority(Container const * const container) const {
	if(container && !container->files().isEmpty())
		return 1;
	return 0;
}

TOC *FilesystemTOCPlugin::get(Container * const container) const {
	return new FilesystemTOC(container);
}

EXPORT_PLUGIN(filesystemTOC, FilesystemTOCPlugin);
