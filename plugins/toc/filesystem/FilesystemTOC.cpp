#include "FilesystemTOC"
#include "Content"

FilesystemTOC::FilesystemTOC(Container * const container):TOC(container) {
}

void FilesystemTOC::load() {
	foreach(QString const &f, container()->files()) {
		TOCEntry *n=new TOCEntry(this, f);
		Content * const c=Content::get(n, f);
		if(c) {
			n->append(c);
			append(n);
		} else
			delete n;
	}
}


