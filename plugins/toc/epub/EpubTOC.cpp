#include <QDomDocument>
#include <QDomNodeList>
#include <ConditionManager>
#include <QDebug>
#include "EpubTOC"
#include "Content"

EpubTOC::EpubTOC(Container * const container):TOC(container) {
	if(!container)
		return;
	
	QIODevice *cont=container->open("META-INF/container.xml");
	if(!cont)
		return;
	
	QStringList const files=container->files();
	
	QDomDocument c;
	c.setContent(cont);
	
	QDomNodeList items=c.elementsByTagName("rootfile");
	for(int i=0; i<items.count(); i++) {
		QDomNamedNodeMap const attr=items.at(i).attributes();
		if(attr.namedItem("media-type").nodeValue() == "application/oebps-package+xml") {
			_opfFileName=attr.namedItem("full-path").nodeValue();
			if(_opfFileName.contains('/')) {
				_root = _opfFileName.section('/', 0, -2);
				_opfFileName = _opfFileName.section('/', -1);
			}
		}
	}
	delete cont;
	if(!_root.isEmpty() && !_root.endsWith('/'))
		_root += "/";
	
	if(_opfFileName.isEmpty()) {
		if(files.contains("content.opf")) {
			// Let's hope it follows the pseudostandard if it
			// doesn't follow the standard...
			_opfFileName = "content.opf";
		} else {
			// Let's see if we can find any opf file at all...
			foreach(QString const &f, files) {
				if(f.toLower().endsWith(".opf")) {
					_opfFileName=f;
					break;
				}
			}
		}
	}

	if(_opfFileName.isEmpty())
		return;

	QIODevice *opfFile=container->open(_root+_opfFileName);
	if(!opfFile)
		return;

	QDomDocument opf;
	opf.setContent(opfFile);

	items=opf.elementsByTagName("item");
	for(int i=0; i<items.count(); i++) {
		QDomNode const item=items.at(i);
		if(item.hasAttributes()) {
			QDomNamedNodeMap const attr=item.attributes();
			if(!attr.contains("media-type") || !attr.contains("href"))
				continue; // bogus entry
			if(attr.namedItem("media-type").nodeValue() == "application/x-dtbncx+xml" || attr.namedItem("href").nodeValue().endsWith(".ncx")) { // The latter case is a workaround for some crappy epub generators listing the ncx as an xhtml file even though it's not
				_ncxFileName=attr.namedItem("href").nodeValue();
			}
			if(attr.namedItem("media-type").nodeValue() != "application/octet-stream") // some broken editors mark CSS and JS files as octet-stream. For genuine application/octet-streams, we fall back to it anyway
				_mimeTypes.insert(_root + attr.namedItem("href").nodeValue(), attr.namedItem("media-type").nodeValue());
		}
	}
	if(_ncxFileName.startsWith('/'))
		_ncxFileName=_ncxFileName.mid(1);

	if(_ncxFileName.isEmpty() || !files.contains(_ncxFileName)) {
		if(files.contains("toc.ncx")) {
			_ncxFileName="toc.ncx"; // Pseudostandard...
		} else {
			// Let's see if there's any NCX at all
			foreach(QString const &f, files) {
				if(f.toLower().endsWith(".ncx")) {
					_ncxFileName=f;
					break;
				}
			}
		}
	}

	QDomNodeList const metadata=opf.elementsByTagName("metadata").at(0).childNodes();
	for(int i=0; i<metadata.count(); i++) {
		QDomElement const n=metadata.at(i).toElement();
		QString const tag=n.tagName().toLower();
		if(tag == "dc:identifier")
			_metadata.insert(tag, n.text());
	}

	if(!files.contains(_ncxFileName) && files.contains(_root+_ncxFileName))
		_ncxFileName=_root+_ncxFileName;
		
	if(!_metadata.contains("dc:identifier")) {
		// Let's look in the NCX header too...
		QIODevice *tocFile=container->open(_ncxFileName);
		if(!tocFile)
			return;
	
		QDomDocument toc;
		toc.setContent(tocFile);
		
		QDomNodeList const head=toc.elementsByTagName("head");
		if(!head.isEmpty()) {
			QDomNodeList const meta=head.at(0).childNodes();
			for(int i=0; i<meta.count(); i++) {
				QDomElement const e=meta.at(i).toElement();
				QString content, name;
				
				if(e.tagName().toLower() == "meta") {   
					name = e.attributes().namedItem("name").nodeValue();
					content = e.attributes().namedItem("content").nodeValue();
				}
				
				if(e.tagName().startsWith("dc:")) {   
					name = e.tagName();
					content = e.attributes().namedItem("content").nodeValue();
				}
				
				if(name=="dc:identifier")
					_metadata.insert(name, content);
			}
		}
		delete tocFile;
	}

	delete opfFile;
}

void EpubTOC::load() {
	QStringList const files=container()->files();
	if(!_opfFileName.isEmpty()) {
		// Parse OPF file for information beyond the ID
		QIODevice *opfFile=container()->open(_root+_opfFileName);
		if(!opfFile)
			return;
		QDomDocument opf;
		opf.setContent(opfFile);
		
		QDomNodeList const childNodes=opf.elementsByTagName("metadata").at(0).childNodes();
		for(int i=0; i<childNodes.count(); i++) {
			QDomElement const n=childNodes.at(i).toElement();
			QString const tag=n.tagName().toLower();
			if(tag == "dc:date") {
				// Special handling required because of extra attributes...
				QString event;
				if(n.hasAttribute("opf:event"))
					event=n.attribute("opf:event");
				else if(n.hasAttribute("xsi:type"))
					event=n.attribute("xsi:type");
				if(!event.isEmpty())
					_eventDates.insert(event, n.text());
			} 
			else if(tag.startsWith("e5:icon"))
			{
				QString v = QUrl(baseUrl()).resolved( n.attributes().namedItem("src").nodeValue() ).toString();
				v = v.remove( baseUrl() );
				_metadata.replace(tag, v );
			}
			else if((tag.startsWith("dc:") && tag != "dc:identifier") || tag.startsWith("e5:"))
			{
				_metadata.insert(tag, n.text());
			}
		}
		
		// No NCX -- let's fall back to parsing the spine
		if(!files.contains(_ncxFileName)) {
			QDomNodeList const spines=opf.elementsByTagName("spine");
			if(spines.count()) {
				QDomNodeList refs=spines.at(0).childNodes();
				for(int i=0; i<refs.count(); i++) {
					QDomElement const e=refs.at(i).toElement();
					if(e.tagName() != "itemref")
						continue;
					QString id=e.attributes().namedItem("idref").nodeValue();
					if(id.isEmpty())
						continue;
					
					// Find the corresponding filename and add it
					// to the chapter list...
					QDomNodeList items=opf.elementsByTagName("item");
					for(int j=0; j<items.count(); j++) {
						QDomNamedNodeMap const attr=items.at(j).attributes();
						QString const type=attr.namedItem("media-type").nodeValue();
						if(attr.namedItem("id").nodeValue() == id && (type.isEmpty() || type=="application/xhtml+xml" || type=="text/html" || type=="text/plain")) {
							TOCEntry *n=new TOCEntry(this, id);
							Content * const c=Content::get(n, _root + attr.namedItem("href").nodeValue());
							if(c) {
								n->append(c);
								append(n);
							} else
								delete n;
							break;
						}
					}
				}
			}
			
			// We're done here, nothing of interest to do...
			delete opfFile;
			return;
		}
		delete opfFile;
	}
	
	
	QIODevice *tocFile=container()->open(_ncxFileName);
	if(!tocFile) {
		// FIXME we should probably fall back to parsing the spine if
		// opening the ncx fails
		return;
	}
	
	/* Parse NCX */ {
		QDomDocument toc;
		toc.setContent(tocFile);
		
		// Conditions...
		QDomNodeList const conditions=toc.elementsByTagName("e5:condition");
		if(!conditions.isEmpty()) {
			for(int i=0; i<conditions.count(); i++) {
				_conditionManager.addCondition(conditions.at(i).toElement());
			}
		}
		
		// Look for extra metadata...
		QDomNodeList const head=toc.elementsByTagName("head");
		if(!head.isEmpty()) {
			QDomNodeList const meta=head.at(0).childNodes();
			for(int i=0; i<meta.count(); i++) {
				QDomElement const e=meta.at(i).toElement();
				
				QString content, name;
				
				if( e.tagName().toLower() == "meta") 
				{   
					name =e.attributes().namedItem("name").nodeValue();
					content =e.attributes().namedItem("content").nodeValue();
				}
				
				if( e.tagName().contains("e5:") ) 
				{   
					name =e.tagName();
					content =e.attributes().namedItem("content").nodeValue();
				}
				
				if(content.isEmpty())
				{
					content= QUrl(baseUrl()).resolved(e.attributes().namedItem("src").nodeValue()).toString(); // workaround for Das Magazin issues <= 2011.1 mistakenly using <meta name="e5:icon" src="file"/>
					content = content.remove(baseUrl() );
				}
				
				if(_metadata.contains(name)) {
					if(_metadata.contains(name, content)) {
						// No need to add duplicated metadata (present in both OPF and NCX)
						continue;
					}
				}	
				
				_metadata.insert(name, content);
				
			}
		}
		
		// Look for per-Article metadata...
		QDomNodeList const navMap=toc.elementsByTagName("navMap");
		int fakeID = 0;
		if(!navMap.isEmpty()) {
			QDomNodeList const navmapEntries=navMap.at(0).childNodes();
			for(int i=0; i<navmapEntries.count(); i++) {
				QDomElement const e=navmapEntries.at(i).toElement();
				if(e.tagName() == "navPoint") {
					QString id=e.attributes().namedItem("id").nodeValue();
					if(id.isEmpty()) {
						id="__e5_navPoint_" + QString::number(++fakeID);
					}
					TOCEntry *tocEntry=new TOCEntry(this, id);
					QMultiHash<QString,QString> &metadata=const_cast<QMultiHash<QString,QString>&>(tocEntry->metadata(false));
					if(e.hasAttribute("e5:type"))
						metadata.insert("e5:type", e.attributes().namedItem("e5:type").nodeValue());
					if(e.hasAttribute("e5:features"))
						metadata.insert("e5:features", e.attributes().namedItem("e5:features").nodeValue());
					QDomNodeList const meta=e.elementsByTagName("meta");
					for(int i=0; i<meta.count(); i++) {
						QString const name=meta.at(i).attributes().namedItem("name").nodeValue();
						if(name == "dc:date") {
							// Special handling required because of extra attributes...
							QString event;
							if(meta.at(i).attributes().contains("opf:event"))
								event=meta.at(i).attributes().namedItem("opf:event").nodeValue();
							else if(meta.at(i).attributes().contains("xsi:type"))
								event=meta.at(i).attributes().namedItem("xsi:type").nodeValue();
							if(!event.isEmpty())
								_eventDates.insert(event, meta.at(i).attributes().namedItem("content").nodeValue());
						} else if(name.startsWith("dc:") || name.startsWith("e5:")) {
							metadata.remove(name);
							metadata.insert(name, meta.at(i).attributes().namedItem("content").nodeValue());
						}
					}
					if(!metadata.contains("dc:title")) {
						// Grab the title from navLabel...
						QDomNodeList const navLabels=e.elementsByTagName("navLabel");
						for(int i=0; i<navLabels.count(); i++) {
							QDomNodeList texts=navLabels.at(i).toElement().elementsByTagName("text");
							if(!texts.count())
								continue;
							metadata.insert("dc:title", texts.at(i).toElement().text());
						}
					}
					if(!metadata.contains("e5:icon")) {
						QDomNodeList const e5Icons=e.elementsByTagName("e5:icon");
						for(int i=0; i<e5Icons.count(); i++) {
							//QString v = QUrl(baseUrl()).resolved(e5Icons.at(i).attributes().namedItem("src").nodeValue()).toString();
							QString src = e5Icons.at(i).attributes().namedItem("src").nodeValue();
							if( src.contains( baseUrl() ) )
							{
								src = src.remove(baseUrl() );
							}
							metadata.replace("e5:icon", src);
							// FIXME handle different icon types instead of break-ing
							break;
						}
					}
					
					if(!metadata.contains("e5:tocImage")) {
						QDomNodeList const e5Icons=e.elementsByTagName("e5:tocImage");
						for(int i=0; i<e5Icons.count(); i++) 
						{
							QString src = e5Icons.at(i).attributes().namedItem("src").nodeValue();
							if( src.contains( baseUrl() ) )
							{
								src = src.remove(baseUrl() );
							}
							metadata.replace("e5:tocImage", src);
							// FIXME handle different icon types instead of break-ing
							break;
						}
					}
					
					
					QDomNodeList const contents=e.elementsByTagName("content");
					bool foundContent=false;
					for(int i=0; i<contents.count(); i++) {
						QDomElement const el=contents.at(i).toElement();
						QString file=el.attributes().namedItem("src").nodeValue();
						if(file.contains('#')) {
							// Some epubs (e.g. CUP 9780511653421) refer to an anchor inside a HTML file
							// in their NCX, e.g. 9780511653421_05_chp1.html#chp1
							file=file.section('#', 0, -2);
							// FIXME we lose the information of the anchor here -- if it turns out to be relevant
							// anywhere (in the CUP books, it just points to the top of the page anyway, and so
							// far we haven't come across any other usage of anchors in navmaps), we need to
							// store it somewhere and make use of it
						}
						Content *c=Content::get(tocEntry, _root + file);
						if(c) {
							
							// insert e5:src attribute:
							QString e5SRCfile=el.attributes().namedItem("e5:src").nodeValue();
							if( !e5SRCfile.isEmpty() )
							{
								c->insert("e5:src", e5SRCfile);
								qDebug()<<c->value("e5:src");
								qDebug()<<tocEntry->title();
							}
							tocEntry->append(c, el.attributes().namedItem("e5:condition").nodeValue().split(' '));
							foundContent=true;
							
							
						}
					}
					
					
                    append(tocEntry);
                    /* 
                     * Allow to open empty tocs
					if(foundContent)
						append(tocEntry);
					else
						delete tocEntry;
					*/
				}
			}
		}
		
	}
	
	delete tocFile;

	// We don't need those anymore -- so let's free up some RAM
	// even if it's just a couple of bytes
	_opfFileName.clear();
	_ncxFileName.clear();
}

bool EpubTOC::isMetadata(QString const &fn) const {
	QString const mt=mimeType(fn);
	if(mt == "application/x-dtbncx+xml" || mt == "application/oebps-package+xml")
		return true;
	if(fn.toLower().section('/', -1) == "mimetype")
		return true;
	if(fn.toLower().startsWith("meta-inf/"))
		return true;
	return TOC::isMetadata(fn);
}

QString EpubTOC::mimeType(QString const &fn) const {
	if(_mimeTypes.contains(fn))
		return _mimeTypes.value(fn);
	return TOC::mimeType(fn);
}
