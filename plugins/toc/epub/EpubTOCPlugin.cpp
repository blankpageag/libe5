#include <QtPlugin>
#include "EpubTOCPlugin"
#include "EpubTOC"

CONST uint8_t EpubTOCPlugin::priority(Container const * const container) const {
	// Presence of META-INF/container.xml should be a good enough
	// hint that it's an epub file...
	// If other formats using the same name come across, we need
	// to go into more detail here and check for the opf and ncx as well
	if(container && container->files().contains("META-INF/container.xml"))
		return 10;
	return 0;
}

TOC *EpubTOCPlugin::get(Container * const container) const {
	return new EpubTOC(container);
}

EXPORT_PLUGIN(epubTOC, EpubTOCPlugin);
