#include <QtPlugin>
#include "HTMLTOCPlugin"
#include "HTMLTOC"

CONST uint8_t HtmlTOCPlugin::priority(Container const * const container) const
{
	QStringList files = container->files();
	if(!container || files.isEmpty()) {
		return 0;
	}

	if(files.contains("toc.ncx") || files.contains("META-INF/container.xml")) {
		return 0;
	}

	foreach(QString const& file, files) {
		if(HtmlTOC::isHtmlFile(file)) {
			return 20;
		}
	}

	return 0;
}


TOC* HtmlTOCPlugin::get(Container * const container) const
{
	return new HtmlTOC(container);
}

EXPORT_PLUGIN(htmlTOC, HtmlTOCPlugin);
