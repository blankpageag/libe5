#include <QtXml>
#include "HTMLTOC"
#include "Content"

HtmlTOC::HtmlTOC(Container * const container) :
	TOC(container)
{
}


void HtmlTOC::load()
{
	QStringList files = container()->files();
	foreach(QString const& file, files) {
		if(!isHtmlFile(file)) {
			continue;
		}

		TOCEntry *entry = new TOCEntry(this, file);
		Content * const content = Content::get(entry, file);
		if(content) {
			QDomElement head = content->dom().elementsByTagName("head").at(0).toElement();
			QDomElement title = head.elementsByTagName("title").at(0).toElement();
			if(!title.isNull()) {
				static_cast<Metadata*>(entry)->insert("dc:title", title.text());
				content->insert("dc:title", title.text());
			}

			QDomNodeList metaTags = head.elementsByTagName("meta");
			for(int i = 0; i < metaTags.count(); i++) {
				QDomElement metaTag = metaTags.at(i).toElement();
				if(!metaTag.isNull()) {
					QString attribute = metaTag.attribute("name");
					QString value = metaTag.attribute("content");
					if(!attribute.isEmpty() && !value.isEmpty()) {
						content->insert(attribute, value);
					}
				}
			}

			entry->append(content);
			append(entry);
		} else
			delete entry;
	}
}


bool HtmlTOC::isHtmlFile(QString const& file)
{
	return QRegExp("\\.(xhtml|html?)$", Qt::CaseInsensitive).indexIn(file) != -1;
}
