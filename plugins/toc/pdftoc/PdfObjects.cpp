#include <QtCore>
#include "PdfObjects.h"

PdfItem::PdfItem(PdfItem *parent) :
	_parent(parent)
{
}


PdfPage* PdfItem::page() const
{
	PdfItem *item = const_cast<PdfItem*>(this);
	do {
		if(item->type() == Page) {
			return dynamic_cast<PdfPage*>(item);
		}
		item = item->parent();
	} while(item != 0);

	return 0;
}


QRectF PdfItem::relativeBoundingRect() const
{
	PdfPage *p = page();
	if(!p) {
		return QRectF();
	}

	QRectF pageRect = p->boundingRect();
	if(pageRect.isEmpty()) {
		return QRectF();
	}

	return QRectF( (_boundingRect.x() - pageRect.x()) / pageRect.width(),
				   (_boundingRect.y() - pageRect.y()) / pageRect.height(),
				   _boundingRect.width() / pageRect.width(),
				   _boundingRect.height() / pageRect.height() );
}


void PdfItem::setBoundingRect(QRectF const& rect)
{
	if(rect != _boundingRect) {
		PdfPage *p = page();
		if(p && includedInRTree()) {
			p->rtree()->remove(this);
		}

		_boundingRect = rect;

		if(p && includedInRTree()) {
			p->rtree()->insert(this);
		}

		if(_parent) {
			_parent->notifyChildGeometryChange();
		}
	}
}


void PdfItem::setParent(PdfItem *parent)
{
	_parent = parent;
}


bool PdfItem::includedInRTree() const
{
	return _parent != 0;
}


QString PdfItem::typeString() const
{
	switch(type()) {
	case Page:
		return "Page";
	case Line:
		return "Line";
	case Rectangle:
		return "Rectangle";
	case Text:
		return "Text";
	case TextBlock:
		return "TextBlock";
	case TextLine:
		return "TextLine";
	case Image:
		return "Image";
	default:
		return "UnknownType";
	}
}




PdfPage::PdfPage(int number) :
	PdfItem(0),
	_number(number),
	_rtree(new PdfRTree)
{
}


PdfPage::~PdfPage()
{
	delete _rtree;
	qDeleteAll(_items);
}


void PdfPage::addItem(PdfItem *item)
{
	if(item) {
		_items.append(item);
		item->setParent(this);
		if(item->includedInRTree()) {
			_rtree->insert(item);
		}
	}
}


bool PdfPage::atSameY(qreal y1, qreal y2, qreal errorMargin) const
{
	qreal diff = qAbs(y2 - y1) / _boundingRect.height();
	return diff < errorMargin;
}


PdfItem* PdfPage::takeItemAt(int index)
{
	PdfItem *item = _items.takeAt(index);
	_rtree->remove(item);
	return item;
}


QList<PdfText*> PdfPage::texts() const
{
	QList<PdfText*> textList;
	foreach(PdfItem *item, _items) {
		if(item->type() == Text) {
			textList << dynamic_cast<PdfText*>(item);
		} else if(item->type() == TextBlock) {
			PdfTextBlock *block = dynamic_cast<PdfTextBlock*>(item);
			for(int i = 0; i < block->textLineCount(); i++) {
				PdfTextLine *line = block->textLineAt(i);
				for(int j = 0; j < line->textCount(); j++) {
					textList << line->textAt(j);
				}
			}
		}
	}

	return textList;
}


PdfFont::PdfFont(QString const& name, QUrl const& source) :
	_name(name),
	_source(source)
{
}


PdfImage::PdfImage(QUrl const& source, QSize const& sourceSize, PdfItem *parent) :
	PdfItem(parent),
	_source(source),
	_sourceSize(sourceSize)
{
}


PdfLine::PdfLine(QLineF const& line, QColor const& color, qreal penWidth, PdfItem *parent) :
	PdfItem(parent),
	_color(color),
	_penWidth(penWidth),
	_line(line)
{
	setBoundingRect( QRectF(line.p1(), line.p2()).normalized() );
}


bool PdfLine::isMostlyHorizontal() const
{
	if(_boundingRect.height() == 0) {		// Prevent division by 0
		return true;
	}

	qreal prop = _boundingRect.width() / _boundingRect.height();
	return qAbs(prop) > 11.111;		// <5 degrees
}


bool PdfLine::isMostlyVertical() const
{
	if(_boundingRect.width() == 0) {		// Prevent division by 0
		return true;
	}

	qreal prop = _boundingRect.height() / _boundingRect.width();
	return qAbs(prop) > 11.111;		// <5 degrees
}


PdfRectangle::PdfRectangle(QColor const& fillColor, QColor const& borderColor, qreal penWidth, PdfItem *parent) :
	PdfItem(parent),
	_fillColor(fillColor),
	_borderColor(borderColor),
	_penWidth(penWidth)
{
}


PdfText::PdfText(QColor const& color, QString const& fontName, qreal fontSize, qreal rotation, QString const& text, PdfItem *parent) :
	PdfItem(parent),
	_color(color),
	_fontName(fontName),
	_fontSize(fontSize),
	_rotation(rotation),
	_semantics(StandardText),
	_text(text)
{
}


bool PdfText::includedInRTree() const
{
	if(!_parent) {
		return false;
	}

	return _parent->type() != TextBlock && _parent->type() != TextLine;
}


void PdfText::setSemantics(TextSemantics semantics)
{
	_semantics = semantics;
}


PdfTextLine::PdfTextLine(PdfItem *parent) :
	PdfItem(parent)
{
}


void PdfTextLine::addText(PdfText *text)
{
	PdfPage *p = page();
	if(p && text->includedInRTree()) {
		p->rtree()->remove(text);
	}
	text->setParent(this);

	_texts.append(text);

	QRectF newRect = _boundingRect.united(text->boundingRect());
	bool changed = newRect != _boundingRect;

	if(p && includedInRTree() && changed) {
		p->rtree()->remove(this);
	}
	_boundingRect = newRect;
	if(p && includedInRTree() && changed) {
		p->rtree()->insert(this);
	}
}


void PdfTextLine::insertText(int index, PdfText *text)
{
	PdfPage *p = page();
	if(p && text->includedInRTree()) {
		p->rtree()->remove(text);
	}
	text->setParent(this);

	_texts.insert(index, text);

	QRectF newRect = _boundingRect.united(text->boundingRect());
	bool changed = newRect != _boundingRect;

	if(p && includedInRTree() && changed) {
		p->rtree()->remove(this);
	}
	_boundingRect = newRect;
	if(p && includedInRTree() && changed) {
		p->rtree()->insert(this);
	}
}


bool PdfTextLine::includedInRTree() const
{
	if(!_parent) {
		return false;
	}

	return _parent->type() != TextBlock;
}


void PdfTextLine::notifyChildGeometryChange()
{
	QRectF newRect;
	foreach(PdfText *text, _texts) {
		newRect.unite(text->boundingRect());
	}

	if(newRect != _boundingRect) {
		setBoundingRect(newRect);
	}
}


PdfTextBlock::PdfTextBlock(PdfItem *parent) :
	PdfItem(parent)
{
}


void PdfTextBlock::addTextLine(PdfTextLine *line)
{
	PdfPage *p = page();
	if(p && line->includedInRTree()) {
		p->rtree()->remove(line);
	}
	line->setParent(this);

	_textLines.append(line);

	QRectF newRect = _boundingRect.united(line->boundingRect());
	bool changed = newRect != _boundingRect;

	if(p && includedInRTree() && changed) {
		p->rtree()->remove(this);
	}
	_boundingRect = newRect;
	if(p && includedInRTree() && changed) {
		p->rtree()->insert(this);
	}
}


void PdfTextBlock::notifyChildGeometryChange()
{
	QRectF newRect;
	foreach(PdfTextLine *line, _textLines) {
		newRect.unite(line->boundingRect());
	}

	if(newRect != _boundingRect) {
		setBoundingRect(newRect);
	}
}


PdfDocument::PdfDocument()
{
}


PdfDocument::~PdfDocument()
{
	clear();
}


void PdfDocument::addFont(PdfFont *font)
{
	_fonts.insert(font->name(), font);
}


void PdfDocument::addPage(PdfPage *page)
{
	_pages.append(page);
}


void PdfDocument::addToLastPage(PdfItem *item)
{
	_pages.last()->addItem(item);
}


void PdfDocument::clear()
{
	qDeleteAll(_pages);
	_pages.clear();
	qDeleteAll(_fonts);
	_fonts.clear();
}


bool PdfDocument::isEmpty() const
{
	return _pages.isEmpty();
}
