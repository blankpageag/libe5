#include <QtGui>
#include "IpeParser.h"
#include "PdfImageConverter.h"

IpeParser::IpeParser(Container * const container) :
	PdfParser(),
	_container(container)
{
}


bool IpeParser::parse(QIODevice *device)
{
	clear();

	// We need to replace this:
	//		<-- Page: 1 1 -->
	// with this:
	//		<!-- Page: 1 1 -->
	// Before passing the stream to the QXmlStreamReader, otherwise it will produce an error, since
	// the XML is not valid.
	QByteArray xmlData = device->readAll();
	xmlData.replace("<-- Page:", "<!-- Page:");

	QXmlStreamReader xml(xmlData);
	QSizeF pageSize;
	bool ok = false;
	while(!xml.atEnd() && !xml.hasError()) {
		if(xml.readNext() != QXmlStreamReader::StartElement) {
			continue;
		}

		// Old versions of pdftoipe
		if(xml.name() == "info" && pageSize.isEmpty()) {
			QList<qreal> pageGeometry = numberListAttribute(xml, "media", &ok);
			if(pageGeometry.count() == 4) {
				pageSize = QSizeF(pageGeometry.at(2), pageGeometry.at(3));
			} else {
				qDebug() << "Invalid page size" << pageGeometry.count();
				return false;
			}

		// Newer versions of pdftoipe
		} else if(xml.name() == "layout" && pageSize.isEmpty()) {
			QList<qreal> pageGeometry = numberListAttribute(xml, "frame", &ok);
			if(pageGeometry.count() == 2) {
				pageSize = QSizeF(pageGeometry.at(0), pageGeometry.at(1));
			} else {
				qDebug() << "Invalid page frame" << pageGeometry.count();
				return false;
			}

		} else if(xml.name() == "page") {
			PdfPage *page = new PdfPage(_document->pageCount() + 1);
			page->setBoundingRect(QRectF(QPointF(0, 0), pageSize));
			_document->addPage(page);

		} else if(xml.name() == "image") {
			if(!parseImage(xml)) {
				clear();
				return false;
			}
		} else if(xml.name() == "path") {
			if(!parsePath(xml)) {
				clear();
				return false;
			}
		} else if(xml.name() == "text") {
			if(!parseText(xml)) {
				clear();
				return false;
			}
		} else if(xml.name() == "font") {
			if(!parseFont(xml)) {
				clear();
				return false;
			}
		}
	}

	if(_document->isEmpty() || xml.hasError()) {
		clear();
		return false;
	}

	invertCoordinates();
	return true;
}


bool IpeParser::parseFont(QXmlStreamReader& xml)
{
	if(xml.tokenType() != QXmlStreamReader::StartElement) {
		return false;
	}

	QString name = xml.attributes().value("name").toString();
	QString type = xml.attributes().value("type").toString();
	int length = xml.attributes().value("length").toString().toInt();

	QByteArray fontContents = QByteArray::fromHex(xml.readElementText().toAscii());
	if(xml.hasError()) {
		return false;
	}

	if(fontContents.length() != length) {
		return false;
	}

	QString extension;
	if(type == "TrueType") {
		extension = "ttf";
	} else if(type == "Type1") {
		extension = "otf";
	} else {
		return true;
	}

	QString fontPath = QString("e5/fonts/%1.%2").arg(name).arg(extension);
	if(!_container->add(fontPath, fontContents)) {
		return false;
	}

	PdfFont *font = new PdfFont(name, QUrl(fontPath));
	_document->addFont(font);
	return true;
}


bool IpeParser::parseImage(QXmlStreamReader& xml)
{
	if(_document->isEmpty()) {
		qDebug() << "No pages";
		return false;
	}
	if(xml.tokenType() != QXmlStreamReader::StartElement) {
		return false;
	}

	QList<qreal> rectangle = numberListAttribute(xml, "rect");
	if(rectangle.count() != 4) {
		return false;
	}

	QRectF rect;
	rect.setLeft(rectangle.at(0));
	rect.setTop(rectangle.at(1));
	rect.setRight(rectangle.at(2));
	rect.setBottom(rectangle.at(3));

	QSize sourceSize(xml.attributes().value("width").toString().toInt(), xml.attributes().value("height").toString().toInt());
	int length = xml.attributes().value("length").toString().toInt();
	QString colorSpace = xml.attributes().value("ColorSpace").toString();

	QByteArray imageContents = QByteArray::fromHex(xml.readElementText().toAscii());
	if(xml.hasError()) {
		return false;
	}

	if(imageContents.length() != length) {
		return false;
	}

	QString imageFormat;
	{
		// Find out the image format of the image (png, jpg,...?)
		QBuffer buffer(&imageContents);
		buffer.open(QIODevice::ReadOnly);
		QImageReader imageReader(&buffer);
		imageReader.setDecideFormatFromContent(true);
		imageFormat = imageReader.format();
	}
	if(imageFormat.isEmpty()) {
		imageFormat = "jpg";
	}

	QString imagePath = _container->add("pdf-image", imageFormat, imageContents);
	QUrl source = _container->baseUrl();
	source = source.resolved(imagePath);

	// Convert CMYK images to RGB
	if(colorSpace.contains("CMYK", Qt::CaseInsensitive)) {
		PdfImageConverter converter;
		converter.convert(_container, imagePath);
	}


	PdfImage *image = new PdfImage(source, sourceSize, _document->lastPage());
	image->setBoundingRect(rect);
	_document->addToLastPage(image);

	return true;
}


bool IpeParser::parsePath(QXmlStreamReader& xml)
{
	if(_document->isEmpty()) {
		qDebug() << "No pages";
		return false;
	}
	if(xml.tokenType() != QXmlStreamReader::StartElement) {
		return false;
	}

	QColor stroke = colorAttribute(xml, "stroke");
	QColor fill = colorAttribute(xml, "fill");
	qreal penWidth = xml.attributes().value("pen").toString().toDouble();

	QStringList textLines = xml.readElementText().split('\n', QString::SkipEmptyParts);
	if(xml.hasError()) {
		return false;
	}

	if(textLines.isEmpty()) {
		return true;
	}

	if(textLines.count() == 2) {		// A line
		// Example of the line definition:
		//	<path stroke="0 0 0" pen="0.3">
		//		695.347 347.022 m
		//		695.347 42.5201 l
		//	</path>

		QString firstLine = textLines.at(0).trimmed();
		firstLine.chop(2);
		QString secondLine = textLines.at(1).trimmed();
		secondLine.chop(2);
		QList<qreal> startPoint = numberList(firstLine);
		QList<qreal> endPoint = numberList(secondLine);
		if(startPoint.count() != 2 || endPoint.count() != 2) {
			return false;
		}

		QLineF line(startPoint.at(0), startPoint.at(1), endPoint.at(0), endPoint.at(1));
		_document->addToLastPage( new PdfLine(line, stroke, penWidth, _document->lastPage()) );

	} else if(textLines.count() == 6 && textLines.last().trimmed() == "h") {		// A rectangle
		// Example of rectangle definition:
		//	<path fill="0.137299 0.121597 0.125488" fillrule="wind">
		//		564.774 1191.74 m
		//		565.522 1191.74 l
		//		565.522 1214.42 l
		//		564.774 1214.42 l
		//		564.774 1191.74 l
		//		h
		//	</path>
		textLines.removeLast();
		for(int i = 0; i < textLines.count(); i++) {
			QString line = textLines.at(i).trimmed();
			QChar vertexType = line.at( line.length() - 1 );
			if( (i == 0 && vertexType != 'm') || (i > 0 && vertexType != 'l') ) {
				return true;	// This path is not a rectangle
			}
			line.chop(2);
			textLines[i] = line;
		}

		// Ensure the path is closed
		if(textLines.first() != textLines.last()) {
			return true;
		}
		textLines.removeLast();

		// Extract the points
		QList<qreal> xPoints;
		QList<qreal> yPoints;
		for(int i = 0; i < textLines.count(); i++) {
			QList<qreal> points = numberList(textLines.at(i));
			if(points.count() != 2) {
				return true;
			}
			if(!xPoints.contains(points.at(0))) {
				xPoints.append(points.at(0));
			}
			if(!yPoints.contains(points.at(1))) {
				yPoints.append(points.at(1));
			}
		}

		// Ensure it's a rectangle
		if(xPoints.count() != 2 || yPoints.count() != 2) {
			return true;
		}
		qSort(xPoints);
		qSort(yPoints);

		QRectF rect;
		rect.setLeft(xPoints.at(0));
		rect.setTop(yPoints.at(0));
		rect.setRight(xPoints.at(1));
		rect.setBottom(yPoints.at(1));

		PdfRectangle *rectangle = new PdfRectangle(fill, stroke, penWidth, _document->lastPage());
		rectangle->setBoundingRect(rect);
		_document->addToLastPage(rectangle);
	}

	return true;
}


bool IpeParser::parseText(QXmlStreamReader& xml)
{
	if(_document->isEmpty()) {
		qDebug() << "No pages";
		return false;
	}
	if(xml.tokenType() != QXmlStreamReader::StartElement) {
		return false;
	}

	QList<qreal> matrix = numberListAttribute(xml, "matrix");
	// The matrix should be a transformation matrix similar to QMatrix, of the form:
	// m11  m12  0
	// m21  m22  0
	//  dx   dy  1
	//
	// Where:
	//	m11 and m22 (elements 0 and 3) specify the horizontal and vertical scale
	//  m21 and m12 (elements 1 and 2) specify the horizontal and vertical shearing
	//  dx and dy (elements 4 and 5) specify the translation
	if(matrix.count() != 6) {
		return false;
	}

	QList<qreal> pos = numberListAttribute(xml, "pos");
	if(pos.count() != 2) {
		return false;
	}

	QColor color = colorAttribute(xml, "stroke");
	QString fontName = xml.attributes().value("fontname").toString();
//	qreal fontSize = xml.attributes().value("size").toString().toDouble();
	qreal fontSize = matrix.at(0);
	QPointF topLeft = QPointF(pos.at(0) + matrix.at(4), pos.at(1) + matrix.at(5));		// apply the translation
	topLeft.setY(topLeft.y() + matrix.at(0));
	qreal rotation = matrix.at(2) * 16.36363636;

	QString contents = fixIpeText(xml.readElementText());
	if(xml.hasError()) {
		return false;
	}

	PdfText *text = new PdfText(color, fontName, fontSize, rotation, contents, _document->lastPage());
	text->setBoundingRect(QRectF(topLeft, QSizeF()));		// We don't know the size of the text, we will have to calculate it later
	_document->addToLastPage(text);

	return true;
}


QColor IpeParser::colorAttribute(QXmlStreamReader const& xml, const char *attributeName, bool *ok)
{
	QList<qreal> colorComponents = numberListAttribute(xml, attributeName, ok);
	if(colorComponents.count() < 3 || colorComponents.count() > 4) {
		return QColor();
	}

	QColor color;
	color.setRedF(colorComponents.at(0));
	color.setGreenF(colorComponents.at(1));
	color.setBlueF(colorComponents.at(2));
	if(colorComponents.count() == 4) {
		color.setAlphaF(colorComponents.at(3));
	}

	return color;
}


QString IpeParser::fixIpeText(QString text)
{
	// Old pdftoipe format
	forever {
		int index = text.indexOf("[[[");
		if(index == -1) {
			break;
		}

		int codeLength = text.mid(index + 3).indexOf("]]]");
		if(codeLength == -1) {
			break;
		}

		QChar nextCharacter = text.at(index + codeLength + 6);
		QChar encodedCharacter;
		QString code = text.mid(index+3, codeLength);
		bool conversionOk = false;
		int unicode = code.toInt(&conversionOk, 16);
		if(conversionOk) {
			encodedCharacter = QChar(unicode);
		}

		text.remove(index, codeLength + 6);
		if(!encodedCharacter.isNull() && encodedCharacter != nextCharacter) {
			text.insert(index, encodedCharacter);
		}
	}

	// New pdftoipe format
	forever {
		int index = text.indexOf("[U+");
		if(index == -1) {
			break;
		}

		int codeLength = text.mid(index + 3).indexOf("]");
		if(codeLength == -1) {
			break;
		}

		QString code = text.mid(index + 3, codeLength);
		bool conversionOk = false;
		int unicode = code.toInt(&conversionOk, 16);

		text.remove(index, codeLength + 4);
		if(conversionOk) {
			text.insert(index, QChar(unicode));
		} else {
			qDebug() << "Unicode conversion failed: " << code;
		}
	}

	text.replace("\\&", "&");
	text.replace("$<$", "<");
	text.replace("$>$", ">");
	text.replace("$&lt;$", "<");
	text.replace("$&gt;$", ">");
	text.replace("\\$", "$");
	return text;
}


QList<qreal> IpeParser::numberListAttribute(QXmlStreamReader const& xml, const char *attributeName, bool *ok)
{
	return numberList(xml.attributes().value(attributeName).toString(), ok);
}


QList<qreal> IpeParser::numberList(QString const& string, bool *ok)
{
	QStringList numberStringList = string.split(' ', QString::SkipEmptyParts);
	QList<qreal> numbers;
	bool conversionOk = false;
	foreach(QString const& numberString, numberStringList) {
		qreal n = static_cast<qreal>(numberString.toDouble(&conversionOk));
		if(!conversionOk) {
			if(ok) {
				*ok = false;
			}
			return QList<qreal>();
		}

		numbers << n;
	}

	if(ok) {
		*ok = true;
	}
	return numbers;
}
