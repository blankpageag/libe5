#include <QtGui>
#include <stdio.h>      // jpeglib needs this to be pre-included
#include <setjmp.h>
#include <jpeglib.h>
#include <lcms2.h>
#include "PdfImageConverter.h"

#define OUTPUT_QUALITY 90

struct my_error_mgr : public jpeg_error_mgr {
	jmp_buf setjmp_buffer;
};

static void my_error_exit (j_common_ptr cinfo)
{
	my_error_mgr* myerr = (my_error_mgr*) cinfo->err;
	char buffer[JMSG_LENGTH_MAX];
	(*cinfo->err->format_message)(cinfo, buffer);
	qWarning("%s", buffer);
	longjmp(myerr->setjmp_buffer, 1);
}

static const int max_buf = 4096;

struct my_jpeg_source_mgr : public jpeg_source_mgr {
	// Nothing dynamic - cannot rely on destruction over longjump
	QIODevice *device;
	JOCTET buffer[max_buf];
	const QBuffer *memDevice;

public:
	my_jpeg_source_mgr(QIODevice *device);
};


static void e5_init_source(j_decompress_ptr)
{
}


static boolean e5_fill_input_buffer(j_decompress_ptr cinfo)
{
	my_jpeg_source_mgr* src = (my_jpeg_source_mgr*)cinfo->src;
	qint64 num_read = 0;
	if (src->memDevice) {
		src->next_input_byte = (const JOCTET *)(src->memDevice->data().constData() + src->memDevice->pos());
		num_read = src->memDevice->data().size() - src->memDevice->pos();
		src->device->seek(src->memDevice->data().size());
	} else {
		src->next_input_byte = src->buffer;
		num_read = src->device->read((char*)src->buffer, max_buf);
	}
	if (num_read <= 0) {
		// Insert a fake EOI marker - as per jpeglib recommendation
		src->next_input_byte = src->buffer;
		src->buffer[0] = (JOCTET) 0xFF;
		src->buffer[1] = (JOCTET) JPEG_EOI;
		src->bytes_in_buffer = 2;
	} else {
		src->bytes_in_buffer = num_read;
	}
	return true;
}


static void e5_skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{
	my_jpeg_source_mgr* src = (my_jpeg_source_mgr*)cinfo->src;
	if (num_bytes > 0) {
		while (num_bytes > (long) src->bytes_in_buffer) {  // Should not happen in case of memDevice
			num_bytes -= (long) src->bytes_in_buffer;
			(void) e5_fill_input_buffer(cinfo);
			// We assume that e5_fill_input_buffer will never return false,
			// so suspension need not be handled.
		}
		src->next_input_byte += (size_t) num_bytes;
		src->bytes_in_buffer -= (size_t) num_bytes;
	}
}


static void e5_term_source(j_decompress_ptr cinfo)
{
	my_jpeg_source_mgr* src = (my_jpeg_source_mgr*)cinfo->src;
	if (!src->device->isSequential())
		src->device->seek(src->device->pos() - src->bytes_in_buffer);
}


inline my_jpeg_source_mgr::my_jpeg_source_mgr(QIODevice *device)
{
	jpeg_source_mgr::init_source = e5_init_source;
	jpeg_source_mgr::fill_input_buffer = e5_fill_input_buffer;
	jpeg_source_mgr::skip_input_data = e5_skip_input_data;
	jpeg_source_mgr::resync_to_restart = jpeg_resync_to_restart;
	jpeg_source_mgr::term_source = e5_term_source;
	this->device = device;
	memDevice = qobject_cast<QBuffer *>(device);
	bytes_in_buffer = 0;
	next_input_byte = buffer;
}




class PdfImageConverterPrivate
{
public:
	void init(QByteArray sourceProfile, QByteArray destinationProfile)
	{
		cmsHPROFILE hInProfile = cmsOpenProfileFromMem(const_cast<char*>(sourceProfile.constData()), sourceProfile.length());
		cmsHPROFILE hOutProfile= cmsOpenProfileFromMem(const_cast<char*>(destinationProfile.constData()), destinationProfile.length());
		_cmsTransform = cmsCreateTransform(hInProfile, TYPE_CMYK_8,
										   hOutProfile, TYPE_RGB_8,
										   INTENT_PERCEPTUAL, 0);
		cmsCloseProfile(hInProfile);
		cmsCloseProfile(hOutProfile);
	}

	QByteArray convert(QIODevice *device)
	{
		struct my_jpeg_source_mgr *iod_src = new my_jpeg_source_mgr(device);
		struct jpeg_decompress_struct cinfo;
		struct my_error_mgr err;

		jpeg_create_decompress(&cinfo);
		cinfo.src = iod_src;
		cinfo.err = jpeg_std_error(&err);
		err.error_exit = my_error_exit;

		if (!setjmp(err.setjmp_buffer)) {
			(void) jpeg_read_header(&cinfo, true);
			(void) jpeg_start_decompress(&cinfo);

			int width = cinfo.output_width;
			int height = cinfo.output_height;
			int components = cinfo.output_components;
			if(components != 4) {
				qWarning() << "The source image is not in the CMYK color space.";
				(void) jpeg_finish_decompress(&cinfo);
				jpeg_destroy_decompress(&cinfo);
				return QByteArray();
			}

			int rowStride = width * components;
			JSAMPARRAY buffer = (*cinfo.mem->alloc_sarray)
								((j_common_ptr) &cinfo, JPOOL_IMAGE, rowStride, 1);
			uchar *outputImage = new uchar[width * height * components];
			long position = 0;

			while (cinfo.output_scanline < cinfo.output_height) {
				(void) jpeg_read_scanlines(&cinfo, buffer, 1);
				memcpy(outputImage + position, reinterpret_cast<uchar*>(buffer[0]), rowStride);
				position += static_cast<long>(rowStride);
			}

			(void) jpeg_finish_decompress(&cinfo);
			jpeg_destroy_decompress(&cinfo);

			QByteArray outputJpeg = applyTransform(outputImage, width, height, components);

			delete outputImage;
			delete iod_src;
			return outputJpeg;
		} else {
			delete iod_src;
			return QByteArray();
		}
	}

	QByteArray applyTransform(uchar *imageData, int width, int height, int components)
	{
		uchar *destImageData = new uchar[width * height * 3];

		cmsDoTransform(_cmsTransform,
					   imageData,
					   destImageData, width * height);

		QImage image(width, height, QImage::Format_RGB32);
		long index = 0;
		int x, y;
		for(y = 0; y < height; y++) {
			for(x = 0; x < width; x++) {
				image.setPixel(x, y, QColor(destImageData[index], destImageData[index+1], destImageData[index+2]).rgb());
				index += 3;
			}
		}

		QByteArray jpegData;
		QBuffer buffer(&jpegData);
		image.save(&buffer, "JPG", OUTPUT_QUALITY);
		return jpegData;
	}

	cmsHTRANSFORM _cmsTransform;
};


PdfImageConverter::PdfImageConverter(QString const& sourceProfile, QString const& destinationProfile) :
	d(new PdfImageConverterPrivate)
{
	QFile source(sourceProfile);
	QFile destination(destinationProfile);
	if(source.open(QIODevice::ReadOnly) && destination.open(QIODevice::ReadOnly)) {
		d->init(source.readAll(), destination.readAll());
	}
}


PdfImageConverter::PdfImageConverter(QIODevice *sourceProfile, QIODevice *destinationProfile) :
	d(new PdfImageConverterPrivate)
{
	d->init(sourceProfile->readAll(), destinationProfile->readAll());
}


PdfImageConverter::~PdfImageConverter()
{
	delete d;
}


bool PdfImageConverter::convert(Container * const container, QString const& imagePath) const
{
	QIODevice *device = container->open(imagePath);
	QByteArray outputJpeg = d->convert(device);
	delete device;
	if(outputJpeg.isEmpty()) {
		return false;
	} else {
		container->add(imagePath, outputJpeg);
		return true;
	}
}
