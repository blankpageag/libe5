#ifndef IPEPARSER_H
#define IPEPARSER_H 1

#include <QXmlStreamReader>
#include "Container"
#include "PdfParser.h"

class IpeParser : public PdfParser
{
public:
	IpeParser(Container * const container);
	bool parse(QIODevice *device);

private:
	bool parseFont(QXmlStreamReader& xml);
	bool parseImage(QXmlStreamReader& xml);
	bool parsePath(QXmlStreamReader& xml);
	bool parseText(QXmlStreamReader& xml);
	static QColor colorAttribute(QXmlStreamReader const& xml, const char *attributeName, bool *ok = 0);
	static QString fixIpeText(QString text);
	static QList<qreal> numberListAttribute(QXmlStreamReader const& xml, const char *attributeName, bool *ok = 0);
	static QList<qreal> numberList(QString const& string, bool *ok = 0);

private:
	Container * const _container;
};

#endif
