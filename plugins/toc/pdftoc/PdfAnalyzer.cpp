#include <QtGui>
#include "PdfAnalyzer.h"

#define COLUMN_ALIGNMENT_ERROR 0.000005
#define CHARACTER_SPACING 0.00005
#define FONTSIZE_RELATIVE_CHARACTER_SPACING 0.45

bool isCrossedByLine(PdfPage *page, QPointF const& a, QPointF const& b)
{
	// For more details on the calculation see:
	// http://www.w3support.net/index.php?db=so&id=563198
	qreal pX = -(b.y() - a.y());
	qreal pY = b.x() - a.x();

	QList<PdfItem*> items = page->rtree()->findAll(QRectF(a, b).normalized());
	foreach(PdfItem *item, items) {
		if(item->type() == PdfItem::Line) {
			PdfLine *line = dynamic_cast<PdfLine*>(item);
			qreal fX = line->end().x() - line->start().x();
			qreal fY = line->end().y() - line->start().x();
			qreal iX = a.x() - line->start().x();
			qreal iY = a.y() - line->start().y();
			qreal quotient = (iX * pX) + (iY * pY);
			qreal denominator = (fX * pX) + (fY * pY);
			if(denominator == 0) {	// Prevent division by 0
				return true;
			}

			qreal h = quotient / denominator;
			if(h >= 0 && h <= 1) {
				return true;
			}
		}
	}

	return false;
}


bool lessXThan(const PdfItem * const a, const PdfItem * const b)
{
	return a->left() < b->left();
}


class TitleExtractor
{
public:
	TitleExtractor(PdfPage *page) :
		_page(page)
	{
	}

	void tagTitles()
	{
		_commonFontSize = mostFrequentSize(findFontSizeFrequencies()) + 1;

		QList<PdfText*> texts = _page->texts();
		foreach(PdfText *text, texts) {
			if(text->fontSize() > _commonFontSize) {
				text->setSemantics(PdfText::Title);
			}
		}
	}

	inline int commonFontSize() const { return _commonFontSize; }

private:
	QHash<int, int> findFontSizeFrequencies()
	{
		QHash<int, int> frequencies;

		QList<PdfText*> texts = _page->texts();
		foreach(PdfText *text, texts) {
			int size = qRound(text->fontSize());
			if(size > 0) {
				frequencies[size] += text->text().length();
			}
		}

		return frequencies;
	}

	int mostFrequentSize(QHash<int, int> const& fontSizeFrequency) const
	{
		int fontSize = 0;
		int maxFrequency = -1;

		QHashIterator<int, int> it(fontSizeFrequency);
		while(it.hasNext()) {
			it.next();
			if(it.value() > maxFrequency) {
				fontSize = it.key();
				maxFrequency = it.value();
			}
		}

		return fontSize;
	}

private:
	PdfPage *_page;
	int _commonFontSize;
};


typedef QMultiMap<qreal, PdfText*> AlignmentGroup;
typedef QMap<qreal, PdfText*>::const_iterator AlignmentGroupConstIterator;

class VerticalTextAlignmentFinder
{
public:
	VerticalTextAlignmentFinder(PdfPage *page) :
		_page(page)
	{
	}

	QList<AlignmentGroup> findAlignmentGroups() const
	{
		QLinkedList<PdfText*> textList = textsSortedByX();
		QLinkedList<PdfText*>::iterator it;
		QList<AlignmentGroup> alignedGroups;
		AlignmentGroup currentGroup;
		qreal groupMaxX = -1;

		for(it = textList.begin(); it != textList.end(); ++it) {
			qreal startX = (*it)->left();

			if(startX > groupMaxX) {
				// Add the current group to the list
				if(currentGroup.count() >= 3) {
					alignedGroups << splitGroup(currentGroup);
				}

				// Create the new group
				currentGroup.clear();
				groupMaxX = startX + (_page->width() * COLUMN_ALIGNMENT_ERROR);
			}

			currentGroup.insert((*it)->top(), *it);
		}

		if(currentGroup.count() >= 3) {
			alignedGroups << splitGroup(currentGroup);
		}

		return alignedGroups;
	}

private:
	QLinkedList<PdfText*> textsSortedByX() const
	{
		// Sort the texts by x coordinate (inserting them in a QMap keeps them sorted)
		QMultiMap<qreal, PdfText*> textMap;
		foreach(PdfItem *item, _page->items()) {
			if(item->type() == PdfItem::Text) {
				textMap.insert(item->left(), reinterpret_cast<PdfText*>(item));
			}
		}

		// Get a linked list with the items sorted by x coordinate
		QLinkedList<PdfText*> textList;
		QMultiMap<qreal, PdfText*>::iterator it;
		for(it = textMap.begin(); it != textMap.end(); ++it) {
			textList << *it;
		}

		return textList;
	}

	QList<AlignmentGroup> splitGroup(AlignmentGroup const& group) const
	{
		AlignmentGroupConstIterator it;
		QList<AlignmentGroup> splittedGroups;
		AlignmentGroup currentSubGroup;
		QPointF lastPoint(-999999, -999999);
		qreal maxY = -_page->height();
		for(it = group.constBegin(); it != group.constEnd(); ++it) {
			qreal startY = it.key();
			QRectF textRect = it.value()->boundingRect();

			// If the next chunk of text is too far below the maximum Y or if there is a line crossing
			// in between, don't consider that chunk as part of this column and split the column instead.
			if(startY > maxY || (lastPoint.y() != -999999 && isCrossedByLine(_page, lastPoint, QPointF(textRect.x(), startY))) ) {
				if(currentSubGroup.count() >= 3) {
					splittedGroups.append(currentSubGroup);
				}

				currentSubGroup.clear();
			}

			currentSubGroup.insert(it.key(), it.value());
			lastPoint = QPointF(textRect.bottomLeft());
			maxY = textRect.bottom() + (it.value()->fontSize() * 2);
		}

		if(currentSubGroup.count() >= 3) {
			splittedGroups.append(currentSubGroup);
		}

		// Remove false positives: groups that are not column starts but casual alignments of
		// caracters within the column
		QMutableListIterator<AlignmentGroup> mit(splittedGroups);
		qreal charSpacing = _page->width() * CHARACTER_SPACING;
		while(mit.hasNext()) {
			PdfText *firstText = *(mit.next().begin());
			ASSERT(firstText != 0);
			if(firstText->text().isEmpty()) {	// Prevent division by 0 when moving the rectangle to the left
				continue;		// We should in fact discard it, but there should be no empty text anyway
			}

			QRectF rect = firstText->boundingRect();
			rect.setX(rect.x() - charSpacing);
			rect.setWidth(charSpacing * 0.9);	// Prevent the RTree from finding the self text block
			QList<PdfItem*> items = _page->rtree()->findAll(rect);
			foreach(PdfItem *item, items) {
				// If there is a character on the left side of this, then this character is part of a bigger
				// text line, so it's a false positive
				if(item->type() == PdfItem::Text) {
					mit.remove();
					continue;
				}
			}
		}

		return splittedGroups;
	}

private:
	PdfPage *_page;
};




class TextLineScanner
{
public:
	TextLineScanner(PdfPage *page) :
		_page(page)
	{
	}

	void scanTextLines()
	{
		findTextDistances();

		// Scan every line starting from the top-left corner of the page
		QList<PdfTextLine*> textLines;
		while(!_textDistances.isEmpty()) {
			// Create a new text line
			PdfTextLine *line = new PdfTextLine;
			textLines << line;

			// Get the next text item, which is the closest item to the top-left corner of the page that hasn't been
			// processed yet.
			qreal dist = 0;
			PdfText *text = 0;
			{
				QMultiMap<qreal, PdfText*>::const_iterator first = _textDistances.constBegin();
				dist = first.key();
				text = first.value();
			}
			ASSERT(text != 0);

			// Take the item out of the page and add it to the text line
			line->addText(text);
			_textDistances.remove(dist, text);
			_page->rtree()->remove(text);
			qDebug() << "BEGAN WITH: " << text->text() << text->boundingRect();

			// Now find all text items besides, that should belong to the same text line and move them to the text line
			PdfText *nextText = 0;
			do {
				// The search rectangle takes from the top-left corner of the text item to a few pixels beyond the right
				// edge of the text. The self text item will of course be ignored. This is done this way because
				// sometimes, text items are inside of the bounding rectangle of the text, so they would be ignored,
				// causing some letters to be lost. One example of this occurs in the first page of baz1_2 where it's
				// written "> SEITE 9". There are 3 texts in this case: "> SEI", "T" and "E 9". The bounding rectangle
				// is inside of the bounding rectangle of "> SEI", so it would not be detected and the text would
				// incorrectly be read as "> SEIE 9", withh "T" lost.
				QRectF rect(text->boundingRect().topLeft(),
							QSizeF(text->width() + text->fontSize() * FONTSIZE_RELATIVE_CHARACTER_SPACING, text->fontSize() / 3));

				// Find the items in the search rectangle using the R-tree
				QList<PdfItem*> rightItems = _page->rtree()->findAll(rect);

				// Sort the items by X coordinate, so we start processing from left to right.
				// Although sorting a list is always slow, the list should usually be small, with no more than 10
				// or 20 items. Thus, this should be relatively fast.
				qSort(rightItems.begin(), rightItems.end(), lessXThan);
				qDebug() << rect << rightItems.count();
				nextText = 0;

				// Process every item in the search rectangle, looking for text items besides the previous text item.
				// Stop if a crossing line or a column start is found.
				foreach(PdfItem *item, rightItems) {
					// Ignore the self text item
					if(item == text) {
						continue;
					}

					if(item->left() < text->left()) {
						continue;
					}

					// Check if the item is a crossing line and stop if so
					if(item->type() == PdfItem::Line) {
						PdfLine *line = reinterpret_cast<PdfLine*>(item);
						if(line->isMostlyVertical()) {
							break;
						}
					}

					if(!_page->atSameY(text->top(), item->top())) {
						qDebug() << "DIFFERENT Y" << text->top() << item->top();
						continue;
					}

					if(item->type() == PdfItem::Text) {
						// TODO: Check whether there is a column start and stop if so
						nextText = reinterpret_cast<PdfText*>(item);
					} else {
						qDebug() << "OTHER ITEM" << item->typeString();
					}

					break;
				}

				// If a text item was found that matches the requirements to belong to the same text line,
				// add it to the line and continue looking for text items starting from it.
				if(nextText) {
					line->addText(nextText);
					text = nextText;

					// Extract the next item from the page, since it belongs to the text line now
					_textDistances.remove(distance(nextText), nextText);
					_page->rtree()->remove(nextText);
					qDebug() << " - " << nextText->text();
				}
			} while(nextText != 0);
		}

		// Extract all texts from the page
		for(int i = _page->itemCount() - 1; i >= 0; i--) {
			if(_page->itemAt(i)->type() == PdfItem::Text) {
				_page->takeItemAt(i);
			}
		}

		// Add the text lines to the page
		foreach(PdfTextLine *textLine, textLines) {
			// DEBUG CODE
			QString t;
			foreach(PdfText *text, textLine->texts()) {
				t.append(text->text());
			}
			qDebug() << "==" << t;

			if(t.contains("sterreich")) {
				foreach(PdfText *text, textLine->texts()) {
					qDebug() << "DEBUG" << text->boundingRect() << text->text();
				}
			}
			// END OF DEBUG CODE

			_page->addItem(textLine);
		}
	}

	/**
	 * Returns the distance from the top-left corner of \a item to the axis (0, 0).
	 */
	static inline qreal distance(PdfItem *item)
	{
		QPointF topLeft = item->boundingRect().topLeft();
		return qSqrt(topLeft.x() * topLeft.x() + topLeft.y() * topLeft.y());
	}

private:
	/**
	 * Calculates the distance from the top-left corner of every text item to the top left corner of the page.
	 * The calculated distances are stored in _textDistances.
	 */
	void findTextDistances()
	{
		foreach(PdfItem *item, _page->items()) {
			if(item->type() == PdfItem::Text) {
				_textDistances.insert(distance(item), reinterpret_cast<PdfText*>(item));
			}
		}
	}

private:
	PdfPage *_page;
	QMultiMap<qreal, PdfText*> _textDistances;		// Key -> distance to the top left corner of the page; Value -> text item
													// See findTextDistances()
};




 PdfAnalyzer::PdfAnalyzer(Container *container, PdfDocument *document) :
	_container(container),
	_document(document)
{
}


bool PdfAnalyzer::analyze()
{
	loadFonts();
	calculateTextSizes();

	for(int i = 0; i < _document->pageCount(); i++) {
		PdfPage *page = _document->page(i);
		TitleExtractor(page).tagTitles();
		QList<AlignmentGroup> textGroups = VerticalTextAlignmentFinder(page).findAlignmentGroups();
		TextLineScanner(page).scanTextLines();

		// Debug the vertically aligned texts by painting them with a red rectangle
		foreach(AlignmentGroup const& group, textGroups) {
			PdfText *first = group.begin().value();
			PdfText *last = (--group.end()).value();

			QRectF rect;
			rect.setTopLeft(first->boundingRect().topLeft());
			rect.setBottomRight(last->boundingRect().bottomLeft());
			rect.setWidth(qMax(1.0, rect.width()));

			PdfRectangle *rectangle = new PdfRectangle(QColor(), Qt::red, 1, page);
			rectangle->setBoundingRect(rect);
			page->addItem(rectangle);
		}
	}
}


void PdfAnalyzer::calculateTextSizes()
{
	for(int i = 0; i < _document->pageCount(); i++) {
		QList<PdfText*> texts = _document->page(i)->texts();
		foreach(PdfText *text, texts) {
			QFontMetrics metrics(QFont(text->fontName(), text->fontSize() * 10));
			QSizeF textSize = QSizeF(metrics.size(Qt::TextIncludeTrailingSpaces, text->text()));
			textSize /= 10;

			QRectF rect = text->boundingRect();
			rect.setSize(textSize);
			text->setBoundingRect(rect);
		}
	}
}


void PdfAnalyzer::loadFonts()
{
	QHash<QString, PdfFont*> fonts = _document->fonts();
	foreach(PdfFont *font, fonts) {
		qDebug() << "Loading font " << font->name();
		QByteArray fontData = _container->content(font->source().path());
		QFontDatabase::addApplicationFontFromData(fontData);
	}
}
