#ifndef PDFPARSER_H
#define PDFPARSER_H 1

#include <QIODevice>
#include "PdfObjects.h"

class PdfParser
{
public:
	PdfParser();
	void clear();
	inline QString errorMessage() const { return _error; }
	inline PdfDocument* document() const { return _document; }
	virtual bool parse(QIODevice *device) =0;

protected:
	QRectF calculateBoundingRect(PdfPage *page) const;
	void invertCoordinates();

protected:
	QString _error;
	PdfDocument *_document;
};

#endif
