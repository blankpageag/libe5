// vim:syntax=cpp.doxygen
#ifndef _PDFTOC_
#define _PDFTOC_ 1

#include <TOC>

class PdfFont;
class PdfPage;

/**
 * @class PdfTOC
 * @brief Table of contents of a parsed PDF
 * @author Víctor Fernández Martínez <victor.fernandez@blankpage.ch>
 * @date 2011
 */
class PdfTOC : public TOC
{
	Q_OBJECT

public:
	/**
	 * @copydoc TOC::TOC
	 */
	PdfTOC(Container * const container);
	
	/**
	 * @copydoc TOC::load
	 */
	void load();
	
	/**
	 * @copydoc TOC::mimeType
	 */
	QString mimeType(QString const &fn) const;

private:
	QString renderPage(PdfPage *page, QHash<QString, PdfFont*> const& fonts);
	static QString fontId(QString const& fontName);

protected:
	QHash<QString,QString>	_mimeTypes;
	QString _pdfFile;
};
#endif
