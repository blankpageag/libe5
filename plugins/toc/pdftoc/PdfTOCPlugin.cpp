#include <QtPlugin>
#include "PdfTOCPlugin"
#include "PdfTOC"

CONST uint8_t PdfTOCPlugin::priority(Container const * const container) const
{
	foreach(QString f, container->files()) {
		if( f.contains(".pdf" ) ) {
			return 100;
		}
	}
	return 0;
}


TOC *PdfTOCPlugin::get(Container * const container) const
{
	return new PdfTOC(container);
}

EXPORT_PLUGIN(pdfTOC, PdfTOCPlugin);
