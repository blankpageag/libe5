#ifndef PDFTOIPEINVOKER_H
#define PDFTOIPEINVOKER_H 1

#include "PdfToolInvoker.h"

class PdfToIpeInvoker : public PdfToolInvoker
{
	Q_OBJECT

public:
	PdfToIpeInvoker(QIODevice *sourcePdf, QObject *parent = 0);
	QStringList arguments() const;
	QString toolName() const;
	QString toolPath() const;
};

#endif
