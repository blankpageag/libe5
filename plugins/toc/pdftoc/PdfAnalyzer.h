#ifndef PDFANALYZER_H
#define PDFANALYZER_H 1

#include "Container"
#include "PdfObjects.h"

class PdfAnalyzer
{
public:
	PdfAnalyzer(Container *container, PdfDocument *document);
	bool analyze();

private:
	void calculateTextSizes();
	void loadFonts();

private:
	Container *_container;
	PdfDocument *_document;
};

#endif
