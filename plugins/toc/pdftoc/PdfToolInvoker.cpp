#include <QtCore>
#include "PdfToolInvoker.h"

PdfToolInvoker::PdfToolInvoker(QIODevice *sourcePdf, QObject *parent) :
	QObject(parent),
	_sourcePdf(sourcePdf),
	_pdfFile("pdftocplugin-XXXXXX.tmp"),
	_outputFile("pdftocplugin-XXXXXX.tmp"),
	_process(new QProcess)
{
	connect(_process, SIGNAL(error(QProcess::ProcessError)),
			this, SLOT(processError(QProcess::ProcessError)));
	connect(_process, SIGNAL(finished(int, QProcess::ExitStatus)),
			this, SLOT(processFinished(int, QProcess::ExitStatus)));
}


PdfToolInvoker::~PdfToolInvoker()
{
	_process->deleteLater();
}


QIODevice* PdfToolInvoker::output() const
{
	return &_outputFile;
}


void PdfToolInvoker::invoke()
{
	_error.clear();

	if(!_sourcePdf) {
		setFailed(tr("Invalid PDF source."));
		return;
	}
	if(!_sourcePdf->isOpen()) {
		setFailed(tr("PDF source not open."));
		return;
	}
	if(!_pdfFile.open() || !_outputFile.open()) {
		setFailed(tr("Couldn't create a temporary file."));
		return;
	}

	_pdfFile.write(_sourcePdf->readAll());

	QStringList arg = arguments();
	arg.replaceInStrings("%INPUT_FILE%", _pdfFile.fileName());
	arg.replaceInStrings("%OUTPUT_FILE%", _outputFile.fileName());

	QString executablePath = toolPath();
	qDebug() << "Extracting the PDF using:" << executablePath;
	_process->setProcessChannelMode(QProcess::MergedChannels);
	_process->start(executablePath, arg);
}


void PdfToolInvoker::processError(QProcess::ProcessError error)
{
	if(error == QProcess::WriteError) {
		return;
	}

	setFailed(_process->errorString());
}


void PdfToolInvoker::processFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
	if(exitStatus == QProcess::CrashExit) {
		setFailed(QString(tr("%1 has crashed.")).arg(toolName()));
		return;
	} if(exitCode == 0) {
		setFinished();
	} else {
		setFailed(QString(tr("%1 failed with exit code %2!")).arg(toolName()).arg(exitCode));
		return;
	}
}


void PdfToolInvoker::setFailed(QString const& error)
{
	_error = error;
	emit failed(error);
}


void PdfToolInvoker::setFinished()
{
	_error.clear();
	_outputFile.seek(0);
	emit finished();
}
