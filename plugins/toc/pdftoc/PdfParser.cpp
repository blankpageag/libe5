#include <QtCore>
#include "PdfParser.h"

PdfParser::PdfParser() :
	_document(new PdfDocument)
{
}


void PdfParser::clear()
{
	_error.clear();
	_document->clear();
}


QRectF PdfParser::calculateBoundingRect(PdfPage *page) const
{
	QRectF rect;

	foreach(PdfItem *item, page->items()) {
		rect.unite(item->boundingRect());
	}

	return rect;
}


void PdfParser::invertCoordinates()
{
	for(int i = 0; i < _document->pageCount(); i++) {
		PdfPage *page = _document->page(i);

		QRectF pageRect = calculateBoundingRect(page);
		if(pageRect.width() < page->width() || pageRect.height() < page->height()) {
			pageRect = QRectF(QPointF(0, 0), page->size());
		}

		foreach(PdfItem *item, page->items()) {
			QRectF rect = item->boundingRect();
			rect.moveTop( pageRect.height() - item->boundingRect().bottom() );
			item->setBoundingRect(rect);
		}
	}
}
