#include <QtCore>
#include <ConditionManager>
#include <ContainerAccessManager>
#include <CompilerTools>
#include <cassert>
#include "Content"
#include "IpeParser.h"
#include "PdfAnalyzer.h"
#include "PdfTOC"
#include "PdfToIpeInvoker.h"

PdfTOC::PdfTOC(Container * const container) :
	TOC(container)
{
}


void PdfTOC::load()
{
	if(!container()) {
		return;
	}

	_pdfFile.clear();
	QStringList const files = container()->files();
	
	foreach(QString f, files) {
		if(f.endsWith(".pdf")) {
			_pdfFile = f;
			break;
		}
	}

	if(_pdfFile.isEmpty()) {
		return;
	}

	QIODevice *pdfDevice = container()->open(_pdfFile);
	PdfToIpeInvoker invoker(pdfDevice);

	QEventLoop loop;
	QWeakPointer<PdfTOC> integrityPtr(this);
	connect(&invoker, SIGNAL(failed(QString const&)),
			&loop, SLOT(quit()));
	connect(&invoker, SIGNAL(finished()),
			&loop, SLOT(quit()));
	QTimer::singleShot(0, &invoker, SLOT(invoke()));
	loop.exec();

	delete pdfDevice;
	if(integrityPtr.isNull()) {
		// Prevent crashes if this was deleted while in the event loop
		return;
	}

	// Parse
	IpeParser parser(container());
	if(!parser.parse(invoker.output())) {
		return;
	}

	// Analyze the document structure
	PdfAnalyzer analyzer(container(), parser.document());
	analyzer.analyze();

	for(int i = 0; i < parser.document()->pageCount(); i++) {
		PdfPage *page = parser.document()->page(i);
		QString html = renderPage(page, parser.document()->fonts());
		container()->add(QString("page%1.html").arg(i), html.toUtf8());
	}
}


QString PdfTOC::mimeType(QString const &fn) const
{
	if(_mimeTypes.contains(fn))
		return _mimeTypes.value(fn);
	return TOC::mimeType(fn);
}


QString PdfTOC::renderPage(PdfPage *page, QHash<QString, PdfFont*> const& fonts)
{
	QString html = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	html += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
			"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
			"<head>\n"
			"  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n"
			"  <style type=\"text/css\">\n";

	foreach(PdfFont *font, fonts) {
		html += QString("    @font-face {\n"
						"      font-family: %1;\n"
						"      src: url('%2');\n"
						"    }\n\n")
				.arg(fontId(font->name())).arg(font->source().toString());
	}

	html += "  </style>\n"
			"</head>\n"
			"<body>\n";

	foreach(PdfItem *item, page->items()) {
		switch(item->type()) {
		case PdfItem::Text: {
			PdfText *text = dynamic_cast<PdfText*>(item);
			html += QString("<div style=\"position: absolute; display: block; left: %1px; top: %2px; font-family: '%3'; font-size: %4px; color: %5; -webkit-transform: rotate(%6deg); -webkit-transform-origin: left bottom; -moz-transform: rotate(%6deg); -moz-transform-origin: left bottom; -o-transform: rotate(%6deg); -o-transform-origin: left bottom\">%7</div>\n")
					.arg(text->left())
					.arg(text->top())
					.arg(fontId(text->fontName()))
					.arg(text->fontSize())
					.arg(text->color().name())
					.arg(text->rotation())
					.arg(text->text());
			break;
		}

		case PdfItem::TextLine: {
			PdfTextLine *textLine = dynamic_cast<PdfTextLine*>(item);
			html += QString("<div style=\"position: absolute; display: block; left: %1px; top: %2px\">")
					.arg(textLine->left())
					.arg(textLine->top());

			foreach(PdfText *text, textLine->texts()) {
				html += QString("<span style=\"font-family: '%1'; font-size: %2px; color: %3\">%4</span>")
						.arg(fontId(text->fontName()))
						.arg(text->fontSize())
						.arg(text->color().name())
						.arg(text->text());
			}

			html += "</div>\n";
			break;
		}

		case PdfItem::Image: {
			PdfImage *image = dynamic_cast<PdfImage*>(item);
			QString source = image->source().toString().mid(container()->baseUrl().length());
			html += QString("<img src=\"%1\" style=\"position: absolute; display: block; left: %2px; top: %3px; width: %4px; height: %5px\"/>\n")
					.arg(source)
					.arg(image->left())
					.arg(image->top())
					.arg(image->width())
					.arg(image->height());
			break;
		}

		case PdfItem::Line: {
			PdfLine *line = dynamic_cast<PdfLine*>(item);
			QRectF rect = line->boundingRect();
			qreal penWidth = qMax(1.0, line->penWidth());
			if(line->width() == 0) {				// Vertical
				rect.setLeft(rect.left() - penWidth / 2);
				rect.setWidth(penWidth);
			} else if(line->height() == 0) {		// Horizontal
				rect.setTop(rect.top() - penWidth / 2);
				rect.setHeight(penWidth);
			} else {
				// Oblicuous lines cannot be generated in HTML
				break;
			}

			html += QString("<div style=\"position: absolute; background-color: %1; left: %2px; top: %3px; width: %4px; height: %5px\"></div>\n")
					.arg(line->color().name())
					.arg(rect.x())
					.arg(rect.y())
					.arg(rect.width())
					.arg(rect.height());
			break;
		}

		case PdfItem::Rectangle: {
			PdfRectangle *rectangle = dynamic_cast<PdfRectangle*>(item);
			QRectF rect = rectangle->boundingRect();
			QString fill = rectangle->fillColor().isValid() ? QString("; background-color: ") + rectangle->fillColor().name() : QString();
			QString border;
			if(rectangle->penWidth() > 0) {
				border = QString("; border: %1px solid %2")
							.arg(qMax(1.0, rectangle->penWidth()))
							.arg(rectangle->borderColor().name());
			}
			html += QString("<div style=\"position: absolute%1%2; left: %3px; top: %4px; width: %5px; height: %6px\"></div>\n")
					.arg(fill)
					.arg(border)
					.arg(rect.x())
					.arg(rect.y())
					.arg(rect.width())
					.arg(rect.height());
			break;
		}

		default:
			break;
		}
	}

	html += "</body>\n</html>\n";
	return html;
}


QString PdfTOC::fontId(QString const& fontName)
{
	int index = fontName.indexOf('+');
	if(index == -1) {
		return fontName;
	} else {
		return fontName.left(index);
	}
}
