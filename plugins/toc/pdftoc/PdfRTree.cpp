#include <QtCore>
#include "PdfObjects.h"
#include "PdfRTree.h"

#define MIN_MAX(x)	qreal min[] = { x.left(), x.top() }; \
					qreal max[] = { x.right(), x.bottom() };

struct PdfRTreeContext
{
	PdfRTree *rtree;
	QList<PdfItem*> items;
};


PdfRTree::PdfRTree() :
	_remainingItems(-1)
{
}


void PdfRTree::clear()
{
	_tree.RemoveAll();
}


int PdfRTree::find(QRectF const& rectangle)
{
	MIN_MAX(rectangle);
	_remainingItems = -1;
	return _tree.Search(min, max, &emitNextItem, this);
}


QList<PdfItem*> PdfRTree::findAll(QRectF const& rectangle, int resultCount)
{
	MIN_MAX(rectangle);
	_remainingItems = resultCount;

	PdfRTreeContext context;
	context.rtree = this;
	_tree.Search(min, max, &appendNextItem, &context);
	return context.items;
}


void PdfRTree::insert(PdfItem *item)
{
	MIN_MAX(item->boundingRect());
	_tree.Insert(min, max, item);
}


void PdfRTree::remove(PdfItem *item)
{
	MIN_MAX(item->boundingRect());
	_tree.Remove(min, max, item);
}


void PdfRTree::stopSearch()
{
	_remainingItems = 0;
}


bool PdfRTree::appendNextItem(PdfItem *item, void *c)
{
	PdfRTreeContext *context = reinterpret_cast<PdfRTreeContext*>(c);
	context->items.append(item);
	if(context->rtree->_remainingItems > 0) {
		context->rtree->_remainingItems--;
	}
	emit context->rtree->itemFound(item);
	return context->rtree->_remainingItems != 0;
}


bool PdfRTree::emitNextItem(PdfItem *item, void *context)
{
	PdfRTree *tree = reinterpret_cast<PdfRTree*>(context);
	emit tree->itemFound(item);
	return tree->_remainingItems != 0;
}
