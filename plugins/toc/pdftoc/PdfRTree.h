#ifndef PDFRTREE_H
#define PDFRTREE_H 1

#include <QObject>
#include <QRectF>
#include "RTree.h"

class PdfItem;

/**
 * An R-Tree which contains PdfItems.
 */
class PdfRTree : public QObject
{
	Q_OBJECT

public:
	PdfRTree();
	void clear();
	int find(QRectF const& rectangle);
	QList<PdfItem*> findAll(QRectF const& rectangle, int resultCount = -1);
	void insert(PdfItem *item);
	void remove(PdfItem *item);

signals:
	void itemFound(PdfItem *item);

public slots:
	void stopSearch();

protected:
	static bool appendNextItem(PdfItem *item, void *context);
	static bool emitNextItem(PdfItem *item, void *context);

protected:
	RTree<PdfItem*, qreal, 2> _tree;
	int _remainingItems;
};

#endif
