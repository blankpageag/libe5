#ifndef PDFOBJECTS_H
#define PDFOBJECTS_H 1

#include <QColor>
#include <QLineF>
#include <QList>
#include <QRectF>
#include <QUrl>
#include "PdfRTree.h"

class PdfPage;
class PdfText;

class PdfItem
{
public:
	enum Type {
		Page = 0,
		Line,
		Rectangle,
		Text,
		TextBlock,
		TextLine,
		Image
	};

	virtual ~PdfItem() {}
	inline QRectF boundingRect() const				{ return _boundingRect; }
	inline qreal height() const						{ return _boundingRect.height(); }
	virtual bool includedInRTree() const;
	inline qreal left() const						{ return _boundingRect.x(); }
	PdfPage* page() const;
	inline PdfItem* parent() const					{ return _parent; }

	/**
	 * Returns the proportions of the bounding rectangle of the item relative to the page size.
	 * e.g. calling this method on an item with a bounding rectangle that takes half the size of the page and is
	 * centered on it, would return a QRectF(0.25, 0.25, 0.5, 0.5).
	 *
	 * Calling this method in a page always returns QRectF(0, 0, 1, 1).
	 */
	QRectF relativeBoundingRect() const;
	void setBoundingRect(QRectF const& rect);
	void setParent(PdfItem *parent);
	inline QSizeF size() const						{ return _boundingRect.size(); }
	inline qreal top() const						{ return _boundingRect.y(); }
	virtual Type type() const =0;
	QString typeString() const;		// FIXME: Remove later. Used only for debugging.
	inline qreal width() const						{ return _boundingRect.width(); }

protected:
	PdfItem(PdfItem *parent);
	virtual void notifyChildGeometryChange() {}

protected:
	QRectF _boundingRect;
	PdfItem *_parent;
};


class PdfPage : public PdfItem
{
public:
	PdfPage(int number);
	~PdfPage();
	void addItem(PdfItem *item);
	bool atSameY(qreal y1, qreal y2, qreal errorMargin = 0.0005) const;
	bool includedInRTree() const					{ return false; }
	inline PdfItem* itemAt(int i) const				{ return _items.at(i); }
	inline int itemCount() const					{ return _items.count(); }
	inline QList<PdfItem*> const& items() const		{ return _items; }
	inline int number() const						{ return _number; }
	inline PdfRTree* rtree() const					{ return _rtree; }
	PdfItem* takeItemAt(int index);
	QList<PdfText*> texts() const;
	Type type() const								{ return Page; }

protected:
	int _number;
	PdfRTree *_rtree;
	QList<PdfItem*> _items;
};


class PdfFont
{
public:
	PdfFont(QString const& name, QUrl const& source);
	inline QString name() const		{ return _name; }
	inline QUrl source() const		{ return _source; }

protected:
	QString _name;
	QUrl _source;
};


class PdfImage : public PdfItem
{
public:
	PdfImage(QUrl const& source, QSize const& sourceSize, PdfItem *parent);
	inline QUrl source() const		{ return _source; }
	inline QSize sourceSize() const	{ return _sourceSize; }
	inline int sourceWidth() const	{ return _sourceSize.width(); }
	inline int sourceHeight() const	{ return _sourceSize.height(); }
	Type type() const				{ return Image; }

protected:
	QUrl _source;
	QSize _sourceSize;
};


class PdfLine : public PdfItem
{
public:
	PdfLine(QLineF const& line, QColor const& color, qreal penWidth, PdfItem *parent);
	inline QColor color() const		{ return _color; }
	inline QPointF end() const		{ return _line.p2(); }
	bool isMostlyHorizontal() const;
	bool isMostlyVertical() const;
	inline QLineF line() const		{ return _line; }
	inline qreal penWidth() const	{ return _penWidth; }
	inline QPointF start() const	{ return _line.p1(); }
	Type type() const				{ return Line; }

protected:
	QColor _color;
	qreal _penWidth;
	QLineF _line;
};


class PdfRectangle : public PdfItem
{
public:
	PdfRectangle(QColor const& fillColor, QColor const& borderColor, qreal penWidth, PdfItem *parent);
	inline QColor borderColor() const	{ return _borderColor; }
	inline QColor fillColor() const		{ return _fillColor; }
	inline qreal penWidth() const		{ return _penWidth; }
	Type type() const					{ return Rectangle; }

protected:
	QColor _borderColor;
	QColor _fillColor;
	qreal _penWidth;
};


class PdfText : public PdfItem
{
public:
	enum TextSemantics {
		StandardText,
		Title,
		ImageLegend
	};

	PdfText(QColor const& color, QString const& fontName, qreal fontSize, qreal rotation, QString const& text, PdfItem *parent);
	inline QColor color() const				{ return _color; }
	inline QString fontName() const			{ return _fontName; }
	inline qreal fontSize() const			{ return _fontSize; }
	bool includedInRTree() const;
	inline qreal rotation() const			{ return _rotation; }
	inline TextSemantics semantics() const	{ return _semantics; }
	void setSemantics(TextSemantics semantics);
	inline QString text() const				{ return _text; }
	Type type() const						{ return Text; }

protected:
	QColor _color;
	QString _fontName;
	qreal _fontSize;
	qreal _rotation;
	TextSemantics _semantics;
	QString _text;
};


class PdfTextLine : public PdfItem
{
public:
	PdfTextLine(PdfItem *parent = 0);
	void addText(PdfText *text);
	bool includedInRTree() const;
	void insertText(int index, PdfText *text);
	PdfText* textAt(int i) const	{ return _texts.at(i); }
	int textCount() const			{ return _texts.count(); }
	QList<PdfText*> texts() const	{ return _texts; }
	Type type() const				{ return TextLine; }

protected:
	void notifyChildGeometryChange();

protected:
	QList<PdfText*> _texts;
};


class PdfTextBlock : public PdfItem
{
public:
	PdfTextBlock(PdfItem *parent);
	void addTextLine(PdfTextLine *line);
	PdfTextLine* textLineAt(int i) const	{ return _textLines.at(i); }
	int textLineCount() const				{ return _textLines.count(); }
	QList<PdfTextLine*> textLines() const	{ return _textLines; }
	Type type() const						{ return TextBlock; }

protected:
	void notifyChildGeometryChange();

protected:
	QList<PdfTextLine*> _textLines;
};


class PdfDocument
{
public:
	PdfDocument();
	~PdfDocument();
	void addFont(PdfFont *font);
	void addPage(PdfPage *page);
	void addToLastPage(PdfItem *item);
	void clear();
	bool isEmpty() const;
	inline PdfFont* font(QString const& name) const		{ return _fonts.value(name); }
	inline QHash<QString, PdfFont*> fonts() const		{ return _fonts; }
	inline PdfPage* lastPage() const					{ return _pages.last(); }
	inline PdfPage* page(int number) const				{ return _pages.at(number); }
	inline int pageCount() const						{ return _pages.count(); }
	inline QList<PdfPage*> pages() const				{ return _pages; }

protected:
	QList<PdfPage*> _pages;
	QHash<QString, PdfFont*> _fonts;
};

#endif
