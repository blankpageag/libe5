#ifndef PDFIMAGECONVERTER_H
#define PDFIMAGECONVERTER_H 1

#include "Container"

class PdfImageConverterPrivate;

/**
 * Converts CMYK jpeg images to RGB using ICC color profiles.
 */
class PdfImageConverter
{
public:
	explicit PdfImageConverter(QString const& sourceProfile = ":/ICC/CMYK Profiles/USWebUncoated.icc", QString const& destinationProfile = ":/ICC/RGB Profiles/AppleRGB.icc");
	explicit PdfImageConverter(QIODevice *sourceProfile, QIODevice *destinationProfile);
	~PdfImageConverter();
	bool convert(Container * const container, QString const& imagePath) const;

private:
	PdfImageConverterPrivate *d;
};

#endif
