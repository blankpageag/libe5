#include <QtCore>
#include "PdfToIpeInvoker.h"

PdfToIpeInvoker::PdfToIpeInvoker(QIODevice *sourcePdf, QObject *parent) :
	PdfToolInvoker(sourcePdf, parent)
{
}


QStringList PdfToIpeInvoker::arguments() const
{
	QStringList args;
	args << "-merge";
	args << "0";			// 0 places the text in a more realistic location. 1 makes the text more readable.
	args << "%INPUT_FILE%";
	args << "%OUTPUT_FILE%";
	return args;
}


QString PdfToIpeInvoker::toolName() const
{
	return "pdftoipe";
}


QString PdfToIpeInvoker::toolPath() const
{
	return QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pdftoipe");
}
