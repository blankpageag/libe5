#ifndef PDFTOOLINVOKER_H
#define PDFTOOLINVOKER_H 1

#include <QProcess>
#include <QTemporaryFile>

class PdfToolInvoker : public QObject
{
	Q_OBJECT

public:
	virtual ~PdfToolInvoker();
	inline QString error() const { return _error; }
	QIODevice* output() const;

signals:
	void failed(QString const& errorMessage);
	void finished();

public slots:
	void invoke();

protected slots:
	void processError(QProcess::ProcessError error);
	void processFinished(int exitCode, QProcess::ExitStatus exitStatus);

protected:
	PdfToolInvoker(QIODevice *sourcePdf, QObject *parent = 0);
	virtual QStringList arguments() const =0;
	virtual void setFailed(QString const& error);
	virtual void setFinished();
	virtual QString toolName() const =0;
	virtual QString toolPath() const =0;

private:
	QString _error;
	QIODevice *_sourcePdf;
	QTemporaryFile _pdfFile;
	mutable QTemporaryFile _outputFile;
	QProcess *_process;
};

#endif
