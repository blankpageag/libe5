#include <QtCore>
#include <e5Logger>
#include "ArticleContent.h"

ArticleContent::ArticleContent(LogicalArticle *article, TOCEntry *tocEntry, QString const &filename) :
	Content(tocEntry, filename),
	_article(article)
{
	_generated = true;
}


ArticleContent::~ArticleContent()
{
	delete _article;
}


void ArticleContent::createHtml()
{
	_html.setContent(_article->createHtml());
}
