#ifndef _TETML_ITEM_
#define _TETML_ITEM_ 1

#include <Content>
#include <e5Logger>
#include <QDomElement>
#include <QRectF>
#include <QList>

namespace e5TetmlToc 
{
	class Paragraph;

	struct Glyph
	{
		Glyph() : _size(0) {}

		QString _font;
		qreal _size;
	};
	
	/**
	 * Basic item that is extracted from the tetml file. It subclassed QDomElement so one can access directly additional xml data (see QDomELement). Get x, y position and with and height of the boundary area. Print basic info via info().
	 * @author Lukas N Mueller
	 */
	class tetmlItem
	{
	public:
		enum Type {
			UnknownItem = 0,
			ImageItem,
			PageItem,
			TextBlockItem,
			StandardParagraphItem,
			TitleParagraphItem,
			ImageLegendParagraphItem,
			WordItem
		};

		virtual ~tetmlItem(){}

		QRectF area() const;
		qreal bottom() const { return area().bottom(); }
		inline QDomElement domElement() const { return _element; }
		qreal height() const { return area().height(); }
		int id() const { return _id; }
		virtual void info();
		qreal right() const { return area().right(); }
		virtual Type type() const =0;
		qreal width() const { return area().width(); }
		qreal x() const { return area().x(); }
		qreal y() const { return area().y(); }

	protected:
		tetmlItem(int id, QDomElement const& element);
		virtual void calculateArea() const =0;
		QString htmlEscape(QString const& string) const;

	protected:
		int _id;
		QDomElement _element;
		mutable QRectF _area;

	private:
		Q_DISABLE_COPY(tetmlItem)
	};
	
	
	/**
	 * Represents a word in the pdf.
	 * @author Lukas N Mueller
	 */
	class Word : public tetmlItem
	{
	public:
		Word(QDomElement const& element);
		virtual Glyph font() const;
		QString formattedText() const;
		void info() { qDebug() <<"Text: " <<text(); tetmlItem::info(); }
		void setY(qreal y);
		QString text() const;
		Type type() const { return WordItem; }

	protected:
		void calculateArea() const;

	private:
		Q_DISABLE_COPY(Word)

	private:
		static int WordIDCounter;
	};

	
	/**
	 * Represents a placed image in the document.
	 * @author Lukas N Mueller
	 */
	class Image : public tetmlItem
	{
	public:
		enum Alignment {
			AlignLeft,
			AlignCenter,
			AlignRight
		};

		Image(QDomElement const& element);
		inline Alignment alignment() const { return _alignment; }
		void info();
		inline QString imagePath() const { return _imagePath; }
		inline bool inlineImage() const { return _inlineImage; }
		inline Paragraph* legend() const { return _legend; }
		QString name() { return _element.attribute("image"); }
		void setAlignment(Alignment alignment) { _alignment = alignment; }
		void setImagePath(QString const& path) { _imagePath = path; }
		void setInline(bool i) { _inlineImage = i; }
		void setLegend(Paragraph *legend);
		void setY(qreal y);
		Type type() const { return ImageItem; }

	protected:
		void calculateArea() const;

	private:
		Q_DISABLE_COPY(Image)

	private:
		static int ImageIDCounter;
		QString _imagePath;
		Alignment _alignment;
		bool _inlineImage;
		Paragraph *_legend;
	};


	/**
	 * Represents a block consisting of words and places images. It belongs to a paragraph.
	 */
	class TextBlock : public tetmlItem
	{
	public:
		TextBlock(QDomElement const& element);
		~TextBlock();
		void adjustFont(QHash<QString, QString> const& iFontMap );
		void appendWord(Word *word);
		const Glyph font() const;
		QString formattedText();
		QString text() const;
		Type type() const { return TextBlockItem; }
		inline int wordCount() const { return _words.count(); }
		inline const QList<Word*>& words() const { return _words; }

	protected:
		void calculateArea() const;

	private:
		Q_DISABLE_COPY(TextBlock)

	private:
		static int TextBlockIDCounter;
		QList<Word*> _words;
		QList<Image*> _images;
	};

	/**
	 * Represents a paragraph consisting of text blocks. Usually it contains only one text block, unless the paragraph
	 * is split across several columns. The area represents the bounding rectangle of the paragraph in the document, but
	 * it might not be a representative value if the paragraph contains more than one text block.
	 * @author Lukas N Mueller
	 */
	class Paragraph : public tetmlItem
	{
	public:
		Paragraph(QDomElement const& element);
		~Paragraph();
		void appendTextBlock(TextBlock *block);
		QString formattedText();
		bool isTitle() const;
		void setLegendFor(Image *image);
		void setType(Type type);
		QString text() const;
		inline TextBlock* textBlockAt(int i) const { return _blocks.at(i); }
		inline int textBlockCount() const { return _blocks.count(); }
		inline const QList<TextBlock*>& textBlocks() const { return _blocks; }
		Type type() const;
		
	protected:
		void calculateArea() const;

	private:
		Q_DISABLE_COPY(Paragraph)

	private:
		static int ParagraphIDCounter;
		Type _type;
		QList<TextBlock*> _blocks;
		Image *_legendImage;
	};
	
	
	/**
	 * Represents a page in the pdf containing paragraph.
	 * @author Lukas N Mueller
	 */
	class Page : public tetmlItem
	{
		
	public:
		Page(int id, QDomElement const& element);
		~Page();
		void addImage(Image *i);
		void addParagraph(Paragraph *p);
		void adjustFont(QHash<QString, QString> const& fontMap);
		void associateImagesToParagraphs();
		inline Image* imageAt(int i) const { return _images.at(i); }
		inline int imageCount() const { return _images.count(); }
		const QList<Image*>& images() const { return _images;}
		virtual void info();
		qreal maximumColumnSeparation() const;
		inline int pageNumber() const { return _element.attribute("number").toInt(); }
		inline Paragraph* paragraphAt(int i) const { return _paragraphs.at(i); }
		inline int paragraphCount() const { return _paragraphs.count(); }
		const QList<Paragraph*>& paragraphs() const { return _paragraphs; }

		/**
		 * Returns the distance between two positions @a a and @a b relative to the size of the page.
		 * The positions may be horizontal or vertical positions, according to @a orientation.
		 *
		 * For instance, the distance between an y position located at the top of the page and an y
		 * position located in the center of the page would be 0.5.
		 */
		qreal relativeDistance(qreal a, qreal b, Qt::Orientation orientation = Qt::Vertical) const;

		qreal relativeWidth(qreal absoluteWidth) const;

		/**
		 * Compares x1 and x2 and returns true if they are at the same width in the page.
		 *
		 * The comparison is fuzzy and accepts an error margin of up to 0.5% the width of the page.
		 *
		 * For example, an x of 310.31 and another of 313.58 in a page with a width of 1291 pixels are considered the
		 * same (0.25% difference). However, an x of 200 and an x of 250 are considered different, thus sameX() would
		 * return false.
		 */
		bool sameX(qreal x1, qreal x2) const;

		/**
		 * Compares y1 and y2 and returns true if they are at the same height in the page.
		 *
		 * The comparison is fuzzy and accepts an error margin of up to 0.5% the height of the page.
		 *
		 * For example, an y of 310.31 and another of 313.58 in a page with a height of 1291 pixels are considered the
		 * same (0.25% difference). However, an y of 200 and an y of 250 are considered different, thus sameY() would
		 * return false.
		 */
		bool sameY(qreal y1, qreal y2) const;

		QString text();
		Type type() const { return PageItem; }

	protected:
		void calculateArea() const;

	private:
		Q_DISABLE_COPY(Page)

	private:
		QList<Paragraph*> _paragraphs;
		QList<Image*> _images;
	};
}

#endif
