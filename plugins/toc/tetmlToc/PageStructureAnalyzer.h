#ifndef PAGESTRUCTUREANALYZER_H
#define PAGESTRUCTUREANALYZER_H 1

#include "LogicalArticle.h"
#include "tetmlItem.h"

class PageStructureAnalyzer
{
public:
	PageStructureAnalyzer(e5TetmlToc::Page *page);
	QList<LogicalArticle*> analyze();
	inline QList<qreal> breakingLines() const { return _breakingLines; }	// Required by tetmlToc::debugHighlightParagraphs()

private:
	void associateImageToArticle(e5TetmlToc::Image *image, LogicalArticle *article);
	bool breakingLineExistsBetween(qreal y1, qreal y2) const;
	QMultiMap<qreal, LogicalArticle*> coveredAreaMap(e5TetmlToc::tetmlItem *item, qreal *maxIntersectionRatio = 0) const;
	void createDefaultArticle();
	LogicalArticle* findClosestArticle(e5TetmlToc::tetmlItem *item) const;
	e5TetmlToc::Paragraph* findNextColumn(LogicalArticle *article, QRectF const& title);
	e5TetmlToc::Paragraph* findParagraphBelow(QPointF const& startPoint, QRectF const& title);
	bool isItemBelow(e5TetmlToc::tetmlItem *item, QPointF const& start, QRectF const& title);
	bool isItemOnTheRight(e5TetmlToc::tetmlItem *item, QPointF const& start);
	void readColumn(LogicalArticle *article, QPointF const& startPoint, QRectF const& title);
	bool titleExistsBelow(qreal y) const;

private:
	e5TetmlToc::Page *_page;
	e5TetmlToc::Paragraph *_footer;
	QList<qreal> _breakingLines;
	QList<LogicalArticle*> _articles;
	QHash<e5TetmlToc::tetmlItem*, LogicalArticle*> _ownerArticle;
};

#endif
