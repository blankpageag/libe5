#include <QtCore>
#include "tetmlItem.h"

using namespace e5TetmlToc; 

int Word::WordIDCounter = 0;
int TextBlock::TextBlockIDCounter = 0;
int Paragraph::ParagraphIDCounter = 0;
int Image::ImageIDCounter = 0;

tetmlItem::tetmlItem(int id, QDomElement const& element) :
	_id(id),
	_element(element)
{
}


QRectF tetmlItem::area() const
{
	if(!_area.isValid()) {
		calculateArea();
	}

	return _area;
}


void tetmlItem::info()
{
	qDebug()<<"Area: "<<area();
}


QString tetmlItem::htmlEscape(QString const& string) const
{
	QString out;
	for(int i = 0; i < string.length(); i++) {
		QChar c = string.at(i);
		if(c == '\n') {
			out += "<br/>\n";
		} else if(c == '<') {
			out += "&lt;";
		} else if(c == '>') {
			out += "&gt;";
		} else if(c == '&') {
			out += "&amp;";
		} else if(c.unicode() > 127) {
			out += "&#" + QString::number(c.unicode()) + ";";
		} else if(c != '\r') {
			out += c;
		}
	}
	return out;
}




Word::Word(QDomElement const& element) :
	tetmlItem(WordIDCounter++, element)
{
}


Glyph Word::font() const
{ 
	Glyph g;
	g._font = _element.elementsByTagName("Box").at(0).toElement().elementsByTagName("Glyph").at(0).toElement().attribute("font");
	g._size = _element.elementsByTagName("Box").at(0).toElement().elementsByTagName("Glyph").at(0).toElement().attribute("size").toDouble();

	return g;
}


QString Word::formattedText() const
{
	QStringList notSpace;
	notSpace <<"."<<","<<"!"<<"?"<<":";

	QString fT;
	fT.append("<span style=\"font-size: ");
	fT.append( QString::number( font()._size ));
	fT.append("px; ");
	fT.append("font-family: ");
	fT.append( font()._font );
	fT.append("; \">");
	fT.append( text() );

	if( !notSpace.contains(text())) {
		fT.append( " " );
	}
	fT.append("</span>");

	return fT;
}


void Word::setY(qreal y)
{
	area();		// Force the calculation of the area if not calculated previously

	QDomElement boxElement = _element.elementsByTagName("Box").at(0).toElement();
	boxElement.setAttribute("ury", y);
	boxElement.setAttribute("lly", y - height());
	calculateArea();
}


QString Word::text() const
{
	if( !_element.elementsByTagName("Text").isEmpty() ) {
		QString word = _element.elementsByTagName("Text").at(0).toElement().text();
		return htmlEscape(word);
	}
	return QString();
}


void Word::calculateArea() const
{
	QDomElement boxElement = _element.elementsByTagName("Box").at(0).toElement();
	_area = QRectF(boxElement.attribute("llx").toDouble(),
				   boxElement.attribute("ury").toDouble(),
				   boxElement.attribute("urx").toDouble() - boxElement.attribute("llx").toDouble(),
				   boxElement.attribute("ury").toDouble() - boxElement.attribute("lly").toDouble());
}




Image::Image(QDomElement const& element) :
	tetmlItem(ImageIDCounter++, element),
	_alignment(AlignCenter),
	_inlineImage(false),
	_legend(0)
{
}


void Image::info()
{
	qDebug();
	qDebug() <<"Image information (id="<<id()<<")";
	qDebug()<<"Name: "<< name();
	qDebug()<<"Area: "<< area();
	qDebug();
}


void Image::setLegend(Paragraph *legend)
{
	if(legend != _legend) {
		_legend = legend;
		_legend->setLegendFor(this);
	}
}


void Image::setY(qreal y)
{
	_element.setAttribute("y", y - height());
	calculateArea();
}


void Image::calculateArea() const
{
	qreal x = _element.attribute("x").toDouble();
	qreal y = _element.attribute("y").toDouble();
	qreal width = _element.attribute("width").toDouble();
	qreal height = _element.attribute("height").toDouble();
	_area = QRectF(x, y + height, width, height);
}




TextBlock::TextBlock(QDomElement const& element) :
	tetmlItem(TextBlockIDCounter++, element)
{
}


TextBlock::~TextBlock()
{
	qDeleteAll(_words);
}


void TextBlock::adjustFont(QHash<QString,QString> const& fontMap)
{
	foreach(Word *word, _words) {
		Glyph glyph = word->font();
		QString font = fontMap.value(glyph._font);
		QDomElement glyphElement = word->domElement().elementsByTagName("Box").at(0).toElement().elementsByTagName("Glyph").at(0).toElement();
		glyphElement.setAttribute("font", font);
	}
}


void TextBlock::appendWord(Word *word)
{
	if(word) {
		_words.append(word);
	}
}


const Glyph TextBlock::font() const
{
	return _words.first()->font();
}


QString TextBlock::formattedText()
{
	QString l;
	foreach(Word *w, _words) {
		l.append( w->formattedText() );
	}
	return l;
}


QString TextBlock::text() const
{
	QString l;
	qreal lastX = -1;
	qreal lastY = -1;
	foreach(Word *w, _words ) {
		QString wordText = w->text();

		bool isSymbol = (wordText == "." || wordText == ",");
		int xMargin = isSymbol ? 4 : 1;

		if(lastY != -1 && w->y() > lastY + 5 && !isSymbol) {
			l.append(" ");
		} else if(lastX != -1 && w->x() > lastX + xMargin) {
			l.append(" ");
		}

		l.append(wordText);

		lastX = w->right();
		lastY = w->y();
	}
	return l;
}


void TextBlock::calculateArea() const
{
	_area = QRectF();

	foreach(Word *word, _words) {
		_area = _area.united(word->area());
	}
}





Paragraph::Paragraph(QDomElement const& element) :
	tetmlItem(ParagraphIDCounter++, element),
	_type(StandardParagraphItem),
	_legendImage(0)
{
}


Paragraph::~Paragraph()
{
	qDeleteAll(_blocks);
}


void Paragraph::appendTextBlock(TextBlock *block)
{
	if(block) {
		_blocks.append(block);
	}
}


QString Paragraph::formattedText()
{
	QString text;
	foreach(TextBlock *block, _blocks) {
		text.append(block->formattedText());
	}
	return text;
}


bool Paragraph::isTitle() const
{
	return type() == TitleParagraphItem;
}


void Paragraph::setLegendFor(Image *image)
{
	if(image != _legendImage) {
		setType(ImageLegendParagraphItem);
		_legendImage = image;
		image->setLegend(this);
	}
}

void Paragraph::setType(Type type)
{
	if(type != StandardParagraphItem && type != TitleParagraphItem && type != ImageLegendParagraphItem) {
		return;
	}

	_type = type;
}


QString Paragraph::text() const
{
	QString t;
	for(int i = 0; i < _blocks.count(); i++) {
		if(i > 0) {
			t += " ";
		}
		t += _blocks.at(i)->text();
	}
	return t;
}


tetmlItem::Type Paragraph::type() const
{
	return _type;
}


void Paragraph::calculateArea() const
{
	_area = QRectF();

	foreach(TextBlock *block, _blocks) {
		_area = _area.united(block->area());
	}
}




Page::Page(int id, QDomElement const& element) :
	tetmlItem(id, element)
{
}


void Page::associateImagesToParagraphs()
{ 	
	qDebug() <<"# Images: "<<_images.count();
	foreach(Image *i, _images) {
		foreach(Paragraph *p, _paragraphs) {
			if(p->area().contains( i->area() )) {
				//qDebug() << "Para "<<p->id() << " contains image "<<i->name();
				break;
			}
		}
	}
}


Page::~Page()
{
	qDeleteAll(_paragraphs);
	qDeleteAll(_images);
}


void Page::addImage(Image *i)
{
	i->setY(height() - i->y());
	_images.append(i);
}


void Page::addParagraph(Paragraph *p)
{
	_paragraphs.append(p);
}


void Page::adjustFont(QHash<QString,QString> const& fontMap )
{
	foreach(Paragraph *p, _paragraphs) {
		foreach(TextBlock *block, p->textBlocks()) {
			block->adjustFont(fontMap);
		}
	}
}


void Page::info()
{
	qDebug();
	qDebug()<<"Page #"<<pageNumber() << " (id="<<id()<<")";
	qDebug() <<"# Paragraph: "<<_paragraphs.count();

	foreach(Paragraph *p, _paragraphs) {
		//p->info();
	}

	qDebug() <<"# Images: "<<_images.count();
	foreach(Image *i, _images) {
		//i->info();
	}
	qDebug();
}


qreal Page::maximumColumnSeparation() const
{
	return width() * 0.05;
}


qreal Page::relativeDistance(qreal a, qreal b, Qt::Orientation orientation) const
{
	qreal relativeValue = (orientation == Qt::Horizontal) ? width() : height();
	if(relativeValue == 0) {
		return 0;
	}

	return (b - a) / relativeValue;
}


qreal Page::relativeWidth(qreal absoluteWidth) const
{
	if(width() <= 0) {
		return 0;
	}

	return absoluteWidth / width();
}


bool Page::sameX(qreal x1, qreal x2) const
{
	return qAbs(x1 - x2) < (width() * 0.009);
}


bool Page::sameY(qreal y1, qreal y2) const
{
	return qAbs(y1 - y2) < (height() * 0.009);
}


void Page::calculateArea() const
{
	_area = QRectF(0, 0, _element.attribute("width").toDouble(), _element.attribute("height").toDouble());
}
