#ifndef LOGICALARTICLE_H
#define LOGICALARTICLE_H 1

#include <QList>
#include <QSet>
#include "tetmlItem.h"

class LogicalArticle
{
public:
	explicit LogicalArticle(e5TetmlToc::Page *page);

	void addTitle(e5TetmlToc::Paragraph *title);
	void addColumnStart(e5TetmlToc::tetmlItem *item);
	void addContents(e5TetmlToc::tetmlItem *item);			// Can be a paragraph or an image
	QRectF boundingRect() const;
	inline QList<e5TetmlToc::tetmlItem*> const& contents() const { return _contents; }
	QRectF contentsBoundingRect() const;
	QString createHtml() const;
	int findSuitablePosition(e5TetmlToc::tetmlItem *item) const;
	void insertContents(int index, e5TetmlToc::tetmlItem *item);
	inline bool isColumnStart(e5TetmlToc::tetmlItem *item) const { return _columnStarts.contains(item); }
	inline e5TetmlToc::Page* page() const { return _page; }
	void setComplete(bool complete);
	void setTitles(QList<e5TetmlToc::Paragraph*> const& titles);
	e5TetmlToc::Paragraph* subtitle() const;
	e5TetmlToc::Paragraph* title() const;
	inline QList<e5TetmlToc::Paragraph*> const& titles() const { return _titles; }
	QRectF titlesBoundingRect() const;

	/**
	 * Returns the bounding rect of the title if there is a title. Otherwise returns the bounding rect
	 * of the contents.
	 */
	QRectF titleOrContentsBoundingRect() const;

private:
	void calculateBoundingRect() const;		// const so it can be called from boundingRect() and co.

private:
	e5TetmlToc::Page *_page;
	bool _complete;
	QList<e5TetmlToc::Paragraph*> _titles;
	QList<e5TetmlToc::tetmlItem*> _contents;
	QSet<e5TetmlToc::tetmlItem*> _columnStarts;
	mutable QRectF _boundingRect;
	mutable QRectF _contentsRect;
	mutable QRectF _titlesRect;
};

#endif
