#include <QtCore>
#include "PageStructureAnalyzer.h"

#define MAX_ALIGNMENT_OFFSET 0.005
#define MAX_SUBTITLE_SPACING 0.005

#define MAX_IMAGELEGEND_LEFTOFFSET_NEGATIVE -0.05
#define MAX_IMAGELEGEND_LEFTOFFSET_POSITIVE 0.1
#define MAX_IMAGELEGEND_RIGHTOFFSET_NEGATIVE -0.5
#define MAX_IMAGELEGEND_RIGHTOFFSET_POSITIVE 0.05
#define MAX_IMAGELEGEND_RELATIVE_DISTANCE 0.0025
#define MAX_IMAGELEGEND_WORDS 32

#define MIN_BREAKINGLINE_RELATIVE_WIDTH 0.75
#define MAX_BREAKINGLINE_APPLICABLE_HEIGHT 0.005

#define MAX_STDPARAGRAPH_TITLE_OFFSET 0.25
#define MAX_ARTICLE_STDPARAGRAPH_OFFSET 0.05

class TitleExtractor
{
public:
	TitleExtractor(e5TetmlToc::Page *page);
	QList<e5TetmlToc::Paragraph*> tagTitles();
	inline int commonFontSize() const { return _commonFontSize; }

private:
	int averageParagraphSize(e5TetmlToc::Paragraph *paragraph) const;
	QHash<int, int> findFontSizeFrequencies();
	int mostFrequentSize(QHash<int, int> const& fontSizeFrequency) const;

private:
	e5TetmlToc::Page *_page;
	int _commonFontSize;
};


class ImageLegendFinder
{
public:
	ImageLegendFinder(e5TetmlToc::Page *page);
	QList<e5TetmlToc::Paragraph*> tagImageLegends();

private:
	bool isParagraphAligned(const e5TetmlToc::Image *image, const e5TetmlToc::Paragraph *paragraph) const;

private:
	e5TetmlToc::Page *_page;
};


class BreakingLineFinder
{
public:
	BreakingLineFinder(e5TetmlToc::Page *page);
	QList<qreal> findBreakingLines();

private:
	e5TetmlToc::Page *_page;
};


/**
 * Groups titles that are one under the other into title and subtitles.
 */
class TitleGrouper
{
public:
	TitleGrouper(e5TetmlToc::Page *page);
	QList< QList<e5TetmlToc::Paragraph*> > groupTitles(QList<e5TetmlToc::Paragraph*> titles);

protected:
	bool isItemBelow(const e5TetmlToc::tetmlItem *above, const e5TetmlToc::tetmlItem *below);
	e5TetmlToc::tetmlItem* findItemBelow(const e5TetmlToc::tetmlItem *item);

private:
	e5TetmlToc::Page *_page;
};


class FooterFinder
{
public:
	FooterFinder(e5TetmlToc::Page *page);
	e5TetmlToc::Paragraph* findFooter() const;

private:
	e5TetmlToc::Page *_page;
};




TitleExtractor::TitleExtractor(e5TetmlToc::Page *page) :
	_page(page),
	_commonFontSize(0)
{
}


QList<e5TetmlToc::Paragraph*> TitleExtractor::tagTitles()
{
	_commonFontSize = mostFrequentSize(findFontSizeFrequencies()) + 1;

	QList<e5TetmlToc::Paragraph*> titles;
	const QList<e5TetmlToc::Paragraph*> paragraphs = _page->paragraphs();
	foreach(e5TetmlToc::Paragraph *paragraph, paragraphs) {
		int avgFontSize = averageParagraphSize(paragraph);
		if(avgFontSize > _commonFontSize) {
			paragraph->setType(e5TetmlToc::tetmlItem::TitleParagraphItem);
			titles << paragraph;
		}
	}

	return titles;
}


int TitleExtractor::averageParagraphSize(e5TetmlToc::Paragraph *paragraph) const
{
	int count = 0;
	qreal sum = 0;

	const QList<e5TetmlToc::TextBlock*> textBlocks = paragraph->textBlocks();
	foreach(e5TetmlToc::TextBlock *textBlock, textBlocks) {
		const QList<e5TetmlToc::Word*> words = textBlock->words();
		foreach(e5TetmlToc::Word *word, words) {
			qreal size = word->font()._size;
			if(size > 0) {
				sum += size;
				count++;
			}
		}
	}

	if(count > 0) {
		return qRound(sum / count);
	} else {
		return 0;		// Prevent division by 0
	}
}


QHash<int, int> TitleExtractor::findFontSizeFrequencies()
{
	QHash<int, int> frequencies;

	const QList<e5TetmlToc::Paragraph*> paragraphs = _page->paragraphs();
	foreach(e5TetmlToc::Paragraph *paragraph, paragraphs) {
		const QList<e5TetmlToc::TextBlock*> textBlocks = paragraph->textBlocks();
		foreach(e5TetmlToc::TextBlock *textBlock, textBlocks) {
			const QList<e5TetmlToc::Word*> words = textBlock->words();
			foreach(e5TetmlToc::Word *word, words) {
				qreal size = word->font()._size;
				if(size > 0) {
					frequencies[ qRound(size) ]++;
				}
			}
		}
	}

	return frequencies;
}


int TitleExtractor::mostFrequentSize(QHash<int, int> const& fontSizeFrequency) const
{
	int fontSize = 0;
	int maxFrequency = -1;

	QHashIterator<int, int> it(fontSizeFrequency);
	while(it.hasNext()) {
		it.next();
		if(it.value() > maxFrequency) {
			fontSize = it.key();
			maxFrequency = it.value();
		}
	}

	return fontSize;
}




ImageLegendFinder::ImageLegendFinder(e5TetmlToc::Page *page) :
	_page(page)
{
}


QList<e5TetmlToc::Paragraph*> ImageLegendFinder::tagImageLegends()
{
	QList<e5TetmlToc::Paragraph*> imageLegends;

	// Select the standard paragraphs as possible image footer candidates
	// Don't accept paragraphs with more than MAX_IMAGELEGEND_WORDS words
	QSet<e5TetmlToc::Paragraph*> nonLegends;
	for(int i = 0; i < _page->paragraphCount(); i++) {
		e5TetmlToc::Paragraph *paragraph = _page->paragraphAt(i);
		if(paragraph->type() == e5TetmlToc::tetmlItem::StandardParagraphItem && paragraph->textBlockCount() == 1 && paragraph->textBlockAt(0)->wordCount() <= MAX_IMAGELEGEND_WORDS) {
			nonLegends.insert(paragraph);
		}
	}

	// Search for image footers of all images
	foreach(e5TetmlToc::Image *image, _page->images()) {
		e5TetmlToc::Paragraph *closestParagraph = 0;

		// Look for a candidate paragraph located under the image
		foreach(e5TetmlToc::Paragraph *paragraph, nonLegends) {
			if(paragraph->y() < image->bottom()) {
				continue;
			}

			if(!isParagraphAligned(image, paragraph)) {
				continue;
			}

			if(!closestParagraph || paragraph->y() < closestParagraph->y()) {
				closestParagraph = paragraph;
			}
		}

		if(closestParagraph) {
			if(qAbs(_page->relativeDistance(image->bottom(), closestParagraph->y())) < MAX_IMAGELEGEND_RELATIVE_DISTANCE) {
				image->setLegend(closestParagraph);
				imageLegends.append(closestParagraph);
				nonLegends.remove(closestParagraph);
				qDebug() << "LEGEND FOR" << image->id() << "FOUND:" << closestParagraph->id() << closestParagraph->text();
			}
		}
	}

	return imageLegends;
}


bool ImageLegendFinder::isParagraphAligned(const e5TetmlToc::Image *image, const e5TetmlToc::Paragraph *paragraph) const
{
	qreal widthFactor = _page->relativeWidth(image->width());
	if(widthFactor == 0) {
		return false;
	}
	widthFactor = 1 / widthFactor;

	qreal leftDistance = _page->relativeDistance(image->x(), paragraph->x(), Qt::Horizontal) * widthFactor;
	qreal rightDistance = _page->relativeDistance(image->right(), paragraph->right(), Qt::Horizontal) * widthFactor;

	return (leftDistance > MAX_IMAGELEGEND_LEFTOFFSET_NEGATIVE && leftDistance < MAX_IMAGELEGEND_LEFTOFFSET_POSITIVE &&
			rightDistance > MAX_IMAGELEGEND_RIGHTOFFSET_NEGATIVE && rightDistance < MAX_IMAGELEGEND_RIGHTOFFSET_POSITIVE);
}




BreakingLineFinder::BreakingLineFinder(e5TetmlToc::Page *page) :
	_page(page)
{
}


QList<qreal> BreakingLineFinder::findBreakingLines()
{
	QList<e5TetmlToc::tetmlItem*> pendingItems;
	pendingItems.reserve(_page->paragraphCount() + _page->imageCount());

	for(int i = 0; i < _page->paragraphCount(); i++) {
		pendingItems << _page->paragraphAt(i);
	}
	for(int i = 0; i < _page->imageCount(); i++) {
		pendingItems << _page->imageAt(i);
	}

	QList<qreal> breakingLines;
	for(int i = 0; i < pendingItems.count(); i++) {
		e5TetmlToc::tetmlItem *item = pendingItems.at(i);
		if(item->type() == e5TetmlToc::tetmlItem::ImageItem || item->type() == e5TetmlToc::tetmlItem::TitleParagraphItem) {
			qreal y = item->y();
			bool isLine = true;

			// Find all other items located at the left or the right of this item
			QList<e5TetmlToc::tetmlItem*> alignedItems;
			bool titleFound = false;
			for(int j = 0; j < pendingItems.count(); j++) {
				e5TetmlToc::tetmlItem *otherItem = pendingItems.at(j);

				// If the y coordinate of otherItem is roughly the same as of item, they are top-aligned
				if(_page->sameY(y, otherItem->y())) {
					// If the otherItem is an image or a title, we accept it as an aligned item
					if(otherItem->type() == e5TetmlToc::tetmlItem::ImageItem || otherItem->type() == e5TetmlToc::tetmlItem::TitleParagraphItem) {
						alignedItems << otherItem;

						if(otherItem->type() == e5TetmlToc::tetmlItem::TitleParagraphItem) {
							titleFound = true;
						}

						// Remove it from the list so we don't have to process it again later
						pendingItems.removeAt(j);
						if(i >= j) {
							i--;
						}

					// Aligned standard paragraphs aren't considered part of a breaking line and they even make it impossible
					// that a breaking line exists.
					} else {
						isLine = false;
						break;
					}

				// If the item is on one side but it's not top-aligned, it breaks the line
				} else if(y > otherItem->y() && y < otherItem->bottom()) {
					isLine = false;
					break;
				}
			}

			// There is a breaking line!
			if(isLine && titleFound) {
				QRectF boundingRect;
				foreach(e5TetmlToc::tetmlItem *alignedItem, alignedItems) {
					boundingRect = boundingRect.united(alignedItem->area());
					y += alignedItem->y();
				}

				// Average the y position of the aligned items
				y /= alignedItems.count() + 1;

				qreal coveredWidthRatio = boundingRect.width() / _page->width();
				// ...and it covers more than 75% of the page width!
				if(coveredWidthRatio > MIN_BREAKINGLINE_RELATIVE_WIDTH) {
					// We won! This is definitely a breaking line.
					breakingLines << y;
				}
			}
		}
	}

	qSort(breakingLines);
	return breakingLines;
}




TitleGrouper::TitleGrouper(e5TetmlToc::Page *page) :
	_page(page)
{
}


QList< QList<e5TetmlToc::Paragraph*> > TitleGrouper::groupTitles(QList<e5TetmlToc::Paragraph*> titles)
{
	QList< QList<e5TetmlToc::Paragraph*> > titleGroups;

	QList<e5TetmlToc::Paragraph*> ungrouppedTitles = titles;
	for(int i = 0; i < ungrouppedTitles.count(); i++) {
		e5TetmlToc::Paragraph *title = ungrouppedTitles.at(i);
		e5TetmlToc::Paragraph *below = dynamic_cast<e5TetmlToc::Paragraph*>(findItemBelow(title));
		if(below && below->isTitle() && _page->relativeDistance(title->bottom(), below->y()) < MAX_SUBTITLE_SPACING ) {
			QList<e5TetmlToc::Paragraph*> group;
			group << title;
			group << below;
			titleGroups << group;

			int belowIndex = ungrouppedTitles.indexOf(below);
			if(belowIndex != -1) {
				ungrouppedTitles.removeAt(belowIndex);
				if(belowIndex < i) {
					i--;
				}
			}
			ungrouppedTitles.removeAt(i);
			i--;
		}
	}

	foreach(e5TetmlToc::Paragraph *title, ungrouppedTitles) {
		QList<e5TetmlToc::Paragraph*> list;
		list << title;
		titleGroups << list;
	}

	return titleGroups;
}


bool TitleGrouper::isItemBelow(const e5TetmlToc::tetmlItem *above, const e5TetmlToc::tetmlItem *below)
{
	if(above == below) {
		return false;
	}
	if(below->y() < above->bottom()) {
		return false;
	}

	qreal leftDifference = qAbs(_page->relativeDistance(above->x(), below->x(), Qt::Horizontal));
	qreal centerDifference = qAbs(_page->relativeDistance(above->area().center().x(), below->area().center().x(), Qt::Horizontal));
	return (leftDifference < MAX_ALIGNMENT_OFFSET || centerDifference < MAX_ALIGNMENT_OFFSET);
}


e5TetmlToc::tetmlItem* TitleGrouper::findItemBelow(const e5TetmlToc::tetmlItem *item)
{
	e5TetmlToc::tetmlItem *closestItem = 0;
	foreach(e5TetmlToc::Image *image, _page->images()) {
		if(isItemBelow(item, image)) {
			if(!closestItem || image->y() < closestItem->y()) {
				closestItem = image;
			}
		}
	}

	foreach(e5TetmlToc::Paragraph *paragraph, _page->paragraphs()) {
		if(isItemBelow(item, paragraph)) {
			if(!closestItem || paragraph->y() < closestItem->y()) {
				closestItem = paragraph;
			}
		}
	}

	return closestItem;
}


FooterFinder::FooterFinder(e5TetmlToc::Page *page) :
	_page(page)
{
}


e5TetmlToc::Paragraph* FooterFinder::findFooter() const
{
	e5TetmlToc::Paragraph *footer = 0;
	foreach(e5TetmlToc::Paragraph *paragraph, _page->paragraphs()) {
		if(!footer || paragraph->bottom() > footer->bottom()) {
			footer = paragraph;
		}
	}

	if(!footer) {
		return 0;
	}

	// TODO: Make sure the footer doesn't have more than one or two lines

	// Make sure the footer contains the page number
	QString text = footer->text();
	QString pageNumber = QString::number(_page->id() + 1);
	if(text == pageNumber || text.startsWith(pageNumber + " ") || text.endsWith(" " + pageNumber)) {
		return footer;
	} else {
		return 0;
	}
}




PageStructureAnalyzer::PageStructureAnalyzer(e5TetmlToc::Page *page) :
	_page(page),
	_footer(0)
{
}


QList<LogicalArticle*> PageStructureAnalyzer::analyze()
{
	qDeleteAll(_articles);
	_articles.clear();
	_ownerArticle.clear();

	QList<e5TetmlToc::Paragraph*> titles = TitleExtractor(_page).tagTitles();
	// TODO: Sort titles by y coordinate

	_breakingLines = BreakingLineFinder(_page).findBreakingLines();
	qDebug() << "BREAKING LINES:" << _breakingLines;
	ImageLegendFinder(_page).tagImageLegends();
	_footer = FooterFinder(_page).findFooter();

	// Create the articles and include their titles
	QList< QList<e5TetmlToc::Paragraph*> > titleGroups = TitleGrouper(_page).groupTitles(titles);
	foreach(QList<e5TetmlToc::Paragraph*> const& group, titleGroups) {
		LogicalArticle *article = new LogicalArticle(_page);
		article->setTitles(group);
		_articles.append(article);
		foreach(e5TetmlToc::Paragraph *p, group) {
			_ownerArticle.insert(p, article);
		}
	}

	// If there are no titles, create one article which will contain all the contents.
	// This case typically happens in pages that are in the middle of one article
	// which contains several pages.
	if(_articles.isEmpty()) {
		createDefaultArticle();
	}

	// Look for the first text block of each article and include it in the article
	foreach(LogicalArticle *article, _articles) {
		QRectF boundingRect = article->titleOrContentsBoundingRect();
		readColumn(article, boundingRect.bottomLeft(), boundingRect);
		// NOTE: The first paragraph of an article is not marked as a column start. This is
		// required by LogicalArticle::createHtml() in order to properly fuse columns
	}

	// Search for the remaining columns of each article and include them
	foreach(LogicalArticle *article, _articles) {
		QRectF boundingRect = article->titleOrContentsBoundingRect();

		forever {
			e5TetmlToc::Paragraph *paragraph = findNextColumn(article, boundingRect);
			if(paragraph) {
				article->addContents(paragraph);
				article->addColumnStart(paragraph);
				_ownerArticle.insert(paragraph, article);

				QRectF area = paragraph->area();
				readColumn(article, area.bottomLeft(), boundingRect.united(area));
			} else {
				break;
			}
		}

		// ONLY FOR DEBUGGING, REMOVE WHEN FINISHED
		qDebug() << "____________";
		foreach(e5TetmlToc::Paragraph *title, article->titles()) {
			qDebug() << title->text();
		}
		qDebug() << "---";
		foreach(e5TetmlToc::tetmlItem *item, article->contents()) {
			e5TetmlToc::Paragraph *paragraph = dynamic_cast<e5TetmlToc::Paragraph*>(item);
			if(paragraph) {
				qDebug() << paragraph->text();
			}
		}
		qDebug() << "‾‾‾‾‾‾‾‾‾‾‾‾";
		qDebug() << "";
		// UNTIL HERE -----------------------------
	}

	// Try to assign the images to the articles
	foreach(e5TetmlToc::Image *image, _page->images()) {
		qreal maxIntersectionRatio = 0;
		QMultiMap<qreal, LogicalArticle*> coveredArea = coveredAreaMap(image, &maxIntersectionRatio);
		QList<LogicalArticle*> articles = coveredArea.values(maxIntersectionRatio);

		// If the image intersects an article, it belongs to it
		if(maxIntersectionRatio > 0) {
			associateImageToArticle(image, articles.first());

		// Otherwise, try to find a suitable article for it
		} else {
			// Find the article that is closer to the image
			e5TetmlToc::tetmlItem *foot = image;
			if(image->legend()) {
				foot = image->legend();
			}
			LogicalArticle *article = findClosestArticle(foot);
			if(article) {
				associateImageToArticle(image, article);
			} else {
				qDebug() << "Could not associate the image" << image->imagePath() << "to any article.";
			}
		}
	}

	// Remove articles that still have no contents
	QMutableListIterator<LogicalArticle*> it(_articles);
	while(it.hasNext()) {
		LogicalArticle *article = it.next();
		if(article->contents().isEmpty()) {
			foreach(e5TetmlToc::Paragraph *title, article->titles()) {
				_ownerArticle.remove(title);
			}
			it.remove();
			delete article;
		}
	}

	return _articles;
}


void PageStructureAnalyzer::associateImageToArticle(e5TetmlToc::Image *image, LogicalArticle *article)
{
	if(!image || !article) {
		return;
	}

	int index = -1;

	e5TetmlToc::Paragraph *paragraphBelow = 0;
	// Find out if the image is inline and its alignment (left or right)
	if(!image->legend()) {
		// Find an article with a text block that starts at the same Y as the image
		for(int i = 0; i < article->contents().count() && !paragraphBelow; i++) {
			e5TetmlToc::tetmlItem *item = article->contents().at(i);
			if(item->type() == e5TetmlToc::tetmlItem::StandardParagraphItem) {
				e5TetmlToc::Paragraph *paragraph = dynamic_cast<e5TetmlToc::Paragraph*>(item);
				foreach(e5TetmlToc::TextBlock *textBlock, paragraph->textBlocks()) {
					if(_page->sameY(textBlock->y(), image->y())) {
						// If the image has the same x as the text block, it's aligned to the left
						if(_page->sameX(textBlock->x(), image->x())) {
							paragraphBelow = paragraph;
							image->setAlignment(e5TetmlToc::Image::AlignLeft);
							image->setInline(true);
							break;

						// If the image has the same right as the text block, it's aligned to the right
						} else if(_page->sameX(textBlock->right(), image->right())) {
							paragraphBelow = paragraph;
							image->setAlignment(e5TetmlToc::Image::AlignLeft);
							image->setInline(true);
							break;
						}
					}
				}
			}
		}
	}

	if(paragraphBelow) {
		index = article->contents().indexOf(paragraphBelow);
	}

	if(!paragraphBelow) {
		// At this stage, the image is not inline but we can look for the paragraph that's below it so we insert it
		// in the right position in the content list.
		e5TetmlToc::tetmlItem *foot = image;
		if(image->legend()) {
			foot = image->legend();
		}

		index = article->findSuitablePosition(foot);
	}

	if(index == -1) {
		index = 0;
	}

	if(image->legend()) {
		article->insertContents(index, image->legend());
	}
	article->insertContents(index, image);
}


bool PageStructureAnalyzer::breakingLineExistsBetween(qreal y1, qreal y2) const
{
	qreal minimum = y1 - _page->height() * MAX_BREAKINGLINE_APPLICABLE_HEIGHT;
	qreal maximum = y2 + _page->height() * MAX_BREAKINGLINE_APPLICABLE_HEIGHT;

	foreach(qreal line, _breakingLines) {
		if(line >= maximum) {
			return false;
		}

		if(line > minimum && line < maximum) {
			return true;
		}
	}

	return false;
}


QMultiMap<qreal, LogicalArticle*> PageStructureAnalyzer::coveredAreaMap(e5TetmlToc::tetmlItem *item, qreal *maxIntersectionRatio) const
{
	if(maxIntersectionRatio) {
		*maxIntersectionRatio = -1;
	}

	QMultiMap<qreal, LogicalArticle*> coveredArea;
	foreach(LogicalArticle *article, _articles) {
		if(article->boundingRect().intersects(item->area())) {
			QRectF intersection = article->boundingRect().intersect(item->area());
			qreal intersectedArea = intersection.width() * intersection.height();
			qreal totalArea = item->area().width() * item->area().height();
			qreal intersectionRatio = intersectedArea / totalArea;
			coveredArea.insert(intersectionRatio, article);

			if(maxIntersectionRatio) {
				if(intersectionRatio > *maxIntersectionRatio) {
					*maxIntersectionRatio = intersectionRatio;
				}
			}
		}
	}
	return coveredArea;
}


void PageStructureAnalyzer::createDefaultArticle()
{
	QList<e5TetmlToc::Paragraph*> candidates = _page->paragraphs();
	e5TetmlToc::Paragraph *firstParagraph = 0;
	qreal distancePoints = -1;
	foreach(e5TetmlToc::Paragraph *paragraph, candidates) {
		if(paragraph->type() == e5TetmlToc::tetmlItem::StandardParagraphItem) {
			qreal points = paragraph->x() * 4 + paragraph->y();
			if(distancePoints == -1 || points < distancePoints) {
				distancePoints = points;
				firstParagraph = paragraph;
			}
		}
	}

	if(firstParagraph) {
		LogicalArticle *article = new LogicalArticle(_page);
		article->addContents(firstParagraph);
		_articles.append(article);
	}
}


LogicalArticle* PageStructureAnalyzer::findClosestArticle(e5TetmlToc::tetmlItem *item) const
{
	e5TetmlToc::tetmlItem *closestItem = 0;
	qreal distancePoints = -1;

	QList<e5TetmlToc::Paragraph*> candidates = _page->paragraphs();
	foreach(e5TetmlToc::Paragraph *paragraph, candidates) {
		if(paragraph->textBlockCount() == 0) {
			continue;
		}

		if(paragraph->type() == e5TetmlToc::tetmlItem::ImageLegendParagraphItem) {
			continue;
		}

		e5TetmlToc::TextBlock *textBlock = paragraph->textBlockAt(0);
		qreal refBlockX = textBlock->right() < item->x() ? textBlock->right() : textBlock->x();
		qreal refBlockY = textBlock->bottom() < item->y() ? textBlock->bottom() : textBlock->y();
		qreal refX = item->right() < textBlock->x() ? item->right() : item->x();
		qreal refY = item->bottom() < textBlock->y() ? item->bottom() : item->y();
		qreal paragraphDistancePoints = qAbs(refBlockX - refX) + (10 * qAbs(refBlockY - refY));
		if(!closestItem || distancePoints == -1 || paragraphDistancePoints < distancePoints) {
			closestItem = paragraph;
			distancePoints = paragraphDistancePoints;
		}
	}

	return _ownerArticle.value(closestItem);
}


e5TetmlToc::Paragraph* PageStructureAnalyzer::findNextColumn(LogicalArticle *article, QRectF const& title)
{
	QRectF rect = article->contentsBoundingRect();
	rect = rect.united(QRectF(rect.x(), title.bottom(), 1, 1));

	e5TetmlToc::tetmlItem *closestItem = 0;
	qreal distancePoints = -1;

	foreach(e5TetmlToc::Image *image, _page->images()) {
		if(isItemOnTheRight(image, rect.topRight())) {
			qreal imageDistancePoints = 4 * qAbs(image->x() - rect.right()) + qAbs(image->y() - rect.y());
			if(!closestItem || distancePoints == -1 || imageDistancePoints < distancePoints) {
				closestItem = image;
				distancePoints = imageDistancePoints;
			}
		}
	}

	foreach(e5TetmlToc::Paragraph *paragraph, _page->paragraphs()) {
		if(paragraph->textBlockCount() == 0) {
			continue;
		}

		e5TetmlToc::TextBlock *textBlock = paragraph->textBlockAt(0);
		if(isItemOnTheRight(textBlock, rect.topRight())) {
			qreal paragraphDistancePoints = 4 * qAbs(textBlock->x() - rect.right()) + qAbs(textBlock->y() - rect.y());
			if(!closestItem || distancePoints == -1 || paragraphDistancePoints < distancePoints) {
				closestItem = paragraph;
				distancePoints = paragraphDistancePoints;
			}
		}
	}

	if(!closestItem) {
		return 0;
	}

	if(_ownerArticle.contains(closestItem)) {		// If the paragraph on the right belongs to an article, we have
		return 0;									// already reached the end of the article.
	}

	if(closestItem->type() == e5TetmlToc::tetmlItem::StandardParagraphItem) {
		return dynamic_cast<e5TetmlToc::Paragraph*>(closestItem);

	} else if(closestItem->type() == e5TetmlToc::tetmlItem::ImageItem || closestItem->type() == e5TetmlToc::tetmlItem::ImageLegendParagraphItem) {
		QPointF start(rect.right(), closestItem->bottom());
		return findParagraphBelow(start, title);

	} else {
		return 0;
	}
}


e5TetmlToc::Paragraph* PageStructureAnalyzer::findParagraphBelow(QPointF const& startPoint, QRectF const& title)
{
	QPointF start(startPoint);

	forever {
		e5TetmlToc::tetmlItem *closestItem = 0;
		qreal distancePoints = -1;

		foreach(e5TetmlToc::Image *image, _page->images()) {
			if(isItemBelow(image, start, title)) {
				qreal imageDistancePoints = qAbs(image->x() - start.x()) + (4 * qAbs(image->y() - start.y()));
				if(!closestItem || distancePoints == -1 || imageDistancePoints < distancePoints) {
					closestItem = image;
					distancePoints = imageDistancePoints;
				}
			}
		}

		foreach(e5TetmlToc::Paragraph *paragraph, _page->paragraphs()) {
			if(paragraph->textBlockCount() == 0) {
				continue;
			}

			e5TetmlToc::TextBlock *textBlock = paragraph->textBlockAt(0);
			if(isItemBelow(textBlock, start, title)) {
				qreal paragraphDistancePoints = qAbs(textBlock->x() - start.x()) + (4 * qAbs(textBlock->y() - start.y()));
				if(!closestItem || distancePoints == -1 || paragraphDistancePoints < distancePoints) {
					closestItem = paragraph;
					distancePoints = paragraphDistancePoints;
				}
			}
		}

		if(!closestItem) {
			return 0;
		}

		if(closestItem == _footer) {	// closestItem is already not null
			return 0;
		}

		// If there is a breaking line between startY and the top of the closest item,
		// no matter what the item is, we can't continue below the line.
		if(breakingLineExistsBetween(start.y(), closestItem->y())) {
			return 0;
		}

		if(_ownerArticle.contains(closestItem)) {		// If the paragraph below already belongs to an article,
			return 0;									// we should not continue below it.
		}

		if(closestItem->type() == e5TetmlToc::tetmlItem::StandardParagraphItem) {
			if(titleExistsBelow(start.y()) && _page->relativeDistance(start.y(), closestItem->y(), Qt::Vertical) > MAX_ARTICLE_STDPARAGRAPH_OFFSET) {
				return 0;
			} else {
				return dynamic_cast<e5TetmlToc::Paragraph*>(closestItem);
			}

		} else if(closestItem->type() == e5TetmlToc::tetmlItem::ImageItem || closestItem->type() == e5TetmlToc::tetmlItem::ImageLegendParagraphItem) {
			start.setY(closestItem->bottom());
			continue;

		} else {
			return 0;
		}
	}
}


bool PageStructureAnalyzer::isItemBelow(e5TetmlToc::tetmlItem *item, QPointF const& start, QRectF const& title)
{
	if(item->y() < start.y()) {
		return false;
	}

	if(item->right() - (item->width() * MAX_STDPARAGRAPH_TITLE_OFFSET) < title.left()) {
		return false;
	}
/* NOTE (vm): This restricted columns to be under a title, so valid columns on the right side that are
   not under a title got discarded. For this reason, the restriction has been dropped. You can see
   this effect in "DU" (DU_816_low+01-120.pdf), page 5 (page4.html). With this restriction, the
   right column is not assigned to any article. Removing the restriction allows findNextColumn()
   to find it, so it gets assigned to the article. No side effects have been observed from removing this
   descrition as of June 22nd, 2011.

	if(item->x() + (item->width() * MAX_STDPARAGRAPH_TITLE_OFFSET) > title.right()) {
		return false;
	}
*/
	return true;
}


bool PageStructureAnalyzer::isItemOnTheRight(e5TetmlToc::tetmlItem *item, QPointF const& start)
{
	QRectF area = item->area();
	area.setTop(area.top() - _page->height() * 0.2 );
	area.setLeft(area.left() - _page->maximumColumnSeparation());
	area.setRight(area.right() - _page->maximumColumnSeparation());

	return area.contains(start);
}


void PageStructureAnalyzer::readColumn(LogicalArticle *article, QPointF const& startPoint, QRectF const& title)
{
	QPointF start(startPoint);

	forever {
		e5TetmlToc::Paragraph *paragraph = findParagraphBelow(start, title);
		if(paragraph && paragraph->textBlockCount() > 0) {
			article->addContents(paragraph);
			_ownerArticle.insert(paragraph, article);

			e5TetmlToc::TextBlock *lastTextBlock = paragraph->textBlockAt(paragraph->textBlockCount() - 1);
			if(lastTextBlock->bottom() < start.y()) {
				start.setX(lastTextBlock->x());
			}
			start.setY(lastTextBlock->bottom());

		} else {
			break;
		}
	}
}


bool PageStructureAnalyzer::titleExistsBelow(qreal y) const
{
	foreach(LogicalArticle *article, _articles) {
		QList<e5TetmlToc::Paragraph*> titles = article->titles();
		foreach(e5TetmlToc::Paragraph *title, titles) {
			if(y < title->y()) {
				return true;
			}
		}
	}

	return false;
}
