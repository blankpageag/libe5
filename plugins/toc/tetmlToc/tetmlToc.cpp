#include <QtCore>
#include <ConditionManager>
#include <ContainerAccessManager>
#include <CompilerTools>
#include <cassert>
#include <Downloader>
#include "ArticleContent.h"
#include "Content"
#include "PageStructureAnalyzer.h"
#include "tetmlToc"
#include "tetmlItem.h"

#include <map>

////
// FIXME: ONLY FOR TESTING TO INCLUDE THOSE LIBs:
#include <QPixmap>
#include <QPainter>
////

#define MAX_TEXTBLOCK_ALIGNMENT_DELTA 0.01

tetmlToc::tetmlToc(Container * const container) :
	TOC(container)
{
}


void tetmlToc::load()
{
	if(!container())
		return;
		
	QStringList const files=container()->files();
	
	QString toc;
	foreach( QString f, files ) {
		if( f.contains(".tetml" ) ) {
			toc = f;
			_root = f.remove(".tetml");
			break;
		}
	}	
	
	
	if( toc.isNull() ) {
		return;
	}

	qDebug() << "Tetml main file found: " <<toc;
	QIODevice *tocFile=container()->open(toc);

	// set the title temparary from the file name:
	_metadata.insert("dc:title", toc.remove( ".tetml"));
	{
		QDomDocument toc;
		toc.setContent(tocFile);
		fixMissingParagraphs(toc);
		parsePages(toc);
		
		// parse resources:
		QHash<QString, QString> fontIds;
		QDomNodeList const fonts = toc.elementsByTagName("Fonts").at(0).toElement().elementsByTagName("Font");
		for(int f=0; f<fonts.count(); f++) {
			QString id = fonts.at(f).toElement().attribute("id");
			QString fType = fonts.at(f).toElement().attribute("name");
			fType = fType.split("-").at(0);
			fontIds.insert( id, fType);
		}

		foreach(e5TetmlToc::Page *page, _pages) {
			page->adjustFont(fontIds);
			// writeToHtml(page);
			qDebug() << "PAGE" << page->id();
			PageStructureAnalyzer analyzer(page);
			QList<LogicalArticle*> articles = analyzer.analyze();
			convertToE5(page, articles, analyzer.breakingLines());
		}
	}
	
	delete tocFile;
}


void tetmlToc::fixMissingParagraphs(QDomDocument& document)
{
	QDomNodeList const tables = document.elementsByTagName("Table");
	for(int i = 0; i < tables.count(); i++) {
		QDomNode table = tables.at(i);
		QDomElement word = table.nextSiblingElement();
		if(word.tagName() == "Word") {
			// The <Para> tag is missing, so we have to create it.
			QDomElement para = document.createElement("Para");

			// Insert the <Para> before the <Word>
			word.parentNode().insertBefore(para, word);

			// Now move all tags into <Para> until a <Para> or <Table> is found
			while(para.nextSibling().isNull() == false) {
				QDomNode nextSibling = para.nextSibling();
				if(nextSibling.isElement()) {
					QDomElement nextElement = nextSibling.toElement();
					if(nextElement.tagName() == "Para" || nextElement.tagName() == "Table") {
						break;
					}
				}

				para.appendChild(nextSibling);
			}
		}
	}
}


void tetmlToc::parsePages(QDomDocument const& doc)
{
	QDomNodeList const pages = doc.elementsByTagName("Page");
	for(int i = 0; i < pages.count(); i++) {
		e5TetmlToc::Page *p = new e5TetmlToc::Page(i, pages.at(i).toElement());
		parseParagraphs(p);
		parseImages(p);
		p->associateImagesToParagraphs();
		_pages.append(p);
	}
}


void tetmlToc::parseParagraphs(e5TetmlToc::Page *page)
{
	for(int j=0; j<page->domElement().elementsByTagName("Para").count(); j++) {
		e5TetmlToc::Paragraph *paragraph = parseParagraph(page, page->domElement().elementsByTagName("Para").at(j).toElement());
		page->addParagraph(paragraph);
	}
}


void tetmlToc::parseImages(e5TetmlToc::Page *page)
{
	QDomNodeList list = page->domElement().elementsByTagName("PlacedImage");
	for(int j = 0; j < list.count(); j++) {
		QDomElement element = list.at(j).toElement();
		e5TetmlToc::Image *img = new e5TetmlToc::Image(element);

		// find file in container:
		QStringList filename = container()->files().filter( "_" + img->name() + "." );
		//ASSERT( filename.size() == 1 );
		page->addImage(img);
		img->setImagePath(filename.at(0));
	}
}


e5TetmlToc::Paragraph* tetmlToc::parseParagraph(e5TetmlToc::Page *page, QDomElement const& para)
{
	e5TetmlToc::TextBlock *block = new e5TetmlToc::TextBlock(para);
	e5TetmlToc::Paragraph *paragraph = new e5TetmlToc::Paragraph(para);
	paragraph->appendTextBlock(block);

	QDomNodeList words = para.elementsByTagName("Word");
	e5TetmlToc::Word *lastWord = 0;
	for(int i = 0; i < words.count(); i++) {
		e5TetmlToc::Word *word = new e5TetmlToc::Word(words.at(i).toElement());
		word->setY(page->height() - word->y());

		qreal wordYMargin = word->y() + page->height() * MAX_TEXTBLOCK_ALIGNMENT_DELTA;
		if(lastWord && wordYMargin < lastWord->y()) {
			block = new e5TetmlToc::TextBlock(para);
			paragraph->appendTextBlock(block);
		}

		block->appendWord(word);
		lastWord = word;
	}

	return paragraph;
}


QString tetmlToc::baseUrl() const
{
	return TOC::baseUrl();// + _root;
}


bool tetmlToc::isMetadata(QString const &fn) const
{
	QString const mt=mimeType(fn);
	if(mt == "application/x-dtbncx+xml" || mt == "application/oebps-package+xml")
		return true;
	if(fn.toLower().section('/', -1) == "mimetype")
		return true;
	if(fn.toLower().startsWith("meta-inf/"))
		return true;
	return TOC::isMetadata(fn);
}


QString tetmlToc::mimeType(QString const &fn) const
{
	if(_mimeTypes.contains(fn))
		return _mimeTypes.value(fn);
	return TOC::mimeType(fn);
}


void tetmlToc::draw(e5TetmlToc::Page *page)
{
	QPixmap drawImage( QSize( 907,1332) );
	QPainter painter(&drawImage);
	painter.fillRect( drawImage.rect(), Qt::white );
	
	
	// insert the paragraphs:
	foreach(e5TetmlToc::Paragraph *p, page->paragraphs()) {
		//p.info();
		painter.setPen( Qt::red);
		painter.drawRect( p->area() );
		painter.setPen( Qt::black);
		painter.drawText( p->area(), Qt::AlignLeft, p->text() );
	}
	
	// insert the paragraphs:
	ContainerAccessManager *m=ContainerAccessManager::instance(0);	
	foreach(e5TetmlToc::Image *i, page->images()) {
		QByteArray fromContainer = container()->content(i->imagePath());
		QPixmap pixmap;
		pixmap.load( fromContainer );
		
		painter.setPen( Qt::blue);
		painter.drawRoundedRect( i->area(), 20, 15);
		painter.setPen( Qt::black);
		painter.drawText(i->area(), Qt::AlignLeft, i->imagePath());
		painter.drawImage( i->area(), pixmap.toImage() );
	}
	m->deref(0);

	QString out = title();
	out.append( QString::number( page->id()) );
	out.append(  ".png");
	qDebug() << "Writing image to " <<out;
	drawImage.save( out );
}


void tetmlToc::writeToHtml(e5TetmlToc::Page *page)
{
	QString out = title();
	out.append( QString::number (page->id()) );
	out.append(  ".html");
	qDebug() << "Writing html to " <<out;
	
	QFile htmFile( out);
	if(!htmFile.open(QFile::WriteOnly))
		return;
	QTextStream writer(&htmFile);
	writer << "<?xml version=\"1.0\" encoding=\"utf-8\"?><!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>";
	writer <<"<style type=\"text/css\"> p {font-family: \"Black-Medium\";font-weight: normal;font-style: normal;font-size: 8px;} </style>";
	writer<<"</head><body>"<<endl;
	
	
	// insert the paragraphs:
	foreach(e5TetmlToc::Paragraph *p, page->paragraphs()) {
		writer<<"<div style=\"display:block; border: 0px solid; position:absolute;";
		writer<<"top:" << p->y() <<"px; left:"<<p->x()<<"px; width:"<<p->width()<<"px; height:"<<p->height()<<"px;\">"<<endl;
		
		//writer<<"<p style=\"font-size: "<<p.font()._size<<"px;\">"<<p.formattedText()<<"</p>"<<endl;
		writer<<"<p>"<<p->formattedText()<<"</p>"<<endl;
		
		writer<<"</div>"<<endl;
		
	}
	
	// insert the paragraphs:
	foreach(e5TetmlToc::Image *i, page->images()) {
		writer<<"<div style=\"display:block; border: 0px solid; position:absolute;";
		writer<<"top:" << i->y() <<"px; left:"<<i->x()<<"px; width:"<<i->width()<<"px; height:"<<i->height()<<"px;\">"<<endl;
		
		// FIXME: hardcoding of path:
		QString url = "tests/data/pdfconversion/dm1/" + i->imagePath();

		writer<<"<img w="<<i->width()<<" h="<<i->height()<<" src=\""<<url<<"\"/>"<<endl;
		
		writer<<"</div>"<<endl;
	}

	writer <<"</body></html>";
	htmFile.close();
}


void tetmlToc::convertToE5(e5TetmlToc::Page *page, QList<LogicalArticle*> const& articles, QList<qreal> const& breakingLines)
{
	QString id = QString::number(page->id());
	TOCEntry *tocEntry = new TOCEntry(this, id);

	// Add the rendered image
	Content *index = Content::get(tocEntry, QString("page%1.xhtml").arg(id));
	tocEntry->append(index);

	// Remove the contents of the body
	QDomElement pageBody = index->dom().elementsByTagName("body").at(0).toElement();
	{
		QDomNodeList children = pageBody.childNodes();
		for(int i = 0; i < children.count(); i++) {
			pageBody.removeChild(children.at(i));
		}
	}

	// Create the <div>s we need
	QDomElement innerContainer;
	{
		QDomElement outerContainer = index->dom().createElement("div");
		outerContainer.setAttribute("style", "display: -webkit-box; -webkit-box-orient: horizontal; -webkit-box-pack: center; "
											 "-webkit-box-align: center; max-width: 100%; max-height: 100%;");
		pageBody.appendChild(outerContainer);

		QString pngFile = QString("page%1.png").arg(id);
		innerContainer = index->dom().createElement("div");
		innerContainer.setAttribute("style", QString("position: relative; left: 0; top: 0; max-width: 100%; max-height: 100%; "
													 "padding: 0; margin: auto; background-image: url('%1'); "
													 "background-size: 100% 100%;").arg(pngFile));
		outerContainer.appendChild(innerContainer);

		QDomElement img = index->dom().createElement("img");
		img.setAttribute("src", pngFile);
		img.setAttribute("style", "max-width: 100%; max-height: 100%; visibility: hidden;");
		innerContainer.appendChild(img);
	}

	// Add the .css file to the container
	container()->add("e5/tetmlarticle.css", QUrl("qrc:/content/tetmlarticle.css"));

	// Process the articles
	for(int i = 0; i < articles.count(); i++) {
		LogicalArticle *article = articles.at(i);

		// Create the html page of the article
		QString fileName = QString("%1.tetml.%2-%3.html").arg(_root).arg(id).arg(i);
		container()->add(fileName, article->createHtml());
		tocEntry->append( new ArticleContent(article, tocEntry, fileName) );

		// Create an interactive area in the rendered page that links to the article
		QString left = QString::number(100 * article->boundingRect().x() / page->width(), 'f', 2) + "%";
		QString top = QString::number(100 * article->boundingRect().y() / page->height(), 'f', 2) + "%";
		QString width = QString::number(100 * article->boundingRect().width() / page->width(), 'f', 2) + "%";
		QString height = QString::number(100 * article->boundingRect().height() / page->height(), 'f', 2) + "%";

		QString style = QString("position: absolute; display: block; left: %1; top: %2; width: %3; height: %4")
				.arg(left).arg(top).arg(width).arg(height);
		QDomElement interactiveArea = index->dom().createElement("a");
		interactiveArea.setAttribute("id", "__e5_interactiveArea" + QString::number(qrand()));
		interactiveArea.setAttribute("href", fileName);
		interactiveArea.setAttribute("style", style);
		innerContainer.appendChild(interactiveArea);
	}

//	debugHighlightParagraphs(page, innerContainer);
//	debugBreakingLines(breakingLines, page, innerContainer);
	append(tocEntry);
}


void tetmlToc::debugBreakingLines(QList<qreal> const& breakingLines, e5TetmlToc::Page *page, QDomElement innerContainer)
{
	foreach(qreal line, breakingLines) {
		QString y = QString::number(100 * line / page->height(), 'f', 2) + "%";
		QString style = QString("position: absolute; display: block; left: 0; top: %2; width: 100%; height: 1px; background-color: red; opacity: 0.5")
							.arg(y);
		QDomElement interactiveArea = innerContainer.ownerDocument().createElement("div");
		interactiveArea.setAttribute("style", style);
		innerContainer.appendChild(interactiveArea);
	}
}


void tetmlToc::debugHighlightParagraphs(e5TetmlToc::Page *page, QDomElement innerContainer)
{
	for(int i = 0; i < page->paragraphCount(); i++) {
		e5TetmlToc::Paragraph *p = page->paragraphAt(i);
		QString left = QString::number(100 * p->area().x() / page->width(), 'f', 2) + "%";
		QString top = QString::number(100 * p->area().y() / page->height(), 'f', 2) + "%";
		QString width = QString::number(100 * p->area().width() / page->width(), 'f', 2) + "%";
		QString height = QString::number(100 * p->area().height() / page->height(), 'f', 2) + "%";
		QString color;
		switch(p->type()) {
		case e5TetmlToc::tetmlItem::StandardParagraphItem:
			color = "green";
			break;
		case e5TetmlToc::tetmlItem::TitleParagraphItem:
			color = "blue";
			break;
		case e5TetmlToc::tetmlItem::ImageLegendParagraphItem:
			color = "yellow";
			break;
		default:
			color = "gray";
			break;
		}

		QString style = QString("position: absolute; display: block; left: %1; top: %2; width: %3; height: %4; background-color: %5; opacity: 0.5")
				.arg(left).arg(top).arg(width).arg(height).arg(color);
		QDomElement interactiveArea = innerContainer.ownerDocument().createElement("div");
		interactiveArea.setAttribute("style", style);
		innerContainer.appendChild(interactiveArea);
	}
}
