#include <QtCore>
#include "LogicalArticle.h"

LogicalArticle::LogicalArticle(e5TetmlToc::Page *page) :
	_page(page)
{
}


void LogicalArticle::addTitle(e5TetmlToc::Paragraph *title)
{
	_boundingRect = QRectF();
	_titlesRect = QRectF();

	_titles.append(title);
}


void LogicalArticle::addColumnStart(e5TetmlToc::tetmlItem *item)
{
	_columnStarts.insert(item);
}


void LogicalArticle::addContents(e5TetmlToc::tetmlItem *item)
{
	_boundingRect = QRectF();
	_contentsRect = QRectF();

	_contents.append(item);
}


QRectF LogicalArticle::boundingRect() const
{
	if(_boundingRect.isNull()) {
		calculateBoundingRect();
	}

	return _boundingRect;
}


QRectF LogicalArticle::contentsBoundingRect() const
{
	if(_contentsRect.isNull()) {
		calculateBoundingRect();
	}

	return _contentsRect;
}


QString LogicalArticle::createHtml() const
{
	QString titleText = _titles.isEmpty() ? QString() : _titles.first()->text();

	QString html = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	html += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
			"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
			"<head>\n"
			"	<title>" + titleText + "</title>\n"
			"	<link rel=\"stylesheet\" href=\"e5/tetmlarticle.css\"/>\n"
			"</head>\n"
			"<body>\n";

	// Title
	if(!_titles.isEmpty()) {
		html += "\t<h1>" + titleText + "</h1>\n";
	}

	for(int i = 1; i < _titles.count(); i++) {
		html += "\t<h2>" + _titles.at(i)->text() + "</h2>\n";
	}

	html += "<br/>\n";

	bool fuseNextParagraph = false;
	for(int i = 0; i < _contents.count(); i++) {
		e5TetmlToc::tetmlItem *item = _contents.at(i);
		switch(item->type()) {
		case e5TetmlToc::tetmlItem::StandardParagraphItem: {
			e5TetmlToc::Paragraph *paragraph = dynamic_cast<e5TetmlToc::Paragraph*>(item);
			QString text = paragraph->text();
			QChar firstChar = text.isEmpty() ? QChar() : text.at(0);

			if(firstChar.category() == QChar::Punctuation_Dash) {
				fuseNextParagraph = false;
			}

			if(fuseNextParagraph) {
				html += " ";
			} else {
				html += "\t<p>";
			}
			fuseNextParagraph = false;

			html += text;

			int next = i + 1;
			if(!text.endsWith('.') && next < _contents.count()) {
				e5TetmlToc::Paragraph *nextParagraph = dynamic_cast<e5TetmlToc::Paragraph*>(_contents.at(next));
				if(nextParagraph &&
				   nextParagraph->type() == e5TetmlToc::tetmlItem::StandardParagraphItem &&
				   isColumnStart(nextParagraph)) {
					fuseNextParagraph = true;
				}
			}

			if(!fuseNextParagraph) {
				html += "</p>\n";
			}
			break;
		}

		case e5TetmlToc::tetmlItem::ImageItem: {
			e5TetmlToc::Image *image = dynamic_cast<e5TetmlToc::Image*>(item);
			if(image->inlineImage()) {
				QString alignmentClass;
				if(image->alignment() == e5TetmlToc::Image::AlignLeft) {
					alignmentClass = "left-floating-legend";
				} else if(image->alignment() == e5TetmlToc::Image::AlignRight) {
					alignmentClass = "right-floating-legend";
				}

				html += QString("\t<div class=\"%1\"><img src=\"%2\"/></div>\n").arg(alignmentClass).arg(image->imagePath());

			} else {
				QString alignmentClass;
				if(image->alignment() == e5TetmlToc::Image::AlignCenter) {
					alignmentClass = "centered-legend";
				} else if(image->alignment() == e5TetmlToc::Image::AlignRight) {
					alignmentClass = "right-legend";
				}

				html += QString("\t<p class=\"image-legend %1\"><img src=\"%2\"/>").arg(alignmentClass).arg(image->imagePath());
				if(image->legend()) {
					html += "<br/>\n";
				} else {
					html += "</p>\n";
				}
			}
			break;
		}

		case e5TetmlToc::tetmlItem::ImageLegendParagraphItem: {
			e5TetmlToc::Paragraph *paragraph = dynamic_cast<e5TetmlToc::Paragraph*>(item);
			html += QString("%1</p>").arg(paragraph->text());
			break;
		}

		default:
			break;
		}
	}

	html += "</body>\n</html>\n";
	return html;
}


int LogicalArticle::findSuitablePosition(e5TetmlToc::tetmlItem *item) const
{
	for(int i = 0; i < _contents.count(); i++) {
		if(_contents.at(i)->y() >= item->bottom() && _contents.at(i)->x() >= item->x()) {
			return i;
		}
	}

	return -1;
}


void LogicalArticle::insertContents(int index, e5TetmlToc::tetmlItem *item)
{
	_boundingRect = QRectF();
	_contentsRect = QRectF();

	_contents.insert(index, item);
}


void LogicalArticle::setComplete(bool complete)
{
	_complete = complete;
}


void LogicalArticle::setTitles(QList<e5TetmlToc::Paragraph*> const& titles)
{
	_titles = titles;
}


e5TetmlToc::Paragraph* LogicalArticle::subtitle() const
{
	if(_titles.count() >= 2) {
		return _titles.at(1);
	} else {
		return 0;
	}
}


e5TetmlToc::Paragraph* LogicalArticle::title() const
{
	if(!_titles.isEmpty()) {
		return _titles.first();
	} else {
		return 0;
	}
}


QRectF LogicalArticle::titlesBoundingRect() const
{
	if(_titlesRect.isNull()) {
		calculateBoundingRect();
	}

	return _titlesRect;
}


QRectF LogicalArticle::titleOrContentsBoundingRect() const
{
	if(_titles.isEmpty()) {
		return contentsBoundingRect();
	} else {
		return titlesBoundingRect();
	}
}


void LogicalArticle::calculateBoundingRect() const
{
	_boundingRect = QRectF();
	_contentsRect = QRectF();
	_titlesRect = QRectF();

	foreach(e5TetmlToc::Paragraph *title, _titles) {
		_boundingRect = _boundingRect.united(title->area());
		_titlesRect = _titlesRect.united(title->area());
	}

	foreach(e5TetmlToc::tetmlItem *item, _contents) {
		_boundingRect = _boundingRect.united(item->area());
		_contentsRect = _contentsRect.united(item->area());
	}
}
