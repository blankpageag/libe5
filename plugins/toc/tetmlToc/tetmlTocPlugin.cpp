#include <QtPlugin>
#include "tetmlTocPlugin"
#include "tetmlToc"

CONST uint8_t tetmlTocPlugin::priority(Container const * const container) const {
	
	foreach( QString f, container->files() )
	{
		if( f.contains(".tetml" ) )
		{
			return 100;
		}
		
	}	
	return 0;
}

TOC *tetmlTocPlugin::get(Container * const container) const {
	return new tetmlToc(container);
}

EXPORT_PLUGIN(tetmlToc, tetmlTocPlugin);
