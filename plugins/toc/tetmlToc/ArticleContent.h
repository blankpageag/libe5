#ifndef _ARTICLE_CONTENT_
#define _ARTICLE_CONTENT_ 1

#include <QList>
#include <QHash>
#include <QRectF>
#include <Content>
#include "LogicalArticle.h"
#include "tetmlItem.h"

/**
 * 
 * @author Lukas N Mueller
 */
class ArticleContent : public Content
{
public:
	ArticleContent(LogicalArticle *article, TOCEntry *tocEntry, QString const &filename);
	~ArticleContent();
	LogicalArticle* article() const;
	
	virtual void createHtml();


protected:
	LogicalArticle *_article;
};


#endif
