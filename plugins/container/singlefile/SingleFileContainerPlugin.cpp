#include <QFileInfo>
#include <QtPlugin>
#include "SingleFileContainerPlugin"
#include "SingleFileContainer"

CONST uint8_t SingleFileContainerPlugin::priority(QUrl const &container) const {
	QFileInfo info(container.toLocalFile());
	return (info.exists() && !info.isDir()) ? 1 : 0;
}

Container *SingleFileContainerPlugin::get(QUrl const &container) const {
	Container *c=new SingleFileContainer(container, 0);
	static_cast<Metadata*>(c)->insert("dc:title", QFileInfo(container.toLocalFile()).baseName());
	return c;
}

EXPORT_PLUGIN(singleFileContainer, SingleFileContainerPlugin);
