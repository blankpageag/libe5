#include "SingleFileContainer"
#include <QFileInfo>
#include <DirTools>

QStringList SingleFileContainer::originalFiles() const {
	return QStringList() << QFileInfo(_url.toLocalFile()).fileName();
}

QIODevice *SingleFileContainer::openOriginal(QString const &filename) const {
	if(filename != QFileInfo(_url.toLocalFile()).fileName())
		return 0;

	QFile *f=new QFile(_url.toLocalFile());
	if(!f || !f->exists()) {
		if(f)
			delete f;
		return 0;
	}
	f->open(QFile::ReadOnly);
	return f;
}


qint64 SingleFileContainer::sizeOriginal(const QString &filename) const
{
	QFileInfo info(_url.toLocalFile());
	if(filename != info.fileName()) {
		return -1;
	}

	return info.size();
}
