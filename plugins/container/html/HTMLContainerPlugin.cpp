#include <QFileInfo>
#include <QtPlugin>
#include "HTMLContainerPlugin"
#include "HTMLContainer"

CONST uint8_t HtmlContainerPlugin::priority(QUrl const &container) const
{
	QFileInfo info(container.toLocalFile());
	bool isHtml = info.suffix().contains("htm", Qt::CaseInsensitive);	// Works for xhtml, htm, html,...
	return (info.exists() && !info.isDir() && isHtml) ? 2 : 0;
}

Container *HtmlContainerPlugin::get(QUrl const &container) const
{
	return new HtmlContainer(container, 0);
}

EXPORT_PLUGIN(htmlContainer, HtmlContainerPlugin);
