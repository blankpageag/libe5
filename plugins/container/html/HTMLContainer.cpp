#include <DirTools>
#include "HTMLContainer"

HtmlContainer::HtmlContainer(QUrl const& url, QObject *parent) :
	Container(url, parent),
	_directory(QFileInfo(url.toLocalFile()).dir())
{
}


QString HtmlContainer::extract(QString const &fn)
{
	if(originalFiles().contains(fn)) {
		return QUrl::fromLocalFile(_directory.absolutePath()).resolved(fn).toLocalFile();
	} else {
		return Container::extract(fn);
	}
}


QStringList HtmlContainer::originalFiles() const
{
	if(_fileListCache.isEmpty()) {
		if(!_directory.exists()) {
			return QStringList();
		}
		_fileListCache = DirTools::files(_directory);
	}
	return _fileListCache;
}


QIODevice* HtmlContainer::openOriginal(QString const &filename) const
{
	QString cleanFileName = filename;
	while(cleanFileName.startsWith('/')) {
		cleanFileName.remove(0, 1);
	}

	QFile *file = new QFile(_directory.absoluteFilePath(cleanFileName));
	if(!file || !file->exists()) {
		if(file) {
			delete file;
		}
		return 0;
	}
	file->open(QFile::ReadOnly);
	return file;
}


qint64 HtmlContainer::sizeOriginal(QString const &filename) const
{
	QFileInfo info(_directory.absoluteFilePath(filename));
	if(info.exists()) {
		return info.size();
	} else {
		return -1;
	}
}
