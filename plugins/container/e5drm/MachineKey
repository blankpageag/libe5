// vim:syntax=cpp.doxygen
#ifndef _MACHINEKEY_
#define _MACHINEKEY_ 1

#include <QObject>
#include <ClassTools>
#undef PRIVATE // botan has its own idea about this...
#include <botan/botan.h>
#include <botan/x509_key.h>
#include <botan/look_pk.h>
#ifdef __GNUC__
#define PRIVATE __attribute__((visibility("hidden")))
#else
#define PRIVATE
#endif

#if BOTAN_VERSION_CODE < BOTAN_VERSION_CODE_FOR(1, 9, 15)
typedef Botan::PK_Decrypting_Key PrivateKey;
typedef Botan::PK_Encrypting_Key PublicKey;
#else
typedef Botan::Private_Key PrivateKey;
typedef Botan::Public_Key PublicKey;
#endif

class MachineKey:public QObject {
	Q_OBJECT
protected:
	MachineKey();
public:
#ifndef Q_OS_WIN
	PURE
#endif
	static MachineKey *instance();
	PublicKey const &publicKey() const;
	PrivateKey const &privateKey() const;
private:
	PRIVATE void loadKeys(QString const &k);
	PRIVATE void generateKeys(QString const &k);
public slots:
	QString publicKeyPEM() const;
	QString const &fingerprint() const;
protected:
	Botan::LibraryInitializer	_botan;
	QString	const			privKey;
	QString const			pubKey;
};
#endif
