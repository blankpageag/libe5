#include <QtPlugin>
#include <QFile>
#include <QDataStream>
#include "e5DRMContainerPlugin"
#include "e5DRMContainer"

CONST uint8_t e5DRMContainerPlugin::priority(QUrl const &container) const {
	QString const file=container.toLocalFile();
	if(!QFile::exists(file))
		return 0;

	QFile f(file);
	if(!f.open(QFile::ReadOnly))
		return 0;

	QDataStream data(&f);
	data.setVersion(QDataStream::Qt_4_5);
	data.setByteOrder(QDataStream::LittleEndian);
	quint16 magic, version;
	data >> magic;
	data >> version;
	if(magic == 0xf00d && version == 0x0000)
		return 10;

	return 0;
}

Container *e5DRMContainerPlugin::get(QUrl const &container) const {
	return new e5DRMContainer(container, 0);
}

EXPORT_PLUGIN(e5drmContainer, e5DRMContainerPlugin);
