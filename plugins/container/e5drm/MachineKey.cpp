#include "MachineKey"
#include <QIOStream>
#include <QNetworkInterface>
#include <QDir>
#include <QFile>
#include <FD>
#include <botan/rsa.h>
#include <string>

extern "C" {
#include <fcntl.h>
#include <sys/stat.h>
#if !defined(Q_OS_WIN) || __GNUC__>=4
#include <sys/time.h>
#endif
#ifdef Q_OS_WIN
#include <sys/utime.h>
#include <winsock2.h>
#endif
}

static MachineKey * _instance = 0;
// FIXME it may not be safe to keep this in memory all the time
static PrivateKey *_privKey = 0;
static PublicKey *_pubKey = 0;
static QString _fingerprint;

using namespace std;

#ifndef Q_OS_WIN
PURE
#endif
static inline QString macAddress() {
	QString mac;

#ifdef Q_OS_LINUX
	QNetworkInterface networkInterface = QNetworkInterface::interfaceFromName("eth0");
#elif defined(Q_OS_MAC) || defined(Q_OS_DARWIN) || defined(Q_OS_BSD4) || defined(Q_OS_FREEBSD) || defined(Q_OS_OPENBSD) || defined(Q_OS_NETBSD) || defined(Q_OS_BSDI)
	QNetworkInterface networkInterface = QNetworkInterface::interfaceFromName("en0");
#else
	QNetworkInterface networkInterface;
#endif
#ifdef Q_OS_WIN
	// On the worst OS of all, we typically want the one with a human
	// readable name of "Local Area Connection" -- but of course that
	// one might not exist because that crappy wannabe OS translates
	// internal IDs, and interfaceFromName() doesn't look at the human
	// readable name...
	// Life would be a lot easier if the scum in Redmond just died.
	QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();
	foreach(QNetworkInterface const &currInterface, interfaces) {
		if(currInterface.humanReadableName()=="Local Area Connection") {
			networkInterface=currInterface;
			break;
		}
	}
#endif

	if(networkInterface.isValid())
		mac = networkInterface.hardwareAddress();

	if(mac.isEmpty()){
		// Our best guess didn't find a network card, so let's
		// take anything we can find...
		QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();

		foreach(QNetworkInterface const &currInterface, interfaces){

			if( !(currInterface.flags() & QNetworkInterface::IsLoopBack)
			 && !(currInterface.flags() & QNetworkInterface::IsPointToPoint)
			 && !currInterface.humanReadableName().toLower().startsWith("vm") // We aren't interested in VMWare fake adapters...
				){

				mac = currInterface.hardwareAddress();
				break;
			}

		}
	}

	return mac;
}

void MachineKey::loadKeys(QString const &hwKey) {
	try {
		Botan::AutoSeeded_RNG rng;
		QString rawPP = hwKey;
#ifdef Q_OS_IPHONE
		// iPhone SDK 3.0 translates stat() into an unresolvable call
		// to _stat$INODE64 for some reason, but this should do the
		// trick...
		rawPP += QString::number(QFileInfo(privKey).created().toTime_t());
#else
		struct stat s;
		stat(QFile::encodeName(privKey), &s);
		rawPP += QString::number(s.st_mtime);
#endif
		Botan::HashFunction *hash = Botan::get_hash("RIPEMD-160");
		hash->update(rawPP.toAscii().data());
		Botan::SecureVector<Botan::byte> passPhrase = hash->final();
		delete hash;
		QString hexPP;
		for(Botan::byte const *b=passPhrase.begin(); b<passPhrase.end(); b++)
			hexPP += QString("00" + QString::number(*b, 16)).right(2);
		Botan::DataSource_Stream keyIn(QFile::encodeName(privKey).data());
		Botan::PKCS8_PrivateKey *inKey = Botan::PKCS8::load_key(keyIn, rng, hexPP.toAscii().data());
		_privKey = dynamic_cast<PrivateKey*>(inKey);

		Botan::DataSource_Stream pubKeyIn(QFile::encodeName(pubKey).data());
		Botan::Public_Key *x509 = Botan::X509::load_key(pubKeyIn);
		_pubKey = dynamic_cast<PublicKey*>(x509);
	} catch(std::exception const &e) {
	}
}

static QString readFile(char const * const filename) {
	// Must use C-style IO because of /sys and /proc stat weirdness
	FD f=open(filename, O_RDONLY);
	char buf[1024];
	memset(buf, 0, 1024);
	read(f, buf, 1024);
	return QString(buf).trimmed();
}

void MachineKey::generateKeys(QString const &hwKey) {
	try {
		Botan::AutoSeeded_RNG rng;

		// Generate a new private key...
		Botan::RSA_PrivateKey key(rng, 2048);

		// Passphrase generation:
		// 1. Machine specific component
		QString rawPP = hwKey;
		// 2. Timestamp
#ifdef _MSC_VER
		// *****ing DreadfulCC doesn't know about time()
		time_t timestamp=QDateTime::currentDateTime().toTime_t();
#else
		time_t timestamp=time(0);
#endif
		timeval tv[2];
		tv[0].tv_sec = tv[1].tv_sec = timestamp;
		tv[0].tv_usec = tv[1].tv_usec = 0;
		rawPP += QString::number(timestamp);
		// 3. RIPEMD160 hash it to hide original components
		Botan::HashFunction *hash = Botan::get_hash("RIPEMD-160");
		hash->update(rawPP.toAscii().data());
		Botan::SecureVector<Botan::byte> passPhrase = hash->final();
		QString hexPP;
		for(Botan::byte const *b=passPhrase.begin(); b<passPhrase.end(); b++)
			hexPP += QString("00" + QString::number(*b, 16)).right(2);

		string pem=Botan::PKCS8::PEM_encode(key, rng, hexPP.toAscii().data());

		// Make sure the target directory actually exists
		QDir d;
		d.mkpath(QDir::homePath() + "/.e5");

		QFile privOut(privKey);
		if(privOut.open(QFile::WriteOnly|QFile::Truncate)) {
			privOut.write(pem.c_str(), pem.length());
			privOut.close();
		}

		// Convert to public key
		pem=Botan::X509::PEM_encode(key);
		QFile pubOut(pubKey);
		if(pubOut.open(QFile::WriteOnly|QFile::Truncate)) {
			pubOut.write(pem.c_str(), pem.length());
			pubOut.close();
		}

#ifndef Q_OS_WIN
		if(utimes(QFile::encodeName(privKey), tv))
			return;
#else
		utimbuf tb;
		tb.actime = tb.modtime = timestamp;
		if(utime(QFile::encodeName(privKey), &tb))
			return;
#endif
	} catch(std::exception const &e) {
		cerr << "Error while generating keypair: " << e.what() << endl;
	}

	loadKeys(hwKey);
}

MachineKey::MachineKey():QObject(0),privKey(QDir::homePath() + "/.e5/privkey.pem"),pubKey(QDir::homePath() + "/.e5/pubkey.pem")
{
	if(QFile::exists("/sys/devices/system/sysset/sysset0/fasm/serial") && QFile::exists("/sys/devices/system/sysset/sysset0/display/serial")) {
		// We're on an iRex reader -- no MAC address, but plenty of
		// other things to go by...
		QString devKey = readFile("/sys/devices/system/sysset/sysset0/fasm/serial") + ":" + readFile("/sys/devices/system/sysset0/display/serial");
		if(QFile::exists(privKey) && QFile::exists(pubKey))
			loadKeys(devKey);

		if(!_privKey || !_pubKey) {
			generateKeys(devKey);
		}
		try {
			Botan::HashFunction *hash = Botan::get_hash("SHA-1");
			hash->update(Botan::X509::PEM_encode(*_pubKey));
			Botan::SecureVector<Botan::byte> fp=hash->final();
			_fingerprint.clear();
			for(Botan::byte const *b=fp.begin(); b<fp.end(); b++)
				_fingerprint += QString("00" + QString::number(*b, 16)).right(2);
			delete hash;
		} catch(std::exception const &e) {
			cerr << "Error while fingerprinting keypair: " << e.what() << endl;
		}
		return;
	}

	QString mac;
	if(QFile::exists(privKey) && QFile::exists(pubKey)) {
		QList<QNetworkInterface> const &interfaces=QNetworkInterface::allInterfaces();
		foreach(QNetworkInterface const &intf, interfaces) {
			mac=intf.hardwareAddress();
			loadKeys(mac);
			if(_privKey && _pubKey)
				break;
		}
	}

	if(!_privKey || !_pubKey) {
		generateKeys(macAddress());
	}

	try {
		Botan::HashFunction *hash = Botan::get_hash("SHA-1");
		hash->update(Botan::X509::PEM_encode(*_pubKey));
		Botan::SecureVector<Botan::byte> fp=hash->final();
		_fingerprint.clear();
		for(Botan::byte const *b=fp.begin(); b<fp.end(); b++)
			_fingerprint += QString("00" + QString::number(*b, 16)).right(2);
		delete hash;
	} catch(std::exception const &e) {
		cerr << "Error while fingerprinting keypair: " << e.what() << endl;
	}
}

MachineKey *MachineKey::instance() {
	if(!_instance)
		_instance=new MachineKey;
	return _instance;
}

PublicKey const &MachineKey::publicKey() const {
	return *_pubKey;
}

// FIXME this may not be the most secure thing to export
PrivateKey const &MachineKey::privateKey() const {
	return *_privKey;
}

QString MachineKey::publicKeyPEM() const {
	return Botan::X509::PEM_encode(*_pubKey).c_str();
}

QString const &MachineKey::fingerprint() const {
	return _fingerprint;
}
