#include "e5DRMContainer"
#include "MachineKey"
#include "AES"
#include <QFile>
#include <QDataStream>
#include <QBuffer>
#include <QIOStream>

using namespace std;

static QHash<QString,Botan::SymmetricKey> keyCache;

static bool openDRMContainer(QFile &f, quint64 &aesEnd) {
	QDataStream data(&f);
	data.setVersion(QDataStream::Qt_4_5);
	data.setByteOrder(QDataStream::LittleEndian);
	quint16 magic, version;
	data >> magic;
	data >> version;

	if(magic == 0xf00d) {
		if(version > 0x0000)
			return false;
		try {
			PrivateKey const &key=MachineKey::instance()->privateKey();
#if BOTAN_VERSION_CODE < BOTAN_VERSION_CODE_FOR(1, 9, 15)
			Botan::PK_Decryptor *decryptor = Botan::get_pk_decryptor(key, "EME1(SHA-256)");
#else
			Botan::PK_Decryptor *decryptor = new Botan::PK_Decryptor_EME(key, "EME1(SHA-256)");
#endif

			quint32 aesLength;
			data >> aesLength;
			quint64 aesBlock=f.pos();
			aesEnd = aesBlock+aesLength;
			QList<Botan::SymmetricKey> possibleKeys;
			if(!keyCache.contains(f.fileName())) {
				data.skipRawData(aesLength);
				while(!data.atEnd()) {
					uint codeLength;
					char *code;
					data.readBytes(code, codeLength);
					try {
						Botan::SecureVector<Botan::byte> decryptedKey=decryptor->decrypt(reinterpret_cast<Botan::byte const * const>(code), codeLength);
						if(!decryptedKey.size()) {
							cerr << "Decryption failed" << endl;
							delete[] code;
							continue;
						}
						QByteArray possibleKey(reinterpret_cast<char const * const>(decryptedKey.begin()), decryptedKey.size());
						QByteArray keyIn;
						bool ok;
						for(int i=0; i<possibleKey.count(); i+=2) {
							QString k(possibleKey.data()+i);
							keyIn.append(k.left(2).toInt(&ok, 16));
							if(!ok) break;
						}
						if(ok)
							possibleKeys << Botan::SymmetricKey(reinterpret_cast<Botan::byte const *>(keyIn.constData()), keyIn.size());
					} catch(std::exception const &e) {
						// This (almost certainly) means that the
						// key was not addressed to us...
						// So let's move on and try the next one
					}
					delete[] code;
				}
				delete decryptor;
			} else
				possibleKeys << keyCache[f.fileName()];

			// It is extremely unlikely (but not 100% impossible) that we
			// have more than 1 possibleKey -- it would be odd for
			// something encoded to another key to produce something that
			// looks exactly like a valid AES key.
			if(possibleKeys.count()==0) {
				cerr << "No matching keys for " << f.fileName() << endl;
				return false;
			}
			foreach(Botan::SymmetricKey k, possibleKeys) {
				AES aes(k);
				f.seek(aesBlock);
				QDataStream contentStream(&f);
				contentStream.setVersion(QDataStream::Qt_4_5);
				contentStream.setByteOrder(QDataStream::LittleEndian);
				char *encryptedHeaderData;
				uint encryptedHeaderLength;
				contentStream.readBytes(encryptedHeaderData, encryptedHeaderLength);
				QByteArray decryptedHeader=aes.decrypt(encryptedHeaderData, encryptedHeaderLength);
				delete[] encryptedHeaderData;

				QDataStream headerStream(&decryptedHeader, QIODevice::ReadOnly);
				headerStream.setVersion(QDataStream::Qt_4_5);
				headerStream.setByteOrder(QDataStream::LittleEndian);
				quint16 magic;
				headerStream >> magic;
				if(magic != 0xaffe) {
					cerr << "Karim header missing" << endl;
					continue;
				}
				quint16 version;
				headerStream >> version;
				if(version > 0x0000) {
					cerr << "Version header wrong" << endl;
					// Too new for this version of the library
					continue;
				}
				keyCache[f.fileName()] = k;
				return true;
			}
		} catch(std::exception const &e) {
			return false;
		}
	}
	return false;
}

QStringList e5DRMContainer::originalFiles() const {
	QString const file=_url.toLocalFile();
	QFile f(file);
	if(!f.open(QFile::ReadOnly))
		return QStringList();

	quint64 aesEnd;
	if(!openDRMContainer(f, aesEnd))
		return QStringList();
	
	try {
		AES aes(keyCache[f.fileName()]);
		QDataStream contentStream(&f);
		contentStream.setVersion(QDataStream::Qt_4_5);
		contentStream.setByteOrder(QDataStream::LittleEndian);

		QStringList files;
		while(f.pos() < aesEnd) {
			char *encryptedFileNameData;
			uint fileNameLength;
			contentStream.readBytes(encryptedFileNameData, fileNameLength);
			QByteArray decryptedFilename=aes.decrypt(encryptedFileNameData, fileNameLength);
			delete[] encryptedFileNameData;
			quint32 length;
			contentStream >> length;
			contentStream.skipRawData(length);
			files << decryptedFilename;
		}
		return files;
	
	} catch(std::exception const &e) {
		cerr << "Botan threw exception " << e.what() << " while opening " << f.fileName() << endl;
	}
	return QStringList();
}

QIODevice* e5DRMContainer::openOriginal(QString const &filename) const {
	QString const file=_url.toLocalFile();
	QFile in(file);
	if(!in.open(QFile::ReadOnly))
		return 0;
	quint64 aesEnd = 0;
	if(!openDRMContainer(in, aesEnd))
		return 0;

	try {
		AES aes(keyCache[in.fileName()]);
		QDataStream contentStream(&in);
		contentStream.setVersion(QDataStream::Qt_4_5);
		contentStream.setByteOrder(QDataStream::LittleEndian);
		while(in.pos() < aesEnd) {
			char *encryptedFileNameData;
			uint fileNameLength;
			contentStream.readBytes(encryptedFileNameData, fileNameLength);
			QByteArray decryptedFilename=aes.decrypt(encryptedFileNameData, fileNameLength);
			delete[] encryptedFileNameData;
			quint32 length;
			contentStream >> length;
			if(filename == decryptedFilename) {
				char *encryptedContentData=new char[length];
				contentStream.readRawData(encryptedContentData, length);
				QByteArray fileContent=aes.decrypt(encryptedContentData, length);
				QIODevice *b=new QBuffer(static_cast<QObject*>(0));
				static_cast<QBuffer*>(b)->setData(reinterpret_cast<char const * const>(fileContent.begin()), fileContent.size());
				static_cast<QBuffer*>(b)->open(QBuffer::ReadOnly);
				delete[] encryptedContentData;
				return b;
			} else
				contentStream.skipRawData(length);
		}
	} catch(std::exception const &e) {
		cerr << "Botan threw exception " << e.what() << " while reading " << file << endl;
		return 0;
	}
}


qint64 e5DRMContainer::sizeOriginal(QString const &filename) const
{
	QString const file=_url.toLocalFile();
	QFile in(file);
	if(!in.open(QFile::ReadOnly))
		return -1;
	quint64 aesEnd = 0;
	if(!openDRMContainer(in, aesEnd))
		return -1;

	try {
		AES aes(keyCache[in.fileName()]);
		QDataStream contentStream(&in);
		contentStream.setVersion(QDataStream::Qt_4_5);
		contentStream.setByteOrder(QDataStream::LittleEndian);
		while(in.pos() < aesEnd) {
			char *encryptedFileNameData;
			uint fileNameLength;
			contentStream.readBytes(encryptedFileNameData, fileNameLength);
			QByteArray decryptedFilename=aes.decrypt(encryptedFileNameData, fileNameLength);
			delete[] encryptedFileNameData;
			quint32 length;
			contentStream >> length;
			if(filename == decryptedFilename) {
				char *encryptedContentData=new char[length];
				contentStream.readRawData(encryptedContentData, length);
				QByteArray fileContent=aes.decrypt(encryptedContentData, length);
				delete[] encryptedContentData;
				return fileContent.length();
			} else
				contentStream.skipRawData(length);
		}
	} catch(std::exception const &e) {
		cerr << "Botan threw exception " << e.what() << " while reading " << file << endl;
		return -1;
	}
}
