#include "ZipfileContainer"
#include <QDir>
#include <QBuffer>
#include <quazip.h>
#include <quazipfile.h>

QStringList ZipfileContainer::originalFiles() const {
	QString const file=_url.toLocalFile();
	if(!QFile::exists(file))
		return QStringList();

	QuaZip zipFile(file);
	if(!zipFile.open(QuaZip::mdUnzip) || !zipFile.goToFirstFile())
		return QStringList();

	QStringList files;
	do {
		files << zipFile.getCurrentFileName().replace('\\', '/');
	} while(zipFile.goToNextFile());
	zipFile.close();
	return files;
}

QIODevice* ZipfileContainer::openOriginal(QString const &filename) const {
	QuaZipFile *f=new QuaZipFile(_url.toLocalFile(), filename);
	if(!f->open(QIODevice::ReadOnly)) {
		delete f;
		return 0;
	}
#ifdef QT_WEBKIT_WORKS_WITH_QUAZIP
	return f;
#else
	QBuffer *b=new QBuffer;
	b->setData(f->readAll());
	f->close();
	delete f;
	if(b->open(QIODevice::ReadOnly))
		return b;
	delete b;
	return 0;
#endif
}


qint64 ZipfileContainer::sizeOriginal(QString const &filename) const
{
	QuaZipFile file(_url.toLocalFile(), filename);
	if(!file.open(QIODevice::ReadOnly)) {
		return -1;
	}

	return file.size();
}
