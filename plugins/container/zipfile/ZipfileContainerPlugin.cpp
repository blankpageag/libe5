#include <QDir>
#include <QtPlugin>
#include "ZipfileContainerPlugin"
#include "ZipfileContainer"
#include <quazip.h>

CONST uint8_t ZipfileContainerPlugin::priority(QUrl const &container) const {
	QString const file=container.toLocalFile();
	if(!QFile::exists(file))
		return 0;

	QuaZip zipFile(file);
	if(!zipFile.open(QuaZip::mdUnzip) || !zipFile.goToFirstFile())
		return 0;

	return 5;
}

Container *ZipfileContainerPlugin::get(QUrl const &container) const {
	return new ZipfileContainer(container, 0);
}

EXPORT_PLUGIN(zipfileContainer, ZipfileContainerPlugin);
