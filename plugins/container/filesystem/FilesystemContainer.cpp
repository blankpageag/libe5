#include "FilesystemContainer"
#include <QDir>
#include <DirTools>

static QUrl escapeUrl(QUrl const &url) {
	if(!url.toString().endsWith('/')) {
		return QUrl::fromEncoded(url.toEncoded() + '/');
	}
	return url;
}

FilesystemContainer::FilesystemContainer(QUrl const &url, QObject *parent):Container(escapeUrl(url),parent) {
}

QStringList FilesystemContainer::originalFiles() const {
	if(_fileListCache.isEmpty()) {
		QDir d(_url.toLocalFile());
		if(!d.exists())
			return QStringList();
		_fileListCache=DirTools::files(d);
	}
	return _fileListCache;
}

QIODevice *FilesystemContainer::openOriginal(QString const &filename) const {
	QFile *f=new QFile(_url.toLocalFile() + "/" + filename);
	if(!f || !f->exists()) {
		if(f)
			delete f;
		return 0;
	}
	f->open(QFile::ReadOnly);
	return f;
}

QString FilesystemContainer::extract(QString const &fn) {
	if(UNLIKELY(!originalFiles().contains(fn)))
		return Container::extract(fn);
	return _url.resolved(fn).toLocalFile();
}

qint64 FilesystemContainer::sizeOriginal(QString const &filename) const
{
	QFileInfo info(_url.toLocalFile() + "/" + filename);
	if(info.exists()) {
		return info.size();
	} else {
		return -1;
	}
}
