#include <QDir>
#include <QtPlugin>
#include "FilesystemContainerPlugin"
#include "FilesystemContainer"

CONST uint8_t FilesystemContainerPlugin::priority(QUrl const &container) const {
	QDir d(container.toLocalFile());
	if(d.exists())
		return 1;
	return 0;
}

Container *FilesystemContainerPlugin::get(QUrl const &container) const {
	return new FilesystemContainer(container, 0);
}

EXPORT_PLUGIN(filesystemContainer, FilesystemContainerPlugin);
