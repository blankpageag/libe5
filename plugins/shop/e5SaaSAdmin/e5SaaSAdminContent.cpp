#include "e5SaaSAdminContent"
#include "e5SaaSAdminShop"
#include "e5SaaSAdminCategory"
#include "e5Commerce"

#include <Consumable>
#include <QDebug>
#include <QNetworkRequest>
#include <QNetworkReply>

#include <Downloader>
#include <Base64>
#include <Uploader>

e5SaaSAdminContent::e5SaaSAdminContent(QString const &author,  QString const &publisher, QString const &releaseDate, QString const &name, QString const &description, QString const &productId, QString const &fileId, QUrl const &iconUrl, e5SaaSAdminCategory *parent):Consumable(author, publisher, releaseDate, name, description, iconUrl, QUrl(), parent)
{
	insert("file_id", fileId);
	insert("product_id", productId);
}

QByteArray e5SaaSAdminContent::get(QNetworkAccessManager* nam, int start, int end) 
{
	e5SaaSAdminShop const * const shop=static_cast<e5SaaSAdminCategory*>(_parent)->shop();
	QUrl url=QUrl::fromEncoded(QString("http://" + shop->contentServer() + "download/" + value("file_id") + "?sessionId=" + shop->sessionID() + "&customerSessionId=" + shop->sessionID() + "&accountId=" + shop->accountID()).toAscii());
	qDebug() << "Get e5SaaSAdmin content " << start << " - " <<end<<" from url: "<<url;
	QByteArray byte = Downloader::get(url, this, nam, start, end);
	return byte; 
}


bool e5SaaSAdminContent::put(QIODevice *device, QNetworkAccessManager *nam)
{
	e5SaaSAdminShop const * const shop = static_cast<e5SaaSAdminCategory*>(_parent)->shop();
	Uploader uploader(QString("http://%1/commerceadmin/index/uploadFile/").arg(shop->url().host()));
	uploader.addObserver(this);
	uploader.insertField("productId", productId());
	uploader.insertField("fileTypeId", "file");
	uploader.insertIODevice("entryFile", "content.e5", device);

	QHash<QByteArray, QByteArray> cookie;
	cookie.insert("PHPSESSID", shop->sessionID().toAscii());
	uploader.setCookie(cookie);

	if(nam) {
		uploader.setNetworkAccessManager(nam);
	}

	QByteArray response = uploader.blockingPost();
	qDebug() << "Response to file upload:" << response;
	return response == "UPLOAD_OK";
}


QString e5SaaSAdminContent::fileId() const
{
	return value("file_id");
}


QString e5SaaSAdminContent::productId() const
{
	return value("product_id");
}
