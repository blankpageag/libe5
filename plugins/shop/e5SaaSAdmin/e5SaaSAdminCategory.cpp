#include "e5SaaSAdminCategory"
#include "e5SaaSAdminShop"
#include "e5SaaSAdminContent"
#include "e5Commerce"

e5SaaSAdminCategory::e5SaaSAdminCategory(QString const &name, QString const &description, int categoryId, e5SaaSAdminCategory *parent, e5SaaSAdminShop *shop):ItemCategory(name, description, QUrl(), parent),_id(categoryId),_shop(shop)
{
}

bool e5SaaSAdminCategory::load() 
{
	_loaded = true;
	if(_id == 0) {
		// This is the root category... We know what goes in there
		new e5SaaSAdminCategory(QObject::tr("Books"), QObject::tr("Books"), 12, this, _shop);
		new e5SaaSAdminCategory(QObject::tr("Magazines"), QObject::tr("Magazines"), 13, this, _shop);
		new e5SaaSAdminCategory(QObject::tr("Newspapers"), QObject::tr("Newspapers"), 14, this, _shop);
		return true;
	}

	QVariantList params;
	params << _id;
	QVariant r = _shop->call(_shop->sessionID(), QString("commerceAdmin_product.all"), params);
	QVariantList products=r.toMap().value("products").toList();
	for(int i=0; i<products.count(); i++) {
		QVariantMap p=products.at(i).toMap();
		new e5SaaSAdminContent(p["author"].toString(), p["publisher"].toString(), p["release_date"].toString(), p["name"].toString(), p["long_description"].toString(), p["product_id"].toString(), p["content_server_file_id"].toString(), p["preview_image_file_id"].toString(), this);
		qDebug() << "Added product" << p["name"].toString();
	}
	return true;
}    
