#include "e5SaaSAdminShop"
#include "e5SaaSAdminCategory"
#include "e5Commerce"

#include <QObject>
#include <QDebug>

#include <Downloader>
#include <Base64>

e5SaaSAdminShop::e5SaaSAdminShop(QUrl url):Shop(QObject::tr("e5 Commerce v2.0 Admin Interface"), QObject::tr("Connection to BlankPage's e5 Commerce v2.0 admin backend"), url),e5Commerce(url)
{
}

bool e5SaaSAdminShop::canUpload()
{
	return uploadAllowed(sessionID(), accountID());
}

ItemCategory *e5SaaSAdminShop::rootCategory()
{
	if(_root)
		return _root;

	if( sessionID().isEmpty() )
		return NULL;

	QVariant result = call(sessionID(), QString("commerceAdmin_admin.profile"), QVariantList() << sessionID());
	if(!result.isValid())
		return 0;

	insert("account_id", result.toMap().value("account").toMap().value("idmd5").toString());
	insert("content_server_hostname", result.toMap().value("content_server_hostname").toString());
	insert("content_server_port", result.toMap().value("content_server_port").toString());
	
	return _root = new e5SaaSAdminCategory("root", "root category", 0, 0, this);
}

QString e5SaaSAdminShop::contentServer() const
{
	return value("content_server_hostname") + ":" + value("content_server_port") + "/"; 
}

e5SaaSAdminShop::~e5SaaSAdminShop()
{
	endSession(sessionID());
}


bool e5SaaSAdminShop::login(QString const &username, QString const &password) 
{ 
	insert("session_id", e5Commerce::login( username, password ) );
	return !value("session_id").isEmpty();
}

QString e5SaaSAdminShop::accountID() const
{
	return value("account_id");
}
