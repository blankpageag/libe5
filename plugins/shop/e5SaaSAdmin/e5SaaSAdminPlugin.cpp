#include <QtPlugin>
#include "e5SaaSAdminPlugin"
#include "e5SaaSAdminShop"

CONST uint8_t e5SaaSAdminPlugin::priority(QUrl const &url) const 
{
	if(url.scheme() != "admin" && url.scheme() != "admins")
		return 0;
	if(url.host().toLower().contains("api.blankpage.ch"))
		return 255;
	return 0;
}

Shop *e5SaaSAdminPlugin::get(QUrl const &url) const {
	QUrl realUrl(url);
	if(realUrl.scheme() == "admin")
		realUrl.setScheme("http");
	else
		realUrl.setScheme("https");
	return new e5SaaSAdminShop(realUrl);
}

EXPORT_PLUGIN(e5SaaSAdmin, e5SaaSAdminPlugin);
