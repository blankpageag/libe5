#include <QtPlugin>
#include "FeedbooksPlugin"
#include "FeedbooksShop"

CONST uint8_t FeedbooksPlugin::priority(QUrl const &url) const {
	if(url.scheme() != "http" && url.scheme() != "https")
		return 0;
	if(url.host().toLower().contains("feedbooks.com"))
		return 255;
	return 0;
}

Shop *FeedbooksPlugin::get(QUrl const &url) const {
	return new FeedbooksShop(url);
}

EXPORT_PLUGIN(feedbooks, FeedbooksPlugin);
