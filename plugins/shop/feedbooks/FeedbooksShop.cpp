#include "FeedbooksShop"
#include <ItemCategory>
#include <Consumable>
#include <QDomDocument>
#include <QDomElement>
#include <QObject>
#include <QDebug>

#include <Downloader>
#include <Base64>

class FeedbooksCategory:public ItemCategory {
public:
	FeedbooksCategory(QString const &name, QString const &description, QUrl const &iconUrl, QString startpoint, FeedbooksCategory *parent=0, FeedbooksShop *shop=0);
	bool load();
private:
	QUrl _startPoint;
	FeedbooksShop *_shop;
};

class FeedbooksShopContent:public Consumable {
public:
	FeedbooksShopContent(QString const &author,  QString const &publisher, QString const &releaseDate, QString const &name, QString const &description, QUrl const &iconUrl, QUrl const &downloadUrl, FeedbooksCategory *parent=0): 
	Consumable( author, publisher, releaseDate, name, description,iconUrl, QUrl(), parent),_downloadUrl(downloadUrl) {}
	QByteArray get(QNetworkAccessManager* nam=NULL, int start=0, int end=-1) { return Downloader::get(_downloadUrl); }
private:
	QUrl const _downloadUrl;
};


FeedbooksCategory::FeedbooksCategory(QString const &name, QString const &description, QUrl const &iconUrl, QString startpoint, FeedbooksCategory *parent, FeedbooksShop *shop):
ItemCategory(name, description, iconUrl, parent),_shop(shop) {
	if(startpoint.isEmpty())
		startpoint = "http://www.feedbooks.com/catalog.atom";
	else if(!startpoint.contains("://")) {
		if(!startpoint.startsWith('/'))
			startpoint = "/" + startpoint;
		startpoint = "http://www.feedbooks.com" + startpoint;
	}
	_startPoint=QUrl::fromEncoded(startpoint.toUtf8());
	
}

bool FeedbooksCategory::load() {
	
	//qDebug() << "Loading content from "<< name();
	
	QByteArray content=Downloader::get(_startPoint);

	QDomDocument doc;
	doc.setContent(content);
	QDomNodeList feeds=doc.elementsByTagName("feed");
	
	
	//qDebug() << doc.toString();
	for(int i=0; i<feeds.count(); i++) 
	{
		QDomNode const feed=feeds.at(i);
		QDomNode c=feed.firstChild();
		
		
		while(!c.isNull()) {
			QDomElement e=c.toElement();
			c=c.nextSibling();
			if(e.isNull())
				continue;

			QString const tag=e.tagName();
			if(tag == "entry") {
								
				QString const title = e.firstChildElement("title").text();
				QString const description = e.firstChildElement("content").text();
				QString const publisher = e.firstChildElement("title").text();
				QString const releaseDate = e.firstChildElement("title").text();
				QString const author = e.firstChildElement("author").firstChildElement("name").text();
				QString icon;
				// e.firstChildElement("updated") contains a timestamp
				QDomElement currentLink = e.firstChildElement("link");
				QUrl link;
				bool haveEverything = false;
				bool isContent;
				while(!currentLink.isNull()) {
					QString const type = currentLink.attribute("type");
					QString const dest = currentLink.attribute("href");
					QString const rel = currentLink.attribute("rel");
					if(rel == "http://opds-spec.org/thumbnail" || rel == "http://opds-spec.org/cover" /* Old version of OPDS */ ||
					   rel == "http://opds-spec.org/image/thumbnail" || rel == "http://opds-spec.org/image/cover" /* Current version of OPDS */) {
						// We prefer covers, but we'll take a thumbnail...
						if(icon.isEmpty() || rel.endsWith("cover")) {
							icon = dest;
						}
					} else if(rel == "http://opds-spec.org/acquisition" /* used to be there */ || rel == "subsection" /* there now */) {
						if(type == "application/atom+xml") {
							if(_shop->_seenUrls.contains(dest)) {
								qDebug() << "Recursion detected on " << dest;
							} else {
								_shop->_seenUrls << dest;
								link=QUrl::fromEncoded(dest.toUtf8());
								isContent=false;
								haveEverything=true;
							}
						} else if(type == "application/epub+zip") {
							link=QUrl(currentLink.attribute("href"));
							isContent=true;
							haveEverything=true;
						}
					}
					currentLink = currentLink.nextSiblingElement("link");
				}
				if(haveEverything) {
					if(isContent)
					{
						//qDebug() << "Add new content "<<title<<" to category "<< this->name();
						new FeedbooksShopContent(author, publisher, releaseDate, title, description, icon, link, this);
					}
					else
						//qDebug() << "Add new category "<<title << " desc: " << description;
						new FeedbooksCategory(title, description, QUrl::fromEncoded(icon.toUtf8()), link.toString(), this, _shop);
				}
			}
		}
	}
	_loaded=true;
}

FeedbooksShop::FeedbooksShop(QUrl url):Shop("Feedbook shop", "Feedbook shop", url),_seenUrls() {}

ItemCategory *FeedbooksShop::rootCategory() 
{
	if(!_root)
		_root=new FeedbooksCategory(QObject::tr("Feedbooks"), QObject::tr("Feedbooks"),QUrl("http://assets2.feedbooks.net/images/layout/logo.png"), _url.toString(), 0, this);
	return _root;
}
