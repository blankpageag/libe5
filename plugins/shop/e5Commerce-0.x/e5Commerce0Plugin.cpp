#include <QtPlugin>
#include "e5Commerce0Plugin"
#include "e5Commerce0Shop"

CONST uint8_t e5Commerce0Plugin::priority(QUrl const &url) const {
	if(url.scheme() != "http" && url.scheme() != "https")
		return 0;
	if(url.host().toLower().contains("akilibooks.com"))
		return 255;
	return 0;
}

Shop *e5Commerce0Plugin::get(QUrl const &url) const {
	return new e5Commerce0Shop(url);
}

EXPORT_PLUGIN(e5commerce0, e5Commerce0Plugin);
