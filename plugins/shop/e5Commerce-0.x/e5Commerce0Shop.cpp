#include "e5Commerce0Shop"
#include <ItemCategory>
#include <Consumable>
#include <QDomDocument>
#include <QDomElement>
#include <QEventLoop>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QObject>
#include <QMap>
#include <QVariant>
#include <QDebug>
#include <CompilerTools>

#include <cassert>

#include <parser.h> // QJSON

#include <Downloader>
#include <Base64>

class e5Commerce0Category:public ItemCategory {
	friend class e5Commerce0Consumable;
public:
	e5Commerce0Category(QString const &name, QString const &description, QUrl const &iconUrl, int id=-1, e5Commerce0Category *parent=0, e5Commerce0Shop *shop=0);
	bool load();
private:
	int _id;
	e5Commerce0Shop *_shop;
};

class e5Commerce0Consumable:public Consumable {
public:
	e5Commerce0Consumable(QString const &author, QString const &publisher, QString const &releaseDate, QString const &name, QString const &description, QUrl const &iconUrl, QString const &contentID, ItemCategory *parent=0):
	Consumable(author, publisher, releaseDate, name, description, iconUrl, QUrl(), parent),_contentID(contentID) {}
	QByteArray get(QNetworkAccessManager* nam=NULL, int start=0, int end=-1);
private:
	QString const _contentID;
};

QByteArray e5Commerce0Consumable::get(QNetworkAccessManager* nam, int start, int end){
	QUrl const &u=static_cast<e5Commerce0Category const *>(ShopItem::parent())->_shop->url();
	qDebug() << "Downloader::get" << (u.resolved(QString("content/books/" + _contentID + "/download/")));
	return Downloader::get(u.resolved(QString("content/books/" + _contentID + "/download/")));
#if 0
	// This is for non-free content -- currently not implemented
	QUrl offerUrl=u.resolved("content/books/" + _contentID + "/offers/");
	QByteArray offers=Downloader::get(offerUrl);
	// At some point we'll get an offer ID here...
	qDebug() << "Download" << _contentID << "(" << _name << ") from " << offerUrl << ":" << offers.data();
	QString offerID="0";
	QUrl purchaseUrl=u.resolved("offers/" + offerID + "/purchase/");
	QNetworkRequest req(purchaseUrl);
	if(!u.userName().isEmpty() || !u.password().isEmpty())
		req.setRawHeader("Authorization", "Basic " + QString(u.userName() + ":" + u.password()).toAscii().toBase64());
	QNetworkAccessManager *tempnam=new QNetworkAccessManager(0);
	QEventLoop l;
	QNetworkReply *reply=tempnam->post(req, QByteArray());
	QObject::connect(reply, SIGNAL(finished()), &l, SLOT(quit()));
	l.exec();
	QUrl booksUrl=u.resolved(QString("bookshelf/books/"));
	QByteArray books=Downloader::get(booksUrl);
	// Get the item ID from whatever structure this returns
	QString itemID="0";
	QUrl downloadUrl=u.resolved("bookshelf/" + itemID + "/download/");
	return Downloader::get(downloadUrl);
#endif
}

e5Commerce0Category::e5Commerce0Category(QString const &name, QString const &description, QUrl const &iconUrl, int id, e5Commerce0Category *parent, e5Commerce0Shop *shop):ItemCategory(name, description, iconUrl, parent),_id(id),_shop(shop) {
}

bool e5Commerce0Category::load() {
	QString API="content/categories/books/";
	if(_id>=0)
		API += QString::number(_id) + "/";
	int page=1, pageCount;
	do {
		QByteArray content=Downloader::get(_shop->url().resolved(QUrl(API + "?page=" + QString::number(page) + "&items_per_page=9999")));
		QJson::Parser parser;
		bool ok;
		QVariant temp=parser.parse(content, &ok);
		QMap<QString,QVariant> master=temp.toMap();
		if(!ok || temp.type() != QVariant::Map || !master.contains("content") || !master.contains("meta")) {
			qWarning() << "Bogus response from server - expected a map with content and meta members, got a" << temp.type();
			return false;
		}
		
		temp=master.value("content");
		if(temp.type() != QVariant::List && temp.type() != QVariant::Map) {
			qWarning() << "Bogus response from server - expected a list, got a" << temp.type();
			return false;
		}
		if(temp.type() == QVariant::List) {
			QList<QVariant> l=temp.toList();
			for(int i=0; i<l.count(); i++) {
				if(l.at(i).type() != QVariant::Map) {
					qWarning() << "Bogus response from server - expected a map inside content";
					continue;
				}
				QMap<QString,QVariant> category=l.at(i).toMap();
				if(!category.contains("id") || !category.contains("label")) {
					qWarning() << "Bogus response from server - category without id and/or label";
					continue;
				}
				new e5Commerce0Category(category.value("label").toString(), category.value("description").toString(), QUrl(), category.value("id").toInt(), this, _shop);
			}
		} else {
			QMap<QString,QVariant> m=temp.toMap();
			if(m.contains("subcategories") && m.value("subcategories").type() == QVariant::List) {
				QList<QVariant> subcategories=m.value("subcategories").toList();
				for(int i=0; i<subcategories.count(); i++) {
					if(subcategories.at(i).type() != QVariant::Map) {
						qWarning() << "Bogus response from server - expected a map inside subcategories, got a" << subcategories.at(i).type();
						continue;
					}
					QMap<QString,QVariant> category=subcategories.at(i).toMap();
					if(!category.contains("id") || !category.contains("label")) {
						qWarning() << "Bogus response from server - subcategory without id and/or label";
						continue;
					}
					new e5Commerce0Category(category.value("label").toString(), category.value("description").toString(), QUrl(), category.value("id").toInt(), this, _shop);
				}
			}
		}
		
		temp=master.value("meta");
		if(temp.type() != QVariant::Map) {
			qWarning() << "Bogus meta response from server - expected a map, got a" << temp.type();
			break;
		}
		pageCount = temp.toMap().value("num_pages").toInt();
	} while(page++ < pageCount);
	if(_id>=0) { // No books in root category...
		page=1;
		API="content/books/?category=" + QString::number(_id);
		do {
			QByteArray content=Downloader::get(_shop->url().resolved(QUrl(API + "&page=" + QString::number(page) + "&items_per_page=9999")));
			QJson::Parser parser;
			bool ok;
			QVariant temp=parser.parse(content, &ok);
			QMap<QString,QVariant> master=temp.toMap();
			if(!ok || temp.type() != QVariant::Map || !master.contains("content") || !master.contains("meta")) {
				qWarning() << "Bogus response from server while getting books - expected a map with content and meta members, got a" << temp.type();
				return false;
			}
			
			temp=master.value("content");
			if(temp.type() != QVariant::List) {
				qWarning() << "Invalid content for books, expected a List, got a" << temp.type();
				break;
			}
			QVariantList contents=temp.toList();
			foreach(QVariant v, contents) 
			{
				
				if(v.type() != QVariant::Map) {
					qWarning() << "Invalid content for books, expected a Map, got a" << temp.type();
					continue;
				}
				QMap<QString,QVariant> m=v.toMap();
				
				QString author;
				foreach(QVariant a, m.value("authors").toList())
				{
					QMap<QString,QVariant> au=a.toMap();
					QString const name(au.value("name").toString());
					if(!author.isEmpty())
						author.append(", ");
					author.append(name);
				}
				
				e5Commerce0Consumable* con = new e5Commerce0Consumable(author, "Test Publisher", 
																	   m.value( "modified" ).toString(), 
																	   m.value("title").toString(), m.value("description").toString(), 
																	   QUrl::fromEncoded(m.value("image_src").toString().toUtf8()), 
																	   m.value("contentID").toString(), this);
			}
			
			temp=master.value("meta");
			if(temp.type() != QVariant::Map) {
				qWarning() << "Bogus meta response from server while getting books - expected a map, got a" << temp.type();
				break;
			}
			pageCount = temp.toMap().value("num_pages").toInt();
		} while(page++ < pageCount);
	}
	_loaded=true;
}

e5Commerce0Shop::e5Commerce0Shop(QUrl url):
Shop("e5 Commerce v1.0", "e5 Commerce v1.0", url),_seenUrls() {
}

bool e5Commerce0Shop::login(QString const &username, QString const &password) {
	_url.setUserName(username);
	_url.setPassword(password);
	qDebug() << "URL is now" << _url;
}

ItemCategory *e5Commerce0Shop::rootCategory() {
	if(!_root)
		_root=new e5Commerce0Category(QObject::tr("e5Commerce 0.x"), "", QUrl(), -1, 0, this);
	return _root;
}
