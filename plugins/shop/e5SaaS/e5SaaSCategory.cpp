#include "e5SaaSCategory"

#include "e5SaaSShop"
#include "e5Commerce"
#include "e5SaaSShopContent"
#include <ItemCategory>
#include <Consumable>
#include <QVariantList>
#include <QObject>
#include <QDebug>




e5SaaSCategory::e5SaaSCategory(QString const &categoryID, QString const &name, QString const &description, e5SaaSCategory *parent, e5SaaSShop *shop):
ItemCategory(name, description, QUrl(), parent),_shop(shop), _categoryID(categoryID)
{
	//qDebug() << "New e5Commerce category: "<< name<<"(id="<<_categoryID<<", "<< description<<")";
    insert("category_id", categoryID );
	loadSubcategories();
}

bool e5SaaSCategory::loadSubcategories()
{
	QVariantList result = _shop->shopConnector()->call(_shop->sessionID(), QString("commerceApi_category.getSubCategories"), QVariantList() << _categoryID).toList();
	foreach(QVariant v, result) {
		QVariantMap m=v.toMap();
		QString const catId=m.value("category_id").toString();
		//verqDebug() << "- " << m.value("category_id").toString() << ":" << m.value("name").toString() << "-" << m.keys();
		if(!_shop->_categories.contains(catId)) {
			e5SaaSCategory *cat=new e5SaaSCategory(catId, m.value("name").toString(), m.value("long_description").toString(), this, _shop);
			_shop->_categories.insert(catId, cat);
		}
	}
	return true;
}

bool e5SaaSCategory::load() 
{
	// Subcategories are already loaded by the categories() optimizer,
	// so we only have to care about products here
	QVariantList params;
	_loaded = true;

	// extract products:
	params << _categoryID << _shop->sandbox();
	//qDebug() <<params;
	QVariantList r = _shop->shopConnector()->call(_shop->sessionID(), QString("commerceApi_product.getProductsByCategory"), params).toMap().value("products").toList();
	foreach( QVariant var, r )
	{
		QVariantMap productMap = var.toMap();
		//qDebug() << productMap;
		e5SaaSShopContent* content = new e5SaaSShopContent(productMap.value("author").toString(),  productMap.value("publisher").toString(), productMap.value("release_date").toString(), productMap.value("name").toString(), productMap.value("long_description").toString(), productMap.value("product_id").toString(), QUrl( productMap.value("preview_image_file_id").toString()), QUrl( productMap.value("table_of_contents_preview_image_file_id").toString()), productMap.value("file_id").toString(), this, _shop);
		
		// set e5 SaaS specific tags:
		content->setPrice( productMap.value("price").toString());
		content->insert( "display_price", productMap.value("display_price").toString());
        
		if( !productMap.value("itunes_id").toString().isEmpty() )
		{
			content->insert("itunes_id", productMap.value("itunes_id").toString() );
		}
        
        if( !productMap.value("sku").toString().isEmpty() )
		{
			content->insert("sku", productMap.value("sku").toString() );
		}
        
        if( !productMap.value("product_type_id").toString().isEmpty() )
		{
			content->insert("product_type_id", productMap.value("product_type_id").toString() );
		}
        
        if( !productMap.value("only_available_in_product_bundle").toString().isEmpty() )
		{
			content->insert("only_available_in_product_bundle", productMap.value("only_available_in_product_bundle").toString() );
		}
        
        if( !productMap.value("product_bundle_product_list").toString().isEmpty() )
		{
			content->insert("product_bundle_product_list", productMap.value("product_bundle_product_list").toString() );
		}
        
        if( !productMap.value("purchased_by_logged_in_customer").toString().isEmpty() )
		{
			content->insert("purchased_by_logged_in_customer", productMap.value("purchased_by_logged_in_customer").toString() );
		}
		
		//qDebug() << "Product " << content->name() << " found in "<<this->name();
	}
}    

QList<ItemCategory const *> e5SaaSCategory::categories(int start, int end) const {
	if(_parent) {
		// For anything other than the root category,
		// go over the children -- the shop's category list
		// isn't hierarchic
		return ItemCategory::categories(start, end);
	}

	int const count=_shop->_categories.count();
	if(start>=count)
		return QList<ItemCategory const *>();
	if(end<0 || end>=count)
		end=count-1;
	QList<ItemCategory const *> categories;
	for(int i=start; i<=end; i++) {
		categories << _shop->_categories.at(i).second;
	}
	return categories;
}

int e5SaaSCategory::categoryCount() const {
	if(_parent) {
		// For anything other than the root category,
		// go over the children -- the shop's category list
		// isn't hierarchic
		return ItemCategory::categoryCount();
	}
	return _shop->_categories.count();
}
