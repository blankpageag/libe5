#include "e5SaaSShopContent"

#include "e5Commerce"
#include "e5SaaSShop"
#include "e5SaaSCategory"
#include "e5Commerce"

#include <Consumable>
#include <QDebug>
#include <QNetworkRequest>
#include <QNetworkReply>

#include <Downloader>
#include <Base64>

e5SaaSShopContent::e5SaaSShopContent(QString const &author,  QString const &publisher, QString const &releaseDate, QString const &name, QString const &description, QString const & productId, QUrl const &iconUrl, QUrl const &tocUrl, QString const &fileId, e5SaaSCategory *parent, e5SaaSShop *shop): 
Consumable( author, publisher, releaseDate, name, description,iconUrl, tocUrl, parent),_shop(shop)
{
	insert("file_id", fileId);
	insert("product_id", productId);
	//qDebug() << "New e5Commerce product: "<< name<<"(desc="<< description<<", fileID="<<fileId<<", productID="<<productId<<")";    
}


QByteArray e5SaaSShopContent::get(QNetworkAccessManager* nam, int start, int end) 
{ 
	QString contentLink= "http://" + _shop->downloadServer() + "download/";
	QString post;
	post += fileId() + "/?";
	post += "sessionId=" + _shop->sessionID() + "&";
	
	/**
	 * When we have a product which is free, the user can download without purchase
	 * this can be done by downloading with acountID=sessionID and sessionID=sessionID. 
	 */
	if( price().toFloat() <= 0 )
	{		
		qDebug() <<"Free download of free product with price " << price();
		post += "customerSessionId=" + _shop->sessionID() + "&";
	}
	else
	{
		qDebug() <<"Download of paid product for customer with price " << price();
		post += "customerSessionId=" + _shop->customerSessionID() + "&";
	}
	post += "accountId=" + _shop->accountID();
	QUrl url = QString(contentLink + post);
	qDebug() << "Get e5Saas content " << start << " - " <<end<<" from url: "<<url;
	QByteArray byte = Downloader::get(url, this, nam, start, end);
	return byte; 
}

bool e5SaaSShopContent::getInPackages(QString filePath, int packageSize, QNetworkAccessManager* nam, int start, int end) 
{ 
	QString contentLink= "http://" + _shop->downloadServer() + "download/";
	QString post;
	post += fileId() + "/?";
	post += "sessionId=" + _shop->sessionID() + "&";
	
	/**
	 * When we have a product which is free, the user can download without purchase
	 * this can be done by downloading with acountID=sessionID and sessionID=sessionID. 
	 */
	if( price().toFloat() <= 0 )
	{		
		qDebug() <<"Free download of free product with price " << price();
		post += "customerSessionId=" + _shop->sessionID() + "&";
	}
	else
	{
		qDebug() <<"Download of paid product for customer with price " << price();
		post += "customerSessionId=" + _shop->customerSessionID() + "&";
	}
	post += "accountId=" + _shop->accountID();
	QUrl url = QString(contentLink + post);
	qDebug() << "Get e5Saas content in "<< packageSize <<"byte packages: " << start << " - " <<end<<" from url: "<<url <<" to "<<filePath;
	return Downloader::download( url, filePath, this, nam, packageSize, start, end);
}


bool e5SaaSShopContent::get(QString filePath, QNetworkAccessManager* nam, int start, int end) 
{ 
	QString contentLink= "http://" + _shop->downloadServer() + "download/";
	QString post;
	post += fileId() + "/?";
	post += "sessionId=" + _shop->sessionID() + "&";
	
	/**
	 * When we have a product which is free, the user can download without purchase
	 * this can be done by downloading with acountID=sessionID and sessionID=sessionID. 
	 */
	if( price().toFloat() <= 0 )
	{		
		qDebug() <<"Free download of free product with price " << price();
		post += "customerSessionId=" + _shop->sessionID() + "&";
	}
	else
	{
		qDebug() <<"Download of paid product for customer with price " << price();
		post += "customerSessionId=" + _shop->customerSessionID() + "&";
	}
	post += "accountId=" + _shop->accountID();
	QUrl url = QString(contentLink + post);
	qDebug() << "Get e5Saas content " << start << " - " <<end<<" from url: "<<url <<" to "<<filePath;
	return Downloader::download( url, filePath, this, nam, start, end);
}


bool e5SaaSShopContent::getInPackages(QString filePath, int packageSize, QByteArray const & receipt, QNetworkAccessManager* nam, int start, int end) 
{ 
	qDebug() <<"Packaged download with iTunes receipt ";
	QString contentLink= "http://" + _shop->downloadServer() + "download/";
	QString getParams;
	getParams += fileId() + "/?";
	getParams += "sessionId=" + _shop->sessionID() + "&";
	//getParams += "customerSessionId=" + _shop->customerSessionID() + "&";
	getParams += "accountId=" + _shop->accountID();
	QUrl url = QString(contentLink + getParams);
	QHash<QByteArray, QByteArray> postParams;
	postParams.insert( "iTunesReceipt", receipt );
	return Downloader::download( url, filePath, postParams, this, nam, packageSize, start, end);
}

QByteArray e5SaaSShopContent::get(QByteArray const & receipt, QNetworkAccessManager* nam, int start, int end) 
{ 
	
	qDebug() <<"Download with iTunes receipt ";

	QString contentLink= "http://" + _shop->downloadServer() + "download/";
	QString getParams;
	getParams += fileId() + "/?";
	getParams += "sessionId=" + _shop->sessionID() + "&";
	//getParams += "customerSessionId=" + _shop->customerSessionID() + "&";
	getParams += "accountId=" + _shop->accountID();
	QUrl url = QString(contentLink + getParams);
	QHash<QByteArray, QByteArray> postParams;
	postParams.insert( "iTunesReceipt", receipt );
	QByteArray byte = Downloader::getWithPost( url, postParams, this, nam, start, end);
	return byte; 
}


int e5SaaSShopContent::downloadFileSize() const
{
	QString contentLink= "http://" + _shop->contentServer() + "download/";
	QString post;
	post += fileId() + "/?";
	//post += "sessionId=" + _shop->sessionID() + "&";
	//post += "customerSessionId=" + _shop->sessionID() + "&";
	//post += "accountId=" + _shop->accountID();
	QUrl url = QString(contentLink + post);
	int s = Downloader::getContentLength(url);
	qDebug() <<"Content size of consumable "<<fileId() << " is: "<<s<<" bytes";
	return s;
}


bool e5SaaSShopContent::restore(QByteArray receipt) const
{
	QVariant results = _shop->shopConnector()->canDownloadFileWithItunesReceipt(_shop->sessionID(), _shop->accountID(), fileId(), receipt );
	return results.toBool();
}


QString e5SaaSShopContent::displayPrice() const
{
	return value( "display_price");
}


QString e5SaaSShopContent::fileId() const
{
	return value( "file_id");
}

QString e5SaaSShopContent::productId() const
{
	return value("product_id");
}

QString e5SaaSShopContent::onlyAvailableInProductBundle() const
{
	return value("only_available_in_product_bundle");
}

bool e5SaaSShopContent::purchase() const 
{ 
	QVariantList params;
	params << productId() << _shop->customerSessionID() << _shop->value("customerPassword") << _shop->sandbox();
	QVariant result = _shop->shopConnector()->call(_shop->sessionID(), QString("commerceApi_customer.simplePurchase"),
																								 params);
	if( !result.toBool())
	{
		qWarning() <<"Purchase of product "<<name()<<"(id="<<productId()<<") failed!";
		return false;
	}
	
	qDebug() <<"Product "<<name()<<"(id="<<productId()<<") successfully purchased.";
	return true; 
}
