#include <QtPlugin>
#include "e5SaaSPlugin"
#include "e5SaaSShop"

CONST uint8_t e5SaaSPlugin::priority(QUrl const &url) const 
{
	//qDebug() << "test e5 saas shop";
	if(url.scheme() != "http" && url.scheme() != "https")
		return 0;
	if(url.host().toLower().contains("api.blankpage.ch"))
		return 255;
	return 0;
}

Shop *e5SaaSPlugin::get(QUrl const &url) const {
	return new e5SaaSShop(url);
}

EXPORT_PLUGIN(e5SaaS, e5SaaSPlugin);
