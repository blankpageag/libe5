#include "e5SaaSShop"


#include "e5SaaSCategory"
#include "e5SaaSShopContent"
#include "e5Commerce"

#include <QObject>
#include <QDebug>

#include <Downloader>
#include <Base64>


#define _E5_SAAS_NAME_ "e5 Commerce v2.0 backend"
#define _E5_SAAS_DESCRIPTION_ "Connection the to e5 Commerce 2.0 backend from BlankPage AG."

e5SaaSShop::e5SaaSShop(QUrl url):
Shop(_E5_SAAS_NAME_, _E5_SAAS_DESCRIPTION_, url), _shopConnector(NULL)
{
	//qDebug() << "Initiating "<<name()<<" ("<<description()<<") with sandbox mode "<<sandbox();
}


bool e5SaaSShop::checkSession()
{
	// dont use this api since you need to be logged in as a end user which is not the case for ios:

	/*
	QVariant res = shopConnector()->validateSession( sessionID(), customerSessionID(), accountID() );
	qDebug() << "Session status: "<< res.toBool();
	 */

	QMap<QString, QVariant> result = shopConnector()->call(sessionID(), QString("commerceApi_store.info"), sandbox()).toMap();
	insert( "name", result.value("name").toString());
	result = shopConnector()->call(sessionID(), QString("commerceApi_app.info"), sandbox()).toMap();

	QString acID = result.value("account_id").toString();
	return !(acID.isEmpty());
}


ItemCategory *e5SaaSShop::rootCategory() 
{
	if( sessionID().isEmpty() )
		return NULL;
	
	if(!_root)
		loadCategories();

	return _root;
}

void e5SaaSShop::loadCategories() {
	QMap<QString, QVariant> result = shopConnector()->call(sessionID(), QString("commerceApi_store.info"), sandbox()).toMap();
	QString rootCatID = result.value("root_category_id").toString();
	QString rootCatName = result.value("root_category_name").toString();
	QString rootCatDesc = result.value("root_category_description").toString();
	_root=new e5SaaSCategory(rootCatID, rootCatName, rootCatDesc, NULL, this);

//	result = shopConnector()->call(sessionID(), QString("commerceApi_store.getSubCategories"), rootCatID).toList();
//	qDebug() << "Categories:" << result;
}

QList<ShopContent> e5SaaSShop::getAllProductsInStore() {
	QVariantList params;
	params << sandbox() << 0 << 9999;
    
	QVariantList result = shopConnector()->call(sessionID(), QString("commerceApi_product.getAllProductsInStore"), params).toMap().value("products").toList();
	
    QList<ShopContent> allProducts = QList<ShopContent>();
    
    foreach( QVariant var, result )
	{
		QVariantMap productMap = var.toMap();
		//qDebug() << productMap;
		e5SaaSShopContent *content = new e5SaaSShopContent(productMap.value("author").toString(),  productMap.value("publisher").toString(), productMap.value("release_date").toString(), productMap.value("name").toString(), productMap.value("long_description").toString(), productMap.value("product_id").toString(), QUrl( productMap.value("preview_image_file_id").toString()), QUrl( productMap.value("table_of_contents_preview_image_file_id").toString()), productMap.value("file_id").toString(), NULL, this);
		
		// set e5 SaaS specific tags:
		content->setPrice( productMap.value("price").toString());
		content->insert( "display_price", productMap.value("display_price").toString());
        
        if( !productMap.value("file_id").toString().isEmpty() )
		{
			content->insert("file_id", productMap.value("file_id").toString() );
		}
        
		if( !productMap.value("itunes_id").toString().isEmpty() )
		{
			content->insert("itunes_id", productMap.value("itunes_id").toString() );
		}
        
        if( !productMap.value("product_type_id").toString().isEmpty() )
		{
			content->insert("product_type_id", productMap.value("product_type_id").toString() );
		}
        
        if( !productMap.value("only_available_in_product_bundle").toString().isEmpty() )
		{
			content->insert("only_available_in_product_bundle", productMap.value("only_available_in_product_bundle").toString() );
		}
        
        if( !productMap.value("product_bundle_product_list").toString().isEmpty() )
		{
			content->insert("product_bundle_product_list", productMap.value("product_bundle_product_list").toString() );
		}
        
        if( !productMap.value("purchased_by_logged_in_customer").toString().isEmpty() )
		{
			content->insert("purchased_by_logged_in_customer", productMap.value("purchased_by_logged_in_customer").toString() );
		}
        
        allProducts.append(*content);
	}
    
    return allProducts;
}

QList<ShopContent> e5SaaSShop::loadPurchasedProducts() {
	QVariantList params;
	params << sandbox();

	QVariantList result = shopConnector()->call(sessionID(), QString("commerceApi_product.getAllProductsInStorePurchasedByLoggedInUser"), params).toMap().value("products").toList();
	
    QList<ShopContent> purchasedProducts = QList<ShopContent>();
    
    foreach( QVariant var, result )
	{
		QVariantMap productMap = var.toMap();
		//qDebug() << productMap;
		e5SaaSShopContent *content = new e5SaaSShopContent(productMap.value("author").toString(),  productMap.value("publisher").toString(), productMap.value("release_date").toString(), productMap.value("name").toString(), productMap.value("long_description").toString(), productMap.value("product_id").toString(), QUrl( productMap.value("preview_image_file_id").toString()), QUrl( productMap.value("table_of_contents_preview_image_file_id").toString()), productMap.value("file_id").toString(), NULL, this);
		
		// set e5 SaaS specific tags:
		content->setPrice( productMap.value("price").toString());
		content->insert( "display_price", productMap.value("display_price").toString());
        
		if( !productMap.value("itunes_id").toString().isEmpty() )
		{
			content->insert("itunes_id", productMap.value("itunes_id").toString() );
		}
        
        if( !productMap.value("product_type_id").toString().isEmpty() )
		{
			content->insert("product_type_id", productMap.value("product_type_id").toString() );
		}
        
        if( !productMap.value("only_available_in_product_bundle").toString().isEmpty() )
		{
			content->insert("only_available_in_product_bundle", productMap.value("only_available_in_product_bundle").toString() );
		}
        
        if( !productMap.value("product_bundle_product_list").toString().isEmpty() )
		{
			content->insert("product_bundle_product_list", productMap.value("product_bundle_product_list").toString() );
		}
        
        if( !productMap.value("purchased_by_logged_in_customer").toString().isEmpty() )
		{
			content->insert("purchased_by_logged_in_customer", productMap.value("purchased_by_logged_in_customer").toString() );
		}
        
        purchasedProducts.append(*content);
	}
    
    return purchasedProducts;
}

QList<ShopContent> e5SaaSShop::loadSubscriptions() {
	QVariantList params;
	params << 16 << sandbox() << 0 << 9999;
    
	QVariantList result = shopConnector()->call(sessionID(), QString("commerceApi_product.getAllProductsByType"), params).toMap().value("products").toList();
	
    QList<ShopContent> subscriptions = QList<ShopContent>();
    
    foreach( QVariant var, result )
	{
		QVariantMap productMap = var.toMap();
		//qDebug() << productMap;
		e5SaaSShopContent *content = new e5SaaSShopContent(productMap.value("author").toString(),  productMap.value("publisher").toString(), productMap.value("release_date").toString(), productMap.value("name").toString(), productMap.value("long_description").toString(), productMap.value("product_id").toString(), QUrl( productMap.value("preview_image_file_id").toString()), QUrl( productMap.value("table_of_contents_preview_image_file_id").toString()), productMap.value("file_id").toString(), NULL, this);
		
		// set e5 SaaS specific tags:
		content->setPrice( productMap.value("price").toString());
		content->insert( "display_price", productMap.value("display_price").toString());
        
		if( !productMap.value("itunes_id").toString().isEmpty() )
		{
			content->insert("itunes_id", productMap.value("itunes_id").toString() );
		}
        
        if( !productMap.value("product_type_id").toString().isEmpty() )
		{
			content->insert("product_type_id", productMap.value("product_type_id").toString() );
		}
        
        if( !productMap.value("subscription_duration").toString().isEmpty() )
		{
			content->insert("subscription_duration", productMap.value("subscription_duration").toString() );
		}
        
        if( !productMap.value("product_subscription_product_list").toString().isEmpty() )
		{
			content->insert("product_subscription_product_list", productMap.value("product_subscription_product_list").toString() );
		}
        
        if( !productMap.value("purchased_by_logged_in_customer").toString().isEmpty() )
		{
			content->insert("purchased_by_logged_in_customer", productMap.value("purchased_by_logged_in_customer").toString() );
		}
        
        subscriptions.append(*content);
	}
    
    return subscriptions;
}

void e5SaaSShop::setShopInfo()
{
	QMap<QString, QVariant> result = shopConnector()->call(sessionID(), QString("commerceApi_store.info"), sandbox()).toMap();
	insert( "name", result.value("name").toString());
    insert( "aftersales_url", result.value("aftersales_url").toString());
	insert( "aftersales_url_active", result.value("aftersales_url_active").toString());
    
	result = shopConnector()->call(sessionID(), QString("commerceApi_app.info"), sandbox()).toMap();

	insert( "description", result.value("long_description").toString());
	insert( "account_id", result.value("account_id").toString());
	insert( "content_server_hostname", result.value("content_server_hostname").toString());
	insert( "content_server_port", result.value("content_server_port").toString());
    insert( "download_server_hostname", result.value("download_server_hostname").toString());
	insert( "download_server_port", result.value("download_server_port").toString());

	//qDebug() << "e5 Commerce with session id"<<sessionID()<<"/account id "<<accountID()<<":"<<name() << "("<<description()<<")";
	//qDebug() << "e5 Content Server set to: "<<contentServer();
    //qDebug() << "e5 Download Server set to: "<<downloadServer();
}

QString e5SaaSShop::contentServer() const
{
	return value("content_server_hostname") + ":" + value("content_server_port") + "/"; 
}

QString e5SaaSShop::downloadServer() const
{
	return value("download_server_hostname") + ":" + value("download_server_port") + "/"; 
}

e5SaaSShop::~e5SaaSShop()
{
	bool check = shopConnector()->endSession(sessionID());
	//qDebug() << "e5 Commerce connection with session id '"<<sessionID()<<"' close with result: "<<check;

	if( _shopConnector != NULL ) {
		delete _shopConnector;
		_shopConnector = NULL;
	}
}


bool e5SaaSShop::login(QString const &username, QString const &password) 
{ 
	insert("session_id", shopConnector()->login( username, password ) );
	if( sessionID().isEmpty() ) {
		//qWarning() << "Login to "<<name() <<" with "<< username <<"/" <<password <<" failed!";	
		return false;
	}

	//qDebug() << "e5 Commerce logged in with "<< username <<"/" <<password <<" and session id " << sessionID();	
	setShopInfo();
	return true; 
}

void e5SaaSShop::registerDeviceForPushNotifications(QString appName, QString appVersion, QString appLanguage, QString badgeAllowed, QString alertAllowed, QString soundAllowed, QString deviceName, QString deviceModel, QString systemVersion, QString deviceToken, QString subscriptionReceipt, QString subscriptionId, QString subscriptionExpirationTimestamp, QString isTestDevice)
{
    QStringList params;
	params << appName << appVersion << appLanguage << badgeAllowed << alertAllowed << soundAllowed << deviceName << deviceModel << systemVersion << deviceToken << subscriptionReceipt << subscriptionId << subscriptionExpirationTimestamp << isTestDevice;
	QVariant result = shopConnector()->call(sessionID(), QString("commerceApi_app.registerDeviceForPushNotifications"), params);
	//qDebug() << "e5SaaSShop::registerDeviceForPushNotifications: "<< result.toString();
}

e5Commerce* e5SaaSShop::shopConnector()
{ 
	if( _shopConnector == NULL )
		_shopConnector=new e5Commerce(url());
	return _shopConnector;
}

QString e5SaaSShop::accountID() const
{
	return value("account_id");
}

bool e5SaaSShop::updateCustomerUser(QString language, QString gender, QString firstname, QString lastname, QString dateOfBirth, QString company, QString street, QString city, QString zipCode, QString countryCode) {
    
    QMap<QString, QVariant> data = QMap<QString, QVariant>();
    
    data.insert( "firstname", firstname);
    data.insert( "lastname", lastname);
    data.insert( "company", company);
    data.insert( "street", street);
    data.insert( "city", city);
    data.insert( "postcode", zipCode);
    data.insert( "country_id", countryCode);
    data.insert( "dob", dateOfBirth);
    data.insert( "gender", gender);
    data.insert( "language", language);
    
    QString customerSessionId = value("customerSessionId");
    
    QVariantList params;
	params << customerSessionId << data << sandbox();
    
	QVariant result = shopConnector()->call(sessionID(), QString("commerceApi_customer.update"), params);
	return result.toBool();
}


bool e5SaaSShop::createCustomerUser(QString email, QString password, QString language, QString gender, QString firstname, QString lastname, QString dateOfBirth, QString company, QString street, QString city, QString zipCode, QString countryCode)
{
	QMap<QString, QVariant> data = QMap<QString, QVariant>();
    
    data.insert( "email", email);
    data.insert( "password", password);
    data.insert( "firstname", firstname);
    data.insert( "lastname", lastname);
    data.insert( "company", company);
    data.insert( "street", street);
    data.insert( "city", city);
    data.insert( "postcode", zipCode);
    data.insert( "country_id", countryCode);
    data.insert( "dob", dateOfBirth);
    data.insert( "gender", gender);
    data.insert( "language", language);
    
    QVariantList params;
	params << data << sandbox();
    
	QVariant result = shopConnector()->call(sessionID(), QString("commerceApi_customer.create"), params);
	return result.toBool();
}

bool e5SaaSShop::loginCustomerUser(QString email, QString password)
{
	QVariantList params;
	params << email << password << sandbox();
	QVariant result = shopConnector()->call(sessionID(), QString("commerceApi_customer.login"), params);
	insert("customerSessionId", result.toString() );
	if( customerSessionID().isEmpty() ) {
		//qWarning() << "Could not log to e5Saas with customer "<< email <<"/" <<password <<"!";	
		return false;
	}

	insert( "customerEmail", email);
	insert( "customerPassword", password);
	
    //qDebug() << "Successfully logged in with customer "<< email <<"/" <<password <<" -> customer session id: "<<customerSessionID();	
	
    return true;
}

QMap<QString, QVariant> e5SaaSShop::loadCustomerData() {
    QString customerSessionId = value("customerSessionId");
    QVariantList params;
	params << customerSessionId << sandbox();
	QMap<QString, QVariant> result = shopConnector()->call(sessionID(), QString("commerceApi_customer.info"), params).toMap();
    return result;
}

QList<QVariant> e5SaaSShop::loadCountryList() {
    QList<QVariant> result = shopConnector()->call(sessionID(), QString("commerceApi_customer.countryList")).toList();
    return result;
}

bool e5SaaSShop::logoutCustomerUser(QString customerSessionID)
{
	QVariantList params;
	params << customerSessionID << sandbox();
	QVariant result = shopConnector()->call(sessionID(), QString("commerceApi_customer.logout"), params);
    insert("customerSessionId", "" );
	return result.toBool();
}

bool e5SaaSShop::forgotPassword(QString username)
{
	QVariantList params;
	params << username << sandbox();
	QVariant result = shopConnector()->call(sessionID(), QString("commerceApi_customer.forgotPassword"), params);
	return result.toBool();
}

bool e5SaaSShop::changePassword(QString username, QString oldPassword, QString newPassword)
{
	QVariantList params;
	params << username << oldPassword << newPassword << sandbox();
	QVariant result = shopConnector()->call(sessionID(), QString("commerceApi_customer.changePassword"), params);
	return result.toBool();
}
