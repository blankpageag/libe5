#include "e5PackageWriter"
#include "WebKitTools"
#include <TOC>
#include <Content>
#include <DirTools>
#include <QCoreApplication>
#include <QWebPage>
#include <QWebFrame>
#include <QTextStream>
#include <QDebug>
#include <QMetaObject>
#include <ContainerAccessManager>
#include <Downloader>
#include <quazip.h>
#include <quazipfile.h>
#include <cstdlib>
#include <cassert>

#ifndef LACKING_UNISTD_H
extern "C" {
#include <unistd.h>
}
#endif

using namespace std;

static QString id(QString filename)
{
	return filename.replace(QRegExp("[/.:\\\\\\s]+", Qt::CaseSensitive, QRegExp::RegExp2), "_");
}


static void fixInternalURLs(QDomNamedNodeMap attrs, QString const &attr, QString const &remove)
{
	if(!attrs.contains(attr))
		return;
	QString a=attrs.namedItem(attr).nodeValue();
	if(!a.startsWith(remove))
		return;	
	attrs.namedItem(attr).setNodeValue(a.mid(remove.length()));	
}


static void fixInternalURLs(QDomElement e, QString const &remove)
{
	QDomNamedNodeMap attrs=e.attributes();

	// replace src with originalSrc which are those before libe5 replaced things in HTMLContent::extractmedia 
	if( attrs.contains("originalSrc") )
	{
		attrs.namedItem("src").setNodeValue(attrs.namedItem("originalSrc").nodeValue());
		attrs.removeNamedItem("originalSrc");
	}
	fixInternalURLs(attrs, "src", remove);
	fixInternalURLs(attrs, "poster", remove);
	fixInternalURLs(attrs, "data", remove);
	fixInternalURLs(attrs, "href", remove);
	QDomElement c=e.firstChildElement();
	while(!c.isNull()) {
		fixInternalURLs(c, remove);
		c=c.nextSiblingElement();
	}
}


static void fixRelativeURLs(QDomNamedNodeMap attrs, QString const &attr, QString const &replace, QString const& relativePath)
{
	if(!attrs.contains(attr))
		return;
	QString a=attrs.namedItem(attr).nodeValue();
	if(!a.startsWith(replace))
		return;
	
	attrs.namedItem(attr).setNodeValue( a.replace(0, replace.length(), relativePath));	
}


static void fixRelativeURLs(QDomElement e, QString const &replace, QString const& relativePath)
{
	QDomNamedNodeMap attrs=e.attributes();	
	fixRelativeURLs(attrs, "src", replace, relativePath);
	fixRelativeURLs(attrs, "poster", replace, relativePath);
	fixRelativeURLs(attrs, "data", replace, relativePath);
	fixRelativeURLs(attrs, "href", replace, relativePath);
	QDomElement c=e.firstChildElement();
	while(!c.isNull()) {
		fixRelativeURLs(c, replace, relativePath);
		c=c.nextSiblingElement();
	}
}


static void hideHelpers(QDomElement e)
{
	QDomElement c=e.firstChildElement();
	while(!c.isNull()) {
		if(c.hasAttribute("e5_interactive")) {
			QStringList styles=c.attribute("style").split(';', QString::SkipEmptyParts);
			foreach(QString const &s, styles)
			if(s.startsWith("opacity:")) {
				styles.removeAll(s);
				break;
			}
			styles << "opacity: 0.0";
			c.setAttribute("style", styles.join(";"));
		}
		hideHelpers(c);
		c=c.nextSiblingElement();
	}
}


bool e5PackageWriter::save(Container *c, QString const &file, bool uncompressed, QObject *progressObject, char const * const progressSlot)
{
	qDebug() <<"Save container "<<c->baseUrl() <<"(id="<<c->id()<<", uncompression="<<uncompressed<<") to file/folder "<< file;  

	// If we don't have a unique ID, let's create it now so we don't
	// have to do it on an underpowered mobile device
	if(c->toc()->metadata().contains("dc:identifier"))
		static_cast<Metadata*>(c->toc())->insert("dc:identifier", c->toc()->id());
	
	if(progressObject && progressSlot) {
		ASSERT(QMetaObject::invokeMethod(progressObject, progressSlot, Qt::AutoConnection, Q_ARG(int, 0)));
		QCoreApplication::instance()->processEvents();
	}
#ifdef LACKING_MKDTEMP
#warning Your OS does not have mkdtemp -- temporary filename for export will not be secure. Get a real OS!
	QString const tmpPath=QDir::tempPath() + "/e5Export." + QString::number(QCoreApplication::applicationPid());
#else
	QString const tmpPath=mkdtemp(QFile::encodeName(QDir::tempPath() + "/e5Export.XXXXXX").data());
#endif
	
	
	//qDebug() << "Storing e5 content to temporary directory " << tmpPath;
	
	TOC const * const t = c->toc();
	DirTools::remove(tmpPath);
	DirTools::mkdir(tmpPath + "/META-INF");
	
	QFile mimetype(tmpPath + "/mimetype");
	if(!mimetype.open(QFile::WriteOnly)) {
		qCritical() << "Can't open mimetype file, ignoring because nobody cares anyway";
	} else {
		mimetype.write("application/epub+zip");
		mimetype.close();
	}

	if(!writeContainerXml(tmpPath)) {
		return false;
	}
	
	if(progressObject && progressSlot) {
		ASSERT(QMetaObject::invokeMethod(progressObject, progressSlot, Qt::AutoConnection, Q_ARG(int, 1)));
		QCoreApplication::instance()->processEvents();
	}
	
	QString opf(tmpPath + "/content.opf");
	//qDebug() <<"Writting to opf file " << QFileInfo(opf).absoluteFilePath();
	if( ! e5PackageWriter::writeOPFFile(opf, t, c->files() ) ) {
		qCritical() << "Could not write to opf file "<<opf;
		return false;
	}

	if(progressObject && progressSlot) {
		ASSERT(QMetaObject::invokeMethod(progressObject, progressSlot, Qt::AutoConnection, Q_ARG(int, 2)));
		QCoreApplication::instance()->processEvents();
	}
	
	QString ncx(tmpPath + "/toc.ncx");
	if( !e5PackageWriter::writeNCXFile( ncx, t ) ) {
		qCritical() << "Could not write to ncx file "<<ncx;
		return false;
	}	
	
	if(progressObject && progressSlot) {
		ASSERT(QMetaObject::invokeMethod(progressObject, progressSlot, Qt::AutoConnection, Q_ARG(int, 3)));
		QCoreApplication::instance()->processEvents();
	}
	

	
	ContainerAccessManager *cam=ContainerAccessManager::instance();
	QStringList const files=c->files();
	//qDebug() << "Copying # files:  "<<files.count();
	for(int i=0; i<files.count(); i++) {
		QString const &f=files.at(i);
		// We shouldn't copy the metadata from the original file...
		// They won't have any changes the user might have made.
		if(f.endsWith(".opf") || f.endsWith(".ncx") || f.startsWith(".svn") || f.contains("/.svn") || f.startsWith(".git") || f.contains("/.git") || f == "META-INF/container.xml" || f == "mimetype")
			continue;
		QString const target = tmpPath + "/" + f;
		QString const targetDir = target.section('/', 0, -2);
	
		DirTools::mkdir(targetDir);			
		if(!f.isEmpty() && !f.endsWith("/"))
		{
			if(!Downloader::download(QString(c->baseUrl() + f), target, cam))
			{
				qCritical() << "Can't create " << tmpPath << "/" << f;
			}
		}
		
		if(progressObject && progressSlot) {
			ASSERT(QMetaObject::invokeMethod(progressObject, progressSlot, Qt::AutoConnection, Q_ARG(int, 4+45.0*(i/files.count()) )));
			QCoreApplication::instance()->processEvents();
		}
	}
	cam->deref();
	
	// Overwrite the HTML files with whatever Content::html() claims they
	// should be -- someone may have used Content::setHtml()...
	for(int i=0; i<t->count(); i++) {
		TOCEntry *te=dynamic_cast<TOCEntry*>(t->at(i));
		if(!te)
			continue; // Theoretically can't happen here
		for(int j=0; j<te->count(); j++) 
		{
			Content const * const content=dynamic_cast<Content const * const>(te->at(j));
			if(!content) {
				// FIXME We need to handle nested TOCEntries
				// at some point
				continue;
			}
			
			// check if the content is not automatically generated and the html can be stored directly:
			if( (content->isGenerated()) || ( !content->value("e5:src").isEmpty() ) )
			{	
				writeToE5Template( c, tmpPath, content );
			}
			else 
			{
				writeToHTML( tmpPath, content );
			}
			
		}
	}
	if(progressObject && progressSlot) {
		ASSERT(QMetaObject::invokeMethod(progressObject, progressSlot, Qt::AutoConnection, Q_ARG(int, 50)));
		QCoreApplication::instance()->processEvents();
	}
	
	if(UNLIKELY(uncompressed)) {
		if(!QFile::rename(tmpPath, file)) {
			if(DirTools::copy(tmpPath, file))
				DirTools::remove(tmpPath);
			else
				qWarning() << "Can't move" << tmpPath << "to" << file << ". Leaving it in" << tmpPath << "so you can look at it manually.";
		}
		qDebug() <<"Rename content package from "<< tmpPath <<" to "<<file;
		if(progressObject && progressSlot) {
			ASSERT(QMetaObject::invokeMethod(progressObject, progressSlot, Qt::AutoConnection, Q_ARG(int, 100)));
			QCoreApplication::instance()->processEvents();
		}
	} else {
		
		
		QuaZip zip(file);
		if(!zip.open(QuaZip::mdCreate))
			return false;
		
		QFile in(tmpPath + "/mimetype");
		//qDebug() <<"Compress files with quazip to "<< QFileInfo( in).absoluteFilePath();
		QuaZipFile out(&zip);
		// While probably nothing cares, the epub spec says the mimetype file
		// needs to go first.
		// FIXME the epub spec also says the mimetype file should be
		// uncompressed, but there doesn't seem to be a way to tell
		// QuaZip to do that
		in.open(QFile::ReadOnly);
		QuaZipNewInfo mtInfo("mimetype", tmpPath + "/mimetype");
		mtInfo.externalAttr = 0644 << 16; // Permissions
		out.open(QIODevice::WriteOnly, mtInfo);
		out.write(in.readAll());
		in.close();
		out.close();
		QStringList const files2compress=DirTools::files(tmpPath);
		for(int i=0; i<files2compress.count(); i++) {
			QString const &f=files2compress.at(i);
			if(f == "mimetype")
				// Already added before as per epub spec
				continue;
			in.setFileName(tmpPath + "/" + f);
			if(!in.open(QFile::ReadOnly))
				continue;
			QuaZipNewInfo fileInfo(f, tmpPath + "/" + f);
			fileInfo.externalAttr = 0644 << 16; // Permissions
			out.open(QIODevice::WriteOnly, fileInfo);
			out.write(in.readAll());
			in.close();
			out.close();
			if(progressObject && progressSlot) {
				ASSERT(QMetaObject::invokeMethod(progressObject, progressSlot, Qt::AutoConnection, Q_ARG(int, 50+48.0*(i/files2compress.count()))));
				QCoreApplication::instance()->processEvents();
			}
		}
		if(progressObject && progressSlot) {
			ASSERT(QMetaObject::invokeMethod(progressObject, progressSlot, Qt::AutoConnection, Q_ARG(int, 99)));
			QCoreApplication::instance()->processEvents();
		}
		DirTools::remove(tmpPath);
		if(progressObject && progressSlot) {
			ASSERT(QMetaObject::invokeMethod(progressObject, progressSlot, Qt::AutoConnection, Q_ARG(int, 100)));
			QCoreApplication::instance()->processEvents();
		}
		zip.close();
	}
	
	
	qDebug() <<"Successful storage of e5 Container to  "<<file;
	
	
	return true;
}


bool e5PackageWriter::save(Container *c, QString const &file, QStringList const &removeConditions, QList<QRegExp> keep, bool uncompressed, QObject *progressObject, char const * const progressSlot) {
	
	qDebug() << "Store container "<<c->id() << " to "<<file;
	qDebug() <<"Remove conditions: "<<removeConditions;
	
	TOC * const toc=c->toc();
	QWebPage * const webPage = new QWebPage;
	webPage->settings()->setAttribute(QWebSettings::JavascriptEnabled, false);
	QWebFrame * const webFrame = webPage->mainFrame();
	QUrl const baseUrl = c->baseUrl();

	QSet<QString> neededFiles;
	QList<int> throwawayTocEntries;
	QList<QString> icons = toc->metadata().values("e5:icon");
	foreach(QString const &icon, icons) {
		QUrl iconUrl(icon);
		QString iconFile=iconUrl.path();
		if(iconFile.startsWith('/'))
			iconFile=iconFile.mid(1);
		if(!neededFiles.contains(iconFile))
			neededFiles << iconFile;
	}
	for(int i=0; i<toc->count(); i++) {
		TOCEntry *e=toc->at(i);
		QList<int> throwaway;
		int oldCount = e->count();
		for(int j=0; j<e->count(); j++) 
		{
			Content *c= dynamic_cast<Content*>(e->at(j));
			if(!c)
				continue;
			bool needed=c->conditions().isEmpty();
			foreach(QString const &cond, c->conditions()) {
				if(!removeConditions.contains(cond)) {
					if(c->metadata(false).contains("e5:src")) {
						//neededFiles << QUrl(c->baseUrl()).resolved(c->value("e5:src")).path().mid(1);
						neededFiles << c->value("e5:src");
					}
					needed=true;
					break;
				}
			}
			if(needed) {
				neededFiles << QUrl(c->baseUrl()).path().mid(1) + c->fileName();
//				webFrame->setContent(c->html().toUtf8(), "application/xhtml+xml", QUrl(c->baseUrl() + c->fileName()));
				webFrame->setContent(c->html().toUtf8(), "text/html", QUrl(c->baseUrl() + c->fileName()));
				QSet<QUrl> refs=externalReferences(webFrame, true);
				foreach(QUrl const &url, refs) {
					QString const fn=url.path().mid(1);
					if(!neededFiles.contains(fn) && url.scheme() == baseUrl.scheme() && url.host() == baseUrl.host()) {
						neededFiles << fn;
					}
				}
				// Find icons...
				// Intentionally done here (where it will be called several times for a multi-page
				// article) to avoid keeping icons for navPoints that are thrown away altogether
				QMultiHash<QString,QString> md=e->metadata();
				QList<QString> icons=md.values("e5:icon");
				foreach(QString icon, icons) {
					QUrl iconUrl(icon);
					QString iconFile=iconUrl.path().mid(1);
					if(!neededFiles.contains(iconFile))
						neededFiles << iconFile;
				}
			} else
				throwaway.prepend(j); // We need to remove the last bits first -- removing something from the beginning changes the order of the remaining pieces
		}
		foreach(int j, throwaway)
			e->removeAt(j);
		if(UNLIKELY(e->count() == 0))
			throwawayTocEntries << i;
		if(e->count() == 1 && oldCount != e->count()) 
		{
			// Make the remaining content unconditional...
			Content* c = dynamic_cast<Content*>(e->at(0));
			if(!c)
				continue;
			e->setConditions(c, QStringList());
		}
	}
	foreach(int i, throwawayTocEntries)
		toc->removeAt(i);

	QStringList removedFiles;
	foreach(QString const &f, c->files()) {
		if(!neededFiles.contains(f) && !f.toLower().endsWith(".ncx") && !f.toLower().endsWith(".opf") && f.toLower() != "meta-inf/container.xml") {
			removedFiles << f;
		}
	}
	QStringList excluded;
	foreach(QRegExp const &r, keep) {
		excluded << removedFiles.filter(r);
	}
	foreach(QString const &f, removedFiles) {
		if(excluded.contains(f))
			continue;
		c->remove(f);
	}

	e5PackageWriter::save(c, file, uncompressed, progressObject, progressSlot);
	delete webPage;
}


Container* e5PackageWriter::createNewContainer() 
{
	bool uncompressed = true;
	
	qDebug() <<"Create empty container in ";  
	
#ifdef LACKING_MKDTEMP
#warning Your OS does not have mkdtemp -- temporary filename for export will not be secure. Get a real OS!
	QString const tmpPath=QDir::tempPath() + "/e5Export." + QString::number(QCoreApplication::applicationPid());
#else
	QString const tmpPath=mkdtemp(QFile::encodeName(QDir::tempPath() + "/e5Export.XXXXXX").data());
#endif
	
	//qDebug() << "Storing e5 content to temporary directory " << tmpPath;
	
	DirTools::remove(tmpPath);
	DirTools::mkdir(tmpPath + "/META-INF");
	QFile mimetype(tmpPath + "/mimetype");
	if(!mimetype.open(QFile::WriteOnly)) {
		qDebug() << "Can't open mimetype file, ignoring because nobody cares anyway";
	} else {
		mimetype.write("application/epub+zip");
		mimetype.close();
	}
	
	QFile container(tmpPath + "/META-INF/container.xml");
	qDebug() <<"Writting to meta-info file " << QFileInfo(container).absoluteFilePath();
	if(!container.open(QFile::WriteOnly))
		return false;
	QTextStream containerTs(&container);
	containerTs << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl
		<< "<container version=\"1.0\" xmlns=\"urn:oasis:names:tc:opendocument:xmlns:container\">" << endl
		<< "	<rootfiles>" << endl
		<< "		<rootfile full-path=\"content.opf\" media-type=\"application/oebps-package+xml\"/>" << endl
		<< "	</rootfiles>" << endl
		<< "</container>" << endl;
	container.close();
	
	
	QString opf(tmpPath + "/content.opf");
	//qDebug() <<"Writting to opf file " << QFileInfo(opf).absoluteFilePath();
	
	if( ! e5PackageWriter::writeOPFFile(opf, NULL, QStringList() ) ) {
		qCritical() << "Could not write to opf file "<<opf;
		return false;
	}
	
	QString ncx(tmpPath + "/toc.ncx");
	if( !e5PackageWriter::writeNCXFile( ncx, NULL ) ) {
		qCritical() << "Could not write to ncx file "<<ncx;
		return false;
	}

	ContainerAccessManager *cam=ContainerAccessManager::instance();	

	qDebug() <<"Created a new e5 Container at "<<tmpPath;
	return Container::get(tmpPath);
}


QString xmlEscape(QString s)
{
	return s.replace('&', "&amp;").replace('<', "&lt;").replace('>', "&gt;").replace('"', "&quot;");
}


QString xmlEscapeAttr(QString s)
{
	return xmlEscape(s).replace('\n', "&#xa;");
}


bool e5PackageWriter::writeNCXFile(QString iFilePath, TOC const * const toc )
{
	
	QFile ncx(iFilePath);
	//qDebug() <<"Writting to ncx file " << QFileInfo(ncx).absoluteFilePath();
	
	if(!ncx.open(QFile::WriteOnly))
		return false;
	
	QTextStream ncxTs(&ncx);
	ncxTs << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl
		<< "<!DOCTYPE ncx PUBLIC \"-//NISO//DTD ncx 2005-1//EN\" \"http://www.daisy.org/z3986/2005/ncx-2005-1.dtd\">" << endl
		<< "<ncx xmlns=\"http://www.daisy.org/z3986/2005/ncx/\" xmlns:e5=\"http://dev.blankpage.ch/e5/container/0.6/ncx\" version=\"2005-1\" e5version=\"0.6\">" << endl // FIXME xml:lang should be specified here
		<< "	<head>" << endl;
		
	if( toc != NULL )
	{
		QMultiHash<QString,QString> const &md= toc->metadata(false);
		//qDebug() << "Write out ncx metadata:"<<md;
		for(QMultiHash<QString,QString>::ConstIterator it=md.begin(); it!=md.end(); it++) {
			if(it.key() == "e5:icon") {
				QString icon = it.value();
				ncxTs << "	    <e5:icon src=\"" << icon.remove( toc->baseUrl() ) << "\" purpose=\"icon\"/>" << endl;
			} else
				ncxTs << "		<meta name=\"" << it.key() << "\" content=\"" << xmlEscapeAttr(it.value()) << "\"/>" << endl;
		}
	}
	ncxTs << "	</head>" << endl << endl
	
		// FIXME It is wrong to hardcode those conditions here... The existing
		// conditions from the document should be used.
		<< "	<e5:condition id=\"portrait\">" << endl
		<< "		<orientation type=\"portrait\"/>" << endl
		<< "	</e5:condition>" << endl
		<< "	<e5:condition id=\"landscape\">" << endl
		<< "		<orientation type=\"landscape\"/>" << endl
		<< "	</e5:condition>" << endl << endl
		<< "	<navMap>" << endl;
	
	if( toc != NULL )
	{
		//qDebug() <<"Writting navmap with # of entries " << toc->count();

		for(int i=0; i<toc->count(); i++) 
		{
			TOCEntry const * const e=dynamic_cast<TOCEntry const * const>(toc->at(i));
			if( !e )
				// allow to store empty toc entries
				//if(!e || !e->count())
				continue;
			ncxTs << "		<navPoint";
			
			//qDebug() <<"Writting navpoint " << e->title();
			if(!e->contentType().isEmpty())
				ncxTs << " e5:type=\"" << e->contentType(false).join(" ") << "\"";
			ncxTs << ">" << endl;
			QMultiHash<QString,QString> const &md=e->metadata(false);
			for(QMultiHash<QString,QString>::ConstIterator it=md.begin(); it!=md.end(); it++) {
				if(it.key() == "e5:icon")
				{
					QString icon = it.value();
					ncxTs << "		<e5:icon src=\"" << icon.remove( toc->baseUrl() ) << "\" purpose=\"icon\"/>" << endl;
				}
				else if(it.key() == "e5:tocImage")
				{
					QString image = it.value();
					ncxTs << "		<e5:tocImage src=\"" << image.remove( toc->baseUrl() )<< "\" purpose=\"tocImage\"/>" << endl;
				}
				else if(it.key() != "e5:type")
					ncxTs << "		<meta name=\"" << it.key() << "\" content=\"" << xmlEscapeAttr(it.value()) << "\"/>" << endl;
			}
			ncxTs << "			<navLabel>" << endl
			<< "				<text>" << xmlEscape(e->title()) << "</text>" << endl;
			if(!e->subtitle(false).isEmpty())
				ncxTs << "				<subtitle>" << xmlEscape(e->title()) << "</subtitle>" << endl;
			ncxTs << "			</navLabel>" << endl;
			for(int j=0; j<e->count(); j++) 
			{
				Content * const c = dynamic_cast<Content * const>(e->at(j));
				if(!c) {
					// FIXME We need to handle nested TOCEntries
					continue;
				}
				
				QString const relPath = c->baseUrl().section('/', 3) + c->fileName();
				ncxTs << "			<content src=\"" << relPath << "\"";
				
				// check for html content which was generated and for which a source has tob be provided:
				if( !c->value("e5:src").isEmpty() )
				{
					ncxTs << " e5:src=\"" << c->value("e5:src") <<"\"";
				}				
				
				if(!e->conditions(c).isEmpty())
					ncxTs << " e5:condition=\"" << e->conditions(c).join(" ") << "\"";
				ncxTs << "/>" << endl;
			}
			ncxTs << "		</navPoint>" << endl;
		}
	}
	ncxTs << "	</navMap>" << endl
		<< "</ncx>" << endl;
	ncx.close();
	
	return true;
}

bool e5PackageWriter::writeOPFFile(QString iFilePath, TOC const * const toc, const QStringList iManifestFiles )
{
	QFile opf( iFilePath );
	//qDebug() <<"Writting to opf file " << QFileInfo(opf).absoluteFilePath();
	
	if(!opf.open(QFile::WriteOnly))
		return false;
	
	QTextStream opfTs(&opf);
	opfTs << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl
		<< "<package xmlns=\"http://www.idpf.org/2007/opf\" xmlns:e5=\"http://dev.blankpage.ch/e5/container/0.6/opf\" unique-identifier=\"FIXME:PutAGenuineUniqueIdentifierHere\" version=\"2.0\" e5version=\"0.6\">" << endl
		<< "	<metadata xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:dcterms=\"http://purl.org/dc/terms/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:opf=\"http://www.idpf.org/2007/opf\" xmlns:dtb=\"http://www.daisy.org/z3986/2005/dtbook/\">" << endl;
	
	if( toc != NULL )
	{
		QMultiHash<QString,QString> const &md= toc->metadata(false);
		//qDebug() << "Write out opf metadata:"<<md;
		for(QMultiHash<QString,QString>::ConstIterator it=md.begin(); it!=md.end(); it++)
		{
			if(it.key() == "e5:icon")
			{
				QString icon = it.value();
				opfTs << "	    <e5:icon src=\"" << icon.remove( toc->baseUrl() ) << "\" purpose=\"icon\"/>" << endl;
			}
			else 
			{
				opfTs << "		<" << it.key() << ">" << xmlEscape(it.value()) << "</" << it.key() << ">" << endl;
				//qDebug() <<"OPF:: " << it.key() << " - " <<it.value();
			}
		}
		
		QHash<QString,QString> const &ed=toc->eventDates(false);
		for(QHash<QString,QString>::ConstIterator it=ed.begin(); it!=ed.end(); it++) {
			opfTs << "		<dc:date opf:event=\"" << it.key() << "\" scheme=\"W3CDTF\">" << it.value() << "</dc:date>" << endl;
		}
		
		if(!ed.contains("epub-publication"))
			opfTs << "		<dc:date opf:event=\"epub-publication\" scheme=\"W3CDTF\">" << QDate::currentDate().toString("yyyy-MM-dd") << "</dc:date>" << endl;
	}
	opfTs << "	</metadata>" << endl
		<< "	<manifest>" << endl
		<< "		<item id=\"ncx\" href=\"toc.ncx\" media-type=\"application/x-dtbncx+xml\"/>" << endl;
	
	foreach(QString const &f, iManifestFiles) 
	{
		// Listing the OPF is wrong, and we already put in our own NCX
		if(f.endsWith(".opf") || f.endsWith(".ncx") || f.startsWith(".svn") || f.contains("/.svn") || f.startsWith(".git") || f.contains("/.git"))
			continue;
		opfTs << "		<item id=\"" << id(f) << "\" href=\"" << f << "\" media-type=\"" << toc->mimeType(f) << "\"/>" << endl;
	}
	
	
	if( toc != NULL )
	{
		// write also the icon to the manifest:
		for(int i=0; i<toc->count(); i++) 
		{
			TOCEntry* tocE = dynamic_cast<TOCEntry*>(toc->at(i));
			if(!tocE)
				continue;
			QString const iconPath = tocE->iconPath();
			opfTs << "		<itemref idref=\"" << id(iconPath) << "\"/>" << endl;
		}
	}
	opfTs << "	</manifest>" << endl
	      << "	<spine>" << endl;

	
	if( toc != NULL )
	{
		for(int i=0; i<toc->count(); i++) 
		{
			// only set a spine item for the first content:
			// FIXME this is wrong, there should be spine entries for
			// all contents (minus nonstandard orientation specific ones)
			TOCEntry * te=dynamic_cast<TOCEntry*>(toc->at(i));
			if(!te)
				continue;
			if( ! te->empty() )
			{
				Content const * const content = dynamic_cast<Content const * const>(te->at(0));
				if(content) {
					QString const relPath = content->baseUrl().section('/', 3) + content->fileName();
					opfTs << "		<itemref idref=\"" << id(relPath) << "\"/>" << endl;
				}
				// FIXME else { handle nested TOCEntries }
			}
			
			/*
			 for(int j=0; j<t->at(i)->count(); j++) 
			 {
			 Content const * const content = t->at(i)->at(j);
			 QString const relPath = content->baseUrl().section('/', 3) + content->fileName();
			 opfTs << "		<itemref idref=\"" << id(relPath) << "\"/>" << endl;
			 }
			 */
		}
	}
	opfTs << "	</spine>" << endl
		<< "</package>" << endl;
	opf.close();
	
	return true;
}


bool e5PackageWriter::writeContainerXml(QString const& tmpPath)
{
	QFile container(tmpPath + "/META-INF/container.xml");
	//qDebug() <<"Writting to meta-info file " << QFileInfo(container).absoluteFilePath();
	if(!container.open(QFile::WriteOnly)) {
		return false;
	}

	QTextStream containerTs(&container);
	containerTs << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl
		<< "<container version=\"1.0\" xmlns=\"urn:oasis:names:tc:opendocument:xmlns:container\">" << endl
		<< "	<rootfiles>" << endl
		<< "		<rootfile full-path=\"content.opf\" media-type=\"application/oebps-package+xml\"/>" << endl
		<< "	</rootfiles>" << endl
		<< "</container>" << endl;
	container.close();

	return true;
}


void e5PackageWriter::writeToHTML(QString tmpPath, Content const * const iContent)
{
	QFile f(tmpPath + "/" + iContent->baseUrl().section('/', 3) + iContent->fileName());
	if(!f.open(QFile::WriteOnly|QFile::Truncate)) 
	{
		qCritical() << "Can't write to " << f.fileName() << ", ignoring...";
		// FIXME LUKAS: We should add exception handling here
		return;
	}
	
	// We can't just work directly off c->dom() because we don't want to
	// mess with c->html() when invoked by the caller. This seems to be the
	// best way to really copy a QDomDocument.
	QDomDocument originalDoc=iContent->dom();
	QDomDocument doc;
	
	if(originalDoc.isNull() || originalDoc.documentElement().isNull()) {
		qDebug() << "Warning: exporting empty/invalid " << f.fileName();
	} else {
		doc.appendChild(doc.importNode(originalDoc.documentElement().cloneNode(true), true));
		
		// Get rid of <base href="e5internal://ID/Whatever"/> -- the new document's
		// ID will not be the same as the imported one's
		QDomNodeList bases=doc.elementsByTagName("base");
		if(!bases.isEmpty()) {
			QDomNode base=bases.at(0);
			QString baseHref=base.attributes().namedItem("href").nodeValue();
			if(baseHref.startsWith(iContent->baseUrl())) {
				baseHref=baseHref.mid( iContent->baseUrl().length());
				if(baseHref.isEmpty() || baseHref == "/")
					base.parentNode().removeChild(base);
				else
					base.attributes().namedItem("href").setNodeValue(baseHref);
			}
		}
		// Find and replace all references to e5internal:// URLs
		fixInternalURLs(doc.documentElement(), iContent->baseUrl());
		// We also need to replace e5internal:// URLs referring to
		// files in different subdirectories of the same container,
		// removing e5internal://ID keeping the / after the ID
		fixInternalURLs(doc.documentElement(), iContent->baseUrl().section('/', 0, 2));
		// Let's try to take care of file:// URLs pointing into the
		// original container...
		if(iContent->url().scheme() == "file:") {
			QString const fsUrl = iContent->url().toString();
			fixInternalURLs(doc.documentElement(), fsUrl.left(fsUrl.length()-1));
		}
		
		
		// fix now the base / sign by replacing it with the relative path to the document (for system where base urls somehow dont work)
		QString absPath = iContent->url().path().remove(iContent->fileName());
		if( absPath.at(0) == '/' )
		{
			absPath.remove(0,1);
		}
		QString relPath;
		while( absPath.contains("/") )
		{
			relPath += "../";
			absPath.remove(0, absPath.indexOf("/")+1);
		}
		fixRelativeURLs(doc.documentElement(), "/",  relPath);
		
		// Hide helper rectangles from interactive areas...
		hideHelpers(doc.documentElement());
	}
	f.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
	QDomDocumentType const &dt=originalDoc.doctype();
	QString doctype;
	if(!dt.name().isEmpty()) {
		doctype="<!DOCTYPE " + dt.name();
		if(!dt.publicId().isEmpty())
			doctype += " PUBLIC \"" + dt.publicId() + "\"";
		if(!dt.systemId().isEmpty())
			doctype += " \"" + dt.systemId() + "\"";
		doctype += ">";
	} else
		doctype="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n";
	f.write(doctype.toUtf8());
	f.write(doc.toString().toUtf8());
	f.close();
	
}


void e5PackageWriter::writeToE5Template(Container *c, QString tmpPath, Content const * const iContent)
{
	QString tmpDest = tmpPath + "/" + iContent->baseUrl().section('/', 3) + iContent->fileName();
	// temporary -> check if the src ends with .xhtml -> then we need to store the file
	QFileInfo htmlTest( tmpDest );
	if( ( htmlTest.suffix() == "xhtml" ) || ( htmlTest.suffix() == "html" ))
	{
		writeToHTML(tmpPath, iContent);
	}


	if( (!iContent->value("e5:src").isEmpty() ) && (iContent->value("e5:src") != iContent->fileName()) )
	{
		QIODevice *dev=c->open( iContent->value("e5:src") );
		if(!dev)
		{
			qCritical() << "Could not open template file: "<<iContent->value("e5:src");
			return;
		}	
		QByteArray array = dev->readAll();
		QString fileDEST = tmpPath + QDir::separator() + iContent->value("e5:src");
		QFile f(fileDEST);
		if(!f.open(QFile::WriteOnly|QFile::Truncate)) 
		{
			qCritical() << "Can't write to " << f.fileName() << ", ignoring...";
			// FIXME LUKAS: We should add exception handling here
			return;
		}
		f.write(array);
		f.close();
	}
}

