#ifndef _E5_PACKAGE_WRITER_GLOBALS_
#define _E5_PACKAGE_WRITER_GLOBALS_ 1

#include <e5Logger>
#include <QWebView>

#ifndef WEBKIT_BUG_44876_IS_FIXED // FIXME remove once the bug is fixed for good
extern "C" {
#include <tidy/buffio.h>
#include <tidy/tidy.h>
}
#endif


/**
 * <e5PackageWriterGlobals>: assembly of global functions for various editing functionalities (clean up of html using tidy, etc.)
 */




/**
 * Clean up html string using tidy. For more info see tidy.sourceforge.net
 * @param html QString input html string which is processed by tidy
 * @return QString clean html string from tidy
 * @author Lukas N Mueller
 */
QString processHTMLByTidy( QString html )
{
#ifdef WEBKIT_BUG_44876_IS_FIXED
	QString tidyHtml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" + page()->currentFrame()->documentElement().toOuterXml();
#else
	TidyBuffer output;
	TidyBuffer errors;
	TidyDoc tdoc = tidyCreate();
	tidyBufInit(&output);
	tidyBufInit(&errors);
	tidyOptSetBool(tdoc, TidyXhtmlOut, yes);
	tidyOptSetBool(tdoc, TidyXmlDecl, yes);
	tidyOptSetBool(tdoc, TidyMakeClean, yes);
	tidyOptSetBool(tdoc, TidyMark, no);
	tidyOptSetInt(tdoc, TidyWrapLen, -1);
	tidySetCharEncoding(tdoc, "utf8");
	tidySetErrorBuffer(tdoc, &errors);
	tidyParseString(tdoc, html.toUtf8());
	tidyCleanAndRepair(tdoc);
	tidyRunDiagnostics(tdoc);
	tidySaveBuffer(tdoc, &output);
	QString tidyHtml = QString::fromUtf8(reinterpret_cast<char const * const>(output.bp));
	tidyBufFree(&output);
	tidyBufFree(&errors);
	tidyRelease(tdoc);
#endif
	
	return tidyHtml;
}


/**
 * Compare two html documents based on their <head> and <body> element and its content. 
 * @param a QString first html for comparison
 * @param b QString second html for comparison
 * @return bool true if the test claims a and b are the same document, else false
 * @author Lukas N Mueller
 */
bool compareHTMLDocument(QString a, QString b )
{
	
	QWebView webA;
	webA.setHtml( processHTMLByTidy( a) );
	webA.page()->settings()->setAttribute(QWebSettings::JavascriptEnabled, true);

	QWebView webB;
	webB.setHtml( processHTMLByTidy( b) );
	webB.page()->settings()->setAttribute(QWebSettings::JavascriptEnabled, true);
	
	
	// compare body:
	QWebElement dA = webA.page()->mainFrame()->documentElement().firstChild();
	QWebElement dB = webB.page()->mainFrame()->documentElement().firstChild();

	if( dA.toOuterXml() != dB.toOuterXml() )
	{
		return false;
	}
	
	// compare body:
	dA = dA.nextSibling();
	dB = dB.nextSibling();	
	if( dA.toOuterXml() != dB.toOuterXml() )
	{
		return false;
	}
	
	return true;
}



#endif