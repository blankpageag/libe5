#include <QtGui>
#include <Container>
#include <Content>
#include <TOCEntry>
#include <TOC>
#include "e5PackageEditor"
#include "e5PackageImporter"

e5PackageImporter::e5PackageImporter(QString const& source, Container *destinationContainer, TOCEntry *destinationEntry) :
	_sourceContainer(0),
	_destinationContainer(destinationContainer),
	_destinationEntry(destinationEntry),
	_filterTOCItems(false),
	_conditionChanges(KeepConditions),
	_importAsArticles(false)
{
	if(source.isEmpty()) {
		_errors << QT_TRANSLATE_NOOP("e5PackageImporter", "No file to import has been specified.");
		return;
	}

	_sourceContainer = Container::get( QUrl::fromLocalFile(source) );
	if(!_sourceContainer || !_sourceContainer->toc()) {
		_errors << QT_TRANSLATE_NOOP("e5PackageImporter", "The import file doesn't seem to be valid.");
		qCritical() << "Import file seems not to be valid: " << source;
		return;
	}
	e5PackageEditor::fixupCss(_sourceContainer);

	TOC * const sourceToc = _sourceContainer->toc();
	_errors = sourceToc->validate();
	if(!_errors.isEmpty()) {
		qCritical() << "File " << source << " could not be validated with following error: " << _errors;
		return;
	}

	_prefix = QFileInfo(source).baseName() + "/";

	_sourceFiles = _sourceContainer->files();
	_sourceFiles = _sourceFiles.filter(QRegExp("^((?!(__MACOSX/|\\.DS_Store|Thumbs\\.db$)).)*$", Qt::CaseInsensitive));

	// No need to import metadata (OPF/NCX and friends if we're importing an EPUB file)...
	QMutableStringListIterator it(_sourceFiles);
	while(it.hasNext()) {
		if(_sourceContainer->isMetadata(it.next())) {
			it.remove();
		}
	}

	if(_sourceFiles.isEmpty()) {
		_errors << QT_TRANSLATE_NOOP("e5PackageImporter", "The imported file is empty");
		return;
	}

	_includedFiles = _sourceFiles;
}


void e5PackageImporter::setConditionChanges(ConditionChanges changes)
{
	_conditionChanges = changes;
}


void e5PackageImporter::setDestinationEntry(TOCEntry *destinationEntry)
{
	_destinationEntry = destinationEntry;
}


void e5PackageImporter::setImportAsArticles(bool asArticles)
{
	_importAsArticles = asArticles;
}


void e5PackageImporter::setIncludedFiles(QStringList const& files)
{
	_includedFiles = files;
}


void e5PackageImporter::setIncludedTocItems(QSet<TOCItem*> const& items)
{
	if(items != _includedTOCItems) {
		_includedTOCItems = items;
		_filterTOCItems = true;
	}
}


void e5PackageImporter::undoImport(QList<TOCItem*> const& importedItems)
{
	// Remove imported TOCItems
	foreach(TOCItem *item, importedItems) {
		TOCEntry *parent = item->parentEntry();
		if(!parent) {
			_errors << QT_TRANSLATE_NOOP("e5PackageImporter", "Couldn't undo the import of contents because they don't have a valid parent.");
			return;
		}

		parent->removeAll(item);
		item->deleteLater();
	}

	// Remove imported files
	foreach(QString file, _includedFiles) {
		QString destFile = _prefix + file;
		if(!_alreadyExistingFiles.contains(destFile)) {
			_destinationContainer->remove(destFile);
		}
	}

	// Remove added fonts
	foreach(QString addedFont, _addedFonts) {
		_destinationContainer->remove(addedFont);
	}

	// Restore the duplicated files that were removed during import
	for(QHash<QString, QByteArray>::iterator it = _duplicatedFiles.begin(); it != _duplicatedFiles.end(); ++it) {
		_destinationContainer->add(it.key(), it.value());
	}
	_duplicatedFiles.clear();

	// Restore the duplicated contents that were removed during import
	for(int i = _duplicatedContents.count() - 1; i >= 0; i--) {
		_destinationContainer->toc()->restoreFromSerialized(_duplicatedContents.at(i));
	}
}


QList<TOCItem*> e5PackageImporter::import()
{
	_errors.clear();
	_alreadyExistingFiles.clear();
	_duplicatedFiles.clear();
	_duplicatedContents.clear();

	// Import the files
	foreach(QString const& file, _includedFiles) {
		QString destFile = _prefix + file;

		// FIXME: When undoing an import, we don't want to remove files that were overwritten, since they were already
		// present before. The old version, however, will be lost. We should keep a copy of the old version in order
		// to be able to restore it but this would probably be very memory- and time-consuming.
		if(_destinationContainer->contains(destFile)) {
			_alreadyExistingFiles << destFile;
		}

		// FIXME this can take up lots of RAM if the file being imported contains
		// large files (videos...) because they're loaded into a QByteArray
		// Probably just putting in the reference makes more sense, but then we
		// need to be more intelligent about when we can "delete c;"
		//qDebug() << "Adding file to container: " << prefix + file;
		_destinationContainer->add(destFile, _sourceContainer->content(file));
	}

	// Import the TOC
	QList<TOCItem*> importedItems;

	TOCEntry *parent = _destinationEntry.data();
	if(!_importAsArticles && _destinationEntry.isNull()) {
		parent = _destinationContainer->toc()->addEntry(_sourceContainer->toc()->id());
		importedItems << parent;
	}

	recursiveImport(_sourceContainer->toc(), parent, &importedItems);
	return importedItems;
}


void e5PackageImporter::copyMetadata(TOCItem *source, TOCItem *destination)
{
	Metadata *destinationMetadata = static_cast<Metadata*>(destination);
	QMultiHash<QString,QString> const &sourceMetadata = source->metadata();
	for(QMultiHash<QString,QString>::ConstIterator it = sourceMetadata.begin(); it != sourceMetadata.end(); it++) {
		if(it.key() == "dc:title" && !destinationMetadata->title(false).isEmpty()) {
			continue;
		}

		destinationMetadata->insert(it.key(), it.value());
	}
}


void e5PackageImporter::recursiveImport(TOCItem *sourceItem, TOCEntry *destinationParent, QList<TOCItem*> *items)
{
	if(_filterTOCItems && !_includedTOCItems.contains(sourceItem)) {
		return;
	}

	if(TOCEntry *sourceEntry = dynamic_cast<TOCEntry*>(sourceItem)) {
		for(int i = 0; i < sourceEntry->count(); i++) {
			recursiveImport(sourceEntry->at(i), destinationParent, items);
		}
	} else if(Content *sourceContent = dynamic_cast<Content*>(sourceItem)) {
		if(_importAsArticles) {
			destinationParent = _destinationContainer->toc()->addEntry(sourceItem->id());
			if(!sourceContent->title(false).isEmpty()) {
				static_cast<Metadata*>(destinationParent)->replace("dc:title", sourceContent->title(false));
			}
			*items << destinationParent;
		}

		// Remove duplicated contents from the entry and keep them stored to be able to undo
		for(int i = 0; i < destinationParent->count(); i++) {
			Content *destinationContent = dynamic_cast<Content*>(destinationParent->at(i));
			if(destinationContent && destinationContent->fileName() == sourceContent->fileName()) {
				_duplicatedContents << _destinationContainer->toc()->serialized(destinationContent);

				FileHash duplicated = e5PackageEditor::removeContentFromContainer(_destinationContainer, destinationContent);
				for(FileHash::iterator it = duplicated.begin(); it != duplicated.end(); ++it) {
					_duplicatedFiles.insert(it.key(), it.value());
				}
			}
		}

		QStringList conditions = sourceContent->conditions();
		switch(_conditionChanges) {
		case AllPortrait:
			conditions.removeAll("landscape");
			if(!conditions.contains("portrait")) {
				conditions.append("portrait");
			}
			break;

		case AllLandscape:
			conditions.removeAll("portrait");
			if(!conditions.contains("landscape")) {
				conditions.append("landscape");
			}
			break;

		case SwapOrientations: {
			bool isPortrait = conditions.contains("portrait");
			bool isLandscape = conditions.contains("landscape");
			if(isPortrait && !isLandscape) {
				conditions.removeAll("portrait");
				conditions.append("landscape");
			} else if(!isPortrait && isLandscape) {
				conditions.removeAll("landscape");
				conditions.append("portrait");
			}
			break;
		}

		case RemoveConditions:
			conditions.clear();
			break;

		default:
			break;
		}

		QString fileName = _prefix + sourceContent->baseUrl().section('/', 3) + sourceContent->fileName();
		Content *destinationContent = destinationParent->append(fileName, conditions);
		copyMetadata(sourceContent, destinationContent);
		*items << destinationContent;
	}

	e5PackageEditor::optimizeFonts(_destinationContainer, &_addedFonts);
}
