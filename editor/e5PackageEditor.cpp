#include "e5PackageEditor"


#include <TOC>
#include <Content>
#include <DirTools>
#include <WebKitTools>
#include <QCoreApplication>
#include <QTextStream>
#include <QDebug>
#include <QMetaObject>
#include <ContainerAccessManager>
#include <Downloader>
#include <quazip.h>
#include <quazipfile.h>
#include <cstdlib>
#include <cassert>

#include <QWebPage>
#include <QWebFrame>

#ifndef LACKING_UNISTD_H
extern "C" {
#include <unistd.h>
}
#endif


Content* e5PackageEditor::createBlankContent(Container *container, TOCEntry *entry, QString preferredFileName)
{
	TOC *toc = container->toc();

	if(!entry) {
		entry = toc->addEntry("");
	}

	if(preferredFileName.isEmpty()) {
		preferredFileName = QString("index%1.html").arg(qrand());
	}
	QString basePath = QUrl(entry->baseUrl()).path();
	if(basePath.length() > 1) {
		preferredFileName.prepend(QUrl(entry->baseUrl()).path() + "/");
	}

	QString blankHtml("<?xml version=\"1.0\" encoding=\"utf-8\"?>"
					  "<!DOCTYPE html>"
					  "<html>\n"
					  "<head>\n"
					  "</head>\n"
					  "<body>\n"
					  "</body>\n"
					  "</html>");
	container->add(preferredFileName, blankHtml.toUtf8());

	if(preferredFileName.startsWith("/")) {
		preferredFileName.remove(0, 1);
	}
	Content *content = Content::get(entry, preferredFileName);
	entry->append(content);
	return content;
}


void e5PackageEditor::optimizeContainerForEditing( Container* container, ProgressObserver *observer)
{
	//qDebug() << "check for my templates:";
	QList<Content*> cs;
	TOC* t = container->toc(); 
	for( int i=0;i<t->size();i++ )
	{
		if(observer)
			observer->progress(i, t->size());
		TOCEntry* e = t->at(i);
		for(int j=0;j< e->size();j++ )
		{
			Content* c = dynamic_cast<Content*> (e->at(j));
			if( c != NULL )
			{
				if( !c->value( "e5:src" ).isEmpty() )
				{
					QString file = c->value( "e5:src" );
					Content* tmpCon = Content::get(e, c->value( "e5:src" ));
					tmpCon->setConditions( c->conditions() );
					tmpCon->setHtml( c->html() );
					e->insert(tmpCon, j);					
					removeContentFromContainer( container, c);					
				}
			}
		}
	}
}



FileHash e5PackageEditor::removeContentFromContainer(Container* container, Content* iContent)
{
	if( iContent == NULL ) {
		qCritical() <<"Deletion of NULL TOCEntry requested";
		return QHash<QString, QByteArray>();
	}

	qDebug() << "Deletion of content "<< iContent->fileName() << " from container "<< container->id();

	QSet<QUrl> urls = externalReferences(iContent->dom(), iContent->url());
	urls << QUrl(iContent->baseUrl()).resolved(iContent->fileName());

	// Check if the files are being used by any other content
	for(int i=0; i<container->toc()->count(); i++) {
		TOCEntry *te= container->toc()->at(i);
		for(int j=0; j<te->count(); j++) {
			Content* check = dynamic_cast<Content*>(te->at(j));
			if(!check) {
				// FIXME handle nested TOCEntries
				continue;
			}
			if(check->id() == iContent->id() && check == iContent) {
				continue;
			}

			QSet<QUrl> otherUrls = externalReferences(check->dom(), check->url());
			foreach(QUrl const &u, otherUrls) {
				if(urls.contains(u)) {
					urls.remove(u);
				}
			}
		}
	}

	QHash<QString, QByteArray> removedFiles;
	QString const base=container->toc()->baseUrl();
	foreach(QUrl const &u, urls) {
		QString f=u.toString();
		if(!f.startsWith(base)) // external reference...
			continue;
		QString filePath = f.mid(base.length());
		removedFiles.insert(filePath, container->content(filePath));
		container->remove(filePath);
	}
	
	TOCEntry* par = dynamic_cast<TOCEntry*>(iContent->parent());
	ASSERT( par != NULL );
	int index = par->indexOf( iContent);
	if(index >= 0) {
		par->removeAt(index);
	} else {
		qCritical() << "Content '"<<iContent->fileName()<<"' for deletion was not found in TOCEntry "<<par->title();
	}

	return removedFiles;
}


bool e5PackageEditor::fixupCss(Container *c)
{
	bool anythingModified=false;
	foreach(QString f, c->files()) {
		if(c->mimeType(f) == "text/css") {
			bool modified=false;
			QString css=QString::fromUtf8(c->content(f));
			// Fix DOS style newlines
			css.replace("\r\n", "\n");
			// And MacOS9 style newlines (Indesign crap)
			css.replace('\r', '\n');
			int pos = 0;
			int ff = -1;
			while((ff = css.indexOf("@font-face", pos, Qt::CaseInsensitive)) >= 0) {
				int braceOpen = css.indexOf('{', ff);
				pos = ff + 10;
				if(braceOpen < 0 || braceOpen > pos+15)
					// No { following @font-face, this is either plain
					// invalid, or a weird construct we don't understand
					continue;
				int braceClose = css.indexOf('}', ff);
				int pos = braceOpen;
				forever {
					int src = css.indexOf(QRegExp("src\\s*:\\s*url\\s*", Qt::CaseInsensitive, QRegExp::RegExp2), pos);
					if(src<0 || src>braceClose || (css.indexOf(';', src)<0))
						break;
					pos = src+1;

					QString font=css.mid(src, css.indexOf(';', src)-src);
					if(font.indexOf(QRegExp("\\sformat", Qt::CaseInsensitive, QRegExp::RegExp2))>=0) {
						// Already ok
						continue;
					}

					// Bogus statement found...
					QString format="opentype";
					if(font.contains(".eot"))
						format="embedded-opentype";
					else if(font.contains(".woff"))
						format="woff";
					else if(font.contains(".svg"))
						format="svg";
					css.insert(src + font.length(), " format('" + format + "')");
					modified=true;
				}
			}
			if(modified) {
				anythingModified=true;
				c->add(f, css.toUtf8());
			}
		}
	}
	return anythingModified;
}

QStringList e5PackageEditor::optimizeFonts(Container *c, QSet<QString> *addedFonts)
{
	if(addedFonts) {
		addedFonts->clear();
	}

	bool anythingModified=false;
	QStringList missingFonts;
	foreach(QString f, c->files()) {
		if(c->mimeType(f) == "text/css") {
			QString topLevel=QString("../").repeated(f.count('/'));
			bool modified=false;
			QString css=QString::fromUtf8(c->content(f));
			// Fix DOS style newlines
			css.replace("\r\n", "\n");
			// Fix MacOS9 style newlines (Indesign crap)
			css.replace('\r', '\n');
			int pos = 0;
			int ff = -1;
			while((ff = css.indexOf("@font-face", pos, Qt::CaseInsensitive)) >= 0) {
				int braceOpen = css.indexOf('{', ff);
				pos = ff + 10;
				if(braceOpen < 0 || braceOpen > pos+15)
					// No { following @font-face. This is either plain
					// invalid, or a weird construct we don't understand.
					continue;
				int braceClose = css.indexOf('}', ff);
				int pos = braceOpen;
				forever {
					int src = css.indexOf(QRegExp("src\\s*:\\s*url\\s*", Qt::CaseInsensitive, QRegExp::RegExp2), pos);
					if(src<0 || src>braceClose || (css.indexOf(';', src)<0))
						break;
					pos = src+1;

					QString font=css.mid(src, css.indexOf(';', src)-src);
					QStringList fontBits=font.simplified().replace(" (", "(").replace("( ", "(").replace(" )", ")").split(" ");
					for(int i=0; i<fontBits.count(); i++) {
						if(fontBits[i].startsWith("src") && fontBits[i].contains("url")) {
							QString fontFile=fontBits[i].section('(', 1, -1).section(')', 0, 0).simplified();
							if(fontFile.startsWith('"') || fontFile.startsWith('\''))
								fontFile=fontFile.mid(1);
							if(fontFile.endsWith('"') || fontFile.endsWith('\''))
								fontFile.chop(1);
							QUrl fontUrl=QUrl(c->baseUrl() + f).resolved(QUrl(fontFile));
							QString fontFileName=fontUrl.path().section('/', -1);
							if(fontUrl==QUrl(QString(c->baseUrl() + f)).resolved(QString("/e5/fonts/" + fontFileName)))
								// It's already where it belongs
								continue;

							if(!c->contains("e5/fonts/" + fontFileName)) {
								// We should move the font where it belongs...
								QByteArray font=Downloader::get(fontUrl, ContainerAccessManager::instance());
								if(font.isEmpty()) {
									// Doesn't exist, let's see if it's elsewhere in the container
									fontFile=fontUrl.path().section('/', -1);
									foreach(QString fi, c->files()) {
										if(fi.endsWith("/" + fontFile)) {
											// Probably it's there...
											font=c->content(fi);
										}
									}
								}
								if(font.isEmpty()) {
									if(!missingFonts.contains(fontFile))
										missingFonts << fontFile;
								} else {
									c->add("e5/fonts/" + fontFileName, font);
									if(addedFonts) {
										addedFonts->insert("e5/fonts/" + fontFileName);
									}
								}

								// Let's remove the font everywhere else...
								foreach(QString fi, c->files()) {
									if(fi.endsWith("/" + fontFileName) && !fi.startsWith("e5/fonts/") || fi==fontFileName)
										c->remove(fi);
								}
							} else {
								// Already there, so let's remove it from the new container
								foreach(QString fi, c->files()) {
									if(fi.endsWith("/" + fontFileName) && !fi.startsWith("e5/fonts/") || fi==fontFileName) {
										c->remove(fi);
									}
								}
							}
							fontBits[i]="src:url(\"" + topLevel + "e5/fonts/" + fontFileName;
							if(fontUrl.hasFragment())
								fontBits[i] += "#" + fontUrl.fragment();
							fontBits[i]+="\")";
							pos -= (css.indexOf(';', src)-src);
							css.replace(src, css.indexOf(';', src)-src, fontBits.join(" "));
							pos += fontBits.join(" ").length();
							modified=true;
						}
					}
				}
				if(modified) {
					anythingModified=true;
				}
			}
			if(modified) {
				c->remove(f);
				c->add(f, css.toUtf8());
			}
		}
	}
	return missingFonts;
}
