#include <QtWebKit>
#include "WebKitTools"
#include <QEventLoop>
#include <QIOStream>
#include <QTextStream>
#include <QDebug>
#include <Downloader>
#include <ContainerAccessManager>
#include <parser.h> // QJson

using namespace std;


TrackingNetworkAccessManager::TrackingNetworkAccessManager(QObject *parent) :
	QNetworkAccessManager(parent)
{
}


QNetworkReply* TrackingNetworkAccessManager::createRequest(Operation op, const QNetworkRequest& req, QIODevice *outgoingData)
{
	_resources.insert(req.url());

	if(op == GetOperation && req.url().scheme() == "e5internal") {
		return ContainerAccessManager::instance(this)->get(req);
	} else {
		return QNetworkAccessManager::createRequest(op, req, outgoingData);
	}
}


static void dumpContents(QTextStream &ts, QWebElement const &root, int indent=0)
{
	for(int i=0; i<indent; i++)
		ts << "\t";
	ts << "<" << root.tagName().toLower();
	if(root.hasAttributes()) {
		foreach(QString const &attr, root.attributeNames()) {
			ts << " " << attr << "=\"" << root.attribute(attr) << "\"";
		}
		
	}
	QString const &content=root.toPlainText();
	QWebElement c=root.firstChild();
	if(content.isEmpty() && c.isNull()) {
		ts << "/>";
		if(root.nextSibling().isNull())
			ts << endl;
		return;
	}
	ts << ">";
	if(!c.isNull()) {
		do {
			ts << endl;
			dumpContents(ts, c, indent+1);
			c=c.nextSibling();
		} while(!c.isNull());
		for(int i=0; i<indent; i++)
			ts << "\t";
	} else
		ts << content;
	ts << "</" << root.tagName().toLower() << ">";
	if(root.nextSibling().isNull())
		ts << endl;
}


bool save(QWebFrame const &frame, QString filename)
{
	if(filename.isEmpty()) {
		// FIXME determine filename from frame.url()
		filename="/tmp/out.html";
	}
	QFile f(filename);
	if(!f.open(QIODevice::WriteOnly))
		return false;
	QTextStream ts(&f);
	ts << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
	ts << "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">" << endl;

	// In theory, we could use frame.toHtml() or
	// frame.documentElement().toOuterXml(), but (as of Qt 4.7) they
	// don't generate valid XHTML, making the output useless for further
	// processing with QDomDocument or QXmlStreamReader... :/
	// Also, dumpContents() does a better job w/ indentation.
	QWebElement doc=frame.documentElement();
	dumpContents(ts, doc);
	return true;
}


static QSet<QUrl> slideshowItems(QUrl const &ssUrl)
{
	QSet<QUrl> result;
	result << ssUrl;
	QByteArray ssJson = Downloader::get(ssUrl, ContainerAccessManager::instance());
	QJson::Parser p;
	bool ok;
	QVariant ss = p.parse(ssJson, &ok);
	if(!ok) {
		return result;
	}

	QVariantList items = ss.toMap()["items"].toList();
	foreach(QVariant item, items) {
		QVariantMap m = item.toMap();
		result << ssUrl.resolved(m["url"].toString());
	}
	return result;
}


QSet<QUrl> externalReferences(QWebFrame const &frame, bool subframes)
{
	QUrl const base=frame.baseUrl();
	QSet<QUrl> refs;
	// Catches <img>, <audio>, <video>, <embed>, <script>, <{i,}frame>
	// and friends
	QWebElementCollection withSrc=frame.findAllElements("[src]");
	foreach(QWebElement const &e, withSrc) {
		refs << base.resolved(e.attribute("src"));
	}
	// Catches <object> and friends
	QWebElementCollection withData=frame.findAllElements("[data]");
	foreach(QWebElement const &e, withData) {
		refs << base.resolved(e.attribute("data"));
	}
	// Catches stylesheet linkage etc.
	QWebElementCollection link=frame.findAllElements("link[href]");
	foreach(QWebElement const &e, link) {
		if(e.attribute("rel") == "stylesheet")
			refs << base.resolved(e.attribute("href"));
	}
	// Slideshow specifics
	QWebElementCollection ss=frame.findAllElements("[e5_slideshow_url]");
	foreach(QWebElement const &e, ss) {
		refs.unite(slideshowItems(base.resolved(e.attribute("e5_slideshow_url"))));
	}
	if(subframes) {
		foreach(QWebFrame *f, frame.childFrames())
			refs.unite(externalReferences(f));
	}
	return refs;
}


QSet<QUrl> externalReferences(QDomElement const& element, QUrl const& baseUrl, bool subframes)
{
	QSet<QUrl> refs;

	// Catches <img>, <audio>, <video>, <embed>, <script>, <{i,}frame>
	// and friends
	if(!element.attribute("src").isEmpty()) {
		refs << baseUrl.resolved(element.attribute("src"));
	}

	// Catches <object> and friends
	if(!element.attribute("data").isEmpty()) {
		refs << baseUrl.resolved(element.attribute("data"));
	}

	// Catches stylesheet linkage etc.
	if(element.tagName().compare("link", Qt::CaseInsensitive) == 0 &&
	   element.attribute("rel").compare("stylesheet", Qt::CaseInsensitive) == 0 &&
	  !element.attribute("href").isEmpty()) {
		refs << baseUrl.resolved(element.attribute("href"));
	}

	// Slideshow specifics
	if(!element.attribute("e5_slideshow_url").isEmpty()) {
		refs.unite(slideshowItems(baseUrl.resolved(element.attribute("e5_slideshow_url"))));
	}

	if(subframes) {
		if(element.tagName().compare("frame", Qt::CaseInsensitive) == 0 || element.tagName().compare("iframe", Qt::CaseInsensitive) == 0) {
			// We would have to load the document in a QDomElement, validate it through tidy, etc. as we do when loading
			// a content. Since frame usage is not very common anyway, let's do it the traditional way through WebKit
			QWebPage page;
			page.mainFrame()->load( baseUrl.resolved(element.attribute("src")) );
			refs.unite(externalReferences(page));
		}
	}

	for(QDomElement child = element.firstChildElement(); !child.isNull(); child = child.nextSiblingElement()) {
		refs.unite(externalReferences(child, baseUrl));
	}

	return refs;
}


QStringList cssClasses(QString css)
{
	// Get rid of comments
	while(css.contains("/*") && css.contains("*/")) {
		css=css.section("/*", 0, 0) + " " + css.section("*/", 1, -1);
	}
	// Get rid of the content of definitions...
	while(css.contains('{') && css.contains('}')) {
		// In most cases, this is enough:
		//css=css.section('{', 0, 0) + " " + css.section('}', 1, -1);
		// but there can be nested {} constructs such as
		// @media something {
		//   body {
		//     whatever: xyz;
		//   }
		// }
		int start=css.indexOf('{')+1;
		int end;
		int openBraces=1;
		for(end=start; openBraces; end++) {
			if(css.at(end) == '{')
				openBraces++;
			else if(css.at(end) == '}')
				openBraces--;
		}
		QString inside=css.mid(start, end-start-1);
		if(inside.contains('{'))
			// We need another pass -- class definitions inside @media or the likes
			css = css.left(start-1) + inside + css.mid(end);
		else
			css = css.left(start-1) + css.mid(end);
	}
	// Get rid of newlines and whitespace...
	css=css.replace('\n', ' ').replace('\r', ' ').trimmed();
	QStringList possibleClasses=css.split(' ', QString::SkipEmptyParts);
	QStringList classes;
	foreach(QString const &c, possibleClasses) {
		if(c.startsWith('@') || !c.contains('.'))
			continue;
		QString classname=c.section('.', 1, -1);
		while(classname.endsWith(','))
			classname=classname.left(classname.length()-1);
		if(classname.contains('>'))
			classname=classname.section('>', 0, 0);
		if(classname.contains(':'))
			classname=classname.section(':', 0, 0);
		if(!classes.contains(classname))
			classes << classname;
	}
	classes.removeDuplicates();
	return classes;
}


QString jsId(QWebElement const &e)
{
	if(e.isNull())
		return QString::null;
	else if(e.tagName().toLower() == "body")
		return "document.body";
	else if(e.tagName().toLower() == "html")
		return "document.getElementsByTagName(\"html\")";
	else if(e.hasAttribute("id"))
		return "document.getElementById('" + e.attribute("id") + "')";
	// We don't have an ID -- so we need to find a parent that does,
	// and our relation to it.
	QWebElement p=e.parent();
	if(p.isNull()) // Theoretically can't happen
		return QString::null;
	QString id=jsId(p) + ".firstChild";
	QWebElement c=p.firstChild();
	while(c != e) {
		id += ".nextSibling";
		c=c.nextSibling();
	}
	return id;
}


QSet<QUrl> loadedResources(QUrl const& url)
{
	TrackingNetworkAccessManager accessManager;
	QWebPage page;
	page.settings()->setAttribute(QWebSettings::JavaEnabled, false);
	page.settings()->setAttribute(QWebSettings::PluginsEnabled, false);
	page.setNetworkAccessManager(&accessManager);

	QEventLoop loop;
	QObject::connect(&page, SIGNAL(loadFinished(bool)),
					 &loop, SLOT(quit()));
	page.mainFrame()->load(url);
	loop.exec();

	return accessManager.loadedResources();
}
