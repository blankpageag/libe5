#! /bin/bash
unset CPATH
unset C_INCLUDE_PATH
unset CPLUS_INCLUDE_PATH
unset OBJC_INCLUDE_PATH
unset LIBS
unset DYLD_FALLBACK_LIBRARY_PATH
unset DYLD_FALLBACK_FRAMEWORK_PATH

if [ $# -gt 0 ]; then
	export SDKVER="$1"
else
	export SDKVER="5.1"
fi
export DEVROOT="/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer"
export SDKROOT="$DEVROOT/SDKs/iPhoneOS$SDKVER.sdk"
export PKG_CONFIG_PATH="$SDKROOT/usr/lib/pkgconfig":"$SDKROOT/usr/share/pkgconfig"
export PKG_CONFIG_LIBDIR="$PKG_CONFIG_PATH"
export MAINFOLDER=`pwd`

TOOLCHAINS="`echo 'message(${CMAKE_ROOT})' | cmake -P /dev/stdin 2>&1`/Toolchains"

BOTANARGS="-DBOTAN_INCLUDE_DIR=$SDKROOT/usr/include/botan -DBOTAN_LIBRARY=$SDKROOT/usr/lib/libbotan.a"
ZLIBARGS="-DZLIB_LIBRARY=$SDKROOT/usr/lib/libz.dylib -DZLIB_INCLUDE_DIR=$SDKROOT/usr/include/"
QUAZIPARGS="-DQuaZip_LIBRARY=$SDKROOT/usr/lib/libquazip.a -DQuaZip_INCLUDE_DIR=$SDKROOT/usr/include/quazip"

mkdir build &>/dev/null
cd build
cmake .. $QTARGS $QTEXTARGS $BOTANARGS $ZLIBARGS $QUAZIPARGS -DCMAKE_CXX_FLAGS:STRING="-O2 -fvisibility=hidden -fvisibility-inlines-hidden -fweb -frename-registers -fomit-frame-pointer" -DCMAKE_CXX_FLAGS_RELEASE:STRING="-O2 -fvisibility=hidden -fweb -frename-registers -fomit-frame-pointer" -DCMAKE_BUILD_TYPE=release -Dqjson_INCLUDE_DIR="$SDKROOT/usr/include/qjson" -DCMAKE_TOOLCHAIN_FILE="$TOOLCHAINS/iphone-$SDKVER.toolchain" -DCMAKE_INSTALL_PREFIX="$SDKROOT/usr" -DWITH_NON_XML_HTML:BOOL=OFF
# Workaround for FindQt4.cmake making wild assumptions about Qt dependencies on OSX
find . -name link.txt |xargs sed -i -e "s,QtCore.a,QtCore.a -miphoneos-version-min=$SDKVER -isysroot $SDKROOT --sysroot=$SDKROOT -F$SDKROOT/System/Library/Frameworks -lz -framework CoreFoundation,g"
