#include <e5ItemModel>
#include <QTreeView>
#include <QApplication>

int main(int argc, char **argv) {
	QApplication app(argc, argv);
	QString url = (argc > 1) ? argv[1] : TESTS "/data/e5-content-package";
	Container *c=Container::get(QUrl::fromLocalFile(url.toUtf8()));
	if(!c || !c->toc()) {
		qDebug() << "No handler for" << url << "found";
		return 1;
	}
	QTreeView *v=new QTreeView(0);
	v->setModel(new e5ItemModel(c->toc()));
	v->show();
	app.exec();
}
