#include "EPubContainerTest.h"
#include <TOC>
#include <TOCEntry>
#include <Content>
#include <Downloader>

#include <QIOStream>
using namespace std;

void EPubContainerTest::initTestCase() {
	_container=Container::get(QUrl::fromLocalFile(TESTS "/data/e5Test.e5"));
	QVERIFY(_container);
}

void EPubContainerTest::cleanupTestCase() {
	delete _container;
}

void EPubContainerTest::containerTest() {
	QStringList f=_container->files();
	f.sort();
	QVERIFY(QString(_container->metaObject()->className()) == "ZipfileContainer");
	QVERIFY(f == QStringList()
		<< "10_roten/"
		<< "10_roten/horizontal1.html"
		<< "10_roten/horizontal2.html"
		<< "10_roten/icon.jpg"
		<< "10_roten/images/"
		<< "10_roten/images/Michele_Roten_fmt.jpeg"
		<< "10_roten/index.html"
		<< "10_roten/ma1040_12_hor_Roten.jpg"
		<< "10_roten/ma1040_12_hor_Roten2.png"
		<< "10_roten/template.css"
		<< "11_kueng/"
		<< "11_kueng/horizontal1.html"
		<< "11_kueng/horizontal2.html"
		<< "11_kueng/icon.jpg"
		<< "11_kueng/images/"
		<< "11_kueng/images/39_babar_ipad_fmt.jpeg"
		<< "11_kueng/index.html"
		<< "11_kueng/ma1040_13_hor_Kueng.png"
		<< "11_kueng/ma1040_13_hor_Kueng2.png"
		<< "11_kueng/template.css"
		<< "12_buch/"
		<< "12_buch/horizontal1.html"
		<< "12_buch/horizontal2.html"
		<< "12_buch/icon.jpg"
		<< "12_buch/index.html"
		<< "12_buch/ma1040_14_hor_Buch.png"
		<< "12_buch/ma1040_14_hor_Buch2.png"
		<< "12_buch/template.css"
		<< "13_tag/"
		<< "13_tag/horizontal1.html"
		<< "13_tag/horizontal2.html"
		<< "13_tag/horizontal3.html"
		<< "13_tag/icon.jpg"
		<< "13_tag/images/"
		<< "13_tag/images/ma1040_ver_tag_fmt.jpeg"
		<< "13_tag/index.html"
		<< "13_tag/ma1040_15_hor_Tag.jpg"
		<< "13_tag/ma1040_15_hor_Tag2.png"
		<< "13_tag/ma1040_15_hor_Tag3.png"
		<< "13_tag/template.css"
		<< "14_impressum/"
		<< "14_impressum/horizontal1.html"
		<< "14_impressum/icon.jpg"
		<< "14_impressum/index.html"
		<< "14_impressum/ma1040_16_hor_Impressum.png"
		<< "14_impressum/template.css"
		<< "1_cover/"
		<< "1_cover/horizontal1.html"
		<< "1_cover/icon.jpg"
		<< "1_cover/images/"
		<< "1_cover/images/ma1040_1_hor_Front.jpg"
		<< "1_cover/images/ma1040_ver_fro_fmt.jpeg"
		<< "1_cover/index.html"
		<< "2_editorial/"
		<< "2_editorial/horizontal1.html"
		<< "2_editorial/horizontal2.html"
		<< "2_editorial/icon.jpg"
		<< "2_editorial/index.html"
		<< "2_editorial/ma1040_2_hor_Editorial.png"
		<< "2_editorial/ma1040_2_hor_Editorial2.png"
		<< "2_editorial/template.css"
		<< "3_inhalt/"
		<< "3_inhalt/horizontal1.html"
		<< "3_inhalt/icon.jpg"
		<< "3_inhalt/images/"
		<< "3_inhalt/images/4_EZ_48_Kurzbefehl_Cove_fmt.png"
		<< "3_inhalt/images/8861737_fmt.jpeg"
		<< "3_inhalt/images/Milo_Keller_Ecal_AlpTra_fmt.png"
		<< "3_inhalt/images/ma1040_gte_pluess_fmt.jpeg"
		<< "3_inhalt/images/ma1040_hor_inhalt.jpg"
		<< "3_inhalt/images/ma1040_ver_gte_adaptio_fmt.jpeg"
		<< "3_inhalt/index.html"
		<< "3_inhalt/template.css"
		<< "5_binswanger/"
		<< "5_binswanger/horizontal1.html"
		<< "5_binswanger/horizontal2.html"
		<< "5_binswanger/horizontal3.html"
		<< "5_binswanger/icon.jpg"
		<< "5_binswanger/images/"
		<< "5_binswanger/images/Daniel_Binswanger_fmt.jpeg"
		<< "5_binswanger/index.html"
		<< "5_binswanger/ma1040_3_hor_Binswanger.jpg"
		<< "5_binswanger/ma1040_3_hor_Binswanger2.png"
		<< "5_binswanger/ma1040_3_hor_Binswanger3.png"
		<< "5_binswanger/template.css"
		<< "6_pfeiffer/"
		<< "6_pfeiffer/horizontal1.html"
		<< "6_pfeiffer/icon.jpg"
		<< "6_pfeiffer/images/"
		<< "6_pfeiffer/images/ma1040_ver_pfeiffer_aepfel.jpg"
		<< "6_pfeiffer/index.html"
		<< "6_pfeiffer/ma1040_4_hor_Pfeiffer.jpg"
		<< "6_pfeiffer/template.css"
		<< "7_gespraech/"
		<< "7_gespraech/horizontal1.html"
		<< "7_gespraech/horizontal2.html"
		<< "7_gespraech/horizontal3.html"
		<< "7_gespraech/icon.jpg"
		<< "7_gespraech/images/"
		<< "7_gespraech/images/ma1040_ver_ges_fmt.jpeg"
		<< "7_gespraech/index.html"
		<< "7_gespraech/ma1039_7_hor_Gesprach.jpg.xhtml"
		<< "7_gespraech/ma1039_7_hor_Gesprach2.png.xhtml"
		<< "7_gespraech/ma1039_7_hor_Gesprach3.png.xhtml"
		<< "7_gespraech/ma1040_5_hor_Gespraech.jpg"
		<< "7_gespraech/ma1040_5_hor_Gespraech2.png"
		<< "7_gespraech/ma1040_5_hor_Gespraech3.png"
		<< "7_gespraech/template.css"
		<< "8a/"
		<< "8a/horizontal1.html"
		<< "8a/horizontal10.html"
		<< "8a/horizontal11.html"
		<< "8a/horizontal12.html"
		<< "8a/horizontal2.html"
		<< "8a/horizontal3.html"
		<< "8a/horizontal4.html"
		<< "8a/horizontal5.html"
		<< "8a/horizontal6.html"
		<< "8a/horizontal7.html"
		<< "8a/horizontal8.html"
		<< "8a/horizontal9.html"
		<< "8a/icon.jpg"
		<< "8a/images/"
		<< "8a/images/ma1040_ver_gte_adapti_fmt1.jpeg"
		<< "8a/images/ma1040_ver_gte_adaptio_fmt.jpeg"
		<< "8a/index.html"
		<< "8a/ma1040_6_hor_Adoption.jpg"
		<< "8a/ma1040_6_hor_Adoption10.png"
		<< "8a/ma1040_6_hor_Adoption11.png"
		<< "8a/ma1040_6_hor_Adoption12.png"
		<< "8a/ma1040_6_hor_Adoption2.png"
		<< "8a/ma1040_6_hor_Adoption3.png"
		<< "8a/ma1040_6_hor_Adoption4.png"
		<< "8a/ma1040_6_hor_Adoption5.png"
		<< "8a/ma1040_6_hor_Adoption6.png"
		<< "8a/ma1040_6_hor_Adoption7.jpg"
		<< "8a/ma1040_6_hor_Adoption8.png"
		<< "8a/ma1040_6_hor_Adoption9.png"
		<< "8a/template.css"
		<< "8b/"
		<< "8b/diashow/"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_001.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_002.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_003.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_004.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_005.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_006.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_007.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_008.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_009.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_010.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_011.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_012.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_013.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_014.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_015.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_016.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_017.jpg"
		<< "8b/diashow/Milo_Keller_Ecal_AlpTransit_018.jpg"
		<< "8b/horizontal1.html"
		<< "8b/horizontal10.html"
		<< "8b/horizontal11.html"
		<< "8b/horizontal12.html"
		<< "8b/horizontal13.html"
		<< "8b/horizontal2.html"
		<< "8b/horizontal3.html"
		<< "8b/horizontal4.html"
		<< "8b/horizontal5.html"
		<< "8b/horizontal6.html"
		<< "8b/horizontal7.html"
		<< "8b/horizontal8.html"
		<< "8b/horizontal9.html"
		<< "8b/icon.jpg"
		<< "8b/images/"
		<< "8b/images/ma1040_ver_gte_gottha_fmt1.jpeg"
		<< "8b/images/ma1040_ver_gte_gottha_fmt3.jpeg"
		<< "8b/images/ma1040_ver_gte_gottha_fmt4.jpeg"
		<< "8b/images/ma1040_ver_gte_gotthar_fmt.jpeg"
		<< "8b/images/ma1040_ver_gte_gotthard-5.jpg"
		<< "8b/index.html"
		<< "8b/ma1040_7_hor_Gotthard.jpg"
		<< "8b/ma1040_7_hor_Gotthard10.png"
		<< "8b/ma1040_7_hor_Gotthard11.png"
		<< "8b/ma1040_7_hor_Gotthard12.png"
		<< "8b/ma1040_7_hor_Gotthard13.png"
		<< "8b/ma1040_7_hor_Gotthard2.jpg"
		<< "8b/ma1040_7_hor_Gotthard3.jpg"
		<< "8b/ma1040_7_hor_Gotthard4.jpg"
		<< "8b/ma1040_7_hor_Gotthard5.png"
		<< "8b/ma1040_7_hor_Gotthard6.png"
		<< "8b/ma1040_7_hor_Gotthard7.png"
		<< "8b/ma1040_7_hor_Gotthard8.png"
		<< "8b/ma1040_7_hor_Gotthard9.png"
		<< "8b/template.css"
		<< "8c/"
		<< "8c/horizontal1.html"
		<< "8c/horizontal2.html"
		<< "8c/horizontal3.html"
		<< "8c/horizontal4.html"
		<< "8c/horizontal5.html"
		<< "8c/horizontal6.html"
		<< "8c/icon.jpg"
		<< "8c/images/"
		<< "8c/images/ma1040_gte_pluess_fmt.png"
		<< "8c/index.html"
		<< "8c/ma1040_8_hor_Pluess.png"
		<< "8c/ma1040_8_hor_Pluess2.png"
		<< "8c/ma1040_8_hor_Pluess3.png"
		<< "8c/ma1040_8_hor_Pluess4.png"
		<< "8c/ma1040_8_hor_Pluess5.png"
		<< "8c/ma1040_8_hor_Pluess6.png"
		<< "8c/template.css"
		<< "8d/"
		<< "8d/horizontal1.html"
		<< "8d/horizontal10.html"
		<< "8d/horizontal11.html"
		<< "8d/horizontal12.html"
		<< "8d/horizontal13.html"
		<< "8d/horizontal2.html"
		<< "8d/horizontal3.html"
		<< "8d/horizontal4.html"
		<< "8d/horizontal5.html"
		<< "8d/horizontal6.html"
		<< "8d/horizontal7.html"
		<< "8d/horizontal8.html"
		<< "8d/horizontal9.html"
		<< "8d/icon.jpg"
		<< "8d/images/"
		<< "8d/images/ma1040_ver_noma-1_fmt.jpeg"
		<< "8d/images/ma1040_ver_noma-2_fmt.jpeg"
		<< "8d/images/ma1040_ver_noma-3_fmt.jpeg"
		<< "8d/index.html"
		<< "8d/ma1040_9_hor_Noma.jpg"
		<< "8d/ma1040_9_hor_Noma10.png"
		<< "8d/ma1040_9_hor_Noma11.png"
		<< "8d/ma1040_9_hor_Noma12.png"
		<< "8d/ma1040_9_hor_Noma13.png"
		<< "8d/ma1040_9_hor_Noma2.png"
		<< "8d/ma1040_9_hor_Noma3.png"
		<< "8d/ma1040_9_hor_Noma4.jpg"
		<< "8d/ma1040_9_hor_Noma5.jpg"
		<< "8d/ma1040_9_hor_Noma6.png"
		<< "8d/ma1040_9_hor_Noma7.png"
		<< "8d/ma1040_9_hor_Noma8.png"
		<< "8d/ma1040_9_hor_Noma9.png"
		<< "8d/template.css"
		<< "8e/"
		<< "8e/horizontal1.html"
		<< "8e/horizontal2.html"
		<< "8e/horizontal3.html"
		<< "8e/horizontal4.html"
		<< "8e/horizontal5.html"
		<< "8e/horizontal6.html"
		<< "8e/horizontal7.html"
		<< "8e/horizontal8.html"
		<< "8e/icon.jpg"
		<< "8e/images/"
		<< "8e/images/ma1039_ver_Kurzbefehl-_fmt.jpeg"
		<< "8e/images/ma1039_ver_Kurzbefehl_fmt.jpeg"
		<< "8e/index.html"
		<< "8e/ma1040_10_hor_Kurzbefehl.jpg"
		<< "8e/ma1040_10_hor_Kurzbefehl2.png"
		<< "8e/ma1040_10_hor_Kurzbefehl3.png"
		<< "8e/ma1040_10_hor_Kurzbefehl4.png"
		<< "8e/ma1040_10_hor_Kurzbefehl5.png"
		<< "8e/ma1040_10_hor_Kurzbefehl6.png"
		<< "8e/ma1040_10_hor_Kurzbefehl7.png"
		<< "8e/ma1040_10_hor_Kurzbefehl8.jpg"
		<< "8e/template.css"
		<< "9_gegenwart/"
		<< "9_gegenwart/Fonts/"
		<< "9_gegenwart/Fonts/ASGaramond2.otf"
		<< "9_gegenwart/Fonts/BlackMed.otf"
		<< "9_gegenwart/Fonts/MagazinUmbra.otf"
		<< "9_gegenwart/horizontal1.html"
		<< "9_gegenwart/horizontal2.html"
		<< "9_gegenwart/icon.jpg"
		<< "9_gegenwart/images/"
		<< "9_gegenwart/images/adrianehrat_narziss_ne_fmt.jpeg"
		<< "9_gegenwart/index.html"
		<< "9_gegenwart/ma1040_11_hor_Gegenwart.jpg"
		<< "9_gegenwart/ma1040_11_hor_Gegenwart2.png"
		<< "9_gegenwart/template.css"
		<< "META-INF/"
		<< "META-INF/container.xml"
		<< "ad_helsane/"
		<< "ad_helsane/horizontal.jpg"
		<< "ad_helsane/horizontal.jpg.xhtml"
		<< "ad_helsane/icon.jpg"
		<< "ad_helsane/vertical.jpg"
		<< "ad_helsane/vertical.jpg.xhtml"
		<< "ad_ittinger/"
		<< "ad_ittinger/horizontal.html"
		<< "ad_ittinger/icon.jpg"
		<< "ad_ittinger/images/"
		<< "ad_ittinger/images/horizontal.jpg"
		<< "ad_ittinger/images/vertical.jpg"
		<< "ad_ittinger/index.html"
		<< "ad_mazda/"
		<< "ad_mazda/horizontal.jpg"
		<< "ad_mazda/horizontal.jpg.xhtml"
		<< "ad_mazda/icon.jpg"
		<< "ad_mazda/vertical.jpg"
		<< "ad_mazda/vertical.jpg.xhtml"
		<< "ad_migros/"
		<< "ad_migros/1024x768px_RZ_Natural.png"
		<< "ad_migros/768x1024px_RZ_Natural.png"
		<< "ad_migros/Migros_-_I_Am_-_Natural_Cosmetics_D24_16x9_FHA.MP4"
		<< "ad_migros/icon.png"
		<< "ad_migros/index.html"
		<< "ad_migros/mootools-core_1.2.4.js"
		<< "ad_migros/play.png"
		<< "ad_mobiliar/"
		<< "ad_mobiliar/ad_application.js"
		<< "ad_mobiliar/adpage_mobiliar.html"
		<< "ad_mobiliar/assets/"
		<< "ad_mobiliar/assets/adassets_bg_sprite_landscape.png"
		<< "ad_mobiliar/assets/adassets_bg_sprite_ohne_automarken.png"
		<< "ad_mobiliar/assets/adassets_ipad.png"
		<< "ad_mobiliar/assets/adassets_play_button.png"
		<< "ad_mobiliar/assets/adassets_player.png"
		<< "ad_mobiliar/assets/adassets_replay_button.png"
		<< "ad_mobiliar/assets/adassets_sticker_1.png"
		<< "ad_mobiliar/assets/adassets_sticker_2.png"
		<< "ad_mobiliar/icon.png"
		<< "ad_oris/"
		<< "ad_oris/horizontal.jpg"
		<< "ad_oris/horizontal.jpg.xhtml"
		<< "ad_oris/icon.jpg"
		<< "ad_oris/vertikal.jpg"
		<< "ad_oris/vertikal.jpg.xhtml"
		<< "ad_post/"
		<< "ad_post/horizontal.jpg"
		<< "ad_post/icon.jpg"
		<< "ad_post/index.html"
		<< "ad_post/landscape.html"
		<< "ad_post/post_optimized.mp4"
		<< "ad_post/vertical.jpg"
		<< "ad_swatch/"
		<< "ad_swatch/horizontal.jpg"
		<< "ad_swatch/icon.jpg"
		<< "ad_swatch/vertical.jpg"
		<< "content.opf"
		<< "e5/"
		<< "e5/e5.css"
		<< "e5/e5.js"
		<< "e5/e5_slideshow.css"
		<< "e5/e5_slideshow.js"
		<< "e5/fonts/"
		<< "e5/fonts/ASGaramond.svg.xml"
		<< "e5/fonts/Black-Medium.svg.xml"
		<< "e5/fonts/magazin-garamond-Bold.svg.xml"
		<< "e5/images/"
		<< "e5/images/slideShow_off.png"
		<< "e5/images/slideShow_on.png"
		<< "e5/js/"
		<< "e5/js/Hyphenator_hierIstDasOriginal.js"
		<< "e5/js/alt_Hyphenator.js"
		<< "e5/js/jquery.slickwrap.js"
		<< "e5/js/patterns/"
		<< "e5/js/patterns/bn.js"
		<< "e5/js/patterns/cs.js"
		<< "e5/js/patterns/da.js"
		<< "e5/js/patterns/de.js"
		<< "e5/js/patterns/el-monoton.js"
		<< "e5/js/patterns/el-polyton.js"
		<< "e5/js/patterns/en-gb.js"
		<< "e5/js/patterns/en-us.js"
		<< "e5/js/patterns/es.js"
		<< "e5/js/patterns/fi.js"
		<< "e5/js/patterns/fr.js"
		<< "e5/js/patterns/grc.js"
		<< "e5/js/patterns/gu.js"
		<< "e5/js/patterns/hi.js"
		<< "e5/js/patterns/hu.js"
		<< "e5/js/patterns/hy.js"
		<< "e5/js/patterns/it.js"
		<< "e5/js/patterns/kn.js"
		<< "e5/js/patterns/la.js"
		<< "e5/js/patterns/lt.js"
		<< "e5/js/patterns/ml.js"
		<< "e5/js/patterns/nl.js"
		<< "e5/js/patterns/or.js"
		<< "e5/js/patterns/pa.js"
		<< "e5/js/patterns/pl.js"
		<< "e5/js/patterns/pt.js"
		<< "e5/js/patterns/ru.js"
		<< "e5/js/patterns/sl.js"
		<< "e5/js/patterns/sv.js"
		<< "e5/js/patterns/ta.js"
		<< "e5/js/patterns/te.js"
		<< "e5/js/patterns/tr.js"
		<< "e5/js/patterns/uk.js"
		<< "icon.jpg"
		<< "mimetype"
		<< "toc.ncx");
	QVERIFY(_container->isMetadata("toc.ncx"));
	QVERIFY(!_container->isMetadata("SomethingThatDoesn'tExist"));
	QVERIFY(_container->mimeType("ad_migros/index.html") == "application/xhtml+xml");
	QVERIFY(_container->content("mimetype") == "application/epub+zip");
}

void EPubContainerTest::containerModificationTest() {
	QVERIFY(!_container->files().contains("test.html"));
	_container->add("test.html", "Just a test");
	QVERIFY(_container->files().contains("test.html"));
	QVERIFY(_container->content("test.html") == "Just a test");
	QVERIFY(_container->mimeType("test.html") == "text/html");
	_container->remove("test.html");
	QVERIFY(!_container->files().contains("test.html"));
	_container->remove("icon.jpg");
	QVERIFY(!_container->files().contains("icon.jpg"));
}

void EPubContainerTest::tocTest() {
	TOC *toc=_container->toc();
	QVERIFY(toc->baseUrl() == _container->baseUrl());
	QVERIFY(toc->baseUrl().startsWith("e5internal://"));
	QVERIFY(QString(toc->metaObject()->className()) == "EpubTOC");
	QVERIFY(toc->container() == _container);
	QVERIFY(toc->baseUrl() == _container->baseUrl());
	QVERIFY(toc->validate().isEmpty());
	QVERIFY(toc->count() == 23);
	QVERIFY(toc->author() == "Tamedia AG");
	QVERIFY(toc->publisher() == "Tamedia AG");
	QVERIFY(toc->title() == QString::fromUtf8("Der Händler"));
	QVERIFY(toc->issue() == 40);
	QVERIFY(toc->date().toString(Qt::ISODate) == "2010-10-09");
	QList<TOCEntry*> articles=toc->entriesOfType("advertisement");
	QVERIFY(articles.count() == 7);
}

void EPubContainerTest::tocEntryTest() {
	TOC *toc=_container->toc();
	TOCEntry *first=toc->at(0);
	QVERIFY(first);
	if(!first)
		return;
	QVERIFY(first->title() == "Front");
	QVERIFY(first->author() == "Das Magazin");
	QVERIFY(first->publisher() == "Das Magazin");
	QVERIFY(first->issue() == 40);
	QVERIFY(first->date().toString(Qt::ISODate) == "2010-10-09");
}

void EPubContainerTest::contentTest() {
	TOC *toc=_container->toc();
	TOCEntry *first=toc->at(0);
	QVERIFY(first);
	if(!first)
		return;
	QVERIFY(first->count() == 2);
	QVERIFY(dynamic_cast<Content*>(first->at(0))->fileName() == "index.html");
	QVERIFY(dynamic_cast<Content*>(first->at(1))->fileName() == "horizontal1.html");
	// At some point, this should be true: QVERIFY(first->at(0)->url() == first->baseUrl() + "index.html");
}

void EPubContainerTest::extractTest() {
	QString f=_container->extract("mimetype");
	QVERIFY(f.length());
	QVERIFY(f.startsWith(QDir::tempPath()));
	QByteArray b=Downloader::get(QUrl::fromLocalFile(f));
	QVERIFY(QLatin1String(b) == QLatin1String("application/epub+zip"));
	QFile::remove(f);
}

void EPubContainerTest::conditionsTest() {
	TOC *toc=_container->toc();
	TOCEntry *first=toc->at(0);
	// The tests below currently return an unexpected, but correct result (2).
	// Since the name is meaningless, there's no way libe5 can know that
	// portrait implies !landscape and vice versa.
	// There should be a test for this case once "real" condition handling
	// that doesn't rely on hardcoded names is implemented.
	//QList<Content*> portrait=first->contentForConditions(QStringList() << "portrait");
	//QVERIFY(portrait.count() == 1);
	//QList<Content*> landscape=first->contentForConditions(QStringList() << "landscape");
	//QVERIFY(landscape.count() == 1);
	QList<Content*> notLandscape=first->contentForConditions(QStringList() << "!landscape");
	QVERIFY(notLandscape.count() == 1);
	QList<Content*> notPortrait=first->contentForConditions(QStringList() << "!portrait");
	QVERIFY(notPortrait.count() == 1);
}

QTEST_MAIN(EPubContainerTest)
