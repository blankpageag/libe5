#include <Container>
#include <TOC>
#include <TOCEntry>
#include <Content>
#include <QIOStream>

using namespace std;

int main(int argc, char **argv) {
	QCoreApplication app(argc, argv); // Needed on Windoze because the plugin loader uses QCoreApplication::applicationDirPath. Can be omitted when targeting real OSes.

	Container *d;
	if(argc>1)
		d=Container::get(QUrl::fromLocalFile(argv[1]));
	else
		d=Container::get(QUrl::fromLocalFile(TESTS "/data/e5-content-package"));

	// We delete and reload the container to test the plugin
	// cache's behavior. If this crashes, there's something wrong
	// with the PluginLoader's cache
	delete d;

	if(argc>1)
		d=Container::get(QUrl::fromLocalFile(argv[1]));
	else
		d=Container::get(QUrl::fromLocalFile(TESTS "/data/e5-content-package"));

	cout << "Container is a " << d->metaObject()->className() << " with ID " << d->id() << endl;
	cout << "Files in epub file:" << endl;
	foreach(QString const &f, d->files()) {
		cout << "	" << f << ": " << d->mimeType(f);
		if(d->isMetadata(f))
			cout << " (metadata)";
		cout << endl;
	}
	TOC const *toc=d->toc();
	cout << "TOC is a " << toc->metaObject()->className() << " with ID " << toc->id() << " and " << toc->count() << " entries" << endl;
	for(int i=0; i<toc->count(); i++) {
		cout << "*" << toc->at(i)->id() << endl;
	}
	cout << endl;
	cout << "Author: " << toc->author() << endl;
	cout << "Title: " << toc->title() << endl;

	cout << "Chapters/Articles:" << endl;
	for(int i=0; i<toc->count(); i++) {
		TOCEntry *entry=toc->at(i);
		if(!entry)
			continue;
		cout << toc->at(i)->id() << endl;
		cout << "	Type: " << entry->contentType() << endl
		     << "	Author: " << entry->author() << endl
		     << "	Title: " << entry->title() << endl
		     << "	Subtitle: " << entry->subtitle() << endl
		     << "	Publisher: " << entry->publisher() << endl;

		cout << "Titles, Content IDs, baseURLs and URLs:" << endl;
		for(int i=0; i<entry->count(); i++) {
			Content *c = dynamic_cast<Content*>(entry->at(i));
			if(!c) {
				// FIXME handle nested TOCEntries!
				continue;
			}
			cout << c->title() << "\t" << c->id() << "\t" << c->baseUrl() << "\t" << c->url() << endl;
		}
	}
	delete d;
}
