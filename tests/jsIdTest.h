#include <QObject>
#include <QWebPage>

class jsIdTest:public QObject
{
	Q_OBJECT
private slots:
	void initTestCase();
	void verySimpleTest();
	void simpleTest();
	void mediumTest();
	void complexTest();
private:
	QWebPage *_p;
};
