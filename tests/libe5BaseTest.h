#ifndef _LIBE5_BASE_TEST_
#define _LIBE5_BASE_TEST_ 1

#include <QtTest/QtTest>

class Container;
class libe5BaseTest: public QObject
{
	Q_OBJECT

public:
	
	libe5BaseTest();
	virtual ~libe5BaseTest();
	
	Container* container(){return _testContainer;};
	
	private slots:
	
	void dynamicCastTest();
	
protected:
	
	/**
	 * Opening the a container and storing the container object in this class
	 * @param path to the test content
	 */
	void openTestData(QString iTestPath);
	
	
	/**
	 * Print out the content of a container
	 */
	void printTOC();
	
private:
	
	Container* _testContainer;
		
};


#endif
