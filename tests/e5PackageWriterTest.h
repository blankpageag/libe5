#ifndef _E5_PACKAGE_WRITER_TEST_
#define _E5_PACKAGE_WRITER_TEST_ 1

#include "libe5BaseTest.h"

class e5PackageWriterTest: public libe5BaseTest
{
	Q_OBJECT

public:
	
	private slots:
	
	/**
	 * Testing the writeing of opf file.
	 */
	void opfWritingTest();

	/**
	 * Testing the writeing of ncx file.
	 */
	void ncxWritingTest();
	
protected:

		
};


#endif
