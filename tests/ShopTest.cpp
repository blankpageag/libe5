#include <Shop>
#include <ItemCategory>
#include <Consumable>
#include <QDebug>
#include <QApplication>
#include <cassert>

void dumpCategory(ItemCategory *c, int indent=0) {
	QList<ShopItem*> ch=c->children();
	foreach(ShopItem *i, ch) {
		if(ItemCategory *cat=dynamic_cast<ItemCategory*>(i)) {
			qDebug() << QString("\t").repeated(indent).toAscii().data() << cat->name();
			qDebug() << QString("\t").repeated(indent).toAscii().data() << QString("=").repeated(cat->name().length());
			dumpCategory(cat, indent+1);
		} else if(Consumable *con=dynamic_cast<Consumable*>(i)) {
			qDebug() << QString("\t").repeated(indent).toAscii().data() << con->name();
			//qDebug() << QString("\t").repeated(indent).toAscii().data() << "File size:" << con->get().size();
con->download("/tmp/test.dl");
		}
	}
}

int main(int argc, char **argv) {
	QApplication app(argc, argv);
	QUrl shopUrl;
	if(app.arguments().count()>1)
		shopUrl = QUrl::fromEncoded(argv[1]);
	else
		shopUrl = QUrl("http://www.feedbooks.com/catalog.atom");
	Shop *s=Shop::get(shopUrl);
	if(!s) {
		qDebug() << "No handler for " << shopUrl << " found";
		return 1;
	}
	if(shopUrl.host().contains("akilibooks"))
		s->login("blankpage", "bl4nkp4g3d3v");
	else if(shopUrl.scheme().contains("admin"))
		s->login("testkunde", "test123");
	else if(shopUrl.host().contains("e5c2")) {
		s->login("9f3f5bdb38e6d68fd5577c66bfcce718", "128035bf1a473d9e601f303101ba68d4");
		s->loginCustomerUser("wr@shwups.ch", "saibot");
	}
	qDebug() << "ItemCategories in" << shopUrl << ":";
	QList<ItemCategory const *> categories=s->categories();
	foreach(ItemCategory const * c, categories)
		qDebug() << "\t" << c->name();
	qDebug() << "Categories range 2-5:";
	categories=s->categories(2,5);
	foreach(ItemCategory const * c, categories)
		qDebug() << "\t" << c->name();
	qDebug() << "Products in" << shopUrl << ":";
	QList<Consumable const *> products=s->products();
	foreach(Consumable const * p, products)
		qDebug() << "\t" << p->name();
	assert(s->products(0, 0).count() == 1);
	assert(s->products(1, 1).count() == 1);
	assert(s->products(0, 2).count() == 3);
	ItemCategory *root=s->rootCategory();
	qDebug() << "Content currently available in " << shopUrl << ":";
	dumpCategory(root);
}
