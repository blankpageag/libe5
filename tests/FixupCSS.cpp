#include <QCoreApplication>
#include <Container>
#include <e5PackageEditor>
#include <e5PackageWriter>

int main(int argc, char **argv) {
	QCoreApplication app(argc,argv);
	Container *c=Container::get(QUrl::fromLocalFile(argv[1]));
	if(e5PackageEditor::fixupCss(c)) {
		e5PackageWriter::save(c, "/tmp/fixedcss.e5");
	}
}
