#include "e5TemplateTest.h"


#include <Container>
#include <TOC>
#include <Content>
#include <QIOStream>


#define TEST_DATA TESTS "/e5TemplateTestData/templateTest/"

using namespace std;


void e5TemplateTest::templateParsingTest()
{	
	this->openTestData(QString(TEST_DATA));
	Container* con = this->container();	
    for( int i= 0; i < con->toc()->count(); i++ )
    {
        for( int j= 0; j < con->toc()->at(i)->count(); j++ )
        {
            Content* c = static_cast<Content*> (con->toc()->at(i)->at(j));
            if( c->isGenerated())
                qDebug()<<c->html();
        }
        
    }
    
}


void e5TemplateTest::setHTMLTest()
{
	this->openTestData(QString(TEST_DATA));
	Container* con = this->container();	
	Content* myContent = static_cast<Content*>(con->toc()->at(0)->at(0));
	QVERIFY(myContent->isGenerated());
	QString htmlContentBefore = myContent->html();
	// set now an arbitrary content to test if the template can be changed:
	QString newHTML = "<html><body><p>I AM NOT SUPPOSED TO BE HERE</p></body></html>";
	myContent->setHtml(newHTML);
	myContent->createHtml();
	QVERIFY( myContent->html() == htmlContentBefore );
}
