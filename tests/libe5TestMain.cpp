#include "e5TemplateTest.h"
#include "e5PackageWriterTest.h"
#include "ContainerAccessManagerTest.h"

#include "e5SaasShopTest.h"
#include "e5tetmltocTest.h"
#include <QCoreApplication>

#include <QTest>

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);
	
    /*
	e5SaasShopTest eS;
	QTest::qExec( &eS );
	*/
    
	/*
	 ContainerAccessManagerTest conManTest;
	 QTest::qExec( &conManTest);
	 */
	
	/*
	 e5tetmltocTest t;
	 QTest::qExec( &t);
	*/
	 e5TemplateTest templateTest;
	 QTest::qExec(&templateTest);
	
	/*	
	 e5TemplateTest templateTest;
	 QTest::qExec(&templateTest);
	 */
	
	/**
	 * Can currently not be used on OSX since we cannot write out opened Files
	 * by Lukas
	 */
    /*
	 e5PackageWriterTest packageWriterTest;
	 QTest::qExec(&packageWriterTest);
	 */
	
    return 0;
}
