#include <Container>
#include <TOC>
#include <Content>
#include <QIOStream>

using namespace std;

int main(int argc, char **argv) {
	Container *d;
	if(argc>1)
		d=Container::get(QUrl::fromLocalFile(argv[1]));
	else
		d=Container::get(QUrl::fromLocalFile(TESTS "/InDesignXmlImport"));
	cout << "Files in epub file: " << d->files() << endl;
	TOC *toc=d->toc();
	cout << "Table of contents:" << endl;
	for(int i=0; i<toc->count(); i++) {
		TOCEntry *e=toc->at(i);
		if(!e)
			continue;
		if(e->id() == "Roten.xml") {
			cout << "First view of Roten.xml converted to HTML:" << endl
			     << "==========================================" << endl
			     << static_cast<Content*>(e->at(0))->html().toUtf8().data() << endl;
		}
	}
	delete toc;
	delete d;
}
