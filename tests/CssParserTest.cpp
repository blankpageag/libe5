#include "CssParserTest.h"
#include <Downloader>
#include <QIOStream>

using namespace std;

void CssParserTest::simpleCss() {
	QStringList classes=cssClasses(
		"body {\n"
		"	width: 123px;\n"
		"}\n"
		".test {\n"
		"	font-size: 12px;\n"
		"}\n"
		"p.test1 {\n"
		"	background-color: red;\n"
		"}\n"
	);
	QVERIFY(classes == QStringList() << "test" << "test1");
}

void CssParserTest::complexCss() {
	QStringList classes=cssClasses(
		"@media screen {\n"
		"	.test {\n"
		"		background-color: red;\n"
		"	}\n"
		"	p {\n"
		"		background-color: red;\n"
		"	}\n"
		"	span.test1 {\n"
		"		background-color: red;\n"
		"	}\n"
		"}\n"
		"@media print {\n"
		"	span.test1 {\n"
		"		background-color: red;\n"
		"	}\n"
		"	.test2 {\n"
		"		background-color: red;\n"
		"	}\n"
		"}\n"
		".test3 {\n"
		"	background-color: red;\n"
		"}\n"
	);
	QVERIFY(classes == QStringList() << "test" << "test1" << "test2" << "test3");
}

void CssParserTest::insanelyComplexCss() {
	QStringList classes=cssClasses(Downloader::get(QUrl::fromLocalFile(TESTS "/data/magazin.css")));
	QVERIFY(classes == QStringList() << "generated-style" << "generated-style-2" << "textraster" << "randabfallend" << "mittlerer-bildraster" << "no-iphone" << "randabfallend-no-iphone" << "aufmacherbild" << "bildraster-innen" << "bildraster-mitte" << "bildraster-textraster" << "noIphone" << "bild-roten" << "bild-binswanger" << "au-autorennachweis-pfeiffer-ipad" << "ti-pfeiffer-ipad" << "tx-initial-block-x08" << "z-abschlusspunkt-ma-garamond-x59" << "x01-autorenbox-black-6-7-8-5-x07-INHALT" << "ti-inhalt-1-ma-garamond-20-5-22-5-x04" << "ti-inhalt-2-ma-garamond-10-8-13-6-x04" << "bilder-inhalt" << "x03-bildlegende-black-7-7-9-li-x11" << "tx-byline-as-garamond-5-8-5-x12" << "ti-spitzmarke-as-garamond-14-7-16-5-x01" << "ti-spitzmarke-as-garamond-14-7-16-5-x01-BuchTitel" << "z-black-text-6-7-8-5-x51" << "z-black-text-6-7-8-5-x51-override" << "au-autorennachweis-black-6-7-8-5-raster-x07" << "au-autorenname-12-14" << "z-black-versal-6-7-8-5-x53" << "z-black-versa-impressuml-6-5-7-5" << "z-ma-garamond-10-8-12-27-x53" << "tx-lauftext-reg-me-block-10-8-12-27-x08" << "z-garamond-mt-kursiv-10-8-12-27-x52" << "x01-autorenbox-black-6-7-8-5-x07" << "ti-ma-garamond-31-29-x04" << "tx-interview-frage-x40" << "ti-cover-anriss-16-20-x08" << "tx-as-garamond-lead-14-7-16-5-x06_Line" << "tx-as-garamond-lead-14-7-16-5-x06" << "ti-ma-garamond-31-34-x04" << "ti-ma-garamond-41-39-x04" << "ti-inhalt-2-ma-garamond-10-8-13-6-x04-VERTICAL" << "tx-impressum-as-garamond-6-7-7-5-x08" << "z-leserbriefe-subtitel-as-garamond-7-7-12-27-x53" << "ti-as-garamond-leserbriefe-10-8-12-27-x08" << "einfacher-absatz" << "ti-zwischentite-block-x09" << "tx-interview-antwort-x41" << "tx-lauftext-reg-oe-block-10-8-12-27-x08");
}

QTEST_MAIN(CssParserTest)
