#include "e5SaasShopTest.h"

#include <Shop>
#include <Consumable>
#include <typeinfo>

#define _URL_ "/data/pdfconversion/baz1_2"
#define _URL_FEEDBOOKS_ "http://www.feedbooks.com/catalog.atom"

void e5SaasShopTest::FeedBookTest()
{
	
	QString url = _URL_FEEDBOOKS_;
	Shop *s=Shop::get(QUrl::fromEncoded(url.toUtf8()));
	QVERIFY( s!= NULL );

	QVERIFY( s->rootCategory() != NULL );
	
	// check for categories and consumables, assuming only a single ctagory level exists
	QList <void*> ceckItems;
	foreach( ShopItem* item, s->rootCategory()->children() )
	{
		ceckItems.append(item);
		
		ItemCategory* cat = static_cast<ItemCategory*> (item);
		QVERIFY( cat != NULL );
		
		foreach( ShopItem* con, cat->children() )
		{
			//qDebug() << " add children "<< con->name();
			ceckItems.append(con);
		}		
	}
	
	QVERIFY( !ceckItems.empty() );
	
	foreach( void* item, ceckItems )
	{
		
		ShopItem* test = static_cast<ShopItem*> (item);
		
		ShopItem* castTest = NULL;
		// FIXME: using our own fix-cast for OSX crappy dynamic_cast 
		if( test->type().contains("ItemCategory") )
		{
			castTest = static_cast<ItemCategory*> (test);
		}

		if( castTest == NULL)
		{
			// FIXME: using our own fix-cast for OSX crappy dynamic_cast 
			castTest = static_cast<Consumable*> (test);
		}
		QVERIFY( castTest != NULL );
	}	
}


void e5SaasShopTest::pluginTest()
{
	
	QUrl shopUrl = QUrl(_URL_);
	Shop *s=Shop::get(shopUrl);
	
	
	
	
	//QVERIFY( QString( s->metaObject()->className()).compare( "e5SaasShop") == 0 );
}


void e5SaasShopTest::connectToShopTest()
{
	/*
	QFileInfo info( TEST_DATA_1 );
	QVERIFY( info.exists() );

	Container* c = Container::get( QUrl::fromLocalFile( TEST_DATA_1 ).path() );
	QVERIFY( c != NULL );
	
	TOC* t = c->toc();
	QVERIFY( QString( t->metaObject()->className()).compare( "tetmlToc") == 0 );
	
	t->info();
	 */
	
	
	
	//QVERIFY( false );
	
}


