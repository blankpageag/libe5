#include <QtTest/QtTest>
#include <WebKitTools>

class CssParserTest:public QObject {
	Q_OBJECT
private slots:
	void simpleCss();
	void complexCss();
	void insanelyComplexCss();
};
