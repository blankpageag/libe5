#include <Container>
#include <TOC>
#include <Content>

int main(int argc, char **argv) {
	Container *c=Container::get(QUrl::fromLocalFile(argv[1]));
	c->toc();
	qDebug() << "Content has ID: " << c->id();
	qDebug() << "Used plugins: " << c->metaObject()->className() << ", " << c->toc()->metaObject()->className();
}
