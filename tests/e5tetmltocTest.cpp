#include "e5tetmltocTest.h"

#include <TOC>

#include <Container>
#include <TOC>
#include <Content>
#include <QIOStream>


#define TEST_DATA_2 TESTS "/data/pdfconversion/baz1_2"
#define TEST_DATA_1 TESTS "/data/pdfconversion/baz7_9"
#define TEST_DATA_3 TESTS "/data/pdfconversion/dm1"
using namespace std;


void e5tetmltocTest::parseTETMLTest()
{
	
	QFileInfo info( TEST_DATA_1 );
	QVERIFY( info.exists() );

	Container* c = Container::get( QUrl::fromLocalFile( TEST_DATA_1 ).path() );
	QVERIFY( c != NULL );
	
	TOC* t = c->toc();
	QVERIFY( QString( t->metaObject()->className()).compare( "tetmlToc") == 0 );
	
}


