#include "libe5BaseTest.h"

#include <Container>
#include <TOC>
#include <Content>
#include <QIOStream>


#define TEST_DATA "/data/e5Test.e5"

using namespace std;

libe5BaseTest::libe5BaseTest():
_testContainer(NULL)
{
}

libe5BaseTest::~libe5BaseTest()
{
	delete _testContainer;
	_testContainer = NULL;
}


void libe5BaseTest::dynamicCastTest()
{
	openTestData( TEST_DATA);
	TOC* t = _testContainer->toc();
	QVERIFY( t->count() > 0 );
	
	QList<TOCItem*> testL;
	for( int i=0; i< t->count(); i++ )
	{
		testL.append(t->at(i));
		for( int j=0; j< t->at(i)->count(); j++ )
		{
			testL.append(t->at(i)->at(j));
		}
	}
	
	foreach( TOCItem* item, testL)
	{
		if( TOCEntry* e = dynamic_cast<TOCEntry*> (item) )
			QVERIFY( QString(e->metaObject()->className()) == "TOCEntry" );
		else if( Content * c = dynamic_cast<Content*>(item) ) {
			QVERIFY( QString(item->metaObject()->className()).endsWith("Content") );
		}

	}
		
}



void libe5BaseTest::openTestData(QString iTestPath)
{
	if( _testContainer != NULL )
	{
		delete _testContainer;
		_testContainer = NULL;
	}
	QString s = iTestPath;
	qDebug() << "Open test data from: "<< iTestPath;
	if( !iTestPath.contains( TESTS ) )
	{
		s = TESTS + iTestPath;
	}
	_testContainer = Container::get(QUrl::fromLocalFile(s));
	
	QVERIFY( _testContainer != NULL );
}




void libe5BaseTest::printTOC()
{
	if( _testContainer != NULL )
	{
		for( int i=0; i<_testContainer->toc()->count();i++)
		{
			cout<<"Toc Entry "<<i<<":"<<_testContainer->toc()->at(i)->title()<<endl;
		}
	}
}
