#ifndef _E5_SAAS_SSHOP_TEST_
#define _E5_SAAS_SSHOP_TEST_ 1

#include "libe5BaseTest.h"

class e5SaasShopTest: public libe5BaseTest
{
	Q_OBJECT

public:
	
	private slots:
	
	/**
	 *
	 */
	void FeedBookTest();
	
	
	/**
	 *
	 */
	void pluginTest();
	
	
	
	void connectToShopTest();

	
	
protected:
		
};


#endif
