#include "jsIdTest.h"
#include <WebKitTools>
#include <QTest>
#include <QDebug>

void jsIdTest::initTestCase() {
	_p=new QWebPage(this);
}

void jsIdTest::verySimpleTest() {
	_p->mainFrame()->setContent(
		"<?xml version=\"1.0\"?>"
		"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
		"<head><title>Title</title></head>"
		"<body>"
		"<p id=\"p1\">This is a test</p>"
		"</body>"
		"</html>"
	, "application/xhtml+xml");
	QWebElement e=_p->mainFrame()->findFirstElement("p");
	QVERIFY(!e.isNull());
	QVERIFY(jsId(e) == "document.getElementById('p1')");
}

void jsIdTest::simpleTest() {
	_p->mainFrame()->setContent(
		"<?xml version=\"1.0\"?>"
		"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
		"<head><title>Title</title></head>"
		"<body>"
		"<p>This is a test</p>"
		"</body>"
		"</html>"
	, "application/xhtml+xml");
	QWebElement e=_p->mainFrame()->findFirstElement("p");
	QVERIFY(!e.isNull());
	QVERIFY(jsId(e) == "document.body.firstChild");
}

void jsIdTest::mediumTest() {
	_p->mainFrame()->setContent(
		"<?xml version=\"1.0\"?>"
		"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
		"<head><title>Title</title></head>"
		"<body>"
		"<p id=\"p1\">test</p>"
		"<p id=\"p2\">test<span>test</span></p>"
		"<p id=\"p3\">test<span>test</span><span class=\"test\">test</span></p>"
		"</body>"
		"</html>"
	, "application/xhtml+xml");
	QWebElement e=_p->mainFrame()->findFirstElement("[class~=\"test\"]");
	QVERIFY(!e.isNull());
	QVERIFY(jsId(e) == "document.getElementById('p3').firstChild.nextSibling");
}

void jsIdTest::complexTest() {
	_p->mainFrame()->setContent(
		"<?xml version=\"1.0\"?>"
		"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
		"<head><title>Title</title></head>"
		"<body>"
		"<p>test</p>"
		"<p>test<div>test<span>test</span><span>test</span><span>test<span>test</span><span class=\"test\">test</span></span></div></p>"
		"</body>"
		"</html>"
	, "application/xhtml+xml");
	QWebElement e=_p->mainFrame()->findFirstElement("[class~=\"test\"]");
	QVERIFY(!e.isNull());
	QVERIFY(jsId(e) == "document.body.firstChild.nextSibling.firstChild.firstChild.nextSibling.nextSibling.firstChild.nextSibling");
}
