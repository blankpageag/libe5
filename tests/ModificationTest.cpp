#include <Container>
#include <TOC>
#include <TOCEntry>
#include <Content>
#include <QIOStream>
#include <cassert>
#include <QCoreApplication>

using namespace std;

int main(int argc, char **argv) {
	QCoreApplication app(argc, argv); // Needed for Downloader::get, used when we add a QUrl
	Container *d;
	if(argc>1)
		d=Container::get(QUrl::fromLocalFile(argv[1]));
	else
		d=Container::get(QUrl::fromLocalFile(TESTS "/data/e5-content-package"));

	TOC * const toc=d->toc();

	d->add("test.xhtml", QByteArray("<html><head><title>test</title></head><body><p>Test for added chapter</p></body></html>"));
	TOCEntry *newEntry=toc->addEntry("test_xhtml");
	Metadata *md=static_cast<Metadata*>(newEntry);
	md->insert("dc:title", "e5 Chapter adding test");
	md->insert("e5:subtitle", "Test for Container::add, TOC::addEntry and TOCEntry::append");
	md->insert("dc:publisher", "BlankPage AG");
	md->insert("dc:creator", "e5 Content Modifier");
	Content *c=newEntry->append("test.xhtml");
	assert(c);

	d->add("blankpage.xhtml", QUrl("http://blankpage.ch/"));
	TOCEntry *websiteEntry=toc->addEntry("blankpage_xhtml");
	md=static_cast<Metadata*>(websiteEntry);
	md->insert("dc:title", "www.blankpage.ch main page");
	md->insert("e5:subtitle", "Website of the makers of e5");
	md->insert("dc:publisher", "BlankPage AG");
	Content *wc=newEntry->append("blankpage.xhtml"); // Not what you think. ;) wc is short for "web content".
	assert(wc);

	cout << "Files in epub file: " << d->files() << endl;
	for(int i=0; i<toc->count(); i++) {
		cout << "*" << toc->at(i)->id() << endl;
	}
	cout << endl;
	cout << "Author: " << toc->author() << endl;
	cout << "Title: " << toc->title() << endl;

	cout << "Chapters/Articles:" << endl;
	for(int i=0; i<toc->count(); i++) {
		TOCEntry *entry=toc->at(i);
		cout << toc->at(i)->id() << endl;
		cout << "	Author: " << entry->author() << endl
		     << "	Title: " << entry->title() << endl
		     << "	Subtitle: " << entry->subtitle() << endl
		     << "	Publisher: " << entry->publisher() << endl;

		cout << "Content IDs:" << endl;
		for(int i=0; i<entry->count(); i++) {
			Content *c = static_cast<Content*>(entry->at(i));
			cout << "	" << c->id() << " / " << c->title() << endl;
		}
	}
	cout << "HTML of newly added chapter containing test code:" << endl
	     << c->html() << endl;
	cout << "HTML of newly added chapter containing BlankPage's website:" << endl
	     << wc->html() << endl;
	delete toc;
	delete d;
}
