#include "ContainerAccessManagerTest.h"

#include <ContainerAccessManager>
#include <e5PackageWriter>
#include <Container>
#include <TOC>
#include <Content>
#include <QIOStream>
#include <QCoreApplication>

#define TEST_DATA "/data/e5Test.e5"


using namespace std;


void ContainerAccessManagerTest::containerDeletionTest()
{	
	int argc = 1;
	char * argv[2] = { "e5mobile", 0 };
	QCoreApplication app(argc, argv);
	QVERIFY( ContainerAccessManager::numberOfContainers() == 0);

	
	Container* con = Container::get(QUrl(TESTS TEST_DATA));
	QVERIFY( ContainerAccessManager::numberOfContainers() == 1);

	int testIt = 10;
	QList<Container*> cons;
	for( int i=0;i<testIt;i++ )
	{
		Container *con=e5PackageWriter::createNewContainer();
		cons << con;
		QVERIFY( ContainerAccessManager::numberOfContainers() == i+2);
	}
	
	int i=0;
	foreach(Container* toDelete, cons )
	{
		delete toDelete;
		QVERIFY( ContainerAccessManager::numberOfContainers() == cons.size()-i);
		i++;
	}
	
	
	QVERIFY( ContainerAccessManager::numberOfContainers() == 1);

	
}
