#include "e5PackageWriterTest.h"

#include <ContainerAccessManager>
#include <e5PackageWriter>
#include <Container>
#include <TOC>
#include <Content>
#include <QIOStream>
#include <QCoreApplication>

#define TEST_DATA "/data/e5Test.e5"
//#define TEST_DATA "e5TemplateTestData/templateTest/"
using namespace std;


void e5PackageWriterTest::opfWritingTest()
{
	
	int argc = 1;
	char * argv[2] = { "e5mobile", 0 };
	QCoreApplication app(argc, argv);
	ContainerAccessManager *junk=ContainerAccessManager::instance();

	Container* con = Container::get(QUrl(TESTS TEST_DATA));

	//this->openTestData(TEST_DATA);
	QVERIFY( con != NULL);

	QString tmp = "tmpTest";
	QDir myDir( "/tmp/" );
	//QVERIFY( myDir.mkdir( tmp ) );
	
	QString out  = myDir.absolutePath() + "/" + tmp;
	cout<<"Writing to "<<out<<endl;
	e5PackageWriter::save( con, out, true, NULL, NULL);
	
	
	//QVERIFY( myDir.rmdir( tmp ) );
	
	
}

void e5PackageWriterTest::ncxWritingTest()
{
	QVERIFY(false);
	
}
