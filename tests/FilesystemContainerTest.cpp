#include "FilesystemContainerTest.h"
#include <TOC>
#include <TOCEntry>
#include <Content>
#include <Downloader>

#include <QIOStream>
using namespace std;

void FilesystemContainerTest::initTestCase() {
	_container=Container::get(QUrl::fromLocalFile(TESTS "/data/e5-content-package"));
	QVERIFY(_container);
}

void FilesystemContainerTest::cleanupTestCase() {
	delete _container;
}

void FilesystemContainerTest::containerTest() {
	QStringList f=_container->files();
	f.sort();
	QVERIFY(QString(_container->metaObject()->className()) == "FilesystemContainer");
	QVERIFY(f == QStringList()
		<< ".svn/entries"
		<< ".svn/prop-base/blankpage.png.svn-base"
		<< ".svn/prop-base/heart.mp4.svn-base"
		<< ".svn/text-base/article1-kidsedition.html.svn-base"
		<< ".svn/text-base/article1.html.svn-base"
		<< ".svn/text-base/article1p-page1.html.svn-base"
		<< ".svn/text-base/article1p-page2.html.svn-base"
		<< ".svn/text-base/blankpage.png.svn-base"
		<< ".svn/text-base/heart.mp4.svn-base"
		<< ".svn/text-base/mimetype.svn-base"
		<< ".svn/text-base/test.ncx.svn-base"
		<< ".svn/text-base/test.opf.svn-base"
		<< ".svn/text-base/title.html.svn-base"
		<< "META-INF/.svn/entries"
		<< "META-INF/.svn/text-base/container.xml.svn-base"
		<< "META-INF/container.xml"
		<< "article1-kidsedition.html"
		<< "article1.html"
		<< "article1p-page1.html"
		<< "article1p-page2.html"
		<< "blankpage.png"
		<< "heart.mp4"
		<< "mimetype"
		<< "test.ncx"
		<< "test.opf"
		<< "title.html");
	QVERIFY(_container->isMetadata("test.ncx"));
	QVERIFY(!_container->isMetadata("SomethingThatDoesn'tExist"));
	QVERIFY(_container->mimeType("article1.html") == "application/xhtml+xml");
	QVERIFY(_container->content("mimetype") == "application/epub+zip");
}

void FilesystemContainerTest::containerModificationTest() {
	QVERIFY(!_container->files().contains("test.html"));
	_container->add("test.html", "Just a test");
	QVERIFY(_container->files().contains("test.html"));
	QVERIFY(_container->content("test.html") == "Just a test");
	QVERIFY(_container->mimeType("test.html") == "text/html");
	_container->remove("test.html");
	QVERIFY(!_container->files().contains("test.html"));
	_container->remove("icon.jpg");
	QVERIFY(!_container->files().contains("icon.jpg"));
}

void FilesystemContainerTest::tocTest() {
	TOC *toc=_container->toc();
	QVERIFY(toc->baseUrl() == _container->baseUrl());
	QVERIFY(toc->baseUrl().startsWith("e5internal://"));
	QVERIFY(QString(toc->metaObject()->className()) == "EpubTOC");
	QVERIFY(toc->container() == _container);
	QVERIFY(toc->baseUrl() == _container->baseUrl());
	QVERIFY(toc->validate().isEmpty());
	QVERIFY(toc->count() == 2);
	QVERIFY(toc->author() == "Bernhard Rosenkraenzer");
	QVERIFY(toc->publisher() == "BlankPage");
	QVERIFY(toc->title() == "BlankPage test magazine");
}

void FilesystemContainerTest::tocEntryTest() {
	TOC *toc=_container->toc();
	TOCEntry *first=toc->at(0);
	QVERIFY(first->title() == "Cover page");
	QVERIFY(first->author() == "BlankPage cover makers");
	QVERIFY(first->publisher() == "BlankPage");
}

void FilesystemContainerTest::contentTest() {
	TOC *toc=_container->toc();
	TOCEntry *first=toc->at(0);
	QVERIFY(first->count() == 1);
	QVERIFY(static_cast<Content*>(first->at(0))->fileName() == "title.html");
	// At some point, this should be true: QVERIFY(first->at(0)->url() == first->baseUrl() + "title.html");
}

void FilesystemContainerTest::extractTest() {
	QString f=_container->extract("mimetype");
	QVERIFY(f.length());
	QVERIFY(!f.startsWith(QDir::tempPath()));
	QVERIFY(QFile::exists(f));
	QByteArray b=Downloader::get(QUrl::fromLocalFile(f));
	QVERIFY(QLatin1String(b) == QLatin1String("application/epub+zip"));

	// Auto-extraction of media...
	QString video=_container->extract("heart.mp4");
	Content *c=static_cast<Content*>(_container->toc()->at(1)->at(0));
	QVERIFY(c->html().contains(video));
}

void FilesystemContainerTest::conditionsTest() {
	TOC *toc=_container->toc();
	TOCEntry *first=toc->at(1);
	// The tests below currently return an unexpected, but correct result (2).
	// Since the name is meaningless, there's no way libe5 can know that
	// portrait implies !landscape and vice versa.
	// There should be a test for this case once "real" condition handling
	// that doesn't rely on hardcoded names is implemented.
	//QList<Content*> portrait=first->contentForConditions(QStringList() << "portrait");
	//QVERIFY(portrait.count() == 1);
	//QList<Content*> landscape=first->contentForConditions(QStringList() << "landscape");
	//QVERIFY(landscape.count() == 1);
	QList<Content*> notLandscape=first->contentForConditions(QStringList() << "!landscape");
	QVERIFY(notLandscape.count() == 3);
	QList<Content*> notPortrait=first->contentForConditions(QStringList() << "!portrait");
	QVERIFY(notPortrait.count() == 2);
}

QTEST_MAIN(FilesystemContainerTest)
