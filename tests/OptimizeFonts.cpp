#include <QCoreApplication>
#include <Container>
#include <e5PackageEditor>
#include <e5PackageWriter>

int main(int argc, char **argv) {
	QCoreApplication app(argc,argv);
	Container *c=Container::get(QUrl::fromLocalFile(argv[1]));
	e5PackageEditor::fixupCss(c);
	QStringList missing=e5PackageEditor::optimizeFonts(c);
	qDebug() << "Missing fonts:" << missing;
	e5PackageWriter::save(c, "/tmp/optimizedfonts.e5");
}
