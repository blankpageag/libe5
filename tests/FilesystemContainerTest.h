#include <QtTest/QtTest>
#include <Container>

class FilesystemContainerTest:public QObject {
	Q_OBJECT
private slots:
	void initTestCase();
	void cleanupTestCase();
	void containerTest();
	void containerModificationTest();
	void tocTest();
	void tocEntryTest();
	void contentTest();
	void conditionsTest();
	void extractTest();
private:
	Container *_container;
};
