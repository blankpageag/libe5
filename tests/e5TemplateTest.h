#ifndef _E5_TEMPLATE_TEST_
#define _E5_TEMPLATE_TEST_ 1

#include "libe5BaseTest.h"

class e5TemplateTest: public libe5BaseTest
{
	Q_OBJECT

public:
	
	private slots:
	
	/**
	 * testing of template parsing. Prints out properties of exposed libe5 data types.
	 */
	void templateParsingTest();
	
	
	/**
	 * testing if the setHTml function overrides the template; a template in libe5 editor does not automatically stores the html content but keeps the template.
	 */
	void setHTMLTest();
	
	
protected:
		
};


#endif
