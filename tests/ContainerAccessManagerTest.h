#ifndef _CONTAINER_ACCESS_MANAGER_TEST_
#define _CONTAINER_ACCESS_MANAGER_TEST_ 1

#include "libe5BaseTest.h"

class ContainerAccessManagerTest: public libe5BaseTest
{
	Q_OBJECT

public:
	
	private slots:
	
	/**
	 * Testing if containers are correctly deleted.
	 */
	void containerDeletionTest();
	
protected:

		
};


#endif
