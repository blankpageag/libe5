
window.$ = function(el){ return document.getElementById(el); }

var e5_slideshow = function(div){
  var width = 0;
	var resize = function(){
		div.style.height = '1000px';
		div.style.width = '1000px';
		setTimeout( function(){
			window.scrollTo(0, 1);
      width = window.innerWidth;
			div.style.height = window.innerHeight+1+'px';
			div.style.width = width+'px';
			
			var style = document.createElement('style');
      style.appendChild(document.createTextNode('.e5_slideshow .slides > div { width: '+width+'px; }'));
      document.body.appendChild(style);


		}, 30);
	}
	resize();
	window.addEventListener("orientationchange", resize, false);

  var 
  active = 0,
  sPosE = {x:null,y:null},
  lPosE = {x:null,y:null},
  lPosT = {x:null,y:null},
	diff = {x:0,y:0},
  el = div.querySelector('.slides'),
  footer = div.querySelector('.slideFooter'),
  header = div.querySelector('.slideHeader'),
  footerHideTimer
  ;

  var start = function(e){
    e.preventDefault();
    if(e.touches.length == 1){
      var touch = e.touches[0];
      lPosE = {x:touch.pageX,y:touch.pageY};
      sPosT = lPosT = {x:parseInt(el.style.left),y:parseInt(el.style.top)};
    }
    diff = {x:0,y:0};

    el.style.webkitTransition = 'none';

    window.clearTimeout(footerHideTimer);
    footer.style['-webkit-transition'] = '0.2s linear';
    footer.style.opacity = '1';
    header.style['-webkit-transition'] = '0.2s linear';
    header.style.opacity = '1';
  }
  var move = function(e){
      var touch = e.touches[0];
      var posE = {x:touch.pageX,y:touch.pageY};
      var posT = {x:parseInt(el.style.left)||0,y:parseInt(el.style.top)||0};

      diff = {
        x : posE.x-lPosE.x,
        y : posE.y-lPosE.y 
      };
    el.style['-webkit-transition'] = 'none';
      if( diff.x !== 0 && Math.abs(diff.y)/Math.abs(diff.x) < 0.6){
        el.style.left = posT.x + diff.x + "px";
        console.log(el.style.left)
      }
      lPosT = {x:posT.x,y:posT.y};
      lPosE = {x:posE.x,y:posE.y};
  }
  var end = function(e){
			var speed = -Math.round( diff.x /35 );
      speed = Math.min(1,speed)
      speed = Math.max(-1,speed)
			if( Math.abs(speed) && Math.abs(lPosE.x-sPosE.x) > 60 ){
				active += speed;
        active = Math.min( active, (el.childNodes.length-1)/2-1 );
        active = Math.max( active, 0 );
      }
      el.style['-webkit-transition'] = '0.'+(Math.abs(speed)+1)+'s ease-out';
      el.style.left = -active * width + 'px';

      footerHideTimer = window.setTimeout(function(){
        footer.style['-webkit-transition'] = '2s linear';
        footer.style.opacity = '0';
        header.style['-webkit-transition'] = '2s linear';
        header.style.opacity = '0';
      },2000);
  }

  el.addEventListener('touchstart',start);
  el.addEventListener('touchmove',move); 
  el.addEventListener('touchend',end);
  div.addEventListener('touchstart', function(e){e.preventDefault();}, false );

  this.hide = function(){
    div.style.display = 'none';
  }
};

document.addEventListener('click', function(e){
  var s = e.target.getAttribute('e5_slideshow');
  if( s ){
    $(s).style.display = 'block';
  } 
}, false)

window.addEventListener('load', function(e){
  var els = document.querySelectorAll('.e5_slideshow');
  for(var i=els.length; i > 0; i--) {
    var x = new e5_slideshow( els[i-1] );
  }
}, false)
