export QTDIR="/data/local/qt"
export MAINFOLDER=`pwd`
TOOLCHAINS="`echo 'message(${CMAKE_ROOT})' | cmake -P /dev/stdin 2>&1`/Toolchains"

rm -rf build
mkdir build
cd build

cmake .. $QTARGS -DCMAKE_TOOLCHAIN_FILE="$TOOLCHAINS/android.toolchain" -DCMAKE_INSTALL_PREFIX="/data/local/qt" \
 -DWITH_EXAMPLES:BOOL=OFF -DWITHOUT_DRM:BOOL=ON -DWITHOUT_GRANTLEE:BOOL=ON -DWITHOUT_TIDY:BOOL=ON -DWITHOUT_PLUGINS:BOOL=ON \
 -DQtExt_LIBRARY=/data/local/qt/lib/libQtExt.so -Dqjson_LIBRARIES=/data/local/qt/lib/libqjson.so

cd ..

echo "Resulting binaries will go to /data/local/qt so they can be pushed alongside Qt"
