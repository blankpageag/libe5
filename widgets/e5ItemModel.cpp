#include "e5ItemModel"

#include <TOCEntry>
#include <Content>

#include <QStack>
#include <QMimeData>
#include <QString>
#include <e5Logger>

using namespace std;

e5ItemModel::e5ItemModel(TOC* toc, QObject *parent) :
	QAbstractItemModel(parent),
	_toc(0)
{
	setToc(toc);
}


int e5ItemModel::columnCount(const QModelIndex &parent) const
{
	return 1;
}


QVariant e5ItemModel::data(const QModelIndex &index, int role) const
{
	if(!_toc)
		return QVariant();
	
	if (!index.isValid())
		return QVariant();
	
	if (role != Qt::DisplayRole)
		return QVariant();
	
	TOCItem *ti=static_cast<TOCItem*>(index.internalPointer());
	if(Content *c=dynamic_cast<Content*>(ti)) {
		switch(index.column()) {
			case 0:
				return c->fileName();
			default:
				qWarning() << "Invalid column requested";
		}
	} else if(TOCEntry const * const e=dynamic_cast<TOCEntry const * const>(ti)) {
		switch(index.column()) {
			case 0:
				return e->title();
			default:
				qWarning() << "Invalid column requested";
		}
	} else
		qWarning() << "Something in the TOC is neither a TOCEntry nor a Content";
	return QVariant();
}


TOCItem* const e5ItemModel::item(const QModelIndex &index) const
{
	if(!_toc)
		return 0;
	
	if(!index.isValid())
		return 0;
	
	return static_cast<TOCItem*>(index.internalPointer());
}


QVariant e5ItemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if(section == 0 && role == Qt::DisplayRole)
		return tr("Title");
	return QVariant();
}


QModelIndex e5ItemModel::index(int row, int column, const QModelIndex &parent) const
{
	TOCItem *parentP = parent.isValid() ? static_cast<TOCItem*>(parent.internalPointer()) : _toc;
	TOCEntry *p = dynamic_cast<TOCEntry*>(parentP);
	if(!p) { // Theoretically can't happen, but let's be on the safe side
		qDebug() << "Parent isn't a TOCEntry";
		return QModelIndex();
	}

	if(p->count() <= row) { // Theoretically can't happen, but let's be on the safe side
		qDebug() << "Prompted for item > rowCount";
		return QModelIndex();
	}
	return createIndex(row, column, p->at(row));
}


QModelIndex e5ItemModel::index(TOCItem *item) const
{
	TOCEntry *parent = item->parentEntry();
	if(!item || !parent) {
		return QModelIndex();
	}

	int row = parent->indexOf(item);
	if(row == -1) {
		return QModelIndex();
	}

	return createIndex(row, 0, item);
}


int e5ItemModel::rowCount(const QModelIndex &parent) const
{
	if(!_toc)
		return 0;
	
	if(!parent.isValid()) {
		return _toc->count();
	} else {
		TOCItem *it = static_cast<TOCItem*>(parent.internalPointer());
		TOCEntry *p = dynamic_cast<TOCEntry*>(it);
		if(!p)
			// Content items by definition don't have children
			return 0;
		return p->count();
	}
	
	return 0;
}


QModelIndex e5ItemModel::parent(const QModelIndex &idx) const
{
	TOCItem *it=static_cast<TOCItem*>(idx.internalPointer());
	if(!it || !it->parent() || it->parent() == _toc)
		return QModelIndex();
	
	return index(it->parentEntry());
}


Qt::ItemFlags e5ItemModel::flags(QModelIndex const &index) const
{
	Qt::ItemFlags ret = QAbstractItemModel::flags(index);
	if(index.isValid())
		ret |= Qt::ItemIsDragEnabled;
	return ret;
}


QStringList e5ItemModel::mimeTypes() const
{
	return QStringList() << "application/vnd.blankpage.e5.publish.tocitem";
}


QMimeData *e5ItemModel::mimeData(QModelIndexList const &indexes) const
{
	QMimeData *mimeData=new QMimeData;
	QByteArray encodedData;
	QDataStream stream(&encodedData, QIODevice::WriteOnly);
	foreach(QModelIndex const &index, indexes) {
		// Just dropping any index with a column != 0 is not 100% right, but
		// given it doesn't make any sense whatsoever to treat items as individual
		// columns for this particular type of content, we probably don't
		// need to care...
		if(index.column() == 0) {
			// First of all, make sure we're talking about the same
			// TOC -- this may be cross-application...
			stream << _toc->container()->id();
			// Then tell the receiver how to get to the item we're
			// interested in...
			QStack<qint16> heritage;
			QModelIndex cur=index;
			while(cur.isValid()) {
				heritage.push(cur.row());
				cur=cur.parent();
			}
			stream << static_cast<qint16>(heritage.count());
			while(!heritage.isEmpty())
				stream << heritage.pop();
		}
	}
	mimeData->setData("application/vnd.blankpage.e5.publish.tocitem", encodedData);
	return mimeData;
}


void e5ItemModel::setToc(TOC *toc)
{
	if(toc != _toc) {
		beginResetModel();

		if(_toc) {
			disconnect(_toc, SIGNAL(destroyed(QObject*)),
					   this, SLOT(dereferenceDestroyedToc(QObject*)));
			setTocEntryConnection(_toc, false);
		}

		_toc = toc;

		if(_toc) {
			connect(_toc, SIGNAL(destroyed(QObject*)),
					this, SLOT(dereferenceDestroyedToc(QObject*)));
			setTocEntryConnection(_toc, true);
		}

		endResetModel();

		emit tocChanged(_toc);
	}
}


void e5ItemModel::dereferenceDestroyedToc(QObject *obj)
{
	if(obj == _toc) {
		beginResetModel();
		_toc = 0;
		endResetModel();
		emit tocChanged(0);
	}
}


void e5ItemModel::emitItemAboutToBeRemoved(TOCItem *item, int position)
{
	TOCEntry *entry = qobject_cast<TOCEntry*>(sender());
	if(entry) {
		setTocEntryConnection(qobject_cast<TOCEntry*>(item), false);

		ASSERT(item->parentEntry() == entry);
		beginRemoveRows(index(entry), position, position);

		QModelIndexList persistentIndexes = persistentIndexList();
		foreach(QModelIndex const& index, persistentIndexes) {
			if(index.internalPointer() == item) {
				changePersistentIndex(index, QModelIndex());
			}
		}
	}
}


void e5ItemModel::emitItemAdded(TOCItem *item, int position)
{
	TOCEntry *entry = qobject_cast<TOCEntry*>(sender());
	if(entry) {
		setTocEntryConnection(qobject_cast<TOCEntry*>(item), true);

		ASSERT(item->parentEntry() == entry);
		beginInsertRows(index(entry), position, position);
		endInsertRows();
	}
}


void e5ItemModel::emitItemChanged(TOCItem *item, int position)
{
	QModelIndex index = createIndex(position, 0, item);
	emit dataChanged(index, index);
}


void e5ItemModel::emitItemRemoved()
{
	endRemoveRows();
}


void e5ItemModel::updateMetadata()
{
	TOCItem *item = qobject_cast<TOCItem*>(sender());
	if(!item) {
		return;
	}

	TOCEntry *parentEntry = item->parentEntry();
	if(parentEntry) {
		int row = parentEntry->indexOf(item);
		emit dataChanged(createIndex(row, 0, item), createIndex(row, 1, item));
	}
}


void e5ItemModel::setTocEntryConnection(TOCItem *item, bool connected)
{
	if(!item) {
		return;
	}

	if(connected) {
		connect(item, SIGNAL(metadataChanged()),
				this, SLOT(updateMetadata()));

	} else {
		disconnect(item, SIGNAL(metadataChanged()),
				   this, SLOT(updateMetadata()));
	}

	TOCEntry *entry = qobject_cast<TOCEntry*>(item);
	if(entry) {
		if(connected) {
			connect(entry, SIGNAL(itemAdded(TOCItem*, int)),
					this, SLOT(emitItemAdded(TOCItem*, int)));
			connect(entry, SIGNAL(itemChanged(TOCItem*, int)),
					this, SLOT(emitItemChanged(TOCItem*, int)));
			connect(entry, SIGNAL(itemAboutToBeRemoved(TOCItem*, int)),
					this, SLOT(emitItemAboutToBeRemoved(TOCItem*, int)));
			connect(entry, SIGNAL(itemRemoved(TOCItem*, int)),
					this, SLOT(emitItemRemoved()));
		} else {
			disconnect(entry, SIGNAL(itemAdded(TOCItem*, int)),
					   this, SLOT(emitItemAdded(TOCItem*, int)));
			disconnect(entry, SIGNAL(itemChanged(TOCItem*, int)),
					   this, SLOT(emitItemChanged(TOCItem*, int)));
			disconnect(entry, SIGNAL(itemAboutToBeRemoved(TOCItem*, int)),
					   this, SLOT(emitItemAboutToBeRemoved(TOCItem*, int)));
			disconnect(entry, SIGNAL(itemRemoved(TOCItem*, int)),
					   this, SLOT(emitItemRemoved()));
		}

		for(int i = 0; i < entry->count(); i++) {
			setTocEntryConnection(entry->at(i), connected);
		}
	}
}
