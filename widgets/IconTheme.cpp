#include <QtGui>
#include "IconTheme"

QString s_currentIconPath;

void ensureIconPathFound()
{
	if(s_currentIconPath.isEmpty()) {
		QDir appDir(QCoreApplication::applicationDirPath());
		QStringList candidatePaths;
		candidatePaths << appDir.absoluteFilePath("Icons");
		candidatePaths << appDir.absoluteFilePath("../Resources/Icons");			// Mac OS X bundle
		candidatePaths << appDir.absoluteFilePath("Resources/iconthemes/Oxygen");	// When running from the source tree
		candidatePaths << appDir.absoluteFilePath("../../e5Publish/Resources/iconthemes/Oxygen");	// Wen running from /build
		candidatePaths << "/usr/share/icons/oxygen";								// Fallback: default Oxygen icon theme location

		foreach(QString const& path, candidatePaths) {
			QFileInfo info(path);
			if(info.exists() && info.isDir()) {
				s_currentIconPath = path;
				return;
			}
		}

		s_currentIconPath = ".";
	}
}


QIcon IconTheme::icon(QString const& category, QString const& name)
{
	ensureIconPathFound();
	QIcon icon;
	icon.addFile(s_currentIconPath + "/16x16/" + category + "/" + name + ".png", QSize(16, 16));
	icon.addFile(s_currentIconPath + "/22x22/" + category + "/" + name + ".png", QSize(22, 22));
	icon.addFile(s_currentIconPath + "/32x32/" + category + "/" + name + ".png", QSize(32, 32));
	icon.addFile(s_currentIconPath + "/48x48/" + category + "/" + name + ".png", QSize(48, 48));
	icon.addFile(s_currentIconPath + "/64x64/" + category + "/" + name + ".png", QSize(64, 64));
	return icon;
}


QUrl IconTheme::iconThemeUrl()
{
	ensureIconPathFound();
	return QUrl::fromLocalFile(s_currentIconPath);
}


QPixmap IconTheme::pixmap(QString const& category, QString const& name, int size)
{
	ensureIconPathFound();
	return QPixmap( QString("%1/%2x%2/%3/%4.png").arg(s_currentIconPath).arg(size).arg(category).arg(name) );
}


QUrl IconTheme::pixmapUrl(QString const& category, QString const& name, int size)
{
	ensureIconPathFound();
	return QUrl::fromLocalFile( QString("%1/%2x%2/%3/%4.png").arg(s_currentIconPath).arg(size).arg(category).arg(name) );
}


void IconTheme::setIconTheme(QUrl const& url)
{
	s_currentIconPath = url.toString();
}
