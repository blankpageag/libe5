#ifndef IMPORTDIALOG_P_H
#define IMPORTDIALOG_P_H 1

#include <TOC>
#include "e5ItemModel"

#define CHECKED "checked"
#define UNCHECKED "unchecked"
#define PARTIALLYCHECKED "partiallyChecked"

class ImportModel : public e5ItemModel
{
	Q_OBJECT

public:
	ImportModel(QObject *parent = 0) :
		e5ItemModel(0, parent)
	{
	}

	QVariant data(const QModelIndex &index, int role) const
	{
		if(!_toc || !index.isValid()) {
			return QVariant();
		}

		if(index.column() != 0) {
			qWarning() << "Invalid column requested";
			return QVariant();
		}

		TOCItem *tocItem = static_cast<TOCItem*>(index.internalPointer());
		if(role == Qt::CheckStateRole) {
			return checkStateVariant(tocItem);

		} else if(role == Qt::DisplayRole) {
			if(Content *c=dynamic_cast<Content*>(tocItem)) {
				return c->fileName();
			} else if(TOCEntry const * const e=dynamic_cast<TOCEntry const * const>(tocItem)) {
				return e->title();
			} else {
				qWarning() << "Something in the TOC is neither a TOCEntry nor a Content";
			}
		}

		return QVariant();
	}

	Qt::ItemFlags flags(QModelIndex const &index) const
	{
		Qt::ItemFlags ret = QAbstractItemModel::flags(index);
		if(index.isValid()) {
			ret |= Qt::ItemIsUserCheckable;
		}
		return ret;
	}

	bool setData(QModelIndex const& index, QVariant const& value, int role = Qt::EditRole)
	{
		if(!index.isValid()) {
			return false;
		}
		if(role != Qt::CheckStateRole) {
			return false;
		}

		Qt::CheckState state = static_cast<Qt::CheckState>(value.toInt());
		QString checkedValue = checkString(state);
		TOCItem *tocItem = static_cast<TOCItem*>(index.internalPointer());

		if(dynamic_cast<TOCEntry*>(tocItem) && (state == Qt::Unchecked || state == Qt::Checked)) {
			markAllTOCItems(tocItem, checkedValue, EmitDataChanged);
		}

		tocItem->replace("e5:tocitemselected", checkedValue);
		emit dataChanged(index, index);

		TOCEntry *parent = tocItem->parentEntry();
		do {
			Qt::CheckState parentState = checkState(parent);
			if(state == Qt::Checked) {
				if(parentState == Qt::Unchecked) {
					TOCEntry *grandParent = parent->parentEntry();
					if(grandParent) {
						static_cast<Metadata*>(parent)->replace("e5:tocitemselected", PARTIALLYCHECKED);
						QModelIndex parentIndex = createIndex(grandParent->indexOf(parent), 0, parent);
						emit dataChanged(parentIndex, parentIndex);
					}
				}
			} else if(parentState == Qt::PartiallyChecked) {
				for(int i = 0; i < parent->count(); i++) {
					if(checkState(parent->at(i)) != Qt::Unchecked) {
						return true;
					}
				}

				TOCEntry *grandParent = parent->parentEntry();
				if(grandParent) {
					static_cast<Metadata*>(parent)->replace("e5:tocitemselected", UNCHECKED);
					QModelIndex parentIndex = createIndex(grandParent->indexOf(parent), 0, parent);
					emit dataChanged(parentIndex, parentIndex);
				}
			}

			parent = parent->parentEntry();
		} while(parent && parent != _toc);

		return true;
	}

	static Qt::CheckState checkState(TOCItem *item)
	{
		return checkState(item->value("e5:tocitemselected"));
	}

	static Qt::CheckState checkState(QString const& value)
	{
		if(value == CHECKED) {
			return Qt::Checked;
		} else if(value == PARTIALLYCHECKED) {
			return Qt::PartiallyChecked;
		} else {
			return Qt::Unchecked;
		}
	}

	static QVariant checkStateVariant(TOCItem *item)
	{
		return checkStateVariant(item->value("e5:tocitemselected"));
	}

	static QVariant checkStateVariant(QString const& value)
	{
		if(value == CHECKED) {
			return Qt::Checked;
		} else if(value == UNCHECKED) {
			return Qt::Unchecked;
		} else if(value == PARTIALLYCHECKED) {
			return Qt::PartiallyChecked;
		} else {
			return QVariant();
		}
	}

	static QString checkString(Qt::CheckState state)
	{
		switch(state) {
		case Qt::Checked:
			return CHECKED;
		case Qt::Unchecked:
			return UNCHECKED;
		case Qt::PartiallyChecked:
			return PARTIALLYCHECKED;
		default:
			return QString();
		}
	}

public slots:
	void deselectAll()
	{
		emit layoutAboutToBeChanged();
		markAllTOCItems(_toc, UNCHECKED);
		emit layoutChanged();
	}

	void selectAll()
	{
		emit layoutAboutToBeChanged();
		markAllTOCItems(_toc, CHECKED);
		emit layoutChanged();
	}

	void setToc(TOC *toc)
	{
		if(toc != _toc) {
			markAllTOCItems(toc, CHECKED);
		}
		e5ItemModel::setToc(toc);
	}

private:
	enum DataChangedEmission {
		DonotEmitDataChanged,
		EmitDataChanged
	};

	void markAllTOCItems(TOCItem *item, QString const& value, DataChangedEmission emission = DonotEmitDataChanged);
};

#endif
