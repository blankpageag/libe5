#include <QtGui>
#include "IconTheme"
#include "ImportDialog"
#include "MimeInfo"
#include "ImportDialog_p.h"
#include "ui_ImportDialog.h"

bool caseInsensitiveLessThan(QString const& s1, QString const& s2)
{
	return s1.toLower() < s2.toLower();
}


void findSelectedArticles(TOCItem *item, QSet<TOCItem*> *itemList)
{
	if(TOCEntry *entry = dynamic_cast<TOCEntry*>(item)) {
		for(int i = 0; i < entry->count(); i++) {
			findSelectedArticles(entry->at(i), itemList);
		}
	}

	Qt::CheckState state = ImportModel::checkState(item);
	if(state == Qt::Checked || state == Qt::PartiallyChecked) {
		itemList->insert(item);
	}
}


void findFiles(QStandardItem *item, QStringList *fileList, Qt::CheckState checkState = Qt::Checked)
{
	if(item->hasChildren()) {
		for(int i = 0; i < item->rowCount(); i++) {
			findFiles(item->child(i), fileList, checkState);
		}

	} else if(item->checkState() == checkState) {
		QString file = item->text();
		QStandardItem *parent = item;
		while(parent = parent->parent()) {
			file.prepend(parent->text() + "/");
		}
		fileList->append(file);
	}
}


void ImportModel::markAllTOCItems(TOCItem *item, QString const& value, DataChangedEmission emission)
{
	item->replace("e5:tocitemselected", value);

	TOCEntry *entry = dynamic_cast<TOCEntry*>(item);
	if(entry) {
		for(int i = 0; i < entry->count(); i++) {
			markAllTOCItems(entry->at(i), value);

		}

		if(emission == EmitDataChanged && !entry->isEmpty()) {
			QModelIndex topLeft = createIndex(0, 0, entry->first());
			QModelIndex bottomRight = createIndex(entry->count() - 1, 0, entry->last());
			emit dataChanged(topLeft, bottomRight);
		}
	}
}


void recursiveSetCheckState(QStandardItem *item, Qt::CheckState state)
{
	item->setCheckState(state);
	for(int i = 0; i < item->rowCount(); i++) {
		recursiveSetCheckState(item->child(i), state);
	}
}


void recursiveSetCheckState(QStandardItem *item, Qt::CheckState state, QSet<QString> const& filter)
{
	if(item->hasChildren()) {
		for(int i = 0; i < item->rowCount(); i++) {
			recursiveSetCheckState(item->child(i), state, filter);
		}

		if(item->checkState() == Qt::PartiallyChecked) {	// Remove partially checked when no child is checked
			for(int i = 0; i < item->rowCount(); i++) {
				if(item->child(i)->checkState() != Qt::Unchecked) {
					return;
				}
			}
			item->setCheckState(Qt::Unchecked);
		} else if(item->checkState() == Qt::Unchecked) {	// Set partially checked when a child is checked
			for(int i = 0; i < item->rowCount(); i++) {
				if(item->child(i)->checkState() != Qt::Unchecked) {
					item->setCheckState(Qt::PartiallyChecked);
					return;
				}
			}
		}

	} else if(filter.contains( item->text().section('.', -1).toLower() )) {
		item->setCheckState(state);
	}
}




ImportDialog::ImportDialog(TOC *toc, QWidget *parent) :
	QDialog(parent),
	_ui(new Ui::ImportDialog),
	_toc(toc),
	_fileModel(new QStandardItemModel(this)),
	_fileProxyModel(new QSortFilterProxyModel(this)),
	_verifyingChanges(false),
	_tocModel(new ImportModel(this))
{
	_ui->setupUi(this);
	_ui->_buttonBox->button(QDialogButtonBox::Open)->setText(tr("Import"));
	_ui->_buttonBox->button(QDialogButtonBox::Open)->setIcon(IconTheme::icon("actions", "document-import"));
	_ui->_selectAllArticles->setIcon(IconTheme::icon("actions", "edit-select-all"));
	_ui->_deselectAllArticles->setIcon(IconTheme::icon("actions", "edit-delete"));
	_ui->_selectAllFiles->setIcon(IconTheme::icon("actions", "edit-select-all"));
	_ui->_deselectAllFiles->setIcon(IconTheme::icon("actions", "edit-delete"));
	_ui->_selectHtml->setIcon(IconTheme::icon("mimetypes", "text-html"));
	_ui->_selectCss->setIcon(IconTheme::icon("mimetypes", "text-css"));
	_ui->_selectJavascript->setIcon(IconTheme::icon("mimetypes", "application-javascript"));
	_ui->_selectImages->setIcon(IconTheme::icon("mimetypes", "image-x-generic"));
	_ui->_selectVideos->setIcon(IconTheme::icon("mimetypes", "video-x-generic"));
	_ui->_selectFonts->setIcon(IconTheme::icon("mimetypes", "application-x-font-ttf"));
	connect(_ui->_buttonBox->button(QDialogButtonBox::Open), SIGNAL(clicked()),
			this, SLOT(accept()));
	connect(_ui->_buttonBox->button(QDialogButtonBox::Cancel), SIGNAL(clicked()),
			this, SLOT(reject()));
	_ui->_tabWidget->setCurrentIndex(0);

	// Article tree
	connect(_ui->_selectAllArticles, SIGNAL(clicked()),
			_tocModel, SLOT(selectAll()));
	connect(_ui->_deselectAllArticles, SIGNAL(clicked()),
			_tocModel, SLOT(deselectAll()));
	_tocModel->setToc(toc);
	_ui->_articleView->setModel(_tocModel);

	// File tree
	connect(_ui->_selectAllFiles, SIGNAL(clicked()),
			this, SLOT(selectAllFiles()));
	connect(_ui->_deselectAllFiles, SIGNAL(clicked()),
			this, SLOT(deselectAllFiles()));
	connect(_ui->_selectCss, SIGNAL(clicked(bool)),
			this, SLOT(setCssSelected(bool)));
	connect(_ui->_selectFonts, SIGNAL(clicked(bool)),
			this, SLOT(setFontsSelected(bool)));
	connect(_ui->_selectHtml, SIGNAL(clicked(bool)),
			this, SLOT(setHtmlSelected(bool)));
	connect(_ui->_selectImages, SIGNAL(clicked(bool)),
			this, SLOT(setImagesSelected(bool)));
	connect(_ui->_selectJavascript, SIGNAL(clicked(bool)),
			this, SLOT(setJavascriptSelected(bool)));
	connect(_ui->_selectVideos, SIGNAL(clicked(bool)),
			this, SLOT(setVideosSelected(bool)));
	connect(_fileModel, SIGNAL(itemChanged(QStandardItem*)),
			this, SLOT(verifyChanges(QStandardItem*)));
	_fileProxyModel->setSourceModel(_fileModel);
	_ui->_fileView->setModel(_fileProxyModel);
	_ui->_fileView->sortByColumn(0, Qt::AscendingOrder);

	// Condition changes
	_ui->_conditions->addItem(IconTheme::icon("actions", "document-encrypt"), tr("Keep unmodified"), e5PackageImporter::KeepConditions);
	_ui->_conditions->addItem(IconTheme::icon("actions", "orientation-portrait"), tr("All as portrait"), e5PackageImporter::AllPortrait);
	_ui->_conditions->addItem(IconTheme::icon("actions", "orientation-landscape"), tr("All as landscape"), e5PackageImporter::AllLandscape);
	_ui->_conditions->addItem(IconTheme::icon("actions", "orientation-change"), tr("Swap orientations"), e5PackageImporter::SwapOrientations);
	_ui->_conditions->addItem(IconTheme::icon("actions", "edit-delete"), tr("Remove"), e5PackageImporter::RemoveConditions);
}


ImportDialog::~ImportDialog()
{
	delete _ui;
}


e5PackageImporter::ConditionChanges ImportDialog::conditionChanges() const
{
	return static_cast<e5PackageImporter::ConditionChanges>(_ui->_conditions->itemData(_ui->_conditions->currentIndex()).toInt());
}


bool ImportDialog::isImportAsArticlesEnabled() const
{
	return _ui->_importAsArticles->isChecked();
}


QSet<TOCItem*> ImportDialog::selectedArticles() const
{
	QSet<TOCItem*> articles;
	findSelectedArticles(_toc, &articles);
	return articles;
}


QStringList ImportDialog::selectedFiles() const
{
	QStringList files;
	for(int i = 0; i < _fileModel->rowCount(); i++) {
		findFiles(_fileModel->item(i), &files);
	}

	return files;
}


void ImportDialog::setFiles(QStringList filePaths)
{
	_fileModel->clear();
	_fileModel->setColumnCount(1);
	_fileModel->setHorizontalHeaderLabels(QStringList() << tr("File"));
	qSort(filePaths.begin(), filePaths.end(), caseInsensitiveLessThan);

	QList<QStandardItem*> rootItems;
	QList<QStandardItem*> currentTreePath;
	foreach(QString const& filePath, filePaths) {
		if(filePath.isEmpty() || filePath.endsWith('/')) {
			continue;
		}

		QStringList pathParts = filePath.split('/', QString::SkipEmptyParts);

		// Find the deepest common ancestor between the previous file and the current one
		int commonDepth = -1;
		for(int i = 0; i < qMin(currentTreePath.count(), pathParts.count() - 1); i++) {
			if(currentTreePath.at(i)->text().compare(pathParts.at(i), Qt::CaseInsensitive) == 0) {
				commonDepth = i;
			} else {
				break;
			}
		}

		// Truncate the list to the deepest common ancestor
		while(currentTreePath.count() - 1 > commonDepth) {
			currentTreePath.removeLast();
		}

		// Append starting from the common ancestor
		for(int i = commonDepth + 1; i < pathParts.count(); i++) {
			bool isLeaf = (i == pathParts.count() - 1);
			QIcon icon = isLeaf ? MimeInfo::icon(pathParts.last()) : IconTheme::icon("places", "folder");
			QStandardItem *item = new QStandardItem(icon, pathParts.at(i));
			item->setCheckable(true);
			item->setCheckState(Qt::Checked);
			item->setEditable(false);
			if(i == 0) {
				rootItems.append(item);
			} else {
				QStandardItem *parent = currentTreePath.last();
				parent->appendRow(item);
			}
			if(!isLeaf) {
				currentTreePath.append(item);
			}
		}
	}

	foreach(QStandardItem *item, rootItems) {
		_fileModel->appendRow(item);
	}
	_fileProxyModel->invalidate();
}


void ImportDialog::deselectAllFiles()
{
	_verifyingChanges = true;
	for(int i = 0; i < _fileModel->rowCount(); i++) {
		recursiveSetCheckState(_fileModel->item(i), Qt::Unchecked);
	}

	_ui->_selectCss->setChecked(false);
	_ui->_selectFonts->setChecked(false);
	_ui->_selectHtml->setChecked(false);
	_ui->_selectImages->setChecked(false);
	_ui->_selectJavascript->setChecked(false);
	_ui->_selectVideos->setChecked(false);

	_verifyingChanges = false;
}


void ImportDialog::selectAllFiles()
{
	_verifyingChanges = true;
	for(int i = 0; i < _fileModel->rowCount(); i++) {
		recursiveSetCheckState(_fileModel->item(i), Qt::Checked);
	}

	_ui->_selectCss->setChecked(true);
	_ui->_selectFonts->setChecked(true);
	_ui->_selectHtml->setChecked(true);
	_ui->_selectImages->setChecked(true);
	_ui->_selectJavascript->setChecked(true);
	_ui->_selectVideos->setChecked(true);

	_verifyingChanges = false;
}


void ImportDialog::setFilesSelected(MimeInfo::MimeType type, bool selected)
{
	_verifyingChanges = true;
	for(int i = 0; i < _fileModel->rowCount(); i++) {
		recursiveSetCheckState(_fileModel->item(i), selected ? Qt::Checked : Qt::Unchecked, MimeInfo::extensions(type));
	}
	_verifyingChanges = false;
}


void ImportDialog::setCssSelected(bool selected)
{
	setFilesSelected(MimeInfo::Css, selected);
}


void ImportDialog::setFontsSelected(bool selected)
{
	setFilesSelected(MimeInfo::Font, selected);
}


void ImportDialog::setHtmlSelected(bool selected)
{
	setFilesSelected(MimeInfo::Html, selected);
}


void ImportDialog::setImagesSelected(bool selected)
{
	setFilesSelected(MimeInfo::Image, selected);
}


void ImportDialog::setJavascriptSelected(bool selected)
{
	setFilesSelected(MimeInfo::Javascript, selected);
}


void ImportDialog::setVideosSelected(bool selected)
{
	setFilesSelected(MimeInfo::Video, selected);
}


void ImportDialog::verifyChanges(QStandardItem *item)
{
	if(_verifyingChanges) {
		return;
	}
	_verifyingChanges = true;

	if(item->rowCount() > 0) {
		if(item->checkState() == Qt::Unchecked || item->checkState() == Qt::Checked) {
			recursiveSetCheckState(item, item->checkState());
		}
	}

	QStandardItem *parent = item;
	while(parent = parent->parent()) {
		if(item->checkState() == Qt::Checked) {
			if(parent->checkState() == Qt::Unchecked) {
				parent->setCheckState(Qt::PartiallyChecked);
			}
		} else if(parent->checkState() == Qt::PartiallyChecked) {
			for(int i = 0; i < parent->rowCount(); i++) {
				if(parent->child(i)->checkState() != Qt::Unchecked) {
					_verifyingChanges = false;
					return;
				}
			}

			parent->setCheckState(Qt::Unchecked);
		}
	}

	// Check the file type buttons if all the files of their type are checked, otherwise uncheck them
	bool typesChecked[6] = { true, true, true, true, true, true };
	QStringList uncheckedFiles;
	for(int i = 0; i < _fileModel->rowCount(); i++) {
		findFiles(_fileModel->item(i), &uncheckedFiles, Qt::Unchecked);
	}

	foreach(QString const& file, uncheckedFiles) {
		QString extension = file.section('.', -1).toLower();
		for(int i = 0; i < 6; i++) {
			if(MimeInfo::extensions(static_cast<MimeInfo::MimeType>(i)).contains(extension)) {
				typesChecked[i] = false;
			}
		}
	}

	_ui->_selectCss->setChecked(typesChecked[MimeInfo::Css]);
	_ui->_selectFonts->setChecked(typesChecked[MimeInfo::Font]);
	_ui->_selectHtml->setChecked(typesChecked[MimeInfo::Html]);
	_ui->_selectImages->setChecked(typesChecked[MimeInfo::Image]);
	_ui->_selectJavascript->setChecked(typesChecked[MimeInfo::Javascript]);
	_ui->_selectVideos->setChecked(typesChecked[MimeInfo::Video]);

	_verifyingChanges = false;
}
