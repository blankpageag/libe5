#ifndef MIMEINFO_H
#define MIMEINFO_H

#include <QIcon>

namespace MimeInfo
{
	class MimeData
	{
	public:
		MimeData(QString const& iDescription = QT_TRANSLATE_NOOP("MimeInfo", "Unknown"), QString const& iMimeType = QString(), QString const& iIconCategory = "mimetypes", QString iIconName = "unknown");
		MimeData(MimeData const& other);
		MimeData& operator=(MimeData const& other);

		QString description;
		QString mimeType;
		QString iconCategory;
		QString iconName;
	};

	enum MimeType
	{
		Css = 0,
		Font,
		Html,
		Image,
		Javascript,
		Video
	};

	QSet<QString> extensions(MimeType type);
	QIcon icon(QString const& fileName);
	MimeData mimeData(QString const& fileName);
	void registerMimeData(QString const& extension, MimeData const& mimeData);
}

#endif
