#include "e5Widget"


using namespace std;

e5Widget::e5Widget(QSize iSize):
QWidget()
{
	this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	this->_size = iSize;	
}


e5Widget::e5Widget(QWidget* iParent, QSize iSize):
QWidget(iParent)
{
	this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	this->_size = iSize;	
}
