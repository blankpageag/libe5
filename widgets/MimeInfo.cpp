#include <QtGui>
#include "IconTheme"
#include "MimeInfo"

using namespace MimeInfo;

QHash<QString, MimeInfo::MimeData> s_mimeInfos;

MimeData::MimeData(QString const& iDescription, QString const& iMimeType, QString const& iIconCategory, QString iIconName) :
	description(iDescription),
	mimeType(iMimeType),
	iconCategory(iIconCategory),
	iconName(iIconName)
{
}


MimeData::MimeData(MimeData const& other)
{
	*this = other;
}


MimeData& MimeData::operator=(MimeData const& other)
{
	description = other.description;
	mimeType = other.mimeType;
	iconCategory = other.iconCategory;
	iconName = other.iconName;
	return *this;
}


void ensureMimeDataLoaded()
{
	if(s_mimeInfos.isEmpty()) {
		// CSS
		{
			s_mimeInfos.insert("css", MimeData(QT_TRANSLATE_NOOP("MimeInfo", "Stylesheet"),
											   "text/css", "mimetypes", "text-css"));
		}

		// Fonts
		{
			s_mimeInfos.insert("otf", MimeData(QT_TRANSLATE_NOOP("MimeInfo", "Font"),
											   "application/x-font-otf", "mimetypes", "application-x-font-ttf"));
			s_mimeInfos.insert("ttf", MimeData(QT_TRANSLATE_NOOP("MimeInfo", "Font"),
											   "application/x-font-ttf", "mimetypes", "application-x-font-ttf"));
		}

		// HTML
		{
			MimeData data(QT_TRANSLATE_NOOP("MimeInfo", "HTML document"),
						  "text/html", "mimetypes", "text-html");
			s_mimeInfos.insert("htm", data);
			s_mimeInfos.insert("html", data);
			s_mimeInfos.insert("xhtm", data);
			s_mimeInfos.insert("xhtml", data);
		}

		// Images
		{
			MimeData data(QT_TRANSLATE_NOOP("MimeInfo", "Image"),
						  "image/png", "mimetypes", "image-x-generic");
			s_mimeInfos.insert("png", data);

			data.mimeType = "image/jpeg";
			s_mimeInfos.insert("jpg", data);
			s_mimeInfos.insert("jpeg", data);

			data.mimeType = "image/gif";
			s_mimeInfos.insert("gif", data);

			data.mimeType = "image/tiff";
			s_mimeInfos.insert("tif", data);
			s_mimeInfos.insert("tiff", data);

			data.mimeType = "image/bmp";
			s_mimeInfos.insert("bmp", data);
		}

		// Javascript
		{
			s_mimeInfos.insert("js", MimeData(QT_TRANSLATE_NOOP("MimeInfo", "Javascript code"),
											  "application/javascript", "mimetypes", "application-javascript"));
		}

		// Videos
		{
			MimeData data(QT_TRANSLATE_NOOP("MimeInfo", "Video"),
						  "video/x-msvideo", "mimetypes", "video-x-generic");
			s_mimeInfos.insert("avi", data);

			data.mimeType = "video/mpeg";
			s_mimeInfos.insert("mpg", data);
			s_mimeInfos.insert("mpeg", data);
			s_mimeInfos.insert("m4v", data);

			data.mimeType = "video/mp4";
			s_mimeInfos.insert("mp4", data);

			data.mimeType = "video/quicktime";
			s_mimeInfos.insert("mov", data);

			data.mimeType = "video/ogg";
			s_mimeInfos.insert("ogg", data);
			s_mimeInfos.insert("ogm", data);
			s_mimeInfos.insert("ogv", data);

			data.mimeType = "video/x-flv";
			s_mimeInfos.insert("flv", data);

			data.mimeType = "video/webm";
			s_mimeInfos.insert("webm", data);
		}
	}
}


QSet<QString> MimeInfo::extensions(MimeType type)
{
	switch(type) {
	case Css:
		return QSet<QString>() << "css";

	case Font:
		return QSet<QString>() << "otf" << "ttf";

	case Html:
		return QSet<QString>() << "htm" << "html" << "xhtm" << "xhtml";

	case Image:
		return QSet<QString>() << "png" << "jpg" << "jpeg" << "gif" << "tif" << "tiff" << "bmp";

	case Javascript:
		return QSet<QString>() << "js";

	case Video:
		return QSet<QString>() << "avi" << "mpg" << "mpeg" << "m4v" << "mp4" << "mov" << "ogg" << "ogm" << "ogv" << "flv" << "webm";

	default:
		return QSet<QString>();
	}
}


QIcon MimeInfo::icon(QString const& fileName)
{
	MimeData data = mimeData(fileName);
	return IconTheme::icon(data.iconCategory, data.iconName);
}


MimeData MimeInfo::mimeData(QString const& fileName)
{
	ensureMimeDataLoaded();
	return s_mimeInfos.value(fileName.section('.', -1).toLower());
}


void MimeInfo::registerMimeData(QString const& extension, MimeData const& mimeData)
{
	ensureMimeDataLoaded();
	s_mimeInfos.insert(extension.section('.', -1).toLower(), mimeData);
}
