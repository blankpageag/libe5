#include <QtCore>
#include <QtNetwork>
#include "ShopModel"
#include <Consumable>
#include <QDebug>
#include <Downloader>
#include <ItemCategory>
#include <CompilerTools>
#include <cassert>

ShopModel::ShopModel(Shop *shop, QObject *parent):QAbstractItemModel(parent),_shop(shop) {
	ASSERT( connect(&_accessManager, SIGNAL(finished(QNetworkReply*)),
					this, SLOT(readReply(QNetworkReply*))) );
	_downloadIconTimer.setInterval(0);
	_downloadIconTimer.setSingleShot(true);
	ASSERT( connect(&_downloadIconTimer, SIGNAL(timeout()),
					this, SLOT(fetchIcons())) );
}

QVariant ShopModel::data(QModelIndex const &index, int role) const {
	if(!_shop)
		return QVariant();
	if(!index.isValid())
		return QVariant();
	
	ShopItem const *si=static_cast<ShopItem*>(index.internalPointer());
	if(index.column() == 0 && role == Qt::DisplayRole)
	{
		QString name = si->name();
		return name;
	} 
	else if(index.column() == 1 && role == Qt::DisplayRole) 
	{
		if(si->type().contains("Consumable"))
		{
			Consumable const *sc=static_cast<Consumable const *>(si);
			return sc->description();
		}
	} 
	else if(index.column() == 2 && role == Qt::DisplayRole) 
	{
		if(si->type().contains("Consumable"))
		{
			Consumable const *sc=static_cast<Consumable const *>(si);
			return QString(sc->displayPrice() + "(id=" + sc->productId() + ")");
		}
	} 
	else if(index.column() == 3 && role == Qt::DisplayRole) 
	{
		if(si->type().contains("Consumable"))
		{
			Consumable const *sc=static_cast<Consumable const *>(si);
			return sc->value("file_id");
		}
	}
	/*
	else if(index.column() == 4 && role == Qt::DisplayRole) 
	{
		if(si->type().contains("Consumable"))
		{
			Consumable const *sc=static_cast<Consumable const *>(si);
			return sc->date();
		}
	}
	
  else if(index.column() == 5 && role == Qt::DisplayRole) 
	{
		if(si->type().contains("Consumable"))
		{
			Consumable const *sc=static_cast<Consumable const *>(si);
			return sc->date();
		}
	}
	 */
	else if(index.column() == 0 && role == Qt::DecorationRole) {
		if(_iconCache.contains(si)) {
			return _iconCache.value(si);
		} else {
			if(!_pendingIcons.contains(si) && !_downloadingIcons.contains(si) && si->iconUrl().isValid()) {
				_pendingIcons.insert(si);
				_downloadIconTimer.start();
			}
			return QVariant();
		}
	}
	
	return QVariant();
}

QVariant ShopModel::headerData(int section, Qt::Orientation orientation, int role) const {
	if(section == 0 && role == Qt::DisplayRole)
		return tr("Title");
	else if(section == 1 && role == Qt::DisplayRole)
		return tr("Description");
	else if(section == 2 && role == Qt::DisplayRole)
		return tr("Price");
	else if(section == 3 && role == Qt::DisplayRole)
		return tr("File id");
	/*
	else if(section == 4 && role == Qt::DisplayRole)
		return tr("Publisher");
	else if(section == 5 && role == Qt::DisplayRole)
		return tr("Release Date");
	 */
	return QVariant();
}

QModelIndex ShopModel::index(int row, int column, QModelIndex const &parent) const 
{
	ItemCategory const * const root=_shop->rootCategory();
	
	if(!parent.isValid() && row >= 0 && row<root->children().count())
	{
		return createIndex(row, column, root->children().at(row));
	}
	else if(parent.isValid()) 
	{
		ShopItem* i = static_cast<ShopItem*>(parent.internalPointer());
		ShopItem* toInsert = NULL;
		// FIXME: using our own fix-cast for OSX crappy dynamic_cast 
		if( i->type().contains("ItemCategory") )
		{
			ItemCategory* cat = static_cast<ItemCategory*>(i);
			toInsert = cat->children().at(row);
		}
		else
		{
			// FIXME: using our own fix-cast for OSX crappy dynamic_cast 
			toInsert = static_cast<Consumable*>(i);
		}
		return createIndex(row, column, toInsert);
	}
	return QModelIndex();
}

QModelIndex ShopModel::find(ShopItem const *item, QModelIndex const &parent) {
	if(!item)
		return QModelIndex();
	for(int i=0; i<rowCount(parent); i++) {
		QModelIndex idx=index(i, 0, parent);
		if(idx.internalPointer() == item)
			return idx;
		if(hasChildren(idx)) {
			idx=find(item, idx);
			if(idx.isValid())
				return idx;
		}
	}
	return QModelIndex();
}

QModelIndex ShopModel::parent(QModelIndex const &index) const {
	if(!index.isValid())
		return QModelIndex();
	return const_cast<ShopModel*>(this)->find( static_cast<ShopItem const*>(index.internalPointer())->parent(), QModelIndex() );
}

int ShopModel::rowCount(QModelIndex const &parent) const 
{
	
	ShopItem* i = static_cast<ShopItem*> (parent.internalPointer());
	
	if(parent.isValid())
	{
		ASSERT( i != NULL );
		
		// FIXME: using our own fix-cast for OSX crappy dynamic_cast 
		if( i->type().contains( "ItemCategory") )
		{
			ItemCategory* cat = static_cast<ItemCategory*> (i);
			return cat->children().count();
		}
		else
		{
			return 0;
		}		
	}
	else
	{
		ItemCategory* c = _shop->rootCategory();
		return c->children().count();
	}	
	return 0;
}


int ShopModel::columnCount(QModelIndex const &parent) const
{
	return 4;
}


void ShopModel::fetchIcons()
{
	foreach(ShopItem const *si, _pendingIcons) {
		QNetworkReply *reply = _accessManager.get(QNetworkRequest(si->iconUrl()));
		_downloadingIcons.insert(si, reply);
	}
	_pendingIcons.clear();
}


void ShopModel::readReply(QNetworkReply *reply)
{
	ShopItem const *si = _downloadingIcons.key(reply);
	reply->deleteLater();
	if(si) {
		if(reply->error() != QNetworkReply::NoError) {
			qDebug() << "Download of " << reply->request().url() << "failed with error " << reply->error();
			return;
		}

		QByteArray icon = reply->readAll();
		QPixmap pm;
		pm.loadFromData(icon);
		if(pm.width()>64) {
			pm=pm.scaledToWidth(64);
		}
		_iconCache.insert(si, pm);

/* FIXME: The optimal way to update the view is using dataChanged(). However, the new size of the icon should cause the
   size of the rows to be changed but QTreeView doesn't update the size. layoutChanged() is required in order for the
   view to display the row with the correct size.
		Category *parent = const_cast<Category*>(si->parent());
		int row = parent->children().indexOf(const_cast<ShopItem*>(si));
		if(row != -1) {
			QModelIndex idx = createIndex(row, 0, parent);
			emit dataChanged(idx, idx);
		}*/
		emit layoutAboutToBeChanged();
		emit layoutChanged();
	}
}
